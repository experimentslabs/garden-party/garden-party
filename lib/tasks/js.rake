# frozen_string_literal: true

require 'classes/front_generator'

namespace :js do
  desc 'Run all javascript generators'
  task generate: ['generate:models', 'generate:modules', 'generate:factories', 'generate:constants']

  desc 'Generate javascript models'
  task 'generate:models' => :environment do
    generator = FrontGenerator.new
    generator.generate(models, :models)
  end

  desc 'Generate javascript VueX modules'
  task 'generate:modules' => :environment do
    generator = FrontGenerator.new
    generator.generate(models, :modules)
  end

  desc 'Generate JS factories test file'
  task 'generate:factories_test' => :environment do
    generator = FrontGenerator.new
    generator.generate_factories_tests
  end

  desc 'Generate javascript Fishery factories'
  task 'generate:factories' => :environment do
    generator = FrontGenerator.new
    generator.generate(models, :factories)
  end

  desc 'Generate javascript constants and enums from Ruby code'
  task 'generate:constants' => :environment do
    generator = FrontGenerator.new
    generator.generate([], :constants)
  end

  def models
    return @models if @models

    # Remove the task name.
    ARGV.shift

    # By default, rake considers each 'argument' to be the name of an actual task.
    # It will try to invoke each one as a task.  By dynamically defining a dummy
    # task for every argument, we can prevent an exception from being thrown
    # when rake inevitably doesn't find a defined task with that name.
    ARGV.each do |arg|
      task(arg.to_sym) {} # rubocop:disable Lint/EmptyBlock, Rails/RakeEnvironment
    end

    @models = ARGV
  end
end
