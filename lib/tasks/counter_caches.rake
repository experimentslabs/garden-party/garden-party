# frozen_string_literal: true

require 'classes/counter_caches'

namespace :counter_caches do
  desc 'Synchronize counters'
  task synchronize: :environment do
    CounterCaches.synchronize!
  end
end
