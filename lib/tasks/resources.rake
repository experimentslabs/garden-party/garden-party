# frozen_string_literal: true

require 'garden_party/colors'
namespace :resources do
  desc 'Re-set colors.'
  task reset_colors: [:generate_pictures] do
    Resource.find_each { |resource| resource.update color: (GardenParty::Colors.from_string resource.name) }
  end
end
