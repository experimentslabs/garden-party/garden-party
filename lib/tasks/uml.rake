# frozen_string_literal: true

namespace :uml do
  desc 'Outputs PlantUML diagram of tables and relations'
  task models: :environment do
    raise 'Only available development and test' if Rails.env.production?

    require 'plantuml_builder'

    require 'classes/puml/relations_generator'
    require 'classes/puml/group'

    generator = Puml::RelationsGenerator.new
    generator.exclude_models %w[GeometryRecord Current]

    generator.add_group Puml::Group.new('System',
                                        color:  'DCC',
                                        tables: [
                                          :links,
                                          :users,
                                        ])
    generator.add_group Puml::Group.new('Library',
                                        color:  'CDC',
                                        tables: [
                                          :families,
                                          :genera,
                                          :resource_notes,
                                          :resources,
                                          :taggings,
                                        ],
                                        groups: [
                                          Puml::Group.new('Interactions',
                                                          color:  'DED',
                                                          tables: [
                                                            :resource_interactions,
                                                            :resource_interactions_groups,
                                                          ]),
                                        ])
    generator.add_group Puml::Group.new('Gardens',
                                        color:  'CCD',
                                        tables: [
                                          :elements,
                                          :patches,
                                          :paths,
                                          :layers,
                                        ],
                                        groups: [
                                          Puml::Group.new('Social',
                                                          color:  'DDE',
                                                          tables: [
                                                            :team_mates,
                                                            :notifications,
                                                            :comments,
                                                          ]),
                                          Puml::Group.new('Organization/notes',
                                                          color:  'DDE',
                                                          tables: [
                                                            :harvests,
                                                            :tasks,
                                                            :activities,
                                                            :observations,
                                                          ]),
                                        ])

    uml = generator.generate

    Rails.root.join('docs', 'database.plantuml').write uml
    Rails.root.join('docs', 'database.svg').write PlantumlBuilder::Formats::SVG.new(uml).load
  rescue SocketError => e
    Rails.logger.error("Unable to generate SVG version online: #{e.message}")
    warn <<~TXT
      Unable to generate SVG version online.
      In order to keep the files in sync, you should discard any change made to the diagrams.
        #{e.message}
    TXT

    exit 1
  end
end
