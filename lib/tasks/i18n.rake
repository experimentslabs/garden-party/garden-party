# frozen_string_literal: true

require 'classes/i18n_model_attributes'
require 'classes/i18n_js_generator'

namespace :i18n do
  desc 'Generate ActiveRecord models and attributes list to help i18n-task'
  task 'add-models-attributes': :environment do
    I18nModelAttributes.generate
  end

  desc 'Add missing translations'
  task 'add-missing': :environment do
    system 'i18n-tasks add-missing -v "TRANSLATE_ME %{human_key}"' # rubocop:disable Style/FormatStringToken
  end

  desc 'Generate javascript locale files'
  task 'js:export': :environment do
    # JSON files will be generated twice in development but that's ok: once by
    # the Rails initializer, the other by the generator
    I18nJsGenerator.generate_files
  end
end
