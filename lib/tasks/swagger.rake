# frozen_string_literal: true

SWAGGER_FILE = File.join('docs', 'app', 'public', 'openapi.json')

raise "#{SWAGGER_FILE} not found" unless File.exist? SWAGGER_FILE

namespace :swagger do
  def remove_responses(hash)
    hash.each_value do |path|
      path.each_value do |method|
        method.except! 'responses'
      end
    end
    hash
  end

  def remove_example_values(hash)
    hash.each_value do |path|
      path.each_value do |method|
        next unless method.key? 'requestBody'

        method['requestBody']['content'].each_value do |format|
          format['examples'].each_value do |code|
            code.except! 'value'
          end
        end
      end
    end
    hash
  end

  def clean_paths(hash)
    hash = remove_responses hash
    remove_example_values hash
  end

  def should_commit?(old_version, new_version)
    new_version['paths'] = clean_paths new_version['paths']
    old_version['paths'] = clean_paths old_version['paths']

    return false if old_version == new_version

    diff = Hashdiff.diff(old_version, new_version)
    display_diff diff

    true
  end

  def display_diff(diff)
    list = diff.group_by { |line| line[1] }
    list.each do |key, changes|
      warn key
      changes.each do |change|
        color = change[0] == '-' ? :red : :green
        text  = "  #{change[0]} #{change[2]}"
        warn ActiveSupport::LogSubscriber.new.send(:color, text, color)
      end
    end
  end

  def normalize_hash
    hash = JSON.parse File.read(SWAGGER_FILE)
    hash.deep_sort!
  end

  def uncommitted_changes?(file)
    modified = `git diff --exit-code #{file}`.present?
    warn ActiveSupport::LogSubscriber.new.send(:color, "#{file} has no changes", :green) unless modified

    modified
  end

  desc 'Checks changes out or normalize swagger file if it changed.'
  task changed: :environment do
    next unless uncommitted_changes?(SWAGGER_FILE)

    raw_old_version = `git show HEAD:#{SWAGGER_FILE}`
    old_version     = JSON.parse raw_old_version
    new_version     = normalize_hash

    if should_commit? old_version, new_version
      Rake::Task['swagger:normalize'].invoke
      warn ActiveSupport::LogSubscriber.new.send(:color, "#{SWAGGER_FILE} changed. You should commit the file. We normalized it for you.", :yellow)
      exit 1
    else
      `git checkout #{SWAGGER_FILE}`
      warn ActiveSupport::LogSubscriber.new.send(:color, "No change of interest. #{SWAGGER_FILE} has been checked out to revert changes.", :green)
      exit 0
    end
  end

  # FIXME: Propose patch to rspec_rails_api to have a pretty JSON output
  desc 'Normalizes swagger.json'
  task normalize: :environment do
    Rails.root.join(SWAGGER_FILE).write(JSON.pretty_generate(normalize_hash))
  end
end
