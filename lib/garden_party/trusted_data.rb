# frozen_string_literal: true

require 'net/http'

module GardenParty
  class TrustedDataError < StandardError; end

  module TrustedData
    class << self
      def fetch(url = nil)
        url ||= Rails.configuration.garden_party[:trusted_datasource_url]
        raise 'Missing configuration key: garden_party.trusted_datasource_repository_url' unless url

        base = fetch_file(url)
        build_pack base, File.dirname(url)
      end

      private

      def fetch_file(url = nil)
        uri = URI(url)
        Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
          request = Net::HTTP::Get.new uri

          handle_response http.request(request)
        end
      end

      def handle_response(response)
        raise TrustedDataError, I18n.t('garden_party.trusted_data.errors.target_error_failure', message: response.message) unless (200...400).cover? response.code.to_i

        YAML.safe_load response.body, permitted_classes: [Symbol]
      rescue Psych::SyntaxError
        raise TrustedDataError, I18n.t('garden_party.trusted_data.errors.bad_YAML')
      end

      def build_pack(base_pack, base_url)
        base_pack.each_pair do |name, collection|
          if collection.is_a? String
            part = fetch_file("#{base_url}/#{collection}.yml")
            base_pack[name] = part[:data]
          end
        end

        complete_entries base_pack
      end

      # Adds trusted data optional fields to a response
      def complete_entries(trusted_data)
        {
          families:                     :complete_family,
          genera:                       :complete_genus,
          resources:                    :complete_resource,
          resource_interactions_groups: :complete_resource_interactions_group,
          resource_interactions:        :complete_resource_interaction,
        }.each_pair do |key, method|
          trusted_data[key] ||= []

          trusted_data[key].map! { |entity| send method, entity }
        end

        trusted_data
      end

      # Completes a trusted family record with optional fields
      def complete_family(family)
        family.delete :external_ids
        family[:source] ||= nil

        family
      end

      # Completes a trusted genus record with optional fields
      def complete_genus(genus)
        genus.delete :external_ids
        genus[:source] ||= nil

        genus
      end

      # Completes a trusted resource record with optional fields
      def complete_resource(resource)
        resource.delete :external_ids
        [:latin_name, :parent, :description, :diameter].each { |f| resource[f] ||= nil }
        [:common_names, :sources, :sheltered_sowing_months, :soil_sowing_months, :harvesting_months, :tags]
          .each { |f| resource[f] ||= [] }

        resource[:common_names].sort!

        resource
      end

      # Completes a trusted resource interactions group record with optional fields
      def complete_resource_interactions_group(group)
        group[:description] ||= nil
        group
      end

      # Completes a trusted resource interactions record with optional fields
      def complete_resource_interaction(interaction)
        interaction[:notes] ||= nil
        interaction[:sources] ||= []

        interaction
      end
    end
  end
end
