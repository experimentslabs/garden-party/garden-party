# frozen_string_literal: true

module GardenParty
  # Helpers around GardenParty configuration
  class Config
    class << self
      def server_url
        @server_url ||= Rails.configuration.garden_party.default_url_options.values.join(':')
      end
    end
  end
end
