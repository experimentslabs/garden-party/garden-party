# frozen_string_literal: true

module GardenParty
  class UserPreferences
    # Every key in this should have a "setter method" like "map=", that filters value
    DEFAULTS = {
      'map'   => nil,
      'theme' => nil,
    }.freeze

    ALLOWED_THEMES = [nil, 'light', 'dark'].freeze

    EMAIL_NOTIFICATIONS_DEFAULTS = {
      'new_comment_notification'      => false,
      'system_notification'           => true,
      'team_mate_invite_notification' => true,
      'team_mate_leave_notification'  => true,
      'team_mate_refuse_notification' => true,
      'team_mate_remove_notification' => true,
    }.freeze

    IMMUTABLE_EMAIL_NOTIFICATION_PREFERENCES = ['system_notification'].freeze

    def initialize(preferences)
      @preferences = DEFAULTS.merge 'email_notifications' => {}.merge(EMAIL_NOTIFICATIONS_DEFAULTS)

      DEFAULTS.each_key do |key|
        send(:"#{key}=", preferences[key]) if preferences.key? key
      end

      self.email_notifications = preferences['email_notifications'] if preferences.key? 'email_notifications'
    end

    def theme=(value)
      value = nil if value == ''
      raise "Unsupported theme value: \"#{value}\"" unless ALLOWED_THEMES.include? value

      @preferences['theme'] = value
    end

    def map=(value)
      value = nil if value == ''
      raise "Unsupported map value: \"#{value}\"" unless value.nil? || value.is_a?(Integer)

      @preferences['map'] = value
    end

    def email_notifications=(params)
      params.each_key do |key|
        set_notification_preference key, params[key]
      end
    end

    def self.send_email?(notification_type, recipient)
      underscored = notification_type.to_s.underscore

      raise "Unsupported notification type #{notification_type}" unless EMAIL_NOTIFICATIONS_DEFAULTS.key? underscored

      if recipient.preferences['email_notifications']&.key?(underscored)
        recipient.preferences['email_notifications'][underscored]
      else
        EMAIL_NOTIFICATIONS_DEFAULTS[underscored]
      end
    end

    def to_h
      @preferences
    end

    private

    def cast_boolean(value)
      @boolean_caster ||= ActiveRecord::Type::Boolean.new
      @boolean_caster.cast value
    end

    def set_notification_preference(key, value)
      key = key.to_s
      raise "Unknown key #{key}" unless EMAIL_NOTIFICATIONS_DEFAULTS.key? key

      @preferences['email_notifications'][key] = if IMMUTABLE_EMAIL_NOTIFICATION_PREFERENCES.include? key
                                                   EMAIL_NOTIFICATIONS_DEFAULTS[key]
                                                 else
                                                   bool = cast_boolean value
                                                   bool.nil? ? EMAIL_NOTIFICATIONS_DEFAULTS[key] : bool
                                                 end
    end
  end
end
