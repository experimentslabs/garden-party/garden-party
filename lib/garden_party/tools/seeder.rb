# frozen_string_literal: true

module GardenParty
  module Tools
    class Seeder
      class << self
        def apply(yaml_file)
          raise "Don't seed in production!" if Rails.env.production?

          @seeds = YAML.load_file yaml_file

          # Every section is optional so we can develop with incomplete exports
          seed_families
          seed_genera
          seed_resource_interactions_groups
          seed_resource_interactions
          seed_resources
          seed_parent_resources
        end

        private

        def seed_families
          return unless @seeds[:families].is_a? Array

          @seeds[:families].each do |family|
            Family.create! name:    family[:name],
                           kingdom: family[:kingdom],
                           source:  family[:source],
                           sync_id: family[:id].presence || nil
          end
        end

        def seed_genera
          return unless @seeds[:genera].is_a? Array

          @seeds[:genera].each do |genus|
            Genus.create! name:    genus[:name],
                          source:  genus[:source],
                          family:  Family.find_by(name: genus[:family]),
                          sync_id: genus[:id].presence || nil
          end
        end

        def resource_months(resource)
          attributes = {}
          Resource::BITFIELD_MONTHS_ATTRIBUTES.each_pair do |field, prefix|
            next unless resource[field]

            resource[field].each do |month|
              attributes["#{prefix}_#{GardenParty::BitfieldModelHelper::MONTHS[month - 1]}"] = true
            end
          end

          attributes
        end

        def seed_resources # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
          @seeds[:resources].each do |resource|
            attributes = {
              name:                        resource[:name],
              latin_name:                  resource[:latin_name],
              description:                 resource[:description],
              diameter:                    resource[:diameter],
              sources:                     resource[:sources],
              common_names:                resource[:common_names],
              genus:                       Genus.find_by(name: resource[:genus]),
              sync_id:                     resource[:id].presence || nil,
              tag_list:                    resource[:tags].presence || [],
              resource_interactions_group: resource[:interactions_group] ? ResourceInteractionsGroup.find_by(name: resource[:interactions_group]) : nil,
            }
            attributes.merge! resource_months(resource)

            Resource.create! attributes
          end
        end

        def seed_parent_resources
          return unless @seeds[:resources].is_a? Array

          @seeds[:resources].each do |resource|
            next if resource[:parent].blank?

            Resource.find_by(name: resource[:name]).update parent: Resource.find_by(name: resource[:parent])
          end
        end

        def seed_resource_interactions_groups
          return unless @seeds[:resource_interactions_groups].is_a? Array

          @seeds[:resource_interactions_groups].each do |group|
            ResourceInteractionsGroup.create! name:        group[:name],
                                              description: group[:description],
                                              sync_id:     group[:id].presence
          end
        end

        def seed_resource_interactions
          return unless @seeds[:interactions].is_a? Array

          @seeds[:interactions].each do |resource_interaction|
            ResourceInteraction.create! subject: ResourceInteractionsGroup.find_by(name: resource_interaction[:subject]),
                                        target:  ResourceInteractionsGroup.find_by(name: resource_interaction[:target]),
                                        nature:  resource_interaction[:nature],
                                        notes:   resource_interaction[:notes],
                                        sources: resource_interaction[:sources],
                                        sync_id: resource_interaction[:id].presence
          end
        end
      end
    end
  end
end
