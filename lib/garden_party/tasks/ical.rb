# frozen_string_literal: true

module GardenParty
  module Tasks
    class Ical
      class << self
        # @param [Array<::Task>,ActiveRecord::Relation<::Task>] tasks
        # @return [Icalendar::Calendar]
        def calendar(tasks)
          cal = Icalendar::Calendar.new
          cal.prodid = '-//GardenParty//Tasks calendar'

          tasks.each do |task|
            add_todo(cal, task)
          end
          cal.publish

          cal
        end

        private

        def add_todo(calendar, task) # rubocop:disable Metrics/AbcSize
          calendar.todo do |e|
            e.uid           = "gp-task-#{task.id}@#{GardenParty::Config.server_url}"
            e.dtstamp       = Icalendar::Values::DateTime.new task.created_at
            e.last_modified = Icalendar::Values::DateTime.new task.updated_at
            e.created       = Icalendar::Values::DateTime.new task.created_at
            e.due           = Icalendar::Values::Date.new task.planned_for

            e.summary       = "[#{task.subject.name}] #{task.name}"
            e.description   = task.notes if task.notes.present?

            e.completed     = Icalendar::Values::DateTime.new task.done_at if task.done_at.present?

            e.contact       = task.assignee.username if task.assignee_id
            e.categories    = [task.subject_type]
          end
        end
      end
    end
  end
end
