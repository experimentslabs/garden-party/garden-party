# frozen_string_literal: true

module GardenParty
  module TrustedData
    module Converter # rubocop:disable Metrics/ModuleLength
      # Converts a Family to use as a trusted Family record hash
      #
      # @param family [Family] Family to convert
      #
      # @return [Hash] Attributes for a trusted data Family record
      def self.family_to_td(family)
        # Follow the same order as a Family record in "Garden Party trusted data"
        {
          id:      family.sync_id&.to_i,
          name:    family.name,
          kingdom: family.kingdom,
          source:  family.source,
        }
      end

      # Converts a trusted family record (or a subset of it) to Family attributes
      #
      # @param family [Hash] Trusted family record
      # @param only [Array<Symbol>] List of attributes of trusted record to process when a full hash is useless
      #
      # @return [Hash] Attributes for a Family
      def self.td_to_family(family, only: [])
        attributes = copy_values(family, [:name, :kingdom, :source], only: only)

        attributes[:sync_id] = family[:id] if only.empty? || only.include?(:id)

        attributes
      end

      # Converts a Genus to use as a trusted Genus record hash
      #
      # @param genus [Genus] Genus to convert
      #
      # @return [Hash] Attributes for a trusted data Genus record
      def self.genus_to_td(genus)
        # Follow the same order as a Genus record in "Garden Party trusted data"
        {
          id:     genus.sync_id&.to_i,
          name:   genus.name,
          family: genus.family.name,
          source: genus.source,
        }
      end

      # Converts a trusted genus record (or a subset of it) to Genus attributes
      #
      # @param genus [Hash] Trusted genus record
      # @param only [Array<Symbol>] List of attributes of trusted record to process when a full hash is useless
      #
      # @return [Hash] Attributes for a Genus
      def self.td_to_genus(genus, only: [])
        attributes = copy_values(genus, [:name, :source], only: only)

        attributes[:sync_id] = genus[:id] if only.empty? || only.include?(:id)
        attributes[:family]  = Family.find_by(name: genus[:family]) if only.empty? || only.include?(:family)

        attributes
      end

      # Converts a Resource to use as a trusted Resource record hash
      #
      # @param resource [Resource] Resource to convert
      #
      # @return [Hash] Attributes for a trusted data Resource record
      def self.resource_to_td(resource) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
        # Follow the same order as a Resource record in "Garden Party trusted data"
        {
          id:                      resource.sync_id&.to_i,
          name:                    resource.name,
          latin_name:              resource.latin_name,
          genus:                   resource.genus.name,
          parent:                  resource.parent&.name,
          common_names:            resource.common_names,
          description:             cleanup(resource.description),
          diameter:                resource.diameter,
          sheltered_sowing_months: GardenParty::BitfieldModelHelper.bitfield_months_array(:sow_sheltered_in, resource.bitfield_values(:sheltered_sowing_months)),
          soil_sowing_months:      GardenParty::BitfieldModelHelper.bitfield_months_array(:sow_in_soil_in, resource.bitfield_values(:soil_sowing_months)),
          harvesting_months:       GardenParty::BitfieldModelHelper.bitfield_months_array(:harvest_in, resource.bitfield_values(:harvesting_months)),
          tags:                    resource.tag_list.to_a,
          sources:                 resource.sources.compact_blank,
          interactions_group:      resource.resource_interactions_group&.name,
        }
      end

      # Converts a trusted resource record (or a subset of it) to Resource attributes
      #
      # @param resource [Hash] Trusted resource record
      # @param only [Array<Symbol>] Fields of trusted record to process
      #
      # @return [Hash] Attributes for a Resource
      def self.td_to_resource(resource, only: []) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
        attributes = copy_values resource, [:name, :latin_name, :common_names, :description, :diameter, :sources], only: only

        attributes[:sync_id]  = resource[:id] if only.empty? || only.include?(:id)
        attributes[:tag_list] = resource[:tags] if only.empty? || only.include?(:tags)
        attributes[:genus]    = Genus.find_by(name: resource[:genus]) if only.empty? || only.include?(:genus)
        attributes[:parent]   = Resource.find_by(name: resource[:parent]) if only.empty? || only.include?(:parent)
        attributes[:resource_interactions_group] = ResourceInteractionsGroup.find_by(name: resource[:interactions_group]) if only.empty? || only.include?(:interactions_group)

        attributes.merge! GardenParty::BitfieldModelHelper.months_array_to_bitfields(:sow_sheltered_in, resource[:sheltered_sowing_months]) if only.empty? || only.include?(:sheltered_sowing_months)
        attributes.merge! GardenParty::BitfieldModelHelper.months_array_to_bitfields(:sow_in_soil_in, resource[:soil_sowing_months])        if only.empty? || only.include?(:soil_sowing_months)
        attributes.merge! GardenParty::BitfieldModelHelper.months_array_to_bitfields(:harvest_in, resource[:harvesting_months])             if only.empty? || only.include?(:harvesting_months)

        attributes
      end

      # Converts a ResourceInteractionsGroup to use as a trusted ResourceInteractionsGroup record hash
      #
      # @param group [ResourceInteractionsGroup] Resource interactions group to convert
      #
      # @return [Hash] Attributes for a trusted data ResourceInteractionsGroup record
      def self.resource_interactions_group_to_td(group)
        # Follow the same order as a ResourceInteractionsGroup record in "Garden Party trusted data"
        {
          id:          group.sync_id&.to_i,
          name:        group.name,
          description: group.description,
        }
      end

      # Converts a trusted resource interactions group record (or a subset of it) to ResourceInteractionsGroup attributes
      #
      # @param group [Hash] Trusted resource interactions group record
      # @param only [Array<Symbol>] Fields of trusted record to process
      #
      # @return [Hash] Attributes for a ResourceInteractionsGroup
      def self.td_to_resource_interactions_group(group, only: [])
        attributes = copy_values group, [:name, :description], only: only

        attributes[:sync_id] = group[:id] if only.empty? || only.include?(:id)

        attributes
      end

      # Converts a ResourceInteraction to use as a trusted ResourceInteraction record hash
      #
      # @param interaction [ResourceInteraction] Resource interactions to convert
      #
      # @return [Hash] Attributes for a trusted data ResourceInteraction record
      def self.resource_interaction_to_td(interaction)
        # Follow the same order as a ResourceInteraction record in "Garden Party trusted data"
        {
          id:      interaction.sync_id&.to_i,
          nature:  interaction.nature,
          subject: interaction.subject.name,
          target:  interaction.target.name,
          notes:   interaction.notes,
          sources: interaction.sources&.split("\n") || [],
        }
      end

      # Converts a trusted resource interaction record (or a subset of it) to ResourceInteraction attributes
      #
      # @param interaction [Hash] Trusted resource interaction record
      # @param only [Array<Symbol>] Fields of trusted record to process
      #
      # @return [Hash] Attributes for a ResourceInteractionsGroup
      def self.td_to_resource_interaction(interaction, only: []) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
        attributes = copy_values interaction, [:nature, :notes], only: only

        attributes[:sync_id] = interaction[:id] if only.empty? || only.include?(:id)
        attributes[:subject] = ResourceInteractionsGroup.find_by(name: interaction[:subject]) if only.empty? || only.include?(:subject)
        attributes[:target]  = ResourceInteractionsGroup.find_by(name: interaction[:target]) if only.empty? || only.include?(:target)
        attributes[:sources] = interaction[:sources].join("\n") if only.empty? || only.include?(:sources)

        attributes
      end

      def self.cleanup(text)
        return if text.blank?

        text.gsub('\r\n', "\n")
            .split("\n")
            .map { |l| l.sub(/\s+$/, '') }
            .join("\n")
      end

      # Creates a hash with keys/values from another hash
      #
      # @param fields [Array<Symbol>] List of fields that _should_ be copied by default
      # @param only [Array<Symbol>, nil] List of fields to return
      #
      #
      # @return [Hash] Hash with all `fields` or only `only` fields
      def self.copy_values(hash, fields, only: [])
        # Only process fields in both lists
        fields = only - (only - fields) unless only.empty?

        h = {}
        fields.each { |f| h[f] = hash[f] }

        h
      end
    end
  end
end
