# frozen_string_literal: true

module GardenParty
  # Helper methods to manage tags
  class Tags
    class << self
      # Merges tags from their IDs. The first one will be the remaining one
      #
      # @param ids [Array<Integer>] Identifiers to merge.
      def merge!(ids = [])
        return if ids.empty?

        first_id = ids.shift

        # Update taggings
        ActsAsTaggableOn::Tag.find(ids).each do |tag|
          # Here we use the Tagging model directly instead of using the relation
          # (tag.taggings) to avoid caching the taggings list for the tag.
          ActsAsTaggableOn::Tagging.where(tag_id: tag.id).find_each do |tagging|
            tagged_by_first_tag = ActsAsTaggableOn::Tagging.where taggable_type: tagging.taggable_type, taggable_id: tagging.taggable_id, tag_id: first_id

            # Avoid duplicate tags on the same taggable
            next if tagged_by_first_tag.count.positive?

            tagging.update tag_id: first_id
          end

          tag.destroy!
        end

        # Reset taggings counter on tag
        ActsAsTaggableOn::Tag.reset_counters(first_id, :taggings)

        first_id
      end
    end
  end
end
