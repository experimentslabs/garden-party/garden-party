# frozen_string_literal: true

module GardenParty
  module BitfieldModelHelper
    MONTHS = [
      :january,
      :february,
      :march,
      :april,
      :may,
      :june,
      :july,
      :august,
      :september,
      :october,
      :november,
      :december,
    ].freeze

    BITFIELD_MONTHS = {} # rubocop:disable Style/MutableConstant
    MONTHS.each_with_index { |value, index| BITFIELD_MONTHS[2**index] = value }
    BITFIELD_MONTHS.freeze

    # Creates a hash to use as bitfield definition in models
    #
    # @param prefix [Symbol, String] - Prefix for field name
    #
    # @return [Hash] A hash with bits values as keys and prefixed month names as values
    #
    # Example:
    #   bitfield_months(:do_something_in)
    #   #=> {1=>:do_something_in_january, 2=>:do_something_in_february, 4=>:do_something_in_march, ...}
    def self.bitfield_months(prefix)
      hash = {}
      BITFIELD_MONTHS.each_pair { |key, value| hash[key] = :"#{prefix}_#{value}" }

      hash.freeze
    end

    # Creates an array of the months for which the flag is true
    #
    # @param prefix [Symbol, String] - Prefix for field name
    # @param hash [Hash] - Values
    #
    # @return [Array<Integer>] An array of integers representing the months, starting at 1 for january
    #
    # Example:
    #   resource = Resource.first
    #   hash     = resource.bitfield_values :soil_sowing_months
    #   #=> {sow_in_soil_in_january: false, sow_in_soil_in_february: false, ...}
    #   bitfield_months_array(:sow_in_soil_in, hash)
    #   #=> [4, 5, 6, 7]
    def self.bitfield_months_array(prefix, hash)
      months = prefix_months prefix
      array = []
      hash.each_pair { |key, value| array.push(months.find_index(key) + 1) if value }

      array.freeze
    end

    # Creates an array of booleans from a list of integers
    #
    # @param list [Array<Integer>] - List of months like [1,3,12]
    #
    # @return [Hash] Hash of <prefixed month>:<boolean>
    #
    # Example
    #   list = [4, 5, 6, 7]
    #   months_array_to_bitfields(:sow_in_soil_in, list)
    #   #=> {sow_in_soil_in_january: false, sow_in_soil_in_february: false, ...}
    def self.months_array_to_bitfields(prefix, list)
      out = prefix_months(prefix).zip.map { |m| m.push false }
      return out.to_h unless list

      list.each { |value| out[value - 1][1] = true }

      out.to_h
    end

    # Returns the months list, with each month prefixed
    #
    # @param prefix [String, Symbol] - Prefix to use
    #
    # @return [Array<Symbol>] The months list, prefixed
    #
    # Example:
    #   prefix_months(:a_prefix)
    #   #=> [:a_prefix_january, :a_prefix_february, ...]
    def self.prefix_months(prefix)
      MONTHS.map { |month| :"#{prefix}_#{month}" }
    end
  end
end
