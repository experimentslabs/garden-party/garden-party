# frozen_string_literal: true

module GardenParty
  module MapFinder
    # Returns the map for a given entity's subject
    #
    # @param entity [::Activity, ::Notification, ::Observation, ::Task] Any entity with a polymorphic "subject" relation
    #
    # @return [::Map]
    def self.by_subject(entity) # rubocop:disable Metrics/MethodLength
      raise "Unsupported class #{entity.class}" unless entity.respond_to? :subject_type

      case entity.subject_type
      when 'Element', 'Layer', 'Patch', 'Path', 'TeamMate'
        entity.subject.map
      when 'Map'
        entity.subject
      when 'Observation', 'Task'
        by_subject entity.subject
      when 'Harvest'
        entity.subject.element.map
      else
        raise "Unsupported subject #{entity.subject_type}"
      end
    end

    # Returns the map for a given entity
    #
    # @param entity [::ApplicationRecord] Any record; method will fail if unsupported
    #
    # @return [::Map]
    def self.for(entity) # rubocop:disable Metrics/MethodLength
      case entity
      when Map
        entity
      when Element, Layer, Patch, Path
        entity.map
      when Harvest
        entity.element&.map
      when Activity, Task, Observation
        by_subject entity
      when Comment
        by_subject entity.subject
      else
        raise "Cannot find map for entity #{entity.class.name}"
      end
    end
  end
end
