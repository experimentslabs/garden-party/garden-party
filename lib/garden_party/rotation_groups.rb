# frozen_string_literal: true

module GardenParty
  class RotationGroups
    # Sources:
    #  - "Les bonnes associations au potager", p.11
    #  - "Le nouveau jardin - Guide complet", p.145; p.150-153
    ORDER = [
      :leaf,
      :manure,
      :legume,
      :root,
    ].freeze
    # Quick reference to avoid searching in order every time
    POSITIONS = ORDER.index_with { |v| ORDER.find_index(v) }

    def self.next(group)
      ORDER[POSITIONS[group.to_sym] + 1] || ORDER.first
    end
  end
end
