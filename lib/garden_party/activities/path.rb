# frozen_string_literal: true

module GardenParty
  module Activities
    module Path
      ATTRIBUTE_CHANGES = {
        'name'     => :rename,
        'geometry' => :move_or_resize,
        'layer'    => :layer_change,
      }.freeze

      # @param path [::Path]
      def self.create_create_activity(path)
        Activity.create! action: :create, subject: path
      end

      # @param path [::Path]
      def self.create_update_activity(path)
        path.saved_changes.each_key do |attribute|
          action = ATTRIBUTE_CHANGES[attribute]

          Activity.create! action: action, subject: path, data: { change: path.saved_changes[attribute] } if action
        end
      end

      # @param path [::Path]
      def self.create_destroy_activity(path)
        Activity.create! action: :destroy, subject: path
      end
    end
  end
end
