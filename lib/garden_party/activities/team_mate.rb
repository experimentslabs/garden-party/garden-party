# frozen_string_literal: true

module GardenParty
  module Activities
    module TeamMate
      # @param team_mate [::TeamMate]
      def self.create_create_activity(team_mate)
        create_accept_activity team_mate if team_mate.accepted_at.present?
      end

      # @param team_mate [::TeamMate]
      def self.create_update_activity(team_mate)
        create_accept_activity team_mate if team_mate.accepted_at_previously_was.blank? && team_mate.accepted_at.present?
      end

      # @param team_mate [::TeamMate]
      def self.create_destroy_activity(team_mate)
        return if team_mate.accepted_at.blank?

        Activity.create! action: :leave, subject: team_mate
      end

      private_class_method def self.create_accept_activity(team_mate)
        Activity.create! action: :join, subject: team_mate
      end
    end
  end
end
