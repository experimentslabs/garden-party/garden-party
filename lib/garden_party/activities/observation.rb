# frozen_string_literal: true

module GardenParty
  module Activities
    module Observation
      ATTRIBUTE_CHANGES = {
        'title'   => :rename,
        'content' => :complete,
        'made_at' => :date_fix,
      }.freeze

      # @param observation [::Observation]
      def self.create_create_activity(observation)
        Activity.create! action: :create, subject: observation
      end

      # @param observation [::Observation]
      def self.create_update_activity(observation)
        observation.saved_changes.each_key do |attribute|
          action = ATTRIBUTE_CHANGES[attribute]

          Activity.create! action: action, subject: observation, data: { change: observation.saved_changes[attribute] } if action
        end
      end

      # @param observation [::Observation]
      def self.create_destroy_activity(observation)
        Activity.create! action: :destroy, subject: observation
      end
    end
  end
end
