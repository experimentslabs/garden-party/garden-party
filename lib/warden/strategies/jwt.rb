# frozen_string_literal: true

module Warden
  module Strategies
    class Jwt < Base
      def valid?
        request.authorization.present? && request.path.match?(%r{^/api/})
      end

      def authenticate!
        match = request.authorization.match(/bearer (?<token>.*)/i)
        u = JwtToken.user_from match[:token] if match[:token]
        u.nil? ? fail!(I18n.t('warden.strategies.jwt.invalid_token')) : success!(u)
      end
    end
  end
end

Warden::Strategies.add(:jwt, Warden::Strategies::Jwt)
