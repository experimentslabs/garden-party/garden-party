# frozen_string_literal: true

class FrontGenerator # rubocop:disable Metrics/ClassLength
  FACTORIES_PATH = Rails.root.join('app', 'javascript', 'specs', 'factories')
  EXCLUDED_MODELS = [
    'ApplicationRecord',
    'Current',
    'Link', # No API for this
    'GeometryRecord',
  ].freeze

  FIELDS_META = YAML.load_file(File.join(__dir__, 'front_fields.yml'))

  TYPES = {
    types:   {
      'Boolean'           => 'boolean',
      'EnumType'          => 'string',
      'Integer'           => 'number',
      'Float'             => 'number',
      'Jsonb'             => 'object',
      'Point'             => '{x:number, y:number}',
      'String'            => 'string',
      'Text'              => 'string',
      'TimeZoneConverter' => 'Date|string',
      'TagListType'       => 'string[]',
    },
    setters: {
      'Point'             => ->(field) { "[#{field}.x, #{field}.y]" },
      'TimeZoneConverter' => ->(field) { "#{field} ? new Date(#{field}) : null" },
    },
    faker:   {
      'Boolean'           => 'faker.datatype.boolean()',
      'EnumType'          => 'string()',
      'Integer'           => 'faker.datatype.number()',
      'Float'             => 'faker.datatype.float()',
      'Jsonb'             => 'faker.datatype.json()',
      'Point'             => '{x:faker.datatype.number(), y:faker.datatype.number()}',
      'String'            => 'faker.lorem.sentence()',
      'Text'              => 'faker.lorem.paragraphs()',
      'TimeZoneConverter' => 'faker.datatype.datetime()',
      'TagListType'       => "faker.lorem.words().split(' ')",
    },
  }.freeze

  def generate(models = [], *types)
    load_models(models)
    load_attributes
    render_modules if types.include? :modules
    render_models if types.include? :models
    render_factories if types.include? :factories
    render_constants if types.include? :constants
  end

  def generate_factories_tests
    @factories = {}
    Dir.glob(File.join(FACTORIES_PATH, '**', '*.js')).each do |node|
      next unless File.file?(node)
      next if /\.spec\.js$/i.match?(node)

      content = File.read node
      exports = content.scan(/^export const (\w+)/)
      @factories[node] = exports
    end

    render_factories_test
  end

  private

  def load_models(models = [])
    return @models if @models

    @models = {}
    Rails.root.glob('app/models/*.rb').each do |filename|
      class_name = File.basename(filename, '.rb').classify
      next if EXCLUDED_MODELS.include?(class_name) || (models.size.positive? && models.exclude?(class_name))

      @models[class_name] = {}
    end
  end

  def load_attributes
    raise 'Models not loaded' unless @models

    @models.each_key do |class_name|
      complete_attributes class_name
      add_extra_attributes class_name
    end
  end

  def render_factories
    template = File.read(File.join(__dir__, 'front_generator', 'templates', 'factory.js.erb'))
    renderer = ERB.new(template)
    # "attributes" is bound in templates with "binding"
    @models.each_pair do |model_name, attributes|
      Rails.root.join('app', 'javascript', 'specs', 'factories', "#{model_name.pluralize.underscore}.js").write(renderer.result(binding))
    end
  end

  def render_modules
    template = File.read(File.join(__dir__, 'front_generator', 'templates', 'module.js.erb'))
    renderer = ERB.new(template)
    # "attributes" is bound in templates with "binding"
    @models.each_pair do |model_name, attributes|
      Rails.root.join('app', 'javascript', 'vue', 'stores', 'modules', "#{model_name}Module.js").write(renderer.result(binding))
    end
  end

  def render_models
    template = File.read(File.join(__dir__, 'front_generator', 'templates', 'model.js.erb'))
    renderer = ERB.new(template)
    # "attributes" is bound in templates with "binding"
    @models.each_pair do |model_name, attributes|
      Rails.root.join('app', 'javascript', 'vue', 'classes', 'models', "#{model_name}.js").write(renderer.result(binding))
    end
  end

  def render_factories_test
    template = File.read(File.join(__dir__, 'front_generator', 'templates', 'factories_test.js.erb'))
    renderer = ERB.new(template)
    Rails.root.join('app', 'javascript', 'specs', 'factories.spec.js').write(renderer.result(binding))
  end

  def render_constants
    template = File.read(File.join(__dir__, 'front_generator', 'templates', 'constants.js.erb'))
    renderer = ERB.new(template, trim_mode: '-')
    Rails.root.join('app', 'javascript', 'helpers', 'constants.js').write(renderer.result)
  end

  def attribute_type(attribute, details)
    type = TYPES.dig :types, details[:type]
    type ||= details[:type]
    type = "null|#{type}" if attribute == 'id'

    type
  end

  def faker_method(attribute, details)
    type = TYPES.dig :faker, details[:type]
    type ||= details[:type]
    type = 'sequence' if attribute == 'id'

    type
  end

  def attribute_setter(attribute, details)
    setter = TYPES.dig :setters, details[:type]
    return setter.call(attribute) if setter

    attribute
  end

  def complete_attributes(class_name)
    fields_meta = FIELDS_META[class_name] || {}
    class_name.constantize.attribute_types.each do |attribute, type|
      next if fields_meta['excluded']&.include? attribute

      @models[class_name][attribute] = { type: type.class.name.demodulize, limit: type.limit }
    end
  end

  def add_extra_attributes(class_name)
    fields_meta = FIELDS_META[class_name] || {}
    (fields_meta['additional'] || {}).each_pair do |attribute, type|
      @models[class_name][attribute.to_s] = { type: type }
    end
  end
end
