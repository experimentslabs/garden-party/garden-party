# frozen_string_literal: true

class CounterCaches
  def self.synchronize!
    # Observation counters
    [Element, Map, Patch, Path].each do |model|
      model.find_each do |instance|
        instance.update! observations_count: instance.observations.count
      end
    end

    # Comment counters
    [Observation, Task].each do |model|
      model.find_each do |instance|
        instance.update! comments_count: instance.comments.count
      end
    end
  end
end
