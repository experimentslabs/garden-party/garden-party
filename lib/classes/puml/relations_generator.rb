# frozen_string_literal: true

require_relative 'group'

module Puml
  class RelationsGenerator # rubocop:disable Metrics/ClassLength
    ASSOCIATION_ATTRIBUTES = [
      /_id$/,
      /_type$/,
    ].freeze

    RAILS_ATTRIBUTES = %w[
      created_at
      updated_at
      id
    ].freeze

    # Correspondence table to darken hex colors (in graph arrows)
    DARKEN_TABLE = { 'a' => '4', 'b' => '5', 'c' => '6', 'd' => '7', 'e' => '8', 'f' => '9' }.freeze

    def initialize
      @groups = []

      @excluded_tables = [
        /^versions$/,
        /_blobs$/,
        /_attachments$/,
      ]
      @excluded_models = %w[
        ApplicationRecord
      ]
    end

    def add_group(group)
      @groups << group
    end

    def exclude_tables(tables)
      @tables += tables
    end

    def exclude_table(table)
      @tables << table
    end

    def exclude_models(models)
      @excluded_models += models
    end

    def exclude_model(model)
      @excluded_models << model
    end

    def generate # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity
      tables    = {}
      relations = []

      Rails.root.glob('app/models/*.rb').sort.each do |f|
        class_name = File.basename(f, '.rb').camelize
        next if @excluded_models.include? class_name

        # Standard attributes
        model              = class_name.constantize
        table_name         = model.table_name
        tables[table_name] = determine_attributes model

        # Relations
        model.reflect_on_all_associations.each do |association|
          next if (association.macro == :has_many && association.type.blank?) || association.options[:polymorphic]
          next if @excluded_tables.find { |r| r.match? association.name.to_s }

          to_table = determine_association_target association
          next if @excluded_tables.find { |r| r.match? to_table }

          as      = determine_alias association
          amounts = determine_amounts association

          arrow_color = Digest::MD5.hexdigest("#{table_name}:#{to_table}")[0..5]
          DARKEN_TABLE.each_pair { |o, r| arrow_color.gsub!(o, r) }

          relations << "#{table_name} \"<color:##{arrow_color}>#{amounts[:from]}</color>\" --[##{arrow_color}]-- \"<color:##{arrow_color}>#{amounts[:to]}</color>\" #{to_table}#{as ? " : <color:##{arrow_color}>#{as}</color>" : ''}"
        end
      end

      output tables, relations
    end

    private

    def determine_attribute_section(attribute)
      if ASSOCIATION_ATTRIBUTES.find { |r| r.match? attribute }
        :associations
      elsif RAILS_ATTRIBUTES.include? attribute
        :common
      else
        :default
      end
    end

    def determine_attributes(model)
      attributes = { common: {}, associations: {}, default: {} }
      model.attribute_types.each do |attribute, type|
        type_name = type.class.name.demodulize&.underscore

        attributes[determine_attribute_section(attribute)][attribute] = type_name == 'time_zone_converter' ? 'datetime' : type_name
      end
      attributes
    end

    def determine_association_target(association)
      return association.options[:class_name].constantize.table_name if association.options[:class_name]

      association.plural_name.to_s
    end

    def determine_alias(association)
      as = nil
      as = association.name if association.options[:class_name]
      as = association.options[:as] if association.options[:as]

      as
    end

    def determine_amounts(association)
      if association.macro == :has_many
        from_amount = '0..n'
        to_amount   = '1'
      else
        from_amount = '1'
        to_amount   = '0..n'
      end

      { from: from_amount, to: to_amount }
    end

    def output(tables, relations) # rubocop:disable Metrics/MethodLength
      relations.sort!

      out = "@startuml\n"
      out += "' This file was generated using the \"rake uml:models\" task. Do not modify it by hand.\n"
      out += "' Some indices may be incorrect, this file is for a general comprehension only.\n\n"
      out += "skinparam linetype ortho\n"
      out += "left to right direction\n\n"

      out += @groups.map(&:to_puml).join

      out += "\n"

      tables.each_pair do |name, fields|
        out += output_table(name, fields)
      end

      out += relations.join("\n")

      "#{out}\n\n@enduml"
    end

    def output_table(name, fields)
      attributes = output_table_attributes fields[:default]
      attributes += "  .. associations ..\n#{output_table_attributes(fields[:associations])}" if fields[:associations].keys.count.positive?
      attributes += "  .. common fields ..\n#{output_table_attributes(fields[:common])}" if fields[:common].keys.count.positive?
      "object #{name} {\n#{attributes}}\n\n"
    end

    def output_table_attributes(attributes)
      output = ''
      attributes.each_pair { |k, t| output += "  #{k} <#{t}>\n" }

      output
    end
  end
end
