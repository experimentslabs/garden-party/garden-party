# frozen_string_literal: true

module Puml
  class Group
    ##
    # @param name   [String]             Group name
    # @param color  [String]             Hex color without the "#". Short colors are supported (FFF). For transparency, use full colors+alpha (FFFFFF60)
    # @param tables [Array<Symbol>]      List of tables in this group
    # @param groups [Array<Puml::Group>] List of sub groups
    def initialize(name, color:, tables: [], groups: [])
      @name   = name
      @tables = tables
      @groups = groups
      @color  = color
    end

    def to_puml
      objects_and_packages = "#{objects}\n#{packages}".chomp
      <<~TXT

        package "#{@name}" ##{@color};line:#{@color} {
        #{objects_and_packages}
        }
      TXT
    end

    private

    def packages
      return '' if @groups.empty?

      pad @groups.map(&:to_puml).join
    end

    def objects
      return '' if @tables.empty?

      @tables.map { |table| "  object #{table}" }.join("\n")
    end

    ##
    # @param lines  [Array<String>]
    #
    # @return [String]
    def pad(lines)
      lines.split("\n").map { |line| "  #{line}".sub(/ +$/, '') }.join("\n")
    end
  end
end
