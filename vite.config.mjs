import { defineConfig } from 'vite'

import RubyPlugin from 'vite-plugin-ruby'
import vue from '@vitejs/plugin-vue'
import deadFile from 'vite-plugin-deadfile'

export default defineConfig({
  plugins: [
    RubyPlugin(),
    vue(),
    deadFile({
      root: 'app/javascript',
      throwWhenFound: true,
      exclude: [
        'specs/**',
        'channels/**',
        /.scss$/i,
        /.spec.js(.snap)?$/i,
      ],
    }),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        api: 'modern',
      },
    },
  },
  test: {
    environment: 'happy-dom',
    setupFiles: [
      // Root is "app/javascript"
      '../../vitest.setup.mjs',
    ],
    // Needs to be true as vi-canvas-mock uses "vi" as if it was a global
    globals: true,
    restoreMocks: true,
    coverage: {
      // Default is generated in app/javascript
      reportsDirectory: '../../coverage-js',
      reportOnFailure: true,
      reporter: ['text', 'json', 'html'],
      all: true,
      exclude: [
      ],
    },
    server: {
      deps: {
        inline: [
          // '@experiments-labs/rise_ui',
        ],
      },
    },
  },
})
