# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~>3.1.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 7.2.0'

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem 'sprockets-rails'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'
# Use Puma as the app server
gem 'puma', '~> 6.0'
# Use SCSS for stylesheets
# gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
# gem 'webpacker', '~> 5.0'
gem 'vite_rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# App specific gems
gem 'acts-as-taggable-on'
gem 'bitfields'
gem 'devise'
gem 'devise-i18n'
gem 'devise_invitable'
gem 'diffy'
gem 'hamlit-rails'
gem 'hashdiff'
gem 'i18n-js'
gem 'icalendar'
gem 'image_optim'
gem 'json-schema'
gem 'jwt'
gem 'kaminari'
gem 'oj'
gem 'paper_trail'
gem 'pundit'
gem 'rabl'
gem 'rails-i18n'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

# ffi < 1.17 requires a version of "rubygems" that is not part of Ruby 3.1.2
# See https://github.com/ffi/ffi/issues/1103
gem 'ffi', '< 1.17.0'

group :development, :test do
  gem 'brakeman'
  gem 'cucumber-rails', require: false
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'haml_lint', require: false
  gem 'i18n-tasks'
  gem 'rspec-rails'
  gem 'rspec-rails-api'
  gem 'simplecov', require: false

  gem 'database_cleaner'
  gem 'rubocop', require: false
  gem 'rubocop-capybara'
  gem 'rubocop-factory_bot'
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'rubocop-rspec_rails'
  # Call 'debugger' anywhere in the code to stop execution and get a debugger console
  gem 'debug'

  # Other gems for tasks
  gem 'deepsort'
  gem 'plantuml_builder', require: false
end

group :development do
  gem 'listen', '~> 3.3'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  # gem 'rack-mini-profiler', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'capybara-screenshot', github: 'el-cms/capybara-screenshot', branch: 'custom-prefixes'
  gem 'selenium-webdriver', '>= 4.9.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
