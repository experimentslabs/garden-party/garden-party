import * as VueTestUtils from '@vue/test-utils'
import failOnConsole from 'vitest-fail-on-console'
import { vi } from 'vitest'
import 'vi-canvas-mock'

failOnConsole()

window.I18n = { locale: 'en', translations: { en: {} } }
window.ResizeObserver = vi.fn().mockImplementation(() => ({
  observe: vi.fn(),
  unobserve: vi.fn(),
  disconnect: vi.fn(),
}))

VueTestUtils.config.global.mocks.$_ruiIconsFiles = { default: '' }
VueTestUtils.config.global.mocks.$t = (v) => v
VueTestUtils.config.global.mocks.$d = date => date ? date.toUTCString() : null
VueTestUtils.config.global.mocks.$d = date => date ? date.toUTCString() : null
