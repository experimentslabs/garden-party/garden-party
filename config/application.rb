# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GardenParty
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.2
    config.autoload_paths << Rails.root.join('app', 'models', 'notifications')

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w[assets classes tasks templates vue_i18n_scanner.rb])

    VERSION = '0.16.1'

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    config.eager_load_paths << Rails.root.join('app', 'models', 'notifications')

    # Load defaults
    config.garden_party = config_for(:garden_party_defaults)

    # Instance configuration may not have overrides for current environment
    # so we load it first and only merge if it contains something
    instance_config = config_for(:garden_party_instance) if Rails.root.join('config', 'garden_party_instance.yml').exist?
    config.garden_party.deep_merge! instance_config if instance_config

    raise 'Missing secret_keys.jwt value in Garden Party instance configuration' if config.garden_party.secret_keys[:jwt].blank?

    config.action_mailer.default_url_options = config.garden_party.default_url_options
    config.action_mailer.default_options = config.garden_party.default_email_options
  end
end
