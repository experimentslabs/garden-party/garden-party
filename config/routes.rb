# frozen_string_literal: true

Rails.application.routes.draw do
  concern :paginatable do
    get '(page/:page)', action: :index, on: :collection, as: ''
  end

  # Creates API routes for common sub-routes on placeable elements
  concern :api_placeable do |options|
    options[:scope].resources :activities, only: [:index]
    options[:scope].resources :observations, only: [:index]
    options[:scope].resources :tasks, only: [:index]
  end

  devise_for :users, skip: [:invitations], controllers: {
    registrations: 'account/account',
  }

  devise_scope :user do
    # Disable every invitation routes except "/accept", we're managing these in the API
    get 'users/invitations/accept', to: 'devise/invitations#edit', as: :accept_user_invitation
    put 'users/invitations', to: 'devise/invitations#update', as: :user_invitation
  end

  root to: 'app#home'

  namespace :account do
    scope path: :preferences, controller: :preferences do
      get :general, as: :general_preferences
      put :general, to: 'preferences#update_general_preferences', as: nil

      get :notifications, as: :notifications_preferences
      put :notifications, to: 'preferences#update_notifications_preferences', as: nil
    end

    resource :calendar, only: [:show, :edit, :destroy] do
      put 'refresh', to: 'calendars#refresh'
    end
  end

  get '/app', to: 'app#app'
  get '/app/*path', to: 'app#app'
  get '/shared/maps/:map_id', to: 'app#shared', as: :shared_map
  get '/shared/maps/:map_id/*path', to: 'app#shared'
  get '/changelog', to: 'app#changelog'

  # Tools
  get '/tools', to: 'tools#index'
  get '/tools/soil-analysis', to: 'tools#soil_analysis'

  resources :links, except: [:show]
  resources :jwt_tokens, only: [:index, :create, :destroy]

  namespace :api do
    api_routes_exceptions = [:new, :edit]
    no_index_api_routes_exceptions = [:index, :new, :edit]

    with_options except: api_routes_exceptions do |api_route|
      # Community-related endpoints
      api_route.resources :families
      api_route.resources :genera
      api_route.resources :resource_interactions
      api_route.resources :resource_interactions_groups
      api_route.resources :resource_notes, except: [:index]
      api_route.resources :resources do
        api_route.resources :resource_notes, only: [:index], path: 'notes'
      end

      # User-related endpoints
      api_route.resource :account, only: [:show, :update]

      # Map-related endpoint, for updates and display
      api_route.resources :activities, only: [:show]
      api_route.resources :comments, except: no_index_api_routes_exceptions do
        member do
          put :remove
        end
      end
      api_route.resources :elements, except: no_index_api_routes_exceptions do
        api_route.resources :harvests, only: [:index]
        concerns :api_placeable, scope: api_route
        member do
          post '/duplicate', to: 'elements#duplicate'
        end
      end
      api_route.resources :harvests, except: no_index_api_routes_exceptions
      api_route.resources :layers, except: no_index_api_routes_exceptions
      api_route.resources :notifications, only: [:index, :show, :destroy] do
        collection do
          patch '/archive_all', to: 'notifications#archive_all', as: :archive_all
          delete '/destroy_all_archived', to: 'notifications#destroy_all_archived', as: :destroy_all_archived
        end
        patch '/archive', to: 'notifications#archive', on: :member, as: :archive
      end
      api_route.resources :observations, except: no_index_api_routes_exceptions do
        api_route.resources :comments, only: [:index]
        get '/pictures/:picture_id', to: 'observations#picture', on: :member, as: :picture
        get '/pictures/:picture_id/thumbnail', to: 'observations#picture_thumbnail', on: :member, as: :picture_thumbnail
      end
      api_route.resources :patches, except: no_index_api_routes_exceptions do
        api_route.resources :elements, only: [:index]
        concerns :api_placeable, scope: api_route
      end
      api_route.resources :paths, except: no_index_api_routes_exceptions do
        concerns :api_placeable, scope: api_route
      end
      api_route.resources :tasks, except: no_index_api_routes_exceptions do
        api_route.resources :comments, only: [:index]
      end
      api_route.resources :team_mates, only: [:create, :show] do
        member do
          patch '/accept', to: 'team_mates#accept'
          delete '/refuse', to: 'team_mates#refuse'
          delete '/leave', to: 'team_mates#leave'
          delete '/remove', to: 'team_mates#remove'
        end
      end
      api_route.resources :users, only: [:show] do
        collection do
          post '/search', to: 'users#search'
          post '/invite', to: 'users#invite'
        end
        post '/resend_invitation', to: 'users#resend_invitation', on: :member
      end

      # Maps and their own data, for listings
      api_route.resources :maps do
        get :picture, on: :member
        # Observations directly linked to the map
        api_route.resources :observations, only: [:index]
        api_route.resources :elements, only: [:index]
        api_route.resources :layers, only: [:index]
        api_route.resources :patches, only: [:index]
        api_route.resources :paths, only: [:index]
        api_route.resources :tasks, only: [:index] do
          get '/names', to: 'tasks#names', on: :collection
        end
        api_route.resources :team_mates, only: [:index]
        # Activities for anything linked to the map
        get '/all_activities', to: 'activities#all_for_map', as: :all_activities
        # Harvests for anything linked to the map
        get '/all_harvests', to: 'harvests#all_for_map', as: :all_harvests
        # Observations for anything linked to the map
        get '/all_observations', to: 'observations#all_for_map', as: :all_observations
        # Tasks for anything linked to the map
        get '/all_tasks', to: 'tasks#all_for_map', as: :all_tasks
      end
      get 'shared/maps/:id', to: 'maps#shared', as: :shared_map
    end
  end

  namespace :admin do
    resources :families, concerns: :paginatable
    resources :genera, concerns: :paginatable
    resources :resource_interactions_groups, concerns: :paginatable
    resources :resource_interactions, concerns: :paginatable
    resources :resource_notes, only: [:destroy]
    resources :resources, concerns: :paginatable
    resources :links, except: [:show] do
      put :approve, on: :member
    end
    resources :tags, except: [:new] do
      collection do
        post :batch_process
      end
    end

    get '/import', to: 'import#index'
    get '/import/differences', to: 'import#differences'
    get '/import/library_names', to: 'import#library_names'

    %w[family genus resource resource_interactions_group resource_interaction].each do |r|
      post "/import/#{r}", controller: :import, action: r
      put "/import/#{r}", controller: :import, action: r
    end
  end

  get '/up', to: 'health_check#up'
end
