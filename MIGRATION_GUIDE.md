# Garden Party migration guide

<!-- Keep the toc for the documentation app -->
[[TOC]]

This guide assumes you have successfully installed a Garden Party instance, and you have all the required knowledge to
proceed.

Also, this guide assumes you have a dedicated user to run the application (let's say `garden_party`) and another who
owns the files (let's say `deploy`), and you connect to the server with your user, with `sudo` rights.

The general steps are as follows:

1. Update the application files (assets, dependencies, etc...)
1. Run the database migrations
1. Restart the server

**Take a look at the [release notes](#release-notes) below, there may be important changes to take care of.**

## Update application files

Depending on how you installed the application, this step is slightly different.

### Updating with Git

Pull the desired version (let's say `master`). Reset hard:

```sh
sudo -u deploy git fetch --prune
sudo -u deploy git reset --hard master
```

Update dependencies

```sh
sudo -u deploy bundle install
sudo -u deploy yarn install
```

Build the assets

```sh
RAILS_ENV=production sudo -u deploy bundle exec rails i18n:js:export
RAILS_ENV=production sudo -u deploy bundle exec rails assets:precompile
```

We use [this small](docs/scripts/demo-instance-update.sh) script to update our demo instance (managed with Git), feel
free to inspire yourself from it.s

### Updating with a package

Get a fresh package and unpack it somewhere:

```sh
curl -O  https://gitlab.com/api/v4/projects/22733551/packages/generic/slim-build/latest/garden-party-latest.tar.gz
tar -xf garden-party-latest.tar.gz -C ~/garden-party-latest
```

Remove and replace all the files of the installation, as the _deployment user_, except for:
- `tmp/`,
- `logs/`,
- `storage/`,
- `config/master.key`,
- `config/credentials.yml.enc`
- `config/garden_party_instance.yml`
- `config/database.yml`

Re-set the rights for the user who _runs the server_ (here, `garden_party`)

## Database migrations

This is a simple step; you still want to backup the database first, just in case...

```sh
RAILS_ENV=production sudo -u deploy bundle exec rails db:migrate
```

## Restart the server

If you run Puma:

```sh
sudo -u deploy touch tmp/restart.txt
```

Else, just restart the server you installed.

## Release notes

### Unreleased

### 0.16.0 to 0.16.1

- Follow the general migration steps

### 0.15 to 0.16.0

- Maps, elements, patches and paths now have a cache for observations count. Once the migrations are applied,
  synchronize the cache once with:
  ```sh
  rake counter_caches:synchronize
  ```
  Run this task whenever you experience bad values in the counter caches.

### 0.14 to 0.15.0

- Follow the general migration steps

### 0.14 to 0.15.0

- Follow the general migration steps

### 0.13.3 to 0.14.0

- Follow the general migration steps

- Garden Party now use Ruby 3.1.2 - the default on a Debian 12 (Bookworm).
- NodeJS 20 is now required
- `object` and `object_changes` columns of the `version` table are changed from `text` to `jsonb`. The migration is
  tested but backing up the database is a good idea

**Configuration:**

- New key: `instance.allowed_cors_origins` : default blocks CORS requests
- New key: `secret_keys.jwt` : used to sign JWT tokens. **REQUIRED**; generate one with `bundle exec rails secret`

**Other:**

The CI now creates [packages](https://gitlab.com/experimentslabs/garden-party/garden-party/-/packages)
and [releases](https://gitlab.com/experimentslabs/garden-party/garden-party/-/releases). Production documentation and
upgrade guide are updated with deployment direction to use packages.

### 0.13.0 to 0.13.x

- Follow the general migration steps

### 0.12.0 to 0.13.0

- Follow the general migration steps

### 0.11.x to 0.12.0

- We now use [Vips](https://github.com/libvips/ruby-vips) to process images:
  it's the default processor for Rails 7.
  Ensure you have the corresponding package installed on the server
  (`libvips42` on debian-based distributions).
- Default configuration changed: Default values are `production` values, and
  other environments override them. Update the instance configuration
  accordingly before restarting the server

### 0.10.0 to 0.11.0

- The configuration file changed (`config/garden_party_default.yml`). Update
  the instance configuration accordingly before restarting the server
  (`config/garden_party_instance.yml`).
- Follow the general migration steps

### 0.9.x to 0.10.0

- Follow the general migration steps

### 0.8.x to 0.9.0

- Update Node to Node 16.x
- Follow the general migration steps

**When deploying, don't run `bundle exec rails assets:precompile` anymore** as we
switched to Vite. Use this instead:

```sh
RAILS_ENV=production bin/vite build
```

Update your scripts accordingly.

### 0.7.0 to 0.8.0

- Follow the general migration steps
- You _may_ want to add a trusted data source and maybe contribute to the [data source
  project](https://gitlab.com/experimentslabs/garden-party/data).
  To define the datasource, fill `trusted_datasource_url` in `config/garden_party_instance.yml`

### 0.6.1 to 0.7.0

- Follow the general migration steps
- You can remove `public/pictures`

### 0.6.0 to 0.6.1

- Follow the general migration steps

### 0.5.0 to 0.6.0

- Follow the general migration steps

### 0.4.0 to 0.5.0

- Genera _must_ have family assigned to them now. If your database is not
  complete, migration will fail. You can check for missing families with
  ```rb
  Genus.where family_id: nil
  ```
- Families and genera now _must_ have unique names. Migration will fail
  unless you fixed duplicated names. Find them with
  ```rb
  # Returns duplicated names only
  Family.find_by_sql('SELECT name FROM families GROUP BY name HAVING COUNT(*) > 1').pluck :name
  Genus.find_by_sql('SELECT name FROM genera GROUP BY name HAVING COUNT(*) > 1').pluck :name
  ```
- Then, follow the general migration steps

### 0.3.0 to 0.4.0

- Follow the general migration steps

### 0.2.0 to 0.3.0

- Follow the general migration steps

### 0.1.0 to 0.2.0

- Follow the general migration steps
- At the end, run the rake task to re-analyze SVG pictures used as maps
  backgrounds:
  ```shell
  bundle exec rake maps:analyze_svg
  ```
  This will fix picture sizes and allow proper map scaling.

### 0.0.16 to 0.0.17

- Follow the general migration steps
- The layers system changed; resources now have colors. If you prefer
  resources to have distinct colors instead of the one from their old
  layer:
  ```shell
  bundle exec rake resources:reset_colors
  ```
  Or you can only re-generate pictures:
  ```shell
  bundle exec rake resources:generate_pictures
  ```
- You can safely delete `plants` and all numerical directories in
  `public/pictures/resources`

### 0.0.15 to 0.0.16

- Follow the general migration steps, everything should be fine.

### 0.0.14 to 0.0.15

- Configuration files changed; you need to rename
  `config/garden_party.yml` to `config/garden_party_instance.yml`. This
  file is now merged with `config/garden_party_defaults.yml`.
