# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Admin::ResourcePolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:resource) { FactoryBot.create :resource }

  permissions '.scope' do
    before { FactoryBot.create_list :resource, 2 }

    context 'with authenticated admin' do
      it 'returns all the resources' do
        expect(Pundit.policy_scope!(user_admin, [:admin, Resource]).count).to eq 2
      end
    end
  end

  permissions :index?, :show?, :new?, :create?, :edit?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, resource)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, resource)
      end
    end
  end
end
