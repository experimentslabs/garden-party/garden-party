# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Admin::ActsAsTaggableOn::TagPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:tag) { ActsAsTaggableOn::Tag.create! name: 'some tag' }

  permissions '.scope' do
    before do
      2.times { ActsAsTaggableOn::Tag.create! name: Faker::Lorem.word }
    end

    context 'with authenticated admin' do
      it 'returns all the families' do
        expect(Pundit.policy_scope!(user_admin, [:admin, ActsAsTaggableOn::Tag]).count).to eq 2
      end
    end
  end

  permissions :index?, :create?, :batch_process? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, ActsAsTaggableOn::Tag)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, ActsAsTaggableOn::Tag)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, ActsAsTaggableOn::Tag)
      end
    end
  end

  permissions :show?, :edit?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, tag)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, tag)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, tag)
      end
    end
  end
end
