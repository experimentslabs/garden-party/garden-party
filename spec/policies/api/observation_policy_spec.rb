# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ObservationPolicy, type: :policy do
  let(:map) { FactoryBot.create :map, publicly_available: false }
  let(:users) do
    {
      anonymous:         nil,
      other_user:        FactoryBot.create(:user),
      map_owner:         map.user,
      team_mate:         FactoryBot.create(:team_mate, :accepted, map: map).user,
      pending_team_mate: FactoryBot.create(:team_mate, map: map).user,
    }
  end
  let(:observations) do
    {
      klass:                 Observation,
      owner_observation:     FactoryBot.create(:observation, subject: map, user: users[:map_owner]),
      team_mate_observation: FactoryBot.create(:observation, subject: map, user: users[:team_mate]),
      draft_observation:     Observation.new(subject: map),
    }
  end

  user_types = [
    :anonymous,
    :other_user,
    :map_owner,
    :team_mate,
    :pending_team_mate,
  ].freeze

  permissions '.scope' do
    before do
      FactoryBot.create_list :observation, 2
    end

    it 'returns all observations' do # rubocop:disable RSpec/ExampleLength
      aggregate_failures do
        # NOTE: Access to the list of observations is first blocked by the access to the map
        expect(Pundit.policy_scope!(users[:anonymous], [:api, Observation]).count).to eq 2
        expect(Pundit.policy_scope!(users[:map_owner], [:api, Observation]).count).to eq 2
        expect(Pundit.policy_scope!(users[:team_mate], [:api, Observation]).count).to eq 2
        expect(Pundit.policy_scope!(users[:pending_team_mate], [:api, Observation]).count).to eq 2
        expect(Pundit.policy_scope!(users[:other_user], [:api, Observation]).count).to eq 2
      end
    end
  end

  {
    # Access to index is not tested as it should be blocked by the Map policy
    public_map:  [
      # Actions                                  Authorized objects      Granted users
      [[:create?],                               :draft_observation,     [:map_owner, :team_mate]],
      [[:show?, :picture?, :picture_thumbnail?], :owner_observation,     user_types],
      [[:update?],                               :owner_observation,     [:map_owner]],
      [[:update?],                               :team_mate_observation, [:team_mate]],
      [[:destroy?],                              :owner_observation,     [:map_owner]],
      [[:destroy?],                              :team_mate_observation, [:map_owner, :team_mate]],
    ],
    private_map: [
      [[:create?],                               :draft_observation,     [:map_owner, :team_mate]],
      [[:show?, :picture?, :picture_thumbnail?], :owner_observation,     [:map_owner, :team_mate]],
      [[:update?],                               :owner_observation,     [:map_owner]],
      [[:update?],                               :team_mate_observation, [:team_mate]],
      [[:destroy?],                              :owner_observation,     [:map_owner]],
      [[:destroy?],                              :team_mate_observation, [:map_owner, :team_mate]],
    ],
  }.each_pair do |map_visibility, matrix|
    context "when the map is #{map_visibility == :public_map ? 'public' : 'private'}" do
      if map_visibility == :public_map
        before do
          map.update! publicly_available: true
        end
      end

      matrix.each do |line|
        permissions(*line[0]) do
          it "grants access to #{line[2].join(', ')}" do
            aggregate_failures do
              line[2].each { |user_sym| expect(described_class).to permit(users[user_sym], observations[line[1]]) }
            end
          end

          denied = user_types - line[2]
          if denied.count.positive?
            it "denies access to #{denied.join(', ')}" do
              aggregate_failures do
                denied.each { |user_sym| expect(described_class).not_to permit(users[user_sym], observations[line[1]]) }
              end
            end
          end
        end
      end
    end
  end
end
