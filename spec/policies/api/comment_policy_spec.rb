# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::CommentPolicy, type: :policy do
  let(:public_map) { false }
  let(:map) { FactoryBot.create :map, publicly_available: public_map }
  let(:observation) { FactoryBot.create :observation, subject: map }
  let(:comment) { FactoryBot.create :comment, subject: observation }
  let(:users) do
    {
      anonymous:          nil,
      map_owner:          map.user,
      other_user:         FactoryBot.create(:user),
      accepted_team_mate: FactoryBot.create(:team_mate, :accepted, map: map).user,
      pending_team_mate:  FactoryBot.create(:team_mate, map: map).user,
      comment_owner:      comment.user,
    }
  end

  permissions :show? do
    context 'when the map is public' do
      let(:public_map) { true }

      it 'grants or denies access' do # rubocop:disable RSpec/ExampleLength
        aggregate_failures do
          expect(described_class).to permit(users[:anonymous], comment)
          expect(described_class).to permit(users[:other_user], comment)
          expect(described_class).to permit(users[:map_owner], comment)
          expect(described_class).to permit(users[:accepted_team_mate], comment)
          expect(described_class).to permit(users[:pending_team_mate], comment)
        end
      end
    end

    context 'when the map is private' do
      it 'grants or denies access' do # rubocop:disable RSpec/ExampleLength
        aggregate_failures do
          expect(described_class).not_to permit(users[:anonymous], comment)
          expect(described_class).not_to permit(users[:other_user], comment)
          expect(described_class).to permit(users[:map_owner], comment)
          expect(described_class).to permit(users[:accepted_team_mate], comment)
          expect(described_class).not_to permit(users[:pending_team_mate], comment)
        end
      end
    end
  end

  permissions :create? do
    context 'when the map is public' do
      let(:public_map) { true }

      it 'grants or denies access' do # rubocop:disable RSpec/ExampleLength
        aggregate_failures do
          expect(described_class).not_to permit(users[:anonymous], comment)
          expect(described_class).to permit(users[:other_user], comment)
          expect(described_class).to permit(users[:map_owner], comment)
          expect(described_class).to permit(users[:accepted_team_mate], comment)
          expect(described_class).to permit(users[:pending_team_mate], comment)
        end
      end
    end

    context 'when the map is private' do
      it 'grants or denies access' do # rubocop:disable RSpec/ExampleLength
        aggregate_failures do
          expect(described_class).not_to permit(users[:anonymous], comment)
          expect(described_class).not_to permit(users[:other_user], comment)
          expect(described_class).to permit(users[:map_owner], comment)
          expect(described_class).to permit(users[:accepted_team_mate], comment)
          expect(described_class).not_to permit(users[:pending_team_mate], comment)
        end
      end
    end
  end

  permissions :update?, :remove? do
    context 'when the map is public' do
      let(:public_map) { true }

      it 'grants or denies access' do # rubocop:disable RSpec/ExampleLength
        aggregate_failures do
          expect(described_class).not_to permit(users[:anonymous], comment)
          expect(described_class).not_to permit(users[:other_user], comment)
          expect(described_class).to permit(users[:map_owner], comment)
          expect(described_class).not_to permit(users[:accepted_team_mate], comment)
          expect(described_class).not_to permit(users[:pending_team_mate], comment)
          expect(described_class).to permit(comment.user, comment)
        end
      end
    end

    context 'when the map is private' do
      it 'grants or denies access' do # rubocop:disable RSpec/ExampleLength
        aggregate_failures do
          expect(described_class).not_to permit(users[:anonymous], comment)
          expect(described_class).not_to permit(users[:other_user], comment)
          expect(described_class).to permit(users[:map_owner], comment)
          expect(described_class).not_to permit(users[:accepted_team_mate], comment)
          expect(described_class).to permit(users[:accepted_team_mate], FactoryBot.create(:comment, user: users[:accepted_team_mate], subject: observation))
          expect(described_class).not_to permit(users[:pending_team_mate], comment)
          # User commented once and now map is private or user is no longer a team mate
          expect(described_class).not_to permit(comment.user, comment)
        end
      end
    end
  end
end
