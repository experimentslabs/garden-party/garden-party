# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::UserPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }

  before do
    FactoryBot.create_list :user, 1
    FactoryBot.build_list(:user, 3).each(&:invite!)
  end

  permissions '.scope' do
    context 'with no authenticated user' do
      it 'returns no users' do
        expect(Pundit.policy_scope!(nil, [:api, User]).count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the active users' do
        user
        # The 4 users created in "before" + the 2 from the RSpec setup + the user
        expect(Pundit.policy_scope!(user, [:api, User]).count).to eq 4
      end
    end
  end

  permissions :show?, :resend_invitation? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, User.last)
      end
    end

    # Seems weird when reading it, but it's ok: we're checking the ability
    # of a given user to act as a map owner on a user
    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, User.last)
      end
    end
  end

  permissions :search?, :invite? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, User)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, User)
      end
    end
  end
end
