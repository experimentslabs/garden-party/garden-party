# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::TaskPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:task) do
    map = FactoryBot.create :map, user: user
    patch = FactoryBot.create :patch, :with_element, map: map
    element = patch.elements.first
    task = FactoryBot.create :task, subject: element
    task
  end
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: task.subject.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: task.subject.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      task
      FactoryBot.create_list :task, 2
    end

    context 'with no authenticated user' do
      it 'returns all tasks' do
        expect(Pundit.policy_scope!(nil, [:api, Task]).count).to eq 3
      end
    end

    context 'with authenticated map owner' do
      it 'returns all tasks' do
        expect(Pundit.policy_scope!(user, [:api, Task]).count).to eq 3
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available tasks' do
        expect(Pundit.policy_scope!(team_mate, [:api, Task]).count).to eq 3
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available tasks' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Task]).count).to eq 3
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, task)
      end
    end

    context 'with authenticated user' do
      context 'when user is not task owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, task)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, task)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, task)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, task)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Task)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Task)
      end
    end
  end
end
