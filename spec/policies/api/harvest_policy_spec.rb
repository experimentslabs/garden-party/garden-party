# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::HarvestPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:harvest) do
    map = FactoryBot.create :map, user: user
    element = FactoryBot.create :element, :point, map: map
    FactoryBot.create :harvest, user: user, element: element
  end
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: harvest.element.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: harvest.element.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      harvest
      FactoryBot.create_list :harvest, 2
    end

    context 'with no authenticated user' do
      it 'returns all harvesting records' do
        expect(Pundit.policy_scope!(nil, [:api, Harvest]).count).to eq 3
      end
    end

    context 'with authenticated map owner' do
      it 'returns all harvesting records' do
        expect(Pundit.policy_scope!(user, [:api, Harvest]).count).to eq 3
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available harvesting records' do
        expect(Pundit.policy_scope!(team_mate, [:api, Harvest]).count).to eq 3
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available harvesting records' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Harvest]).count).to eq 3
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, harvest)
      end
    end

    context 'with authenticated user' do
      context 'when user is not harvest owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, harvest)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, harvest)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, harvest)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, harvest)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Harvest)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Harvest)
      end
    end
  end
end
