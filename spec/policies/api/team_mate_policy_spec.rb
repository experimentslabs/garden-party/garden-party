# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::TeamMatePolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:team_mate) do
    map = FactoryBot.create :map, user: user
    FactoryBot.create :team_mate, map: map
  end
  let(:other_team_mate) { FactoryBot.create :team_mate }
  let(:other_user) { other_team_mate.map.user }
  let(:unrelated_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      team_mate
      other_team_mate
      other_user
    end

    context 'with no authenticated user' do
      it 'returns no team mates' do
        expect(Pundit.policy_scope!(nil, [:api, TeamMate]).count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the team mates available' do
        # Owner + user
        expect(Pundit.policy_scope!(user, [:api, TeamMate]).count).to eq 2
      end
    end
  end

  permissions :create?, :show? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, TeamMate)
      end
    end

    # Seems weird when reading it, but it's ok: we're checking the ability
    # of a given user to act as a map owner on a team mate
    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, team_mate)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, team_mate)
        end
      end
    end
  end

  permissions :accept?, :refuse?, :leave? do
    context 'with no authenticated member' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, TeamMate)
      end
    end

    context 'with authenticated member' do
      context 'when user is the map owner' do
        it 'denies access' do
          expect(described_class).not_to permit(user, team_mate)
        end
      end

      context 'when user is the invitation target' do
        it 'grants access' do
          expect(described_class).to permit(team_mate.user, team_mate)
        end
      end

      context 'when user is unrelated' do
        it 'denies access' do
          expect(described_class).not_to permit(unrelated_user, team_mate)
        end
      end
    end
  end

  permissions :remove? do
    context 'with no authenticated member' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, TeamMate)
      end
    end

    context 'with authenticated member' do
      context 'when user is the map owner' do
        it 'grants access' do
          expect(described_class).to permit(user, team_mate)
        end
      end

      context 'when user is the invitation target' do
        it 'denies access' do
          expect(described_class).not_to permit(team_mate.user, team_mate)
        end
      end

      context 'when user is unrelated' do
        it 'denies access' do
          expect(described_class).not_to permit(unrelated_user, team_mate)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, TeamMate)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, TeamMate)
      end
    end
  end
end
