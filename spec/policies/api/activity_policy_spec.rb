# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ActivityPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:activity) do
    # Creates 1 activity (layer creation)
    # Creates 1 activity (owner is a team mate)
    map = FactoryBot.create :map, user: user
    layer = map.layers.first
    Activity.find_by subject: layer
  end
  let(:team_mate) do
    # Creates 1 activity (:join)
    relation = FactoryBot.create :team_mate, :accepted, map: activity.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: activity.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      activity
      team_mate
      pending_team_mate
      # Other unrelated activity
      FactoryBot.create :activity
    end

    context 'with no authenticated user' do
      it 'returns no activities' do
        expect(Pundit.policy_scope!(nil, [:api, Activity]).count).to eq 0
      end
    end

    context 'with authenticated owner' do
      it 'returns all the owned activities' do
        expect(Pundit.policy_scope!(user, [:api, Activity]).count).to eq 3
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available activities' do
        expect(Pundit.policy_scope!(team_mate, [:api, Activity]).count).to eq 3
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available activities' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Activity]).count).to eq 0
      end
    end
  end

  permissions :show? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, activity)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, activity)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, activity)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, activity)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, activity)
        end
      end
    end
  end
end
