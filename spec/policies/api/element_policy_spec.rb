# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ElementPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:element) do
    map   = FactoryBot.create :map, user: user
    patch = FactoryBot.create :patch, :with_element, map: map
    patch.elements.first
  end
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: element.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: element.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      # Patch element
      element
      # Point element
      map = element.map
      FactoryBot.create :element, :point, map: map, layer: map.layers.first
      # Some unrelated elements
      FactoryBot.create_list :element, 2, :point
    end

    context 'with no authenticated user' do
      it 'returns no elements' do
        expect(Pundit.policy_scope!(nil, [:api, Element]).count).to eq 0
      end
    end

    context 'with authenticated map owner' do
      it 'returns all the owned elements' do
        expect(Pundit.policy_scope!(user, [:api, Element]).count).to eq 2
      end
    end

    context 'with authenticated team_mate' do
      it 'returns all elements' do
        expect(Pundit.policy_scope!(team_mate, [:api, Element]).count).to eq 2
      end
    end

    context 'with authenticated pending team_mate' do
      it 'returns no elements' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Element]).count).to eq 0
      end
    end

    context 'with authenticated other user' do
      it 'returns no elements' do
        expect(Pundit.policy_scope!(other_user, [:api, Element]).count).to eq 0
      end
    end
  end

  permissions :create?, :show?, :update?, :duplicate?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, element)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, element)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, element)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, element)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, element)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Element)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Element)
      end
    end
  end
end
