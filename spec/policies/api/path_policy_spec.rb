# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::PathPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:path) do
    map = FactoryBot.create :map, user: user
    FactoryBot.create :path, map: map
  end
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: path.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: path.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      path
      FactoryBot.create_list :path, 2
    end

    context 'with no authenticated user' do
      it 'returns no paths' do
        expect(Pundit.policy_scope!(nil, [:api, Path]).count).to eq 0
      end
    end

    context 'with authenticated owner' do
      it 'returns all the owned paths' do
        expect(Pundit.policy_scope!(user, [:api, Path]).count).to eq 1
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available paths' do
        expect(Pundit.policy_scope!(team_mate, [:api, Path]).count).to eq 1
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available paths' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Path]).count).to eq 0
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, path)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, path)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, path)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, path)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, path)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Path)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Path)
      end
    end
  end
end
