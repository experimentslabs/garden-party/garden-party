# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::NotificationPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:notification) { FactoryBot.create :notification, recipient: user }
  let(:other_notification) { FactoryBot.create :notification }

  permissions '.scope' do
    before do
      notification
      other_notification
    end

    context 'with no authenticated user' do
      it 'returns no notifications' do
        expect(Pundit.policy_scope!(nil, [Notification]).count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the notifications concerning the user' do
        expect(Pundit.policy_scope!(user, [Notification]).count).to eq 1
      end
    end
  end

  permissions :show?, :archive?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Notification)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(user, other_notification)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, notification)
        end
      end
    end
  end

  permissions :index?, :archive_all?, :destroy_all_archived? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Notification)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Notification)
      end
    end
  end
end
