# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::LayerPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:layer) { FactoryBot.create(:map, user: user).layers.first }
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: layer.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: layer.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      layer
      # Unrelated layer
      FactoryBot.create :layer
    end

    context 'with no authenticated user' do
      it 'returns no layers' do
        expect(Pundit.policy_scope!(nil, [:api, Layer]).count).to eq 0
      end
    end

    context 'with authenticated owner' do
      it 'returns all the owned layers' do
        expect(Pundit.policy_scope!(user, [:api, Layer]).count).to eq 1
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available layers' do
        expect(Pundit.policy_scope!(team_mate, [:api, Layer]).count).to eq 1
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available layers' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Layer]).count).to eq 0
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, layer)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, layer)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, layer)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, layer)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, layer)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Layer)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Layer)
      end
    end
  end
end
