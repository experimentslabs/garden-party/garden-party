# frozen_string_literal: true

require 'rails_helper'
require 'pundit/rspec'

RSpec.describe JwtTokenPolicy, type: :policy do
  subject { described_class }

  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let!(:token) { FactoryBot.create :jwt_token, user: user }

  permissions '.scope' do
    before do
      FactoryBot.create_list :jwt_token, 2, user: other_user
    end

    context 'with no authenticated user' do
      it 'returns no activities' do
        expect(Pundit.policy_scope!(nil, JwtToken).count).to eq 0
      end
    end

    context 'with authenticated owner' do
      it 'returns all the owned activities' do
        expect(Pundit.policy_scope!(user, JwtToken).count).to eq 1
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, JwtToken)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, JwtToken)
      end
    end
  end

  permissions :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, JwtToken)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(other_user, JwtToken)
      end
    end
  end

  permissions :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, token)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(other_user, token)
      end
    end

    context 'with authenticated owner' do
      it 'grants access' do
        expect(described_class).to permit(user, token)
      end
    end
  end
end
