# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'

ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../config/environment', __dir__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'rspec/rails'

# Add additional requires below this line. Rails is not loaded until this point!
require 'capybara-screenshot/rspec'

require 'support/acceptance_entities'

# Helpers
require 'support/devise'
require 'support/migrations_helpers'
require 'support/request_helpers'
require 'support/system/helpers'
require 'support/system/js_helpers'

# Shared contexts
require 'support/shared_contexts'
require 'support/system/shared_contexts'

# Disable paper_trail by default and add helpers to play with it
require 'paper_trail/frameworks/rspec'

FactoryBot.use_parent_strategy = false
# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec', 'support', '**', '*.rb')].sort.each { |f| require f }

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.
begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec::Rails::DIRECTORY_MAPPINGS[:acceptance] = %w[spec acceptance]
RSpec::Rails::DIRECTORY_MAPPINGS[:migration] = %w[spec migrations]

# Change screenshots path
Capybara.save_path = Rails.root.join('tmp', 'rspec_screenshots')

RSpec.configure do |config|
  # Helper methods
  config.include SystemHelpers::Helpers, type: :system
  config.include SystemHelpers::JsHelpers, type: :system

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_paths = [
    Rails.root.join('spec', 'fixtures'),
  ]

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # You can uncomment this line to turn off ActiveRecord support entirely.
  # config.use_active_record = false

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, type: :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
  config.before(:suite) do
    FactoryBot.create :user_known, username: 'Matthew'
    FactoryBot.create :user_admin_known, username: 'Eva'

    # Controllers where users can create entities _should_ all be already protected by Devise "authenticate_user"
    verify_authorized_unless = %w[index new create]
    [ApplicationController, Api::ApiApplicationController].each do |klass|
      klass.after_action :verify_authorized, unless: proc { |c| verify_authorized_unless.include?(c.action_name) }
      klass.after_action :verify_policy_scoped, if: proc { |c| c.action_name == 'index' }
    end

    # Load all tasks
    GardenParty::Application.load_tasks

    # Clear system test screenshots
    FileUtils.rm_rf Capybara.save_path

    # Remove logs
    logs = [
      Rails.root.join('log', 'test.log'),
    ]
    FileUtils.rm logs
    FileUtils.touch logs
  end

  config.after(:suite) do
    DatabaseCleaner.clean_with(:truncation)
    FileUtils.rm_rf Rails.root.join('tmp', 'resources')
  end

  config.before do
    Faker::UniqueGenerator.clear
    # Exclude all hardcoded usernames to avoid collisions
    Faker::Internet.unique.exclude :username, [separators: %w[- _], specifier: 3], %w[Matthew Eva Bobby Alice Alicia Luke]
  end

  config.before(type: :system) do
    driven_by :selenium, using: ENV.fetch('HEADLESS', 'true') == 'false' ? :firefox : :headless_firefox
  end

  config.include RSpec::Rails::RequestExampleGroup, type: :acceptance
end
