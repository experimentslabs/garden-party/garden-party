# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GardenParty::RotationGroups do
  describe '.next' do
    it 'returns the next group' do
      expect(described_class.next(:leaf)).to eq :manure
    end

    it 'loops' do
      expect(described_class.next(:root)).to eq :leaf
    end
  end
end
