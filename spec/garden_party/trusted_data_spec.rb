# frozen_string_literal: true

require 'rails_helper'
require 'garden_party/trusted_data'

RSpec.describe GardenParty::TrustedData do
  let(:raw_trusted_data) { YAML.load_file file_fixture('trusted_data.yml') }

  describe '.complete_family' do
    let(:complete_family) { described_class.send :complete_family, raw_trusted_data[:families].first }

    it 'returns a complete hash' do
      expected = [:id, :kingdom, :name, :source]

      expect(complete_family.keys.sort).to eq expected
    end
  end

  describe '.complete_genus' do
    let(:complete_genus) { described_class.send :complete_family, raw_trusted_data[:genera].first }

    it 'returns a complete hash' do
      expected = [:family, :id, :name, :source]

      expect(complete_genus.keys.sort).to eq expected
    end
  end

  describe '.complete_resource' do
    let(:complete_resource) { described_class.send :complete_resource, raw_trusted_data[:resources].first }

    it 'returns a complete hash' do # rubocop:disable RSpec/ExampleLength
      expected = [
        :common_names,
        :description,
        :diameter,
        :genus,
        :harvesting_months,
        :id,
        :interactions_group,
        :latin_name,
        :name,
        :parent,
        :sheltered_sowing_months,
        :soil_sowing_months,
        :sources,
        :tags,
      ]

      expect(complete_resource.keys.sort).to eq expected
    end
  end

  describe '.complete_resource_interactions_group' do
    let(:complete_resource_interactions_group) { described_class.send :complete_resource_interactions_group, raw_trusted_data[:resource_interactions_groups].first }

    it 'returns a complete hash' do
      expected = [:description, :id, :name]

      expect(complete_resource_interactions_group.keys.sort).to eq expected
    end
  end

  describe '.complete_resource_interaction' do
    let(:complete_resource_interaction) { described_class.send :complete_resource_interaction, raw_trusted_data[:resource_interactions].first }

    it 'returns a complete hash' do
      expected = [:id, :nature, :notes, :sources, :subject, :target]

      expect(complete_resource_interaction.keys.sort).to eq expected
    end
  end
end
