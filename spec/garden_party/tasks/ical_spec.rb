# frozen_string_literal: true

require 'rails_helper'

module GardenParty
  module Tasks
    RSpec.describe Ical do
      describe '.calendar' do
        let(:tasks) do
          traits = [:for_element, :for_patch, :for_map, :for_path]
          statuses = [:done, :planned]
          list = []
          traits.each do |trait|
            statuses.each { |status| list << FactoryBot.create(:task, trait, status) }
          end

          list
        end

        it 'generates the calendar' do
          result = described_class.calendar(tasks)

          expect(result.todos.count).to eq 8
        end
      end
    end
  end
end
