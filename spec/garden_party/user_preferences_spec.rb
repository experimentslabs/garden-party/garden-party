# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GardenParty::UserPreferences do
  let(:valid_preferences) { { 'map' => nil, 'theme' => 'dark', 'email_notifications' => { 'system_notification' => false } } }
  let(:instance) { described_class.new valid_preferences }

  describe 'to_h' do
    let(:results) { instance.to_h }

    it 'filters email preferences' do
      expect(results['email_notifications'].keys).to eq described_class::EMAIL_NOTIFICATIONS_DEFAULTS.keys
    end

    it 'coerce some values' do
      expect(results['email_notifications']['system_notification']).to be true
    end
  end

  describe 'email_notifications=' do
    it 'keeps the value for the system notifications to false' do
      instance.email_notifications = { 'system_notification' => false }
      results = instance.to_h
      expect(results['email_notifications']['system_notification']).to be true
    end

    it 'changes only the passed values existing values if not provided' do # rubocop:disable RSpec/ExampleLength
      instance.email_notifications = { 'team_mate_invite_notification' => false }
      results = instance.to_h

      aggregate_failures do
        expect(results['email_notifications']['system_notification']).to be true
        expect(results['email_notifications']['team_mate_invite_notification']).to be false
        expect(results['email_notifications']['team_mate_leave_notification']).to be true
      end
    end

    it 'does not accept new pairs of keys/values' do
      expect { instance.email_notifications = { 'new_key' => false } }.to raise_error(/Unknown key/)
    end

    it 'fallbacks to default' do
      instance.email_notifications = { 'team_mate_invite_notification' => 'something' }
      results = instance.to_h
      expect(results['email_notifications']['team_mate_invite_notification']).to be true
    end
  end

  describe 'send_email?' do
    let(:user) do
      value = FactoryBot.create :user
      value.preferences['email_notifications'] = { 'team_mate_invite_notification' => false }
      value.save!

      value
    end

    it 'returns the right value' do
      aggregate_failures do
        expect(described_class.send_email?('SystemNotification', user)).to be true
        expect(described_class.send_email?('TeamMateInviteNotification', user)).to be false
      end
    end
  end
end
