# frozen_string_literal: true

require 'rails_helper'
require 'garden_party/trusted_data/importer'

RSpec.describe GardenParty::TrustedData::Importer do
  let(:trusted_data) do
    data = YAML.load_file file_fixture('trusted_data.yml')
    GardenParty::TrustedData.send :complete_entries, data
  end
  let(:family) { Family.create sync_id: '57', name: 'Rosaceae', kingdom: 'plant', source: 'https://fr.wikipedia.org/wiki/Rosaceae' }
  let(:genus) { Genus.create sync_id: '118', name: 'Prunus', family: family, source: 'https://fr.wikipedia.org/wiki/Prunus' }
  let(:resource) do
    Resource.create sync_id:      '1',
                    name:         'Abricotier',
                    latin_name:   'Prunus armeniaca',
                    genus:        genus,
                    common_names: ['Abricotier commun'],
                    diameter:     3.5,
                    tag_list:     ['comestible'],
                    sources:      ['https://fr.wikipedia.org/wiki/Abricotier'],
                    description:  <<~MD
                      L’**Abricotier**, parfois appelé **Abricotier commun** (_Prunus armeniaca_), est un arbre fruitier appartenant au genre _Prunus_ de la famille des _Rosaceae_, cultivé pour son fruit, l'abricot.

                      Dans les Grandes Antilles, le terme abricot désigne le fruit d'un arbre appelé **Abricotier des Antilles** Mammea americana L. de la famille des Clusiacées.
                    MD
  end

  describe '#import_family' do
    context 'when family does not exist' do
      it 'imports family' do
        expect do
          described_class.import_family trusted_data[:families].first
        end.to change(Family, :count).by 1
      end
    end

    context 'when family exist with sync_id' do
      before { family.update! sync_id: trusted_data[:families].first[:id] }

      it 'does not import family' do
        expect do
          described_class.import_family trusted_data[:families].first
        end.not_to change(Family, :count)
      end
    end

    context 'when family exist without sync_id' do
      before { family.update! sync_id: nil }

      it 'does not import family' do
        expect do
          described_class.import_family trusted_data[:families].first
        end.not_to change(Family, :count)
      end
    end
  end

  describe '#import_genus' do
    context 'when family exists' do
      before { family }

      context 'when genus does not exist' do
        it 'imports genus' do
          expect do
            described_class.import_genus trusted_data[:genera].first
          end.to change(Genus, :count).by 1
        end
      end

      context 'when genus exist with sync_id' do
        before { genus.update! sync_id: trusted_data[:genera].first[:id] }

        it 'does not import genus' do
          expect do
            described_class.import_genus trusted_data[:genera].first
          end.not_to change(Genus, :count)
        end
      end

      context 'when genus exist without sync_id' do
        before { genus.update! sync_id: nil }

        it 'does not import genus' do
          expect do
            described_class.import_genus trusted_data[:genera].first
          end.not_to change(Genus, :count)
        end
      end
    end

    context 'when family does not exist' do
      it 'does not import genus' do
        expect do
          described_class.import_genus trusted_data[:genera].first
        end.not_to change(Genus, :count)
      end
    end
  end

  describe '#import_resource' do
    context 'when genus exists' do
      before { genus }

      context 'when resource does not exist' do
        it 'imports resource' do
          expect do
            described_class.import_resource trusted_data[:resources].first
          end.to change(Resource, :count).by 1
        end
      end

      context 'when resource exist with sync_id' do
        before { resource.update! sync_id: trusted_data[:resources].first[:id] }

        it 'does not import resource' do
          expect do
            described_class.import_resource trusted_data[:resources].first
          end.not_to change(Resource, :count)
        end
      end

      context 'when resource exist without sync_id' do
        before { resource.update! sync_id: nil }

        it 'does not import resource' do
          expect do
            described_class.import_resource trusted_data[:resources].first
          end.not_to change(Resource, :count)
        end
      end
    end

    context 'when genus does not exist' do
      it 'does not import resource' do
        expect do
          described_class.import_resource trusted_data[:resources].first
        end.not_to change(Resource, :count)
      end
    end
  end

  describe '#import_resource_interaction_group' do
    let(:resource_interactions_group) { ResourceInteractionsGroup.create sync_id: 1, name: 'Pruniers' }

    context 'when group exists' do
      context 'when group exist with sync_id' do
        before { resource_interactions_group.update! sync_id: trusted_data[:resource_interactions_groups].first[:id] }

        it 'does not import group' do
          expect do
            described_class.import_resource_interaction_group trusted_data[:resource_interactions_groups].first
          end.not_to change(ResourceInteractionsGroup, :count)
        end
      end

      context 'when group exist without sync_id' do
        before { resource_interactions_group.update! sync_id: nil }

        it 'does not import group' do
          expect do
            described_class.import_resource_interaction_group trusted_data[:resource_interactions_groups].first
          end.not_to change(ResourceInteractionsGroup, :count)
        end
      end
    end

    context 'when group does not exist' do
      it 'imports group' do
        expect do
          described_class.import_resource_interaction_group trusted_data[:resource_interactions_groups].first
        end.to change(ResourceInteractionsGroup, :count).by 1
      end
    end
  end

  describe '#import_resource_interaction' do
    let(:resource_interaction) do
      subject = described_class.import_resource_interaction_group trusted_data[:resource_interactions_groups].first
      target  = described_class.import_resource_interaction_group trusted_data[:resource_interactions_groups].last

      ResourceInteraction.create! sync_id: 1, subject: subject, target: target, nature: 'parasitism'
    end

    context 'when interaction exists' do
      context 'when interaction exist with sync_id' do
        before { resource_interaction.update! sync_id: trusted_data[:resource_interactions].first[:id] }

        it 'does not import resource' do
          expect do
            described_class.import_resource_interaction trusted_data[:resource_interactions].first
          end.not_to change(ResourceInteraction, :count)
        end
      end

      context 'when interaction exist without sync_id' do
        before { resource_interaction.update! sync_id: nil }

        it 'does not import resource' do
          expect do
            described_class.import_resource_interaction trusted_data[:resource_interactions].first
          end.not_to change(ResourceInteraction, :count)
        end
      end
    end

    context 'when interaction does not exist' do
      # Let's create and delete so groups are created
      before { resource_interaction.destroy }

      it 'imports interaction' do
        expect do
          described_class.import_resource_interaction trusted_data[:resource_interactions].first
        end.to change(ResourceInteraction, :count).by 1
      end
    end
  end
end
