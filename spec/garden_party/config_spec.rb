# frozen_string_literal: true

require 'rails_helper'

module GardenParty
  RSpec.describe Config do
    describe '.server_url' do
      it 'returns the right string' do
        expect(described_class.server_url).to eq 'http://localhost:3000'
      end
    end
  end
end
