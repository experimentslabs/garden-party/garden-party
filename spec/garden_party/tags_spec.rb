# frozen_string_literal: true

require 'rails_helper'
require 'garden_party/tags'

RSpec.describe GardenParty::Tags do
  describe '.merge!' do
    let(:kept_tag) { ActsAsTaggableOn::Tag.create! name: 'first::tag' }
    # All tags ids
    let(:ids) { ActsAsTaggableOn::Tag.order(:id).pluck :id }

    before do
      # One resource tagged with tags that will be merged
      # 2 tags
      # 1 taggable
      FactoryBot.create :resource, tag_list: [kept_tag.name, 'another_tag']
      # 2 tags
      # 2 taggable
      FactoryBot.create_list(:resource, 2).each do |resource|
        resource.update tag_list: [Faker::Lorem.unique.word]
      end
    end

    it 'keeps the first tag of the list' do
      described_class.merge! ids
      expect(kept_tag).not_to be_destroyed
    end

    it 'deletes all other tags' do
      described_class.merge! ids
      expect(ActsAsTaggableOn::Tag.count).to eq 1
    end

    it 'does not create duplicate taggings on the taggable' do
      described_class.merge! ids
      kept_tag.reload
      expect(kept_tag.taggings.count).to eq 3
    end

    it 'resets taggings counter on first tag' do
      described_class.merge! ids
      kept_tag.reload
      expect(kept_tag.taggings_count).to eq 3
    end

    it 'returns the first tag id' do
      expect(described_class.merge!(ids)).to eq kept_tag.id
    end
  end
end
