# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/notification
class NotificationPreview < ActionMailer::Preview
  def new_comment_notification_email
    map = FactoryBot.create :map, publicly_available: true
    author = FactoryBot.create :user
    observation = FactoryBot.create :observation, subject: map
    comment = FactoryBot.create :comment, subject: observation
    notification = NewCommentNotification.new content: comment.content, subject: comment, sender: author, recipient: map.user, map: map
    NotificationMailer.with(notification: notification).new_comment_notification_email
  end

  def new_system_notification_email
    content = {
      title:   'Hello world',
      message: Faker::Lorem.paragraphs.join("\n"),
    }

    notification = SystemNotification.new content:   content,
                                          recipient: FactoryBot.build(:user)
    NotificationMailer.with(notification: notification).new_system_notification_email
  end

  def new_team_mate_invite_notification_email
    owner = FactoryBot.build :user
    map = FactoryBot.build :map, user: owner
    invitee = FactoryBot.build :user
    invitation = FactoryBot.build :team_mate, map: map, user: invitee
    notification = TeamMateInviteNotification.new subject: invitation, sender: owner, recipient: invitee
    NotificationMailer.with(notification: notification).new_team_mate_invite_notification_email
  end

  def new_team_mate_leave_notification_email
    owner = FactoryBot.build :user
    map = FactoryBot.build :map, user: owner
    invitee = FactoryBot.build :user
    notification = TeamMateLeaveNotification.new subject: map, sender: invitee, recipient: owner
    NotificationMailer.with(notification: notification).new_team_mate_leave_notification_email
  end

  def new_team_mate_refuse_notification_email
    owner = FactoryBot.build :user
    map = FactoryBot.build :map, user: owner
    invitee = FactoryBot.build :user
    notification = TeamMateRefuseNotification.new subject: map, sender: invitee, recipient: owner
    NotificationMailer.with(notification: notification).new_team_mate_refuse_notification_email
  end

  def new_team_mate_remove_notification_email
    owner = FactoryBot.build :user
    map = FactoryBot.build :map, user: owner
    invitee = FactoryBot.build :user
    notification = TeamMateRemoveNotification.new subject: map, sender: owner, recipient: invitee
    NotificationMailer.with(notification: notification).new_team_mate_remove_notification_email
  end
end
