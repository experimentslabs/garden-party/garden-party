# frozen_string_literal: true

# spec/mailers/previews/devise_mailer_preview.rb

# Example from:
# https://github.com/heartcombo/devise/wiki/How-To:-Use-custom-mailer
class DevisePreview < ActionMailer::Preview
  # def confirmation_instructions
  #   Devise::Mailer.confirmation_instructions(fake_user, "faketoken")
  # end

  def reset_password_instructions
    DeviseMailer.reset_password_instructions(fake_user, 'faketoken')
  end

  # def unlock_instructions
  #   DeviseMailer.unlock_instructions(fake_user, "faketoken")
  # end

  def email_changed
    DeviseMailer.email_changed(fake_user)
  end

  def password_changed
    DeviseMailer.password_change(fake_user)
  end

  def invited
    DeviseMailer.invitation_instructions(fake_user, 'faketoken')
  end

  private

  def fake_user
    FactoryBot.build :user
  end
end
