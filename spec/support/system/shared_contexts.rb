# frozen_string_literal: true

# rubocop:disable RSpec/MultipleExpectations, RSpec/ExampleLength

# requires a "subject" variable
RSpec.shared_examples_for 'an observation subject', type: :system do
  it 'can manage observations' do
    # Create
    click_on I18n.t('js.elements.observations.new_observation')
    fill_in_observation_with title: 'Something happened'
    expect { click_on I18n.t('generic.save') }.to (change { subject.observations.count }).by 1

    # List
    expect(page).to have_css('.gp-observation__header__main', text: 'Something happened')

    # Edit
    new_title = 'That was ok'
    within('.gp-observation') do
      click_on I18n.t('generic.edit')
    end
    fill_in I18n.t('activerecord.attributes.observation.title'), with: new_title
    expect { click_on I18n.t('generic.save') }.to(change { subject.observations.last.title })
    expect(page).to have_css('.gp-observation__header__main', text: new_title)

    # Destroy
    within('.gp-observation') do
      click_on I18n.t('generic.destroy')
    end
    expect { accept_confirmation_modal }.to (change { subject.observations.count }).by(-1)
    expect(page).to have_no_content new_title
  end
end

# requires a "subject" variable
RSpec.shared_examples_for 'a task subject', type: :system do
  it 'can manage tasks' do
    # Create
    find_all('button', text: I18n.t('generic.new_task')).first.click
    fill_in_task_with name: 'Cleanup'
    expect { click_on I18n.t('generic.save') }.to (change { subject.tasks.count }).by 1

    # List
    expect(page).to have_css('.gpa-task', text: 'Cleanup')

    # Edit
    new_name = 'Cleanup all the things'
    within('.gpa-task') do
      click_on I18n.t('generic.edit')
    end
    fill_in I18n.t('activerecord.attributes.task.name'), with: new_name
    expect { click_on I18n.t('generic.save') }.to(change { subject.tasks.last.name })
    expect(page).to have_css('.gpa-task', text: new_name)

    # Destroy
    within('.gpa-task') do
      click_on I18n.t('generic.destroy')
    end
    expect { accept_confirmation_modal }.to (change { subject.tasks.count }).by(-1)
    expect(page).to have_no_content new_name
  end
end

# rubocop:enable RSpec/MultipleExpectations, RSpec/ExampleLength
