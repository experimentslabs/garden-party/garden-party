# frozen_string_literal: true

module SystemHelpers
  module Helpers
    def login(user)
      sign_in user
      visit '/'
    end

    def reload_page
      visit current_path
    end

    def click_summary(text)
      find('summary', text: text).click
    end

    def accept_browser_alert
      page.driver.browser.switch_to.alert.accept
    end

    def accept_confirmation_modal(text = nil)
      within('.rui-modal', text: text) do
        click_on I18n.t('rui.generic.ok')
      end
    end

    def visit_map(map)
      visit "/app/maps/#{map.id}/map"
    end

    def visit_map_tasks(map)
      visit "/app/maps/#{map.id}/tasks"
    end

    def visit_map_inventory(map)
      visit "/app/maps/#{map.id}/inventory"
    end

    def visit_map_timeline(map)
      visit "/app/maps/#{map.id}/timeline"
    end

    def visit_map_inventory_element_harvests(element)
      visit "/app/maps/#{element.map.id}/inventory/elements/#{element.id}/harvests"
    end

    def visit_map_inventory_patch(patch)
      visit "/app/maps/#{patch.map.id}/inventory/patches/#{patch.id}/observations"
    end

    # Direct access to an inventory page.
    # Patch must have a name to be found in the list.
    def visit_map_inventory_patch_element(element)
      visit "/app/maps/#{element.map.id}/inventory"
      click_on I18n.t('js.inventory.panel.patches')
      click_on element.patch.name
      click_on element.resource.name
    end

    # Opens a dropdown
    #
    # Use no argument to open the first dropdown found in current context
    #
    # @param trigger [nil, String]   String to search for
    # @param inside  [:text, :title] Where to search for `trigger`
    def open_dropdown(trigger: nil, inside: :text)
      selector = 'summary.rui-dropdown__trigger'
      if inside == :title
        find("#{selector}[title=\"#{trigger}\"]").click
      else
        find(selector, text: trigger).click
      end
    end

    def click_on_in_menu(string)
      within '.rui-navbar' do
        click_on string
      end
    end

    def fill_in_task_with(name:, date: nil, notes: nil, assignee: nil)
      date ||= 1.day.from_now
      notes ||= Faker::Lorem.paragraph

      fill_in I18n.t('activerecord.attributes.task.name'), with: name
      select assignee, from: I18n.t('activerecord.attributes.task.assignee') if assignee
      fill_in I18n.t('activerecord.attributes.task.planned_for'), with: date
      fill_in I18n.t('activerecord.attributes.task.notes'), with: notes
    end

    def fill_in_observation_with(title:, date: nil, content: nil)
      date ||= Time.current
      content ||= Faker::Lorem.paragraph

      fill_in I18n.t('activerecord.attributes.observation.title'), with: title
      fill_in I18n.t('js.observations.form.date'), with: date
      fill_in I18n.t('activerecord.attributes.observation.content'), with: content
    end

    def select_in_multiselect(id, value)
      find(".multiselect[aria-owns=\"listbox-#{id}\"]").click
      find("##{id}").fill_in(with: value).send_keys :enter
    end
  end
end
