# frozen_string_literal: true

module SystemHelpers
  module JsHelpers
    MAP_SCRIPTS = File.read File.join(__dir__, 'map_scripts.js')

    # Injects JS methods to manipulate the garden view
    def inject_garden_manipulation_scripts
      sleep 1
      execute_script MAP_SCRIPTS
    end

    def select_patch(patch_id)
      puts evaluate_script "__selectPatch(#{patch_id})"
    end

    def select_element(element_id)
      puts evaluate_script "__selectElement(#{element_id})"
    end
  end
end
