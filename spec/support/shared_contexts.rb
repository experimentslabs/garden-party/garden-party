# frozen_string_literal: true

RSpec.shared_context 'with authenticated member', shared_context: :metadata do
  let(:signed_in_user) { User.find_by(email: 'user@example.com') }
  before do
    sign_in signed_in_user
  end
end

RSpec.shared_context 'with authenticated admin', shared_context: :metadata do
  let(:signed_in_admin) { User.find_by(email: 'admin@example.com') }
  before do
    sign_in signed_in_admin
  end
end
