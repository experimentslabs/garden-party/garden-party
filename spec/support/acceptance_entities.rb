# frozen_string_literal: true

require 'rspec_rails_api'

{
  user:  {
    id:            { type: :integer, description: 'Entity identifier' },
    role:          { type: :string, description: 'Either "user" or "admin"' },
    username:      { type: :string, description: 'Username' },
    account_state: { type: :string, description: 'Either "valid" or "pending"' },
  },
  error: {
    error: { type: :string, description: 'The error' },
  },
}.each_pair { |name, definition| RSpec::Rails::Api::Metadata.add_entity name, definition }
