# frozen_string_literal: true

module DeviseAcceptanceSpecHelpers
  include Warden::Test::Helpers

  def sign_in(user)
    login_as(user)
  end

  def sign_out(user = nil)
    logout(user)
  end
end

RSpec.configure do |config|
  config.include DeviseAcceptanceSpecHelpers, type: :acceptance
  config.include DeviseAcceptanceSpecHelpers, type: :request
  config.include DeviseAcceptanceSpecHelpers, type: :system
end
