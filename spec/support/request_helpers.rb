# frozen_string_literal: true

module RequestHelpers
  def json_response
    raise 'No response available' unless response

    JSON.parse response.body
  end
end

RSpec.configure do |config|
  config.include RequestHelpers, type: :request
end
