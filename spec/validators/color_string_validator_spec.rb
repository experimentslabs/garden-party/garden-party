# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ColorStringValidator do
  let(:model) do
    Class.new do
      include ActiveModel::Model
      attr_accessor :color

      validates :color, color_string: true
    end
  end

  context 'when color is valid' do
    it 'validates' do
      entity = model.new color: '123,123,123'
      entity.validate

      expect(entity.errors.count).to be_zero
    end

    it 'validates when there are spaces' do
      entity = model.new color: '123 ,123, 123'
      entity.validate

      expect(entity.errors.count).to be_zero
    end
  end

  context 'with an invalid color' do
    it 'does not validate incorrect color values' do
      entity = model.new color: '123,260,123'
      entity.validate

      expect(entity.errors.count).to eq 1
    end

    it 'does not validate string color values' do
      entity = model.new color: 'blue,123,123'
      entity.validate

      expect(entity.errors.count).to eq 1
    end

    it 'does not validate incomplete color values' do
      entity = model.new color: '123,123'
      entity.validate

      expect(entity.errors.count).to eq 1
    end
  end
end
