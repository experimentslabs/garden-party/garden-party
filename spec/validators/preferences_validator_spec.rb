# frozen_string_literal: true

require 'rails_helper'

# FIXME: schema validator errors are not localized
RSpec.describe PreferencesValidator do
  let(:test_model) do
    Class.new do
      include ActiveModel::Model
      attr_accessor :preferences

      def self.model_name
        ActiveModel::Name.new(self, nil, 'dummy')
      end

      validates :preferences, preferences: true
    end
  end
  let(:instance) { test_model.new }

  context 'when preferences is empty' do
    it 'validates empty hash' do
      instance.preferences = {}
      expect(instance).to be_valid
    end
  end

  describe 'map validation' do
    context 'with an invalid map' do
      before do
        instance.preferences = { 'map' => 0 }
      end

      it 'does not validate' do
        instance.validate
        expect(instance.errors[:map].first).to eq I18n.t('activerecord.errors.preferences.is_invalid_map')
      end
    end

    context 'with a valid map' do
      before do
        map                  = FactoryBot.create :map
        instance.preferences = { 'map' => map.id }
      end

      it 'validates' do
        expect(instance).to be_valid
      end
    end

    context 'with an empty map' do
      before do
        instance.preferences = { 'map' => nil }
      end

      it 'validates' do
        expect(instance).to be_valid
      end
    end
  end

  describe 'theme validation' do
    context 'with an invalid' do
      before do
        instance.preferences = { 'theme' => 'invalid' }
      end

      it 'does not validate' do
        instance.validate
        expect(instance.errors[:theme].first).to eq I18n.t('activerecord.errors.preferences.is_invalid_theme')
      end
    end

    context 'with a valid theme' do
      before do
        instance.preferences = { 'theme' => 'light' }
      end

      it 'validates' do
        expect(instance).to be_valid
      end
    end

    context 'with an empty theme' do
      before do
        instance.preferences = { 'theme' => nil }
      end

      it 'validates' do
        expect(instance).to be_valid
      end
    end
  end
end
