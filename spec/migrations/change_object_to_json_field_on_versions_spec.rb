# frozen_string_literal: true

require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20240111152951_change_object_to_json_field_on_versions.rb'

RSpec.describe ChangeObjectToJsonFieldOnVersions, :versioning, type: :migration do
  let(:versions) { table(:versions) }

  before do
    # Enforce the "new" serializer
    @paper_trail_serializer = PaperTrail::Serializers::JSON
  end

  after do
    PaperTrail.serializer = @paper_trail_serializer # rubocop:disable RSpec/InstanceVariable
  end

  it 'changes the version fields to JSON' do
    PaperTrail.serializer = PaperTrail::Serializers::YAML

    FactoryBot.create :family

    # YAML expected
    expect(versions.last.attributes_before_type_cast['object_changes']).to match(/^---/)

    migrate!
    reset_column_in_all_models

    PaperTrail.serializer = PaperTrail::Serializers::JSON

    # JSON expected
    expect(versions.last.attributes_before_type_cast['object_changes']).to match(/^\{/)
  end

  it 'is reversible' do
    expect do
      FactoryBot.create :family

      migrate!
      reset_column_in_all_models

      schema_migrate_down!
      reset_column_in_all_models
    end.not_to raise_error

    PaperTrail.serializer = PaperTrail::Serializers::YAML

    # YAML expected
    expect(versions.last.attributes_before_type_cast['object_changes']).to match(/^---/)
  end
end
