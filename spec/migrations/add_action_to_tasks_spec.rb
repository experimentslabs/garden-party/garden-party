# frozen_string_literal: true

require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20211105062125_add_action_to_tasks.rb'

RSpec.describe AddActionToTasks, type: :migration do
  let(:families) { table(:families) }
  let(:genera) { table(:genera) }
  let(:resources) { table(:resources) }

  let(:users) { table(:users) }

  let(:maps) { table(:maps) }
  let(:elements) { table(:elements) }
  let(:layers) { table(:layers) }
  let(:patches) { table(:patches) }

  let(:tasks) { table(:tasks) }

  def fill_database
    now       = Time.current
    tomorrow  = now + 1.day
    yesterday = now - 1.day

    family   = families.create! name: 'A family', kingdom: 0
    genus    = genera.create! name: 'A genus', family_id: family.id
    resource = resources.create! name: 'A resource', latin_name: 'something', genus_id: genus.id, color: '0,0,0'

    user = users.create! role: 'user', email: 'john.doe@example.com', encrypted_password: 'abc123', username: 'John'

    map   = maps.create! name: 'A map', center: '0.0,0.0', user_id: user.id
    layer = layers.create! name: 'A layer', map_id: map.id, position: 1
    patch = patches.create! name: 'A patch', map_id: map.id, layer_id: layer.id, geometry: '{}'

    elements.create! resource_id: resource.id, patch_id: patch.id, implantation_planned_for: tomorrow
    elements.create! resource_id: resource.id, patch_id: patch.id, implanted_at: yesterday, removal_planned_for: tomorrow
    elements.create! resource_id: resource.id, patch_id: patch.id
  end

  it 'is creates tasks from elements states' do
    fill_database

    migrate!
    reset_column_in_all_models
    expect(tasks.count).to eq 2
  end

  it 'is reversible' do
    expect do
      fill_database

      migrate!
      reset_column_in_all_models

      schema_migrate_down!
    end.not_to raise_error
  end
end
