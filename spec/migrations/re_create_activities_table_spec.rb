# frozen_string_literal: true

require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20211109051907_re_create_activities_table.rb'

RSpec.describe ReCreateActivitiesTable, type: :migration do
  let(:users) { table(:users) }
  let(:families) { table(:families) }
  let(:resources) { table(:resources) }
  let(:genera) { table(:genera) }
  let(:layers) { table(:layers) }
  let(:maps) { table(:maps) }
  let(:patches) { table(:patches) }
  let(:elements) { table(:elements) }
  let(:activities) { table(:activities) }

  let(:observations) { table(:observations) }
  let(:tasks) { table(:tasks) }

  let(:user) { users.create! role: 'user', email: 'john.doe@example.com', encrypted_password: 'abc123', username: 'John' }
  let(:other_user) { users.create! role: 'user', email: 'alice.doe@example.com', encrypted_password: 'abc123', username: 'Alice' }

  def fill_database
    now = Time.current
    # Before getting to this point, RSpec already migrated once, up and down.
    # The migration we're testing creates a layer on down, so we need to clean
    # this table first.
    layers.destroy_all

    family   = families.create! name: 'A family', kingdom: 0
    genus    = genera.create! name: 'A genus', family_id: family.id
    resource = resources.create! name: 'A resource', latin_name: 'somethingus', genus_id: genus.id, color: '0,0,0'

    user
    map   = maps.create! name: 'A map', center: '0.0,0.0', user_id: user.id
    layer = layers.create! name: 'A layer', map_id: map.id, position: 0

    patch   = patches.create map_id: map.id, layer_id: layer.id, geometry: '{"geometry": {"type": "Point"}'
    element = elements.create patch_id: patch.id, resource_id: resource.id, implanted_at: (now - 3.days), removed_at: (now - 1.day)

    tasks.create! name: 'map done', subject_type: 'Map', subject_id: map.id, done_at: now
    tasks.create! name: 'map done and assigned', assignee_id: other_user.id, subject_type: 'Map', subject_id: map.id, done_at: now
    tasks.create! name: 'map planned', subject_type: 'Map', subject_id: map.id, planned_for: now
    tasks.create! name: 'patch done', subject_type: 'Patch', subject_id: patch.id, done_at: now
    tasks.create! name: 'patch done and assigned', assignee_id: other_user.id, subject_type: 'Patch', subject_id: patch.id, done_at: now
    tasks.create! name: 'patch planned', subject_type: 'Patch', subject_id: patch.id, planned_for: now
    tasks.create! name: 'element done', subject_type: 'Element', subject_id: element.id, done_at: now
    tasks.create! name: 'element done and assigned', assignee_id: other_user.id, subject_type: 'Element', subject_id: element.id, done_at: now
    tasks.create! name: 'element planned', subject_type: 'Element', subject_id: element.id, planned_for: now
    observations.create! title: 'Something nice', subject_type: 'Map', subject_id: map.id, user_id: user.id, made_at: now
  end

  it 'is reversible' do
    expect do
      fill_database

      migrate!

      schema_migrate_down!
    end.not_to raise_error
  end

  it 'creates activities from existing data' do
    fill_database
    # Initial tasks count
    expect(tasks.count).to eq 9

    migrate!
    reset_column_in_all_models

    # Table activities has been created and filled with done tasks
    # 6 tasks done,
    # 3 tasks planned,
    # 1 element implanted,
    # 1 element removed,
    # 1 patch created,
    # 1 observation made
    expect(activities.count).to eq 13
    # Activities done have the correct user
    user_ids = activities.order(:user_id).pluck(:user_id).uniq
    expect(user_ids).to eq [other_user.id, user.id].sort
  end
end
