# frozen_string_literal: true

require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20240210151457_remove_discarded_at_from_tables'

# This migration test needs a lot of data combinations to ensure the intended behavior is respected.
#
# A few helper methods are created to generate element with or without a discarded state.
#
# Every block that creates data has the expected amount of entries states in the comments.
RSpec.describe RemoveDiscardedAtFromTables, type: :migration do
  let(:families) { table(:families) }
  let(:genera) { table(:genera) }
  let(:resources) { table(:resources) }

  let(:users) { table(:users) }

  let(:maps) { table(:maps) }
  let(:elements) { table(:elements) }
  let(:layers) { table(:layers) }
  let(:patches) { table(:patches) }
  let(:paths) { table(:paths) }

  let(:tasks) { table(:tasks) }
  let(:observations) { table(:observations) }
  let(:harvests) { table(:harvests) }

  let(:user) { users.create! role: 'user', email: 'john.doe@example.com', encrypted_password: 'abc123', username: 'John' }

  let(:now) { Time.current }

  def create_layer(map, discarded_at: nil)
    layers.create name: 'A layer', map_id: map.id, position: 0, discarded_at: discarded_at
  end

  def create_patch(layer, discarded_at: nil)
    patches.create(map_id: layer.map_id, layer_id: layer.id, geometry: '{}', discarded_at: discarded_at)
  end

  def create_path(layer, discarded_at: nil)
    paths.create(map_id: layer.map_id, layer_id: layer.id, geometry: '{}', discarded_at: discarded_at)
  end

  def create_element(patch_or_layer, resource, discarded_at: nil)
    ref = if patch_or_layer.respond_to? :layer_id
            { patch_id: patch_or_layer.id }
          else
            { layer_id: patch_or_layer.id }
          end

    elements.create(ref.merge(resource_id: resource.id, discarded_at: discarded_at))
  end

  def create_observation(subject, subject_type, discarded_at: nil)
    observations.create(subject_id: subject.id, subject_type: subject_type, discarded_at: discarded_at, title: 'An observation', user_id: user.id, made_at: now)
  end

  def create_harvest(element, discarded_at: nil)
    harvests.create(element_id: element.id, discarded_at: discarded_at, quantity: 1, unit: 1, user_id: user.id, harvested_at: now)
  end

  def fill_database
    family   = families.create! name: 'A family', kingdom: 0
    genus    = genera.create! name: 'A genus', family_id: family.id
    resource = resources.create! name: 'A resource', latin_name: 'somethingus', genus_id: genus.id, color: '0,0,0'

    map = maps.create name: 'A map', center: '0.0,0.0', user_id: user.id

    # kept layers       : 0/1
    # kept patches      : 0/1
    # kept paths        : 0/1
    # kept elements     : 0/2
    # kept observations : 0/4
    # kept harvests     : 0/2
    create_layer(map, discarded_at: now).tap do |layer|
      create_patch(layer).tap do |patch|
        create_element(patch, resource).tap do |element|
          create_observation(element, 'Element')
          create_harvest(element)
        end
        create_observation(patch, 'Patch')
      end

      create_path(layer).tap do |path|
        create_observation(path, 'Path')
      end

      create_element(layer, resource).tap do |element|
        create_observation(element, 'Element')
        create_harvest(element)
      end
    end

    # kept layers       : 1/1
    # kept patches      : 0/1
    # kept paths        : 0/1
    # kept elements     : 0/2
    # kept observations : 0/4
    # kept harvests     : 0/2
    create_layer(map).tap do |layer|
      # Discarded patch, content should be deleted
      create_patch(layer, discarded_at: now).tap do |patch|
        create_element(patch, resource).tap do |element|
          create_observation(element, 'Element')
          create_harvest(element)
        end
        create_observation(patch, 'Patch')
      end
      # Discarded path, content should be deleted
      create_path(layer, discarded_at: now).tap do |path|
        create_observation(path, 'Path')
      end

      # Discarded element, content should be deleted
      create_element(layer, resource, discarded_at: now).tap do |element|
        create_observation(element, 'Element')
        create_harvest(element)
      end
    end

    # kept layers       : 1/1
    # kept patches      : 1/1
    # kept paths        : 1/1
    # kept elements     : 1/2
    # kept observations : 0/4
    # kept harvests     : 0/2
    create_layer(map).tap do |layer|
      create_patch(layer).tap do |patch|
        # Discarded element, content should be deleted
        create_element(patch, resource, discarded_at: now).tap do |element|
          create_observation(element, 'Element')
          create_harvest(element)
        end

        # Discarded observation
        create_observation(patch, 'Patch', discarded_at: now)
      end
      create_path(layer).tap do |path|
        # Discarded observation
        create_observation(path, 'Path', discarded_at: now)
      end
      create_element(layer, resource).tap do |element|
        create_observation(element, 'Element', discarded_at: now)
        create_harvest(element, discarded_at: now)
      end
    end

    # kept layers       : 1/1
    # kept patches      : 1/1
    # kept paths        : 1/1
    # kept elements     : 2/2
    # kept observations : 3/4
    # kept harvests     : 1/2
    create_layer(map).tap do |layer|
      create_patch(layer).tap do |patch|
        # Discarded element, content should be deleted
        create_element(patch, resource).tap do |element|
          create_observation(element, 'Element', discarded_at: now)
          create_harvest(element, discarded_at: now)
        end

        create_observation(patch, 'Patch')
      end
      create_path(layer).tap do |path|
        create_observation(path, 'Path')
      end
      create_element(layer, resource).tap do |element|
        create_observation(element, 'Element')
        create_harvest(element)
      end
    end

    # kept layers       : 1/1
    # kept patches      : 1/1
    # kept paths        : 1/1
    # kept elements     : 2/2
    # kept observations : 4/4
    # kept harvests     : 2/2
    create_layer(map).tap do |layer|
      create_patch(layer).tap do |patch|
        # Discarded element, content should be deleted
        create_element(patch, resource).tap do |element|
          create_observation(element, 'Element')
          create_harvest(element)
        end

        create_observation(patch, 'Patch')
      end
      create_path(layer).tap do |path|
        create_observation(path, 'Path')
      end
      create_element(layer, resource).tap do |element|
        create_observation(element, 'Element')
        create_harvest(element)
      end
    end
  end

  it 'is reversible' do
    expect do
      fill_database

      migrate!

      schema_migrate_down!
    end.not_to raise_error
  end

  it 'deletes entries with a discard date' do
    fill_database
    aggregate_failures do
      expect(layers.count).to eq 5
      expect(patches.count).to eq 5
      expect(paths.count).to eq 5
      expect(elements.count).to eq 10
      expect(observations.count).to eq 20
      expect(harvests.count).to eq 10
    end

    migrate!
    reset_column_in_all_models

    aggregate_failures do
      expect(layers.count).to eq 4
      expect(patches.count).to eq 3
      expect(paths.count).to eq 3
      expect(elements.count).to eq 5
      expect(observations.count).to eq 7
      expect(harvests.count).to eq 3
    end
  end
end
