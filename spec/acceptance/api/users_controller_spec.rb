# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::UsersController, type: :acceptance do
  resource 'Users', 'Find and display users'

  form_errors_entity :user_form_errors, [:email, :username]

  parameters :search_payload,
             search: { type: :string, description: 'Partial username, with a minimum of 3 characters' }
  parameters :invite_payload,
             username: { type: :string, description: 'Username proposition. Final user will be able to change when finalizing the account creation process' },
             email:    { type: :string, description: 'Email address of the person to invite' }

  parameters :path_params,
             id: { type: :integer, description: 'Target user identifier' }

  let(:invitation_params) do
    {
      email:    Faker::Internet.unique.email,
      username: Faker::Internet.unique.username(separators: %w[- _], specifier: 3),
    }
  end
  let(:user) { FactoryBot.create :user }
  let(:invited_user) { User.invite! invitation_params }

  before do
    FactoryBot.create :user, username: 'Bobby'
    FactoryBot.create :user, username: 'Alice'
    FactoryBot.create :user, username: 'Alicia'
  end

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_post('/api/users/search', 'Searches for users') do
      request_params defined: :search_payload

      for_code 200, expect_many: :user do |url|
        test_response_of url, payload: { username: 'alic' }
      end
    end

    on_get('/api/users/:id', 'Show one team mate') do
      path_params defined: :path_params

      for_code 200, expect_one: :user do |url|
        test_response_of url, path_params: { id: user.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post '/api/users/invite', 'Invite an user' do
      request_params defined: :invite_payload

      for_code 201, expect_one: :user do |url|
        test_response_of url, payload: invitation_params
      end

      for_code 422, expect_one: :user_form_errors do |url|
        payload = { email: Faker::Internet.unique.email, username: '' }
        test_response_of url, payload: payload
      end
    end

    on_post '/api/users/:id/resend_invitation', 'Resend an invitation email' do
      path_params defined: :path_params

      for_code 200, expect_one: :user do |url|
        test_response_of url, path_params: { id: invited_user.id }
      end
    end
  end
end
