# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::LayersController, type: :acceptance do
  resource 'Layers', 'Manage layers'

  entity :layer,
         id:                 { type: :integer, description: 'Entity identifier' },
         name:               { type: :string, description: 'Layer name' },
         position:           { type: :integer, description: 'Position in the layers list' },
         visible_by_default: { type: :boolean, required: false, description: 'User preference' },
         map_id:             { type: :integer, description: 'Map identifier' },
         created_at:         { type: :datetime, description: 'Creation date' },
         updated_at:         { type: :datetime, description: 'Update date' }

  form_errors_entity :layer_form_errors, [:name, :position, :map]

  parameters :create_payload,
             name:               { type: :string, description: 'Layer name' },
             position:           { type: :integer, required: false, description: 'Position in the layers list' },
             visible_by_default: { type: :boolean, required: false, description: 'User preference' },
             map_id:             { type: :integer, description: 'Map identifier' }
  parameters :update_payload,
             name:               { type: :string, description: 'Layer name' },
             position:           { type: :integer, required: false, description: 'Position in the layers list' },
             visible_by_default: { type: :boolean, required: false, description: 'User preference' }

  parameters :index_path_params,
             map_id: { type: :integer, description: 'Target map identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target layer identifier' }

  let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
  let(:owned_layer) { FactoryBot.create :layer, map: owned_map }
  let(:owned_layers) { FactoryBot.create_list :layer, 2, map: owned_map }
  let(:other_layer) { FactoryBot.create :layer }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/maps/:map_id/layers', 'List layers') do
      path_params defined: :index_path_params
      for_code 200, expect_many: :layer do |url|
        owned_layers

        test_response_of url, path_params: { map_id: owned_map.id }
      end
    end

    on_get('/api/layers/:id', 'Show one layer') do
      path_params defined: :path_params

      for_code 200, expect_one: :layer do |url|
        test_response_of url, path_params: { id: owned_layer.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/layers', 'Create new layer') do
      request_params defined: :create_payload

      let(:layer_attributes) { FactoryBot.build(:layer, map: owned_map).attributes }

      for_code 201, expect_one: :layer do |url|
        test_response_of url, payload: layer_attributes
      end

      for_code 422, expect_one: :layer_form_errors do |url|
        bad_attributes = layer_attributes.merge 'name' => ''
        test_response_of url, payload: bad_attributes
      end

      for_code 401, expect_one: :error do |url|
        layer_attributes = FactoryBot.build(:layer, map: other_layer.map).attributes
        test_response_of url, payload: layer_attributes
      end
    end

    on_put('/api/layers/:id', 'Update a layer') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200, expect_one: :layer do |url|
        test_response_of url, path_params: { id: owned_layer.id }, payload: { name: 'New name' }
      end

      for_code 422, expect_one: :layer_form_errors do |url|
        test_response_of url, path_params: { id: owned_layer.id }, payload: { name: '' }
      end
    end

    on_delete('/api/layers/:id', 'Destroys a layer') do
      path_params defined: :path_params
      let(:layer) { FactoryBot.create :layer }

      for_code 204 do |url|
        test_response_of url, path_params: { id: owned_layer.id }
      end

      for_code 401, expect_one: :error do |url|
        test_response_of url, path_params: { id: other_layer.id }
      end
    end
  end
end
