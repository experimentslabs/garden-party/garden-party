# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ActivitiesController, type: :acceptance do
  resource 'Activities', 'Display activities'

  entity :activity,
         id:           { type: :integer, description: 'Entity identifier' },
         action:       { type: :string, description: 'Action' },
         data:         { type: :object, required: false, description: 'Additional data. Refer to the code for the possible values' },
         subject_type: { type: :string, description: 'Related subject type' },
         subject_id:   { type: :integer, description: 'Related subject identifier' },
         subject_name: { type: :string, required: false, description: 'Historized subject representation' },
         map_id:       { type: :integer, description: 'Map identifier' },
         user_id:      { type: :integer, description: 'User responsible for the action' },
         username:     { type: :string, description: 'Historized name of the user responsible for the action' },
         happened_at:  { type: :datetime, description: 'When the event took place' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' }

  parameters :index_path_params,
             map_id: { type: :integer, description: 'Target map identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target activity identifier' }

  # Will create 2 activities (layer + team mate)
  let(:map) { FactoryBot.create :map, user: signed_in_user }
  let(:layer) { FactoryBot.create :layer, map: map }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    describe 'lists' do
      on_get('/api/elements/:element_id/activities', 'All activities for a given element') do
        path_params fields: {
          element_id: { type: :integer, description: 'Unique element identifier' },
        }

        for_code 200, expect_many: :activity do |url|
          element = FactoryBot.create :element, :point, layer: layer
          test_response_of url, path_params: { element_id: element.id }
        end
      end

      on_get('/api/patches/:patch_id/activities', 'All activities for a given patch') do
        path_params fields: {
          patch_id: { type: :integer, description: 'Unique patch identifier' },
        }

        for_code 200, expect_many: :activity do |url|
          patch = FactoryBot.create :patch, map: map
          test_response_of url, path_params: { patch_id: patch.id }
        end
      end

      on_get('/api/paths/:path_id/activities', 'All activities for a given path') do
        path_params fields: {
          path_id: { type: :integer, description: 'Unique path identifier' },
        }

        for_code 200, expect_many: :activity do |url|
          path = FactoryBot.create :path, map: map

          test_response_of url, path_params: { path_id: path.id }
        end
      end

      on_get('/api/maps/:map_id/all_activities', 'List all map-related activities') do
        path_params defined: :index_path_params

        for_code 200, expect_many: :activity do |url|
          test_response_of url, path_params: { map_id: map.id }
        end
      end
    end

    on_get('/api/activities/:id', 'Show one activity') do
      path_params defined: :path_params

      for_code 200, expect_one: :activity do |url|
        activity = map.activities.last
        test_response_of url, path_params: { id: activity.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end
  end
end
