# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::CommentsController, type: :acceptance do
  resource 'Comments', 'Manage comments'
  entity :comment,
         id:                   { type: :integer, description: 'Entity identifier' },
         content:              { type: :string, required: false, description: 'Comment name' },
         user_id:              { type: :integer, description: 'User identifier' },
         username:             { type: :string, description: 'Username' },
         removed_at:           { type: :datetime, required: false, description: 'Removal date. Comment is considered removed when set' },
         removed_by_map_owner: { type: :boolean, description: 'For a removed item, states if it was removed by map owner' },
         subject_type:         { type: :string, description: 'Related subject type' },
         subject_id:           { type: :integer, description: 'Related subject identifier' },
         created_at:           { type: :datetime, description: 'Creation date' },
         updated_at:           { type: :datetime, description: 'Update date' }

  form_errors_entity :comment_form_error, [:observation, :content]

  parameters :create_comment_payload,
             content:      { type: :string, required: false, description: 'Comment name' },
             subject_type: { type: :string, description: 'Related subject type' },
             subject_id:   { type: :integer, description: 'Related subject identifier' }
  parameters :update_comment_payload,
             content: { type: :string, required: false, description: 'Comment name' }

  parameters :comment_path_params,
             id: { type: :integer, description: 'Target comment identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user, publicly_available: true }
    let(:observation) { FactoryBot.create :observation, subject: owned_map }
    let!(:owned_comment) { FactoryBot.create :comment, subject: observation }

    on_get('/api/observations/:observation_id/comments', 'List comments for an observation') do
      path_params fields: {
        observation_id: { type: :integer, description: 'Observation identifier' },
      }

      before do
        FactoryBot.create_list :comment, 2, subject: observation
      end

      for_code 200, expect_many: :comment do |url|
        test_response_of url, path_params: { observation_id: observation.id }
      end
    end

    on_get('/api/tasks/:task_id/comments', 'List comments for a task') do
      path_params fields: {
        task_id: { type: :integer, description: 'Task identifier' },
      }

      let(:task) { FactoryBot.create :task, subject: owned_map }

      before do
        FactoryBot.create_list :comment, 2, subject: task
      end

      for_code 200, expect_many: :comment do |url|
        test_response_of url, path_params: { task_id: task.id }
      end
    end

    on_get('/api/comments/:id', 'Show one comment') do
      path_params defined: :comment_path_params

      for_code 200, expect_one: :comment do |url|
        test_response_of url, path_params: { id: owned_comment.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/comments', 'Create new comment') do
      request_params defined: :create_comment_payload
      let(:comment_attributes) { FactoryBot.attributes_for :comment, subject_type: 'Observation', subject_id: observation.id }

      for_code 201, expect_one: :comment do |url|
        test_response_of url, payload: { comment: comment_attributes }
      end

      for_code 422, expect_one: :comment_form_error do |url|
        bad_attributes = comment_attributes.merge 'content' => nil
        test_response_of url, payload: { comment: bad_attributes }
      end
    end

    on_put('/api/comments/:id', 'Update a comment') do
      path_params defined: :comment_path_params
      request_params defined: :update_comment_payload

      for_code 200, expect_one: :comment do |url|
        test_response_of url, path_params: { id: owned_comment.id }, payload: { content: 'Some new content' }
      end

      for_code 422, expect_one: :comment_form_error do |url|
        test_response_of url, path_params: { id: owned_comment.id }, payload: { content: '' }
      end
    end

    on_put('/api/comments/:id/remove', 'Censors/removes a comment content') do
      path_params defined: :comment_path_params

      for_code 200, expect_one: :comment do |url|
        test_response_of url, path_params: { id: owned_comment.id }
      end
    end
  end
end
