# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::TeamMatesController, type: :acceptance do
  resource 'Team mates', 'Manage team mates'

  entity :team_mate,
         id:          { type: :integer, description: 'Entity identifier' },
         user:        { type: :object, description: 'User object', attributes: :user },
         user_id:     { type: :integer, description: 'Entity identifier' },
         map_id:      { type: :integer, description: 'Map identifier' },
         map:         { type: :object, description: 'Map basic information', attributes: {
           name: { type: :string, description: 'Map name' },
         } },
         accepted_at: { type: :datetime, required: false, description: 'Invitation acceptation date' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  form_errors_entity :team_mate_form_errors, [:user]

  parameters :create_payload,
             user_id: { type: :integer, description: 'Entity identifier' },
             map_id:  { type: :integer, description: 'Map identifier' }

  parameters :index_path_params,
             map_id: { type: :integer, description: 'Target map identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target team mate identifier' }

  let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
  let(:team_mate) { FactoryBot.create :team_mate, map: owned_map }
  let(:team_mates) { FactoryBot.create_list :team_mate, 2, map: owned_map }
  let(:other_team_mate) { FactoryBot.create :team_mate }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/maps/:map_id/team_mates', 'List team mates') do
      path_params defined: :index_path_params
      for_code 200, expect_many: :team_mate do |url|
        team_mates

        test_response_of url, path_params: { map_id: owned_map.id }
      end
    end

    on_get('/api/team_mates/:id', 'Show one team mate') do
      path_params defined: :path_params

      for_code 200, expect_one: :team_mate do |url|
        test_response_of url, path_params: { id: team_mate.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/team_mates', 'Create new team mate') do
      request_params defined: :create_payload

      let(:team_mate_attributes) { FactoryBot.build(:team_mate, map: owned_map).attributes }

      for_code 201, expect_one:  :team_mate do |url|
        test_response_of url, payload: team_mate_attributes
      end

      for_code 422, expect_one: :team_mate_form_errors do |url|
        bad_attributes = team_mate_attributes.merge 'user_id' => 'yes'
        test_response_of url, payload: bad_attributes
      end

      for_code 401, expect_one: :error do |url|
        team_mate_attributes = FactoryBot.build(:team_mate, map: other_team_mate.map).attributes
        test_response_of url, payload: team_mate_attributes
      end
    end

    on_patch('/api/team_mates/:id/accept', 'Accepts an invitation and deletes the invitation notification. Manage app state as you want to reflect this.') do
      path_params defined: :path_params
      let(:team_mate) { FactoryBot.create :team_mate, user: signed_in_user }

      for_code 200, expect_one: :team_mate do |url|
        test_response_of url, path_params: { id: team_mate.id }
      end

      for_code 401, expect_one: :error do |url|
        test_response_of url, path_params: { id: other_team_mate.id }
      end
    end

    on_delete('/api/team_mates/:id/refuse', 'Refuses an invitation and deletes the invitation notification. Manage app state as you want to reflect this.') do
      path_params defined: :path_params
      let(:team_mate) { FactoryBot.create :team_mate, user: signed_in_user }

      for_code 204 do |url|
        test_response_of url, path_params: { id: team_mate.id }
      end

      for_code 401, expect_one: :error do |url|
        test_response_of url, path_params: { id: other_team_mate.id }
      end
    end

    on_delete('/api/team_mates/:id/leave', 'Leave a team') do
      path_params defined: :path_params
      let(:team_mate) { FactoryBot.create :team_mate, user: signed_in_user }

      for_code 204 do |url|
        test_response_of url, path_params: { id: team_mate.id }
      end

      for_code 401, expect_one: :error do |url|
        test_response_of url, path_params: { id: other_team_mate.id }
      end
    end

    on_delete('/api/team_mates/:id/remove', 'Removes a team mate') do
      path_params defined: :path_params

      for_code 204 do |url|
        test_response_of url, path_params: { id: team_mate.id }
      end

      for_code 401, expect_one: :error do |url|
        test_response_of url, path_params: { id: other_team_mate.id }
      end
    end
  end
end
