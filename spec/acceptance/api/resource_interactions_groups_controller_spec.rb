# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ResourceInteractionsGroupsController, type: :acceptance do
  resource 'Resource interactions groups', 'Manage resource interactions groups'

  entity :resource_interactions_group,
         id:          { type: :integer, description: 'Entity identifier' },
         name:        { type: :string, description: 'Group name' },
         description: { type: :string, required: false, description: 'Group description' },
         sync_id:     { type: :integer, required: false, description: 'Trusted data source identifier' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  form_errors_entity :resource_interactions_group_form_errors, [:name, :description]

  parameters :create_update_payload,
             name:        { type: :string, required: false, description: 'Group name' },
             description: { type: :string, required: false, description: 'Group description' },
             sync_id:     { type: :integer, required: false, description: 'Trusted data source identifier. Available to admins only' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/resource_interactions_groups', 'List groups') do
      for_code 200, expect_many: :resource_interactions_group do |url|
        FactoryBot.create_list :resource_interactions_group, 2
        test_response_of url
      end
    end

    on_get('/api/resource_interactions_groups/:id', 'Show one group') do
      path_params defined: :path_params

      for_code 200, expect_one: :resource_interactions_group do |url|
        resource_interactions_group = FactoryBot.create :resource_interactions_group
        test_response_of url, path_params: { id: resource_interactions_group.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/resource_interactions_groups', 'Create new group') do
      request_params defined: :create_update_payload

      for_code 201, expect_one: :resource_interactions_group do |url|
        resource_interactions_group_attributes = FactoryBot.build(:resource_interactions_group).attributes
        test_response_of url, payload: resource_interactions_group_attributes
      end

      for_code 422, expect_one: :resource_interactions_group_form_errors do |url|
        test_response_of url, payload: { name: nil }
      end
    end

    on_put('/api/resource_interactions_groups/:id', 'Update a group') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:resource_interactions_group) { FactoryBot.create :resource_interactions_group }

      for_code 200, expect_one: :resource_interactions_group do |url|
        test_response_of url, path_params: { id: resource_interactions_group.id }, payload: { name: 'A group' }
      end

      for_code 422, expect_one: :resource_interactions_group_form_errors do |url|
        test_response_of url, path_params: { id: resource_interactions_group.id }, payload: { name: nil }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/resource_interactions_groups/:id', 'Destroys a group') do
      path_params defined: :path_params
      let(:resource_interactions_group) { FactoryBot.create :resource_interactions_group }

      for_code 204 do |url|
        test_response_of url, path_params: { id: resource_interactions_group.id }
      end
    end
  end
end
