# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::TasksController, type: :acceptance do
  resource 'Tasks', 'Manage tasks'

  subject_fields = {
    subject_id:   { type: :integer, description: 'Subject identifier' },
    subject_type: { type: :string, description: 'Subject type' },
  }

  subject_field = {
    subject: { type: :object, required: false, description: 'Updated subject when task changes updated it' },
  }

  base_task = {
    id:             { type: :integer, description: 'Entity identifier' },
    name:           { type: :string, description: 'Task name' },
    notes:          { type: :string, required: false, description: 'Additional notes' },
    planned_for:    { type: :datetime, required: false, description: 'Date when the task is planned' },
    done_at:        { type: :datetime, required: false, description: 'Date when the task was performed' },
    action:         { type: :string, required: false, description: 'Action to perform on subject when task is done' },
    assignee_id:    { type: :integer, required: false, description: 'Assigned user identifier' },
    comments_count: { type: :integer, description: 'Comments count' },
    created_at:     { type: :datetime, description: 'Creation date' },
    updated_at:     { type: :datetime, description: 'Update date' },
  }.merge(subject_fields)

  entity :task, base_task
  entity :task_with_subject, base_task.merge(subject_field)
  entity :updated_subject_status, subject_fields.merge(subject_field)

  form_errors_entity :task_form_errors, [:name, :notes, :planned_for, :done_at, :subject_id, :subject_type, :assignee_id]

  parameters :create_payload,
             name:         { type: :string, description: 'Task name' },
             notes:        { type: :string, description: 'Additional notes' },
             planned_for:  { type: :datetime, required: false, description: 'Date when the task is planned (required if task is not done)' },
             done_at:      { type: :datetime, required: false, description: 'Date when the task was performed (required if task is not planned)' },
             subject_id:   { type: :integer, description: 'Subject identifier' },
             subject_type: { type: :string, description: 'Subject type' },
             assignee_id:  { type: :string, required: false, description: 'Assigned user identifier' }
  parameters :update_payload,
             notes:       { type: :string, description: 'Additional notes' },
             planned_for: { type: :datetime, required: false, description: 'Date when the task is planned (required if task is not done)' },
             done_at:     { type: :datetime, required: false, description: 'Date when the task was performed (required if task is not planned)' },
             assignee_id: { type: :string, description: 'Assigned user identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_patch) { FactoryBot.create :patch, :with_element, map: owned_map }
    let(:owned_path) { FactoryBot.create :path, map: owned_map }
    let(:owned_task) { FactoryBot.create :task, subject: owned_patch.elements.first }

    describe 'lists' do
      before do
        FactoryBot.create_list :task, 2, subject: owned_patch.elements.first
        FactoryBot.create_list :task, 2, subject: owned_map
        FactoryBot.create_list :task, 2, subject: owned_patch
        FactoryBot.create_list :task, 2, subject: owned_path
      end

      index_base_path_params = {
        'filters[status]' => { type: :string, required: false, description: 'Status, either `pending`, `overdue` or `done`' },
      }

      on_get('/api/elements/:element_id/tasks', 'List tasks for an element') do
        path_params fields: index_base_path_params.merge({
                                                           element_id: { type: :integer, description: 'Target element identifier' },
                                                         })

        for_code 200, expect_many: :task do |url|
          test_response_of url, path_params: { element_id: owned_patch.elements.first.id }
        end
      end

      on_get('/api/maps/:map_id/tasks', 'List tasks for a given map') do
        path_params fields: index_base_path_params.merge({
                                                           map_id: { type: :integer, description: 'Target map identifier' },
                                                         })

        for_code 200, expect_many: :task do |url|
          test_response_of url, path_params: { map_id: owned_map.id }
        end
      end

      on_get('/api/patches/:patch_id/tasks', 'List tasks for a given patch') do
        path_params fields: index_base_path_params.merge({
                                                           patch_id: { type: :integer, description: 'Target patch identifier' },
                                                         })

        for_code 200, expect_many: :task do |url|
          test_response_of url, path_params: { patch_id: owned_patch.id }
        end
      end

      on_get('/api/paths/:path_id/tasks', 'List tasks for a given path') do
        path_params fields: index_base_path_params.merge({
                                                           path_id: { type: :integer, description: 'Target path identifier' },
                                                         })

        for_code 200, expect_many: :task do |url|
          test_response_of url, path_params: { path_id: owned_path.id }
        end
      end

      on_get('/api/maps/:map_id/all_tasks', 'List tasks for a map and its elements, patches and paths') do
        path_params fields: index_base_path_params.merge({
                                                           map_id: { type: :integer, description: 'Target map identifier' },
                                                         })

        for_code 200, expect_many: :task do |url|
          test_response_of url, path_params: { map_id: owned_map.id }
        end
      end

      on_get('/api/maps/:map_id/tasks/names', 'List task names used on a map') do
        path_params fields: index_base_path_params.merge({
                                                           map_id: { type: :integer, description: 'Target map identifier' },
                                                         })

        for_code 200, expect_many: :string do |url|
          test_response_of url, path_params: { map_id: owned_map.id }
        end
      end
    end

    on_get('/api/tasks/:id', 'Show one task') do
      path_params defined: :path_params

      for_code 200, expect_one: :task do |url|
        test_response_of url, path_params: { id: owned_task.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/tasks', 'Create new task') do
      request_params defined: :create_payload
      let(:task_attributes) do
        FactoryBot.build(:task, subject: owned_patch.elements.first).attributes
      end

      for_code 201, expect_one: :task_with_subject do |url|
        test_response_of url, payload: task_attributes
      end

      for_code 422, expect_one: :task_form_errors do |url|
        bad_attributes = task_attributes.merge 'name' => ''
        test_response_of url, payload: bad_attributes
      end
    end

    on_put('/api/tasks/:id', 'Update an task') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200, expect_one: :task_with_subject do |url|
        test_response_of url, path_params: { id: owned_task.id }, payload: { name: 'New name' }
      end

      for_code 422, expect_one: :task_form_errors do |url|
        test_response_of url, path_params: { id: owned_task.id }, payload: { planned_for: nil, done_at: nil }
      end
    end

    on_delete('/api/tasks/:id', 'Destroys an task') do
      path_params defined: :path_params

      for_code 200, expect_one: :updated_subject_status do |url|
        test_response_of url, path_params: { id: owned_task.id }
      end
    end
  end
end
