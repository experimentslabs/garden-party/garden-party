# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::FamiliesController, type: :acceptance do
  resource 'Families', 'Manage families'

  entity :family,
         id:         { type: :integer, description: 'Entity identifier' },
         name:       { type: :string, description: 'Family name' },
         kingdom:    { type: :string, description: 'Family kingdom' },
         source:     { type: :string, required: false, description: 'Link to description source' },
         sync_id:    { type: :integer, required: false, description: 'Trusted data source identifier' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Update date' }

  form_errors_entity :family_form_errors, [:name, :kingdom, :source]

  parameters :create_update_payload,
             kingdom: { type: :string, description: 'Kingdom. Can be "plant" or "animal"' },
             name:    { type: :string, description: 'Family name' },
             source:  { type: :string, description: 'URL to a better description' },
             sync_id: { type: :integer, required: false, description: 'Trusted data source identifier. Available to admins only' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/families', 'List families') do
      for_code 200, expect_many:  :family do |url|
        FactoryBot.create_list :family, 2
        test_response_of url
      end
    end

    on_get('/api/families/:id', 'Show one family') do
      path_params defined: :path_params

      for_code 200, expect_one: :family do |url|
        family = FactoryBot.create :family
        test_response_of url, path_params: { id: family.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/families', 'Create new family') do
      request_params defined: :create_update_payload

      for_code 201, expect_one: :family do |url|
        family_attributes = FactoryBot.build(:family).attributes
        test_response_of url, payload: family_attributes
      end

      for_code 422, expect_one: :family_form_errors do |url|
        test_response_of url, payload: { name: '' }
      end
    end

    on_put('/api/families/:id', 'Update a family') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:family) { FactoryBot.create :family }

      for_code 200, expect_one: :family do |url|
        test_response_of url, path_params: { id: family.id }, payload: { name: 'New name' }
      end

      for_code 422, expect_one: :family_form_errors do |url|
        test_response_of url, path_params: { id: family.id }, payload: { name: '' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/families/:id', 'Destroys a family') do
      path_params defined: :path_params
      let(:family) { FactoryBot.create :family }

      for_code 204 do |url|
        test_response_of url, path_params: { id: family.id }
      end
    end
  end
end
