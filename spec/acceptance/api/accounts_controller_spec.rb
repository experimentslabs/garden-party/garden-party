# frozen_string_literal: true

require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::AccountsController, type: :acceptance do
  resource 'Account', 'Manage account information'

  entity :preferences,
         map:                 { type: :integer, required: false, description: 'Favorite map identifier' },
         theme:               { type: :string, required: false, description: 'Application theme. Null is for browser/system theme' },
         email_notifications: { type: :object, description: 'Notification preferences per notification type', attributes: {
           new_comment_notification:      { type: :boolean, description: 'Whether to send email on NewCommentNotification type' },
           system_notification:           { type: :boolean, description: 'Whether to send email on SystemNotification type. Cannot be changed' },
           team_mate_invite_notification: { type: :boolean, description: 'Whether to send email on TeamMateInviteNotification type.' },
           team_mate_leave_notification:  { type: :boolean, description: 'Whether to send email on TeamMateLeaveNotification type.' },
           team_mate_refuse_notification: { type: :boolean, description: 'Whether to send email on TeamMateRefuseNotification type.' },
           team_mate_remove_notification: { type: :boolean, description: 'Whether to send email on TeamMateRemoveNotification type.' },
         } }

  entity :user_account,
         id:          { type: :integer, description: 'User identifier' },
         role:        { type: :string, description: 'User role (user or admin)' },
         email:       { type: :string, description: 'Email address' },
         username:    { type: :string, description: 'Username' },
         preferences: { type: :object, description: 'Preferences', attributes: :preferences },
         created_at:  { type: :datetime, description: 'Account creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  # We only support preference edition for now
  form_errors_entity :user_account_form_errors, [:map, :theme]

  parameters :update_account_payload,
             preferences: { type: :object, description: 'New user preferences', attributes: {
               map:                 { type: :integer, required: false, description: 'The new value, or unchanged one. Should be present anyway and `null` is supported.' },
               theme:               { type: :string, required: false, description: 'The new value, or unchanged one. Should be present anyway and `null` is supported.' },
               email_notifications: { type: :object, required: false, description: 'Notification preferences per notification type', attributes: {
                 system_notification:           { type: :boolean, required: false, description: 'Whether to send email on SystemNotification type. Cannot be changed' },
                 team_mate_invite_notification: { type: :boolean, required: false, description: 'Whether to send email on TeamMateInviteNotification type.' },
                 team_mate_leave_notification:  { type: :boolean, required: false, description: 'Whether to send email on TeamMateLeaveNotification type.' },
                 team_mate_refuse_notification: { type: :boolean, required: false, description: 'Whether to send email on TeamMateRefuseNotification type.' },
                 team_mate_remove_notification: { type: :boolean, required: false, description: 'Whether to send email on TeamMateRemoveNotification type.' },
               } },
             } }

  on_get('/api/account', 'Display account information') do
    path_params fields: {
      fail_on_error: { type: :boolean, required: false, description: 'Whether to return a 401 or a 204 when user is not signed in (default: `true`)' },
    }

    for_code 200, expect_one: :user_account do |url|
      sign_in FactoryBot.create :user
      test_response_of url
    end

    for_code 204 do |url|
      test_response_of url, path_params: { fail_on_error: false }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end

  on_put('/api/account', 'Update account information') do
    request_params defined: :update_account_payload

    for_code 200, expect_one: :user_account do |url|
      sign_in FactoryBot.create :user
      test_response_of url, payload: { preferences: { theme: 'light', map: nil } }
    end
  end
end
