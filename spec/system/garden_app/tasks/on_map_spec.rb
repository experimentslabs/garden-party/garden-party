# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Tasks management: on map', type: :system do
  let(:map) { FactoryBot.create :map }

  before do
    login map.user
    visit_map map
  end

  it 'can manage tasks' do
    # Create
    click_on I18n.t('js.layouts.garden.info.add_map_task')
    fill_in_task_with name: 'Maw the lawn'
    expect { click_on I18n.t('generic.save') }.to (change { map.tasks.count }).by 1

    # List
    click_on I18n.t('js.layouts.garden.toolbar.todo')
    expect(page).to have_css('.gpa-task', text: 'Maw the lawn')

    # Edit
    new_name = 'Maw the lawn in the backyard'
    within('.gpa-task') do
      click_on I18n.t('generic.edit')
    end
    fill_in I18n.t('activerecord.attributes.task.name'), with: new_name
    expect { click_on I18n.t('generic.save') }.to(change { map.tasks.last.name })
    expect(page).to have_css('.gpa-task', text: new_name)

    # Destroy
    within('.gpa-task') do
      click_on I18n.t('generic.destroy')
    end
    expect { accept_confirmation_modal }.to (change { map.tasks.count }).by(-1)
    expect(page).to have_no_content new_name
  end
end
