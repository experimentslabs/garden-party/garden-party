# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Tasks management: general tasks behavior', type: :system do
  let(:map) { FactoryBot.create :map }

  before do
    [
      [map, 4.days.from_now],
      [FactoryBot.create(:patch, map: map, name: 'Greenies'), 1.day.from_now],
      [FactoryBot.create(:element, :point, layer: map.layers.first), Time.current],
      [FactoryBot.create(:path, map: map, layer: map.layers.first), 2.days.ago],
    ].each { |pair| FactoryBot.create :task, :planned, subject: pair[0], planned_for: pair[1] }
    FactoryBot.create :task, :done, subject: map

    login map.user
    visit_map_tasks map
  end

  it 'can manage tasks' do
    # List
    expect(find_all('.gpa-task').count).to eq 4
    within('.gpa-tasks__day--overdue') do
      expect(find_all('.gpa-task').count).to eq 1
    end

    within('.gpa-tasks__day--today') do
      expect(find_all('.gpa-task').count).to eq 1
    end

    # Edit
    within('.gpa-tasks__day--overdue') do
      click_on I18n.t('generic.edit')
      fill_in I18n.t('activerecord.attributes.task.name'), with: 'Mow the lawn'
      click_on I18n.t('generic.save')
      expect(current_scope).to have_content 'Mow the lawn'
    end

    # Destroy
    within('.gpa-tasks__day--overdue') do
      click_on I18n.t('generic.destroy')
    end
    accept_confirmation_modal
    expect(page).to have_no_content 'Mow the lawn'

    # Finish
    within('.gpa-tasks__day--today') do
      click_on I18n.t('js.elements.last_action.finish')
    end
    # Task is still visible
    expect(find_all('.gpa-task').count).to eq 3
    expect(find_all('.gpa-task.gpa-task--done').count).to eq 1

    # Display finished tasks
    check I18n.t('js.tasks.show_done')
    expect(find_all('.gpa-task').count).to eq 4
    expect(find_all('.gpa-task.gpa-task--done').count).to eq 2
  end
end
