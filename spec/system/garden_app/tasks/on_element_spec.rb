# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Task management: on element', type: :system do
  let(:map) { FactoryBot.create :map }
  let(:patch) { FactoryBot.create :patch, map: map, name: 'The patch' }
  let(:element) { FactoryBot.create :element, patch: patch }

  before do
    login map.user
    visit_map_inventory_patch_element element
    within('.gpa-inventory.gpa-inventory--patches > .gpa-inventory__view') do
      click_on I18n.t('js.generic.tasks')
    end
  end

  it_behaves_like 'a task subject' do
    subject { element }
  end
end
