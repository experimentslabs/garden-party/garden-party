# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Inventory: general behavior', type: :system do
  before do
    map = FactoryBot.create :map
    login map.user
    visit_map_inventory map
  end

  it 'displays patches by default' do
    expect(page).to have_css '.rui-tabs > .rui-button.rui-button--active', text: I18n.t('js.inventory.panel.patches')
  end
end
