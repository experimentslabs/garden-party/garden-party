# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Observation management: on map', type: :system do
  let(:map) { FactoryBot.create :map }

  before do
    login map.user
    FactoryBot.create :observation, subject: map

    visit_map map
  end

  # NOTE: picture management is tested in element observations (./on_element_spec.rb)
  it 'can manage observations' do
    # Create
    click_on I18n.t('js.layouts.garden.info.add_map_observation')
    fill_in_observation_with title: 'Something happened'
    expect { click_on I18n.t('generic.save') }.to (change { map.observations.count }).by 1

    # List
    click_on I18n.t('js.elements.action_buttons.show_observations')
    expect(page).to have_css('.gp-observation__header__main', text: 'Something happened')

    # Edit
    new_title = 'That was ok'
    within('.gp-observation:first-of-type') do
      click_on I18n.t('generic.edit')
    end
    fill_in I18n.t('activerecord.attributes.observation.title'), with: new_title
    expect { click_on I18n.t('generic.save') }.to(change { map.observations.last.title })
    expect(page).to have_css('.gp-observation__header__main', text: new_title)

    # Destroy
    within('.gp-observation:first-of-type') do
      click_on I18n.t('generic.destroy')
    end
    expect { accept_confirmation_modal I18n.t('rui.confirm_dialog.are_you_sure') }.to (change { map.observations.count }).by(-1)
    expect(page).to have_no_content new_title
  end
end
