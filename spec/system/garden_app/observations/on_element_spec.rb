# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Observation management: on element', type: :system do
  let(:map) { FactoryBot.create :map }
  let(:patch) { FactoryBot.create :patch, map: map, name: 'The patch' }
  let(:element) { FactoryBot.create :element, patch: patch }

  before do
    login map.user
    visit_map_inventory_patch_element element
  end

  it 'handles pictures' do
    within('.gpa-inventory.gpa-inventory--patches > .gpa-inventory__view') do
      click_on I18n.t('js.elements.observations.new_observation')
    end

    # Fill the form
    within '.rui-modal' do
      fill_in I18n.t('activerecord.attributes.observation.title'), with: 'It speaks!'
      fill_in I18n.t('activerecord.attributes.observation.content'), with: 'It even said "Hello world"'
      attach_file I18n.t('activerecord.attributes.observation.pictures'), [
        Rails.root.join('spec', 'fixtures', 'files', 'observation_upload.png'),
        Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'),
      ]
      # Well...
      sleep 1
      expect(find_all('._thumbnail').count).to eq 2

      within '.rui-thumbnails' do
        find('._thumbnail:first-child button.rui-button--destructive').click
      end
      expect(find_all('._thumbnail').count).to eq 1

      click_on I18n.t('generic.save')
    end

    # An observation has a picture
    find('summary', text: 'It speaks!').click
    thumbnails = find_all('._thumbnail')
    expect(thumbnails.count).to eq 1

    # Display a picture
    thumbnails.first.click
    expect(page).to have_css '.rui-image-viewer__image'
  end

  it_behaves_like 'an observation subject' do
    subject { element }
  end
end
