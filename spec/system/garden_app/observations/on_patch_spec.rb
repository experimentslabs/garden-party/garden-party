# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Observation management: on patch', type: :system do
  let(:map) { FactoryBot.create :map }
  let(:patch) { FactoryBot.create :patch, map: map, name: 'Greenies' }

  before do
    login map.user
    visit_map_inventory_patch patch
  end

  # NOTE: picture management is tested in element observations (./on_element_spec.rb)
  it_behaves_like 'an observation subject' do
    subject { patch }
  end
end
