# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Timeline' do
  # Background:
  #   Given I have an account and i'm logged in
  #   And I have a map named "My garden"
  #
  #   And I have a patch named "Nice one" with some "Pipe-weed" in it
  #   And I have a tree named "Tally McShadow"
  #   And There is a finished a task on the "My garden" map
  #   Given I have observed the element "Tally McShadow" 2 times
  #   And I have observed the patch "Nice one" 1 time with picture
  #   And I access the timeline for "My garden"
  #
  # Scenario: Display observations timeline
  #   Then I see 3 observations
  #
  # Scenario: Display picture on observations timeline
  #   When I display the first picture in the timeline
  #   Then I see the first picture in full screen.
  #
  # Scenario: Display tasks
  #   When I display the tasks in the timeline
  #   Then I see 1 task

  let(:map) { FactoryBot.create :map, :user_submitted_png }
  let(:element) { FactoryBot.create :element, :point, layer: map.layers.first }

  before do
    patch = FactoryBot.create :patch, map: map
    path = FactoryBot.create :path, map: map, layer: map.layers.first
    FactoryBot.create :observation, subject: map
    FactoryBot.create :observation, :with_pictures, subject: element
    FactoryBot.create :observation, subject: patch
    FactoryBot.create :observation, subject: path
    FactoryBot.create :task, subject: map

    login map.user
    visit_map_timeline map
  end

  it 'displays an interactive timeline' do
    # Check for quantities
    expect(find_all('.gp-timeline-entry').count).to eq 4

    # Display a picture
    find('._thumbnail').click
    expect(page).to have_css '.rui-image-viewer__image'
    click_on I18n.t('rui.generic.close')

    check I18n.t('js.timeline.display_tasks')
    expect(find_all('.gp-timeline-entry').count).to eq 5
  end
end
