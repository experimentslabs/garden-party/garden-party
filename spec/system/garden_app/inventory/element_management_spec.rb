# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Inventory: element management', type: :system do
  let(:map) { FactoryBot.create :map }
  let!(:element) { FactoryBot.create :element, :point, map: map, layer: map.layers.first }

  before do
    login map.user
    visit_map_inventory map
    click_on I18n.t('js.inventory.panel.elements')
    click_on element.display_name
  end

  it 'shows an elements details' do
    expect(page).to have_css 'h2', text: element.display_name
  end

  it 'can rename an element' do
    within '.gpa-inventory__view' do
      open_dropdown
      click_on I18n.t('generic.edit')
    end

    fill_in I18n.t('activerecord.attributes.element.name'), with: 'New name'

    expect { click_on I18n.t('generic.save') }.to(change { element.reload.display_name })
    expect(page).to have_content "New name (#{element.resource.name})"
  end

  it 'can destroy an element' do
    within '.gpa-inventory__view' do
      open_dropdown
      click_on I18n.t('js.elements.action_buttons.destroy_element')
    end

    expect { accept_confirmation_modal }.to change(Element, :count).by(-1)
    expect(page).to have_no_content element.display_name
    expect(page).to have_content I18n.t('js.inventory.please_select_an_entry')
  end

  it 'can duplicate the element' do
    expect { click_on I18n.t('js.elements.action_buttons.duplicate') }.to change(Element, :count).by 1

    expect(page).to have_content "#{I18n.t('models.element.duplicate_name_suffix', name: element.resource.name)} (#{element.resource.name})"
  end

  describe 'implanting an element' do
    it 'sets the implantation date now' do
      click_on I18n.t('js.elements.action_buttons.implant')

      choose I18n.t('activerecord.attributes.element/implantation_mode.soil_sowing')
      click_on I18n.t('generic.save')

      expect(page).to have_content I18n.t('activerecord.attributes.element.implanted_at')
      expect(page).to have_content I18n.t('activerecord.attributes.element/implantation_mode.soil_sowing')

      element.reload
      expect(element.implanted_at).not_to be_nil
    end

    it 'sets the implantation date in the future' do
      click_on I18n.t('js.elements.action_buttons.implant')

      fill_in I18n.t('js.elements.action_buttons.plantation_date'), with: 2.days.from_now
      choose I18n.t('activerecord.attributes.element/implantation_mode.soil_sowing')
      click_on I18n.t('generic.save')

      expect(page).to have_content I18n.t('js.elements.item_content.implantation_planned_for')
      expect(page).to have_content I18n.t('activerecord.attributes.element/implantation_mode.soil_sowing')

      element.reload
      expect(element.implantation_planned_for).not_to be_nil
    end

    context 'when element implantation is already planned' do
      let!(:element) { FactoryBot.create :element, :point, :implantation_planned, map: map, layer: map.layers.first }

      it 'validates the planned implantation date' do
        click_on I18n.t('js.elements.item_content.implant_now')

        expect(page).to have_content I18n.t('activerecord.attributes.element.implanted_at')

        element.reload
        expect(element.implantation_planned_for).not_to be_nil
      end
    end
  end

  describe 'removing an element' do
    let!(:element) { FactoryBot.create :element, :point, :implanted, map: map, layer: map.layers.first }

    it 'sets the removal date now' do
      click_on I18n.t('js.elements.action_buttons.remove')
      click_on I18n.t('generic.save')

      expect(page).to have_content I18n.t('activerecord.attributes.element.removed_at')

      element.reload
      expect(element.removed_at).not_to be_nil
    end

    it 'sets the removal date in the future' do
      click_on I18n.t('js.elements.action_buttons.remove')

      fill_in I18n.t('js.elements.action_buttons.removal_date'), with: 2.days.from_now
      click_on I18n.t('generic.save')

      expect(page).to have_content I18n.t('js.elements.item_content.removal_planned_for')

      element.reload
      expect(element.removal_planned_for).not_to be_nil
    end

    context 'when element removal is already planned' do
      let!(:element) { FactoryBot.create :element, :point, :removal_planned, map: map, layer: map.layers.first }

      it 'validates the planned removal date' do
        click_on I18n.t('js.elements.item_content.remove_now')

        expect(page).to have_content I18n.t('activerecord.attributes.element.removed_at')

        element.reload
        expect(element.removal_planned_for).not_to be_nil
      end
    end
  end

  describe 'on removed elements' do
    let!(:removed_elements) do
      [
        # 2 year old
        FactoryBot.create(:element, :point, map: map, layer: map.layers.first, implanted_at: 3.years.ago, removed_at: 2.years.ago),
        FactoryBot.create(:element, :point, map: map, layer: map.layers.first, implanted_at: 3.years.ago, removed_at: 2.years.ago),
        # last year
        FactoryBot.create(:element, :point, map: map, layer: map.layers.first, implanted_at: 3.years.ago, removed_at: 1.year.ago),
      ]
    end

    it 'groups removed elements by year in collapsible blocks' do
      # Removed elements are created too late
      reload_page

      last_year = 1.year.ago.year.to_s
      before_last_year = 2.years.ago.year.to_s

      expect(page).to have_content I18n.t('js.inventory.removed_elements')

      within '.gpa-inventory__panel' do
        expect(page).to have_css('details', text: last_year)
        expect(page).to have_css('details', text: before_last_year)

        find('details', text: before_last_year).click
        within '.rui-collapsible-list-item', text: before_last_year do
          expect(current_scope).to have_link removed_elements[0].display_name
          expect(current_scope).to have_link removed_elements[1].display_name
        end

        expect(current_scope).to have_no_link removed_elements.last.display_name
        find('details', text: last_year).click
        within '.rui-collapsible-list-item', text: last_year do
          expect(current_scope).to have_link removed_elements.last.display_name
        end

        click_on removed_elements.last.display_name
      end

      expect(page).to have_css 'h2', text: removed_elements.last.display_name
    end
  end
end
