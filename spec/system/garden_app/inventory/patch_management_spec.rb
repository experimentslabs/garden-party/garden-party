# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Inventory management', type: :system do
  let(:map) { FactoryBot.create :map }
  let!(:patch) { FactoryBot.create :patch, map: map, name: 'My patch' }

  before do
    login map.user
    visit_map_inventory map
    click_on patch.name
  end

  it 'displays patch details' do
    expect(page).to have_css 'h2', text: patch.name
  end

  it 'can rename a patch' do
    within '.gpa-inventory:not(.gpa-inventory--patches) > .gpa-inventory__view' do
      open_dropdown
      click_on I18n.t('generic.edit')
    end

    fill_in I18n.t('activerecord.attributes.element.name'), with: 'New name'

    expect { click_on I18n.t('generic.save') }.to(change { patch.reload.name })
    expect(page).to have_css 'h2', text: 'New name'
  end

  it 'can destroy a patch' do
    within '.gpa-inventory:not(.gpa-inventory--patches) > .gpa-inventory__view' do
      open_dropdown
      click_on I18n.t('generic.destroy')
    end

    expect { accept_confirmation_modal }.to change(Patch, :count).by(-1)
    expect(page).to have_no_content patch.name
  end
end
