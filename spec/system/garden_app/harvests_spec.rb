# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Harvests', type: :system do
  let(:user) { FactoryBot.create :user }
  let(:map) { FactoryBot.create :map, :user_submitted_png, user: user }
  let!(:element) { FactoryBot.create :element, :point, map: map }
  # JS translations plurals are separated by a pipe
  let(:piece_strings) { I18n.t('js.unit.piece').split('|').map(&:strip) }

  before do
    login user
  end

  describe 'access harvests list' do
    before do
      visit_map_inventory_element_harvests element
    end

    context 'with records' do
      before do
        FactoryBot.create_list :harvest, 3, quantity: 2, unit: Harvest.units[:piece], element: element
      end

      it('displays the list of records') do
        amount = find_all('tr', text: "2 #{piece_strings[1]}").count
        expect(amount).to eq 3
      end
    end

    context 'without records' do
      it 'displays a message' do
        expect(page).to have_content I18n.t('js.harvests.element_harvests.no_record_yet')
      end
    end
  end

  describe 'create record' do
    before do
      visit_map_inventory_element_harvests element

      within('.gpa-inventory__view') do
        click_on I18n.t('js.harvests.element_harvests.new_harvest'), class: 'rui-button--creative'
      end
    end

    context 'with valid data' do
      it 'creates a new record' do
        fill_in I18n.t('activerecord.attributes.harvest.quantity'), with: 1
        select piece_strings[0], from: I18n.t('activerecord.attributes.harvest.unit')

        aggregate_failures do
          expect { click_on I18n.t('generic.save') }.to change(Harvest, :count).by 1
          expect(page).to have_css('tr', text: "1 #{piece_strings[0]}")
        end
      end
    end

    context 'with invalid data' do
      it 'displays an error' do
        fill_in I18n.t('activerecord.attributes.harvest.quantity'), with: 0
        select piece_strings[0], from: I18n.t('activerecord.attributes.harvest.unit')

        aggregate_failures do
          expect { click_on I18n.t('generic.save') }.not_to change(Harvest, :count)
          expect(page).to have_content I18n.t('rui.form_errors.has_errors')
        end
      end
    end
  end

  describe 'update record' do
    context 'with valid data' do
      let!(:harvest) { FactoryBot.create :harvest, element: element, quantity: 1, unit: :piece }

      before do
        visit_map_inventory_element_harvests element
        click_on I18n.t('generic.edit')
      end

      it 'updates the record' do
        fill_in I18n.t('activerecord.attributes.harvest.quantity'), with: 10

        within('.rui-modal form') do
          click_on I18n.t('generic.edit')
        end
        expect(page).to have_css('tr', text: "10 #{piece_strings[0]}")
      end

      it 'displays an error' do
        fill_in I18n.t('activerecord.attributes.harvest.quantity'), with: 0

        aggregate_failures do
          within('.rui-modal form') do
            expect { click_on I18n.t('generic.edit') }.not_to(change { harvest.reload.quantity })
          end
          expect(page).to have_css('tr', text: "1 #{piece_strings[0]}")
          expect(page).to have_content I18n.t('rui.form_errors.has_errors')
        end
      end
    end
  end

  describe 'destroy a record' do
    before do
      FactoryBot.create :harvest, element: element, quantity: 1, unit: :piece
      visit_map_inventory_element_harvests element
    end

    it 'destroys the harvest' do
      click_on I18n.t('generic.destroy')
      aggregate_failures do
        expect { accept_confirmation_modal }.to change(Harvest, :count).by(-1)
        expect(page).to have_content I18n.t('js.harvests.element_harvests.no_record_yet')
      end
    end
  end
end
