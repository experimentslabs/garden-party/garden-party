# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Account management', type: :system do
  let(:user) { FactoryBot.create :user }

  before do
    login user

    click_summary I18n.t('layouts.application.header.account')
    click_on I18n.t('layouts.application.header.calendar_links')
  end

  describe 'agenda link' do
    example 'create the link for the first time' do
      expect(page).to have_no_css "input[value*='?token=#{user.reload.calendar_token}']"

      click_on I18n.t('account.calendars.edit.generate_token')

      expect(page).to have_content I18n.t('account.refresh_calendar_token.success')
      expect(page).to have_css "input[value*='?token=#{user.reload.calendar_token}']"
    end

    example 'renew an agenda link' do
      user.rotate_calendar_token!
      first_token = user.calendar_token
      reload_page
      expect(page).to have_css "input[value*='?token=#{user.reload.calendar_token}']"

      click_on I18n.t('account.calendars.edit.renew')
      accept_browser_alert

      expect(page).to have_content I18n.t('account.refresh_calendar_token.success')
      expect(page).to have_css "input[value*='?token=#{user.reload.calendar_token}']"
      expect(user.calendar_token).not_to eq first_token
    end

    example 'destroy an agenda link' do
      user.rotate_calendar_token!
      reload_page
      expect(page).to have_css "input[value*='?token=#{user.reload.calendar_token}']"

      click_on I18n.t('generic.destroy')
      accept_browser_alert

      expect(page).to have_content I18n.t('account.destroy_calendar_token.success')
      expect(page).to have_no_css "input[value*='?token=#{user.reload.calendar_token}']"
    end
  end
end
