# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Account management', type: :system do
  let(:user) { FactoryBot.create :user }
  let(:map) { FactoryBot.create :map, :user_submitted_png, user: user }
  let(:element) { FactoryBot.create :element, :in_patch, patch: FactoryBot.create(:patch, map: map) }

  before do
    element

    login user

    click_summary I18n.t('layouts.application.header.account')
    click_on I18n.t('layouts.application.header.password_and_preferences')
  end

  describe 'account changes' do
    example 'changing the password' do
      fill_in I18n.t('activerecord.attributes.user.password'), with: '44b0883cc7322822'
      fill_in I18n.t('activerecord.attributes.user.password_confirmation'), with: '44b0883cc7322822'
      fill_in I18n.t('activerecord.attributes.user.current_password'), with: 'password'

      expect { click_on I18n.t('generic.save') }.to(change { user.reload.encrypted_password })

      expect(page).to have_content I18n.t('devise.registrations.updated')
    end

    example 'changing email' do
      fill_in I18n.t('activerecord.attributes.user.email'), with: 'my_new@email.com'
      fill_in I18n.t('activerecord.attributes.user.current_password'), with: 'password'
      click_on I18n.t('generic.save')
      expect(user.reload.email).to eq 'my_new@email.com'
    end
  end

  describe 'account destruction' do
    example 'deleting account' do
      # Expecting a change with expect.to change... is too quick and fails so we track the count manually
      user_count = User.count

      click_summary I18n.t('devise.registrations.edit.cancel_my_account')

      within 'form', text: I18n.t('devise.registrations.edit.cancel_my_account') do
        fill_in I18n.t('activerecord.attributes.user.current_password'), with: 'password'
        click_on I18n.t('devise.registrations.edit.cancel_my_account')
      end
      accept_browser_alert

      expect(page).to have_content I18n.t('devise.registrations.destroy.success')
      expect(User.count).to eq(user_count - 1)
    end
  end

  describe 'app preferences' do
    before do
      click_on I18n.t('layouts.account.preferences.application')
    end

    example 'changing the theme' do
      select I18n.t('theme.dark'), from: I18n.t('activerecord.attributes.user.theme')

      expect { click_on I18n.t('generic.save') }.to(change { user.reload.preferences['theme'] })

      expect(find_all('body.rui-theme--dark').count).to eq 1
    end
  end
end
