# frozen_string_literal: true

FactoryBot.define do
  factory :element do
    resource
    patch

    # Used to create "point elements"; map will be used to determine layer
    transient do
      map { create :map } # rubocop:disable FactoryBot/FactoryAssociationWithStrategy
    end

    trait :in_patch do
      patch
      layer { nil }
      geometry { nil }
    end

    trait :implantation_planned do
      implantation_planned_for { 1.day.from_now }
    end

    trait :implanted do
      implanted_at { 1.day.ago }
    end

    trait :removal_planned do
      implanted
      removal_planned_for { 1.day.from_now }
    end

    trait :removed do
      implanted
      removed_at { Date.current }
    end

    trait :point do
      patch { nil }
      layer { map.layers.first }
      geometry { { type: 'Feature', properties: nil, geometry: { type: 'Point', coordinates: [rand(100), rand(100)] } } }
    end

    trait :in_patch_factory do
      patch { nil }
    end
  end
end
