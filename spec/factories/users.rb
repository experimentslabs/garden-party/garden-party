# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    password { 'password' }
    username { Faker::Internet.unique.username separators: %w[- _], specifier: 3 }

    factory :user_known do
      email { 'user@example.com' }
    end

    factory :user_admin do
      role { 'admin' }

      factory :user_admin_known do
        email { 'admin@example.com' }
      end
    end

    trait :with_calendar_token do
      calendar_token { Faker::Crypto.unique.md5 }
    end
  end
end
