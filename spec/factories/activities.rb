# frozen_string_literal: true

FactoryBot.define do
  factory :activity do
    action { 'create' }
    user

    # Default trait
    for_observation

    trait :for_observation do
      subject factory: [:observation]
    end
  end
end
