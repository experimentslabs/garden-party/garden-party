# frozen_string_literal: true

FactoryBot.define do
  factory :family do
    name { Faker::Lorem.unique.word }
    # Default trait
    plant

    trait :plant do
      kingdom { 'plant' }
    end

    trait :animal do
      kingdom { 'animal' }
    end
  end
end
