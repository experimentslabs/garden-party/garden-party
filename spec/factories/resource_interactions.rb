# frozen_string_literal: true

FactoryBot.define do
  factory :resource_interaction do
    subject factory: [:resource_interactions_group]
    target factory: [:resource_interactions_group]
    nature { 0 }
  end
end
