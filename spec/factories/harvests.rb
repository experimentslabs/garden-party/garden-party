# frozen_string_literal: true

FactoryBot.define do
  factory :harvest do
    harvested_at { Time.current }
    quantity { Faker::Number.decimal l_digits: 2, r_digits: 2 }
    unit { Harvest.units.keys.sample }

    element factory: [:element, :point]
    user
  end
end
