# frozen_string_literal: true

FactoryBot.define do
  factory :genus do
    name { Faker::Lorem.unique.word.titleize }
    family
  end
end
