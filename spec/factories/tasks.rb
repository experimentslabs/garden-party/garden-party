# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    name { Faker::Verb.ing_form }
    # Traits
    for_element
    done

    trait :done do
      done_at { Time.current }
    end

    trait :planned do
      planned_for { 1.day.from_now }
      done_at { nil }
    end

    trait :for_element do
      subject factory: [:element, :point]
    end

    trait :for_patch do
      subject factory: [:patch]
    end

    trait :for_map do
      subject factory: [:map]
    end

    trait :for_path do
      subject factory: [:path]
    end
  end
end
