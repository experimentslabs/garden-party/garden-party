# frozen_string_literal: true

FactoryBot.define do
  factory :observation do
    title { Faker::Lorem.sentence }
    content { Faker::Lorem.paragraph }
    user
    # Default traits
    for_element

    trait :for_element do
      subject factory: [:element, :point]
    end

    trait :for_patch do
      subject factory: [:patch]
    end

    trait :with_pictures do
      pictures do
        [
          Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'observation_upload.png'), 'image/png'),
        ]
      end
    end
  end
end
