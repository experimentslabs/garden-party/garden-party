# frozen_string_literal: true

FactoryBot.define do
  factory :resource_note do
    content { Faker::Lorem.paragraph }
    user
    resource
  end
end
