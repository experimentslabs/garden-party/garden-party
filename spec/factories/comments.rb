# frozen_string_literal: true

FactoryBot.define do
  factory :comment do
    content { Faker::Lorem.paragraph }
    user
    # Default trait
    for_observation

    trait :removed do
      removed_at { Time.current }
    end

    trait :for_observation do
      subject factory: :observation
    end

    trait :for_task do
      subject factory: :task
    end
  end
end
