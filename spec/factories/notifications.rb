# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    # Custom initialization for STI support
    initialize_with { type.present? ? type.constantize.new : Notification.new }

    # Default trait
    system_notification

    trait :system_notification do
      type { 'SystemNotification' }
      content { { title: Faker::Lorem.sentence(word_count: 3), message: Faker::Lorem.sentence } }
      recipient factory: [:user]
    end

    trait :invitation do
      transient do
        team_mate { create :team_mate } # rubocop:disable FactoryBot/FactoryAssociationWithStrategy
      end
      type { 'TeamMateInviteNotification' }
      subject { team_mate }

      recipient { team_mate.user }
      sender { team_mate.map.user }
    end
  end
end
