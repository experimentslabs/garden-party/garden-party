# frozen_string_literal: true

FactoryBot.define do
  factory :link do
    title { Faker::Lorem.sentence }
    url { Faker::Internet.url }
    description { Faker::Lorem.paragraph }
    user
    # Default traits
    pending

    trait :approved do
      approved_by factory: [:user_admin]
    end

    trait :pending do
      approved_by_id { nil }
    end
  end
end
