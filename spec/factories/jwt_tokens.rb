# frozen_string_literal: true

FactoryBot.define do
  factory :jwt_token do
    description { Faker::Lorem.sentence }
    expires_at { 1.hour.from_now }
    user
  end
end
