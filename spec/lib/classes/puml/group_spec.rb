# frozen_string_literal: true

require 'rails_helper'

require 'classes/puml/group'

RSpec.describe Puml::Group do
  describe '#to_puml' do
    it 'renders groups' do # rubocop:disable RSpec/ExampleLength
      expected = <<~TXT

        package "G" #AAA;line:AAA {
          object t1
          object t2

          package "SG" #BBB;line:BBB {
            object st1
            object st2

            package "SSG1" #CCC;line:CCC {
              object sst1_1
              object sst1_2
            }

            package "SSG2" #DDD;line:DDD {

            }
          }
        }
      TXT

      # rubocop:disable Naming/VariableNumber
      group = described_class.new('G', color: 'AAA', tables: [:t1, :t2], groups: [
                                    described_class.new('SG', color: 'BBB', tables: [:st1, :st2], groups: [
                                                          described_class.new('SSG1', color: 'CCC', tables: [:sst1_1, :sst1_2]),
                                                          described_class.new('SSG2', color: 'DDD'),
                                                        ]),
                                  ])
      # rubocop:enable Naming/VariableNumber

      expect(group.to_puml).to eq expected
    end
  end
end
