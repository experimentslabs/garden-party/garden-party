# frozen_string_literal: true

require 'rails_helper'

RSpec.describe JwtToken, type: :model do
  let(:user) { FactoryBot.create :user }
  let(:token) { FactoryBot.create(:jwt_token, user: user, expires_at: expiration_date).token }

  describe '.user_from' do
    context 'with a valid token' do
      let(:expiration_date) { 1.hour.from_now }

      it 'returns the user' do
        expect(described_class.user_from(token)).to eq user
      end
    end

    context 'with an expired token' do
      let(:expiration_date) { 1.hour.ago }

      it 'returns nil' do
        expect(described_class.user_from(token)).to be_nil
      end
    end

    context 'with an invalid token' do
      it 'returns nil' do
        expect(described_class.user_from('abcd')).to be_nil
      end
    end
  end

  describe 'scopes' do
    describe '.still_valid' do
      it 'returns the right values' do
        FactoryBot.create :jwt_token, user: user, expires_at: 1.hour.ago
        FactoryBot.create_list :jwt_token, 2, user: user, expires_at: 1.hour.from_now

        expect(described_class.still_valid.count).to eq 2
      end
    end
  end

  describe '#expired?' do
    let(:token) { described_class.new expires_at: expiration_date }

    context 'when expiration date is in the future' do
      let(:expiration_date) { 1.hour.from_now }

      it 'returns false' do
        expect(token.expired?).to be false
      end
    end

    context 'when expiration date is over' do
      let(:expiration_date) { 1.hour.ago }

      it 'returns true' do
        expect(token.expired?).to be true
      end
    end

    context 'when expiration date is missing' do
      let(:expiration_date) { nil }

      it 'returns true' do
        expect(token.expired?).to be true
      end
    end
  end

  describe '#expires_in' do
    let(:token) { described_class.new expires_at: expiration_date }

    context 'when expiration date is in the future' do
      let(:expiration_date) { 1.hour.from_now }

      it 'returns a positive number' do
        result = token.expires_in
        aggregate_failures do
          expect(result).to be_a Integer
          expect(result).to be > 0
        end
      end
    end

    context 'when expiration date is in the past' do
      let(:expiration_date) { 1.hour.ago }

      it 'returns a negative number' do
        result = token.expires_in
        aggregate_failures do
          expect(result).to be_a Integer
          expect(result).to be < 0
        end
      end
    end

    context 'when expiration date is missing' do
      let(:expiration_date) { nil }

      it 'returns nil' do
        expect(token.expires_in).to be_nil
      end
    end
  end

  describe '#token' do
    let(:token) { FactoryBot.build :jwt_token, user: user }

    context 'when entity was saved' do
      before do
        token.save!
      end

      it 'returns a valid token string' do
        value = token.token

        aggregate_failures do
          expect(value).to be_a String
          expect(described_class.decode(value)['jti']).to eq token.id
        end
      end
    end

    context 'when entity was not saved yet' do
      it 'raises an error' do
        expect { token.token }.to raise_error 'Record should be persisted to generate the token'
      end
    end
  end
end
