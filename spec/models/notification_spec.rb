# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notification, type: :model do
  describe 'scopes' do
    before do
      FactoryBot.create :notification, archived_at: Time.current
      FactoryBot.create_list :notification, 2
    end

    describe '.unread' do
      it 'returns the unread notifications' do
        expect(described_class.unread.count).to eq(2)
      end
    end

    describe '.archived' do
      it 'returns the archived notifications' do
        expect(described_class.archived.count).to eq(1)
      end
    end
  end

  describe 'archive!' do
    it 'marks the notification as archived' do
      notification = FactoryBot.create :notification
      notification.archive!

      expect(notification.archived_at).not_to be_nil
    end
  end
end
