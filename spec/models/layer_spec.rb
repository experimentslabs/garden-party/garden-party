# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Layer, type: :model do
  let(:map) { FactoryBot.create :map }
  let(:first_layer) { map.layers.first }

  describe 'scopes' do
    let(:maps) { FactoryBot.create_list :map, 3 }

    describe '.in_maps' do
      it 'returns all layers in given maps' do # rubocop:disable RSpec/ExampleLength
        ids = [maps.first.id, maps.second.id]
        results = described_class.in_maps(ids)

        aggregate_failures do
          expect(results.count).to eq 2
          expect(results.pluck(:map_id)).to eq ids
        end
      end
    end

    describe '.in_map' do
      it 'returns all layers in given map' do # rubocop:disable RSpec/ExampleLength
        id = maps.first.id
        results = described_class.in_map(id)

        aggregate_failures do
          expect(results.count).to eq 1
          expect(results.pluck(:map_id)).to eq [id]
        end
      end
    end
  end

  context 'when creating the layer' do
    context 'when it is the first layer' do
      it 'has the right position' do
        layer = map.layers.first
        expect(layer.position).to eq 1
      end
    end

    context 'when there are other layers' do
      context 'when inserted with a position' do
        it 'has the right position' do
          layer = described_class.create name: 'A layer', map: map, position: 1
          expect(layer.position).to eq 1
        end

        it 'update other layers position' do
          described_class.create name: 'A layer', map: map, position: 1
          first_layer.reload
          expect(first_layer.position).to eq 2
        end

        it 'does not change layers on other maps' do
          other_map = FactoryBot.create :map
          described_class.create name: 'A layer', map: map, position: 1
          expect(other_map.layers.last.position).to eq 1
        end
      end

      context 'when inserted without a position' do
        it 'has the last position' do
          layer = described_class.create name: 'A layer', map: map
          expect(layer.position).to eq 2
        end
      end
    end
  end

  context 'when changing position' do
    it 'changes the position' do
      layer = described_class.create name: 'A layer', map: map
      layer.update position: 1
      expect(layer.position).to eq 1
    end

    it 'updates other layers position' do
      layer = described_class.create name: 'A layer', map: map
      layer.update position: 1
      first_layer.reload
      expect(first_layer.position).to eq 2
    end
  end
end
