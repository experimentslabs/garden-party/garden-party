# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Activity do
  describe 'hooks' do
    describe 'before_validation' do
      let(:map) { FactoryBot.create :map }
      let(:instance) { described_class.new subject: map }

      it 'fills the attributes' do
        expect { instance.validate }.to change(instance, :user_id).from(nil).to(map.user_id)
                                    .and change(instance, :username).from(nil).to(map.user.username)
                                    .and change(instance, :map_id).from(nil).to(map.id)
                                    .and change(instance, :subject_name).from(nil).to(map.name)
      end
    end
  end
end
