# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamMate, type: :model do
  let(:instance) { FactoryBot.create :team_mate }

  describe 'hooks' do
    describe 'after_create' do
      context 'when user is the owner' do
        it 'does not notify the user' do
          expect do
            # Easy way to create a TeamMate with a map owner
            FactoryBot.create :map
          end.not_to change(Notification, :count)
        end
      end

      context 'when user is not the owner' do
        it 'notifies for the invited user' do
          user = FactoryBot.create :user

          expect do
            FactoryBot.create :team_mate, user: user
          end.to change(user.notifications, :count).by 1
        end
      end
    end
  end

  describe '.accept!' do
    it 'notifies the map owner' do
      instance
      expect do
        instance.accept!
      end.to change(Notification.where(type: 'TeamMateAcceptNotification', recipient: instance.map.user), :count).by 1
    end
  end

  describe '.refuse!' do
    it 'notifies the map owner' do
      instance
      expect do
        instance.refuse!
      end.to change(Notification.where(type: 'TeamMateRefuseNotification', recipient: instance.map.user), :count).by 1
    end
  end

  describe '.leave!' do
    it 'notifies the map owner' do
      instance
      expect do
        instance.leave!
      end.to change(Notification.where(type: 'TeamMateLeaveNotification', recipient: instance.map.user), :count).by 1
    end
  end

  describe '.remove!' do
    it 'notifies the invited user' do
      instance
      expect do
        instance.remove!
      end.to change(Notification.where(type: 'TeamMateRemoveNotification', recipient: instance.user), :count).by 1
    end
  end
end
