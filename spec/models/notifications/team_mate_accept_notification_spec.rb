# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamMateAcceptNotification, type: :model do
  describe 'validation' do
    # Valid values
    let(:notification_subject) { FactoryBot.build :team_mate }
    let(:recipient) { notification_subject.map.user }
    let(:sender) { notification_subject.user }
    let(:instance) { described_class.new subject: notification_subject, sender: sender, recipient: recipient }

    context 'when recipient is the map owner' do
      context 'when sender is the invitee' do
        it 'validates' do
          expect(instance).to be_valid
        end
      end

      context 'when sender is not the invitee' do
        it 'fails validation' do
          instance.sender = FactoryBot.create :user
          instance.validate
          expect(instance.errors[:sender][0]).to eq I18n.t('activerecord.errors.models.team_mate_accept_notification.sender.bad_sender')
        end
      end
    end

    context 'when recipient is not the map owner' do
      it 'fails validation' do
        instance.recipient = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:recipient][0]).to eq I18n.t('activerecord.errors.models.team_mate_accept_notification.recipient.bad_recipient')
      end
    end

    context 'when subject is not a TeamMate' do
      it 'fails validation' do
        instance.subject = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:subject][0]).to eq I18n.t('activerecord.errors.models.team_mate_accept_notification.subject.bad_subject')
      end
    end
  end

  describe '.create_for!' do
    it 'creates a new notification' do
      expect do
        described_class.create_for! FactoryBot.create :team_mate
      end.to change(described_class, :count).by 1
    end
  end
end
