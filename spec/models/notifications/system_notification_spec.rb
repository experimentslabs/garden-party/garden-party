# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SystemNotification, type: :model do
  include ActiveJob::TestHelper

  describe 'validation' do
    it 'must not have a subject' do
      notification = described_class.new content: { title: 'Welcome', message: 'Hello world' }, subject: User.first
      notification.validate

      expect(notification.errors[:subject][0]).to eq I18n.t('activerecord.errors.models.system_notification.subject.should_not_be_present')
    end

    describe 'content' do
      it 'must have a title' do
        notification = described_class.new content: { message: 'Hello world' }
        notification.validate

        expect(notification.errors[:content][0]).to eq I18n.t('activerecord.errors.models.system_notification.content.title_missing')
      end

      it 'must have a message' do
        notification = described_class.new content: { title: 'Welcome' }
        notification.validate

        expect(notification.errors[:content][0]).to eq I18n.t('activerecord.errors.models.system_notification.content.message_missing')
      end
    end
  end

  describe 'hooks' do
    describe 'after_create' do
      it 'sends an email' do
        expect do
          perform_enqueued_jobs do
            FactoryBot.create :notification, :system_notification
          end
        end.to change(ActionMailer::Base.deliveries, :count).by(1)
      end
    end
  end

  describe '.notify_all!' do
    it 'creates a notification for everyone' do
      FactoryBot.create_list :user, 10

      # Two users are created in the "before :suite" hook
      expect do
        described_class.notify_all! title: 'Hello', message: 'Welcome and have fun'
      end.to change(Notification, :count).by(12)
    end
  end

  describe '.notify!' do
    it 'creates a notification for specified user' do
      user = FactoryBot.create :user

      # Two users are created in the "before :suite" hook
      expect do
        described_class.notify! user, title: 'Hello', message: 'Welcome and have fun'
      end.to change(Notification, :count).by(1)
    end
  end
end
