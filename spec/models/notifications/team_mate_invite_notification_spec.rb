# frozen_string_literal: true

require 'rails_helper'

# Empty TeamMate-like model to be able to create records without running TeamMate
# hooks.
class GhostTeamMate < ApplicationRecord
  self.table_name = :team_mates
  belongs_to :user
  belongs_to :map
end

RSpec.describe TeamMateInviteNotification, type: :model do
  include ActiveJob::TestHelper

  describe 'validation' do
    # Valid values
    let(:notification_subject) do
      GhostTeamMate.create FactoryBot.build(:team_mate).attributes
      TeamMate.last
    end
    let(:recipient) { notification_subject.user }
    let(:sender) { notification_subject.map.user }
    let(:instance) { described_class.new subject: notification_subject, sender: sender, recipient: recipient }

    context 'when recipient is the invitee' do
      context 'when sender is the map owner' do
        it 'validates' do
          expect(instance).to be_valid
        end
      end

      context 'when sender is not the map owner' do
        it 'fails validation' do
          instance.sender = FactoryBot.create :user
          instance.validate
          expect(instance.errors[:sender][0]).to eq I18n.t('activerecord.errors.models.team_mate_invite_notification.sender.bad_sender')
        end
      end
    end

    context 'when recipient is not the invitee' do
      it 'fails validation' do
        instance.recipient = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:recipient][0]).to eq I18n.t('activerecord.errors.models.team_mate_invite_notification.recipient.bad_recipient')
      end
    end

    context 'when subject is not a TeamMate' do
      it 'fails validation' do
        instance.subject = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:subject][0]).to eq I18n.t('activerecord.errors.models.team_mate_invite_notification.subject.bad_subject')
      end
    end
  end

  describe 'hooks' do
    let(:instance) { FactoryBot.create :notification, :invitation }

    describe 'before_destroy' do
      it 'refuses the invitation' do
        allow(instance.subject).to receive :refuse!
        instance.destroy
        expect(instance.subject).to have_received(:refuse!)
      end
    end

    describe 'after_create' do
      context 'when invitee has an active account' do
        it 'sends an email' do
          expect do
            perform_enqueued_jobs { FactoryBot.create :team_mate }
          end.to change(ActionMailer::Base.deliveries, :count).by(1)
        end
      end

      context 'when invitee does not have an active account' do
        it 'does not send an email' do
          invitee = User.invite! username: 'JackTheGardener', email: 'jack@example.com'
          expect do
            perform_enqueued_jobs { FactoryBot.create :team_mate, user: invitee }
          end.not_to change(ActionMailer::Base.deliveries, :count)
        end
      end
    end
  end

  describe '.create_for!' do
    it 'creates a new notification' do
      team_mate = FactoryBot.create :team_mate
      expect do
        described_class.create_for! team_mate
      end.to change(described_class, :count).by 1
    end
  end
end
