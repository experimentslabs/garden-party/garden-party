# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NewCommentNotification, type: :model do
  describe 'validation' do
    # Valid values
    let(:notification_subject) { FactoryBot.build :comment }
    let(:recipient) { GardenParty::MapFinder.for(notification_subject).user }
    let(:sender) { notification_subject.user }
    let(:instance) { described_class.new subject: notification_subject, sender: sender, recipient: recipient }

    context 'when recipient is unspecified' do
      it 'fails validation' do
        instance.recipient = nil
        instance.validate
        expect(instance.errors[:recipient][0]).to eq I18n.t('activerecord.errors.models.new_comment_notification.recipient.bad_recipient')
      end
    end

    context 'when subject is not a Comment' do
      it 'fails validation' do
        instance.subject = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:subject][0]).to eq I18n.t('activerecord.errors.models.new_comment_notification.subject.bad_subject')
      end
    end

    context 'when sender is not the Comment author' do
      it 'fails validation' do
        instance.sender = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:sender][0]).to eq I18n.t('activerecord.errors.models.new_comment_notification.sender.bad_sender')
      end
    end
  end

  describe '.create_for!' do
    let(:comment) { FactoryBot.create :comment }
    let(:map) { GardenParty::MapFinder.for comment }
    let!(:team_mate) do
      user = FactoryBot.create :team_mate, :accepted, map: map
      map.reload
      user
    end

    context 'when comment author is not a team member' do
      it 'creates a new notification for every team member' do
        expect do
          described_class.create_for! comment
        end.to change(described_class, :count).by 2
      end
    end

    context 'when comment author is a team member' do
      before do
        team_mate.update! user: comment.user
      end

      it 'creates a new notification for other team member' do
        expect do
          described_class.create_for! comment
        end.to change(described_class, :count).by 1
      end
    end
  end
end
