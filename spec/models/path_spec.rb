# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Path, type: :model do
  let(:map) { FactoryBot.create :map }

  describe 'scopes' do
    let(:maps) { FactoryBot.create_list :map, 3 }

    before do
      maps.each do |map|
        FactoryBot.create :path, map: map
      end
    end

    describe '.in_maps' do
      it 'returns all paths in given maps' do # rubocop:disable RSpec/ExampleLength
        ids = [maps.first.id, maps.second.id]
        results = described_class.in_maps(ids)

        aggregate_failures do
          expect(results.count).to eq 2
          expect(results.pluck(:map_id).uniq).to eq ids
        end
      end
    end

    describe '.in_map' do
      it 'returns all paths in given map' do # rubocop:disable RSpec/ExampleLength
        id = maps.first.id
        results = described_class.in_map(id)

        aggregate_failures do
          expect(results.count).to eq 1
          expect(results.pluck(:map_id).uniq).to eq [id]
        end
      end
    end
  end
end
