# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:now) { Time.current }
  let(:tomorrow) { now + 1.day }
  let(:day_after_tomorrow) { now + 2.days }
  let(:task) { FactoryBot.create :task, :for_element, action: 'something', planned_for: tomorrow }

  describe 'scopes' do
    let(:maps) { FactoryBot.create_list :map, 3 }

    before do
      maps.each do |map|
        # Create tasks with all possible subject types
        FactoryBot.create :task, subject: map
        FactoryBot.create(:path, map: map).tap { |path| FactoryBot.create :task, subject: path }
        FactoryBot.create(:patch, :with_element, map: map).tap do |patch|
          FactoryBot.create :task, subject: patch
          FactoryBot.create :task, subject: patch.elements.first
        end
        FactoryBot.create(:element, :point, map: map).tap { |element| FactoryBot.create :task, subject: element }
      end
    end

    describe '.all_for_maps' do
      it 'returns all tasks for the given map' do # rubocop:disable RSpec/ExampleLength
        ids = [maps.first.id, maps.second.id]

        results = described_class.all_for_maps(ids)
        results_map_ids = results.map { |r| GardenParty::MapFinder.by_subject(r).id }

        aggregate_failures do
          expect(results.count).to eq 10
          expect(results_map_ids.uniq).to eq ids
        end
      end
    end

    describe '.all_for_map' do
      it 'returns all tasks for the given map' do # rubocop:disable RSpec/ExampleLength
        id = maps.first.id
        results = described_class.all_for_map(id)
        results_map_ids = results.map { |r| GardenParty::MapFinder.by_subject(r).id }

        aggregate_failures do
          expect(results.count).to eq 5
          expect(results_map_ids.uniq).to eq [id]
        end
      end
    end
  end

  describe '.update_without_subject_hooks' do
    it 'does not perform subject hooks' do
      allow(task).to receive :perform_action
      task.update_without_subject_hooks planned_for: day_after_tomorrow

      expect(task).not_to have_received(:perform_action)
    end
  end

  describe '.destroy_without_subject_hooks' do
    it 'does not perform subject hooks' do
      allow(task).to receive :perform_action
      task.destroy_without_subject_hooks

      expect(task).not_to have_received(:perform_action)
    end
  end

  describe '.last_significant_change' do
    let(:task) { FactoryBot.create :task, :planned, :for_element, action: 'something', planned_for: tomorrow }

    context 'when the task is already finished' do
      it 'returns nil' do
        task.update! done_at: Time.current

        task.update! planned_for: Time.current.yesterday
        expect(task.last_significant_change).to be_nil
      end
    end

    context 'when the task is pending' do
      context 'when the task date is changed' do
        it 'returns :update' do
          task.update_without_subject_hooks planned_for: Time.current
          expect(task.last_significant_change).to eq :update
        end
      end

      context 'when the task is finished' do
        it 'returns :finish' do
          task.update_without_subject_hooks done_at: Time.current
          expect(task.last_significant_change).to eq :finish
        end
      end

      context 'when the task is destroyed' do
        it 'returns :destroy' do
          task.destroy_without_subject_hooks
          expect(task.last_significant_change).to eq :destroy
        end
      end

      context 'when another field is changed' do
        it 'returns nil' do
          task.update_without_subject_hooks notes: 'Something new'
          expect(task.last_significant_change).to be_nil
        end
      end
    end
  end
end
