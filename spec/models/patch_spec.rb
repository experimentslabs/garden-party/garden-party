# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Patch, type: :model do
  let(:map) { FactoryBot.create :map }

  describe 'scopes' do
    let(:maps) { FactoryBot.create_list :map, 3 }

    before do
      maps.each do |map|
        FactoryBot.create :patch, map: map
      end
    end

    describe '.in_maps' do
      it 'returns all patches in given maps' do # rubocop:disable RSpec/ExampleLength
        ids = [maps.first.id, maps.second.id]
        results = described_class.in_maps(ids)

        aggregate_failures do
          expect(results.count).to eq 2
          expect(results.pluck(:map_id).uniq).to eq ids
        end
      end
    end

    describe '.in_map' do
      it 'returns all patches in given map' do # rubocop:disable RSpec/ExampleLength
        id = maps.first.id
        results = described_class.in_map(id)

        aggregate_failures do
          expect(results.count).to eq 1
          expect(results.pluck(:map_id).uniq).to eq [id]
        end
      end
    end
  end

  context 'when creating a patch' do
    let(:first_element) { FactoryBot.build :element, :in_patch_factory }
    let(:second_element) { FactoryBot.build :element, :in_patch_factory }

    context 'when it is a polygon with associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :polygon).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element, second_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(2)
      end
    end

    context 'when it is a circle with associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :circle).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element, second_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(2)
      end
    end
  end
end
