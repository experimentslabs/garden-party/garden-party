# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'scopes' do
    describe 'search' do
      before do
        FactoryBot.create :user, username: 'Bobby'
        FactoryBot.create :user, username: 'Alice'
        FactoryBot.create :user, username: 'Alicia'
      end

      it 'does not care of the search string case' do
        expect(described_class.search('bob').count).to eq 1
      end

      it 'returns all matches' do
        expect(described_class.search('lic').count).to eq 2
      end
    end
  end

  describe 'validation' do
    describe 'calendar_token' do
      it 'cannot be used for two users' do
        token = 'a_string'
        FactoryBot.create :user, calendar_token: token

        expect { FactoryBot.create :user, calendar_token: token }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it 'can be blank on multiple users' do
        FactoryBot.create :user, calendar_token: nil

        expect { FactoryBot.create :user, calendar_token: nil }.to change(described_class, :count).by 1
      end

      # Depending on indexes, validation for uniqueness sometimes fails when the entity being updated is
      # saved, as "a record" exists with the unique value, be it the same one.
      # Even if this mostly occurs on compound indexes, let's check it anyway...
      it 'do not prevent user to be updated' do
        old_email = 'bad_email@somewhere.com'
        new_email = 'bob@somewhere.com'
        user = FactoryBot.create :user, email: old_email, calendar_token: 'a_token'

        expect { user.update! email: new_email }.to change { user.reload.email }.from(old_email).to(new_email)
      end
    end
  end

  describe '#destroy' do
    context 'when user had lots of activities' do
      let!(:user) { map.user }
      let(:map) { FactoryBot.create :map }
      let(:second_map) { FactoryBot.create :map }

      def generate_map_activity(map)
        # Do things in the map
        patch = FactoryBot.create :patch, map: map
        FactoryBot.create :element, :in_patch, patch: patch
        FactoryBot.create(:element, :point, map: map).tap do |element|
          FactoryBot.create :task, subject: element
          element.update implanted_at: Time.current
          element.update removed_at: Time.current
        end
        FactoryBot.create :task, subject: map
        FactoryBot.create :observation, subject: map
      end

      def as_user(user, &)
        old_current = Current.user
        Current.user = user

        yield

        Current.user = old_current
      end

      before do
        second_user = FactoryBot.create :user
        invitation = nil

        # Be invited
        as_user second_map.user do
          invitation = FactoryBot.create :team_mate, user: user, map: second_map
        end

        as_user user do
          # Generate activity on owned map
          generate_map_activity map
          # Generate activity on other user's map
          invitation.accept!
          generate_map_activity second_map

          # Interact with the library
          FactoryBot.create(:family).update! name: 'New name'
          FactoryBot.create(:genus).update! name: 'New name'
          FactoryBot.create(:resource).update! name: 'New name'
          FactoryBot.create(:resource_note, user: user).update! content: 'new content'

          # Add a link
          FactoryBot.create :link, user: user

          # Invite user
          invitation = FactoryBot.create :team_mate, user: second_user, map: map
        end

        # Second user does things in user's map
        as_user second_user do
          invitation.accept!
          generate_map_activity map
        end
      end

      it 'destroys the user' do # rubocop:disable RSpec/ExampleLength
        expect { user.destroy }
          .to change(described_class, :count).by(-1)
          .and change(Map, :count).by(-1)
          .and change(ResourceNote, :count).by(-1)
          .and change(Family, :count).by(0) # rubocop: disable RSpec/ChangeByZero
          .and change(Genus, :count).by(0) # rubocop: disable RSpec/ChangeByZero
          .and change(Resource, :count).by(0) # rubocop: disable RSpec/ChangeByZero
          .and change { second_map.reload.tasks.count }.by(0) # rubocop: disable RSpec/ChangeByZero
      end
    end
  end

  describe '#rotate_calendar_token!' do
    let(:token) { 'a_token' }
    let!(:user) { FactoryBot.create :user, calendar_token: token }

    it 'updates the user with a new token' do
      expect { user.rotate_calendar_token! }.to change { user.reload.calendar_token }.from(token).to(/^\w{32}$/)
    end

    it 'handles collisions' do
      allow(SecureRandom).to receive(:hex).and_return('a_token', 'last_try')

      aggregate_failures do
        expect { user.rotate_calendar_token! }.to change { user.reload.calendar_token }.from(token).to 'last_try'
        expect(SecureRandom).to have_received(:hex).twice
      end
    end
  end
end
