# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Element, type: :model do
  let(:dates) do
    now = Time.current.beginning_of_day
    {
      now:         now,
      yesterday:   now - 1.day,
      tomorrow:    now + 1.day,
      in_two_days: now + 2.days,
    }
  end

  describe 'validation' do
    it 'cannot be planned for removal without being implanted' do
      element = FactoryBot.build :element, removal_planned_for: dates[:now]
      element.validate
      expect(element.errors[:implanted_at]).not_to be_blank
    end

    it 'cannot be removed without being implanted' do
      element = FactoryBot.build :element, removed_at: dates[:now]
      element.validate
      expect(element.errors[:implanted_at]).not_to be_blank
    end

    it 'cannot be planned for removal before being implanted' do
      element = FactoryBot.build :element, implanted_at: dates[:now], removal_planned_for: dates[:yesterday]
      element.validate
      expect(element.errors[:removal_planned_for]).not_to be_blank
    end

    it 'cannot be removed before being implanted' do
      element = FactoryBot.build :element, implanted_at: dates[:now], removed_at: dates[:yesterday]
      element.validate
      expect(element.errors[:removed_at]).not_to be_blank
    end

    context 'when it is a point and is in a patch at the same time' do
      let(:element) do
        resource = FactoryBot.create :resource
        valid_geometry = FactoryBot.build(:element, :point, layer: nil).geometry
        map     = FactoryBot.create :map
        layer   = map.layers.first
        patch   = FactoryBot.create :patch, map: map, layer: layer

        described_class.new geometry: valid_geometry, layer: layer, patch: patch, resource: resource
      end

      it 'is invalid' do
        element.validate

        expect(element.errors.messages).to eq({ patch: [I18n.t('errors.messages.present')] })
      end
    end
  end

  describe 'hooks' do
    describe '.update_tasks' do
      describe 'related to implantation date' do
        let(:element) { FactoryBot.create :element, :point, implantation_planned_for: dates[:tomorrow] }

        context 'when setting the planned implantation date' do # rubocop:disable RSpec/NestedGroups
          it 'creates a task to implant the element' do
            expect do
              element
            end.to change(Task, :count).by 1
          end
        end

        context 'when updating the planned implantation date' do  # rubocop:disable RSpec/NestedGroups
          it 'updates the task to implant the element' do
            element.update! implantation_planned_for: dates[:in_two_days]

            expect(Task.last.planned_for).to eq dates[:in_two_days]
          end
        end

        context 'when removing the planned implantation date' do  # rubocop:disable RSpec/NestedGroups
          it 'destroys the task to implant the element' do
            element

            expect do
              element.update! implantation_planned_for: nil
            end.to change(Task, :count).by(-1)
          end

          it 'does not implant the element' do
            element
            element.update! implantation_planned_for: nil
            element.reload

            expect(element.implanted_at).to be_nil
          end
        end
      end

      describe 'related to the removal date' do
        let(:element) do
          element = FactoryBot.create :element, :point, implanted_at: dates[:now]
          element.update! removal_planned_for: dates[:tomorrow]
          element
        end

        context 'when setting the planned removal date' do # rubocop:disable RSpec/NestedGroups
          it 'creates a task to remove the element' do
            expect do
              element
            end.to change(Task, :count).by 1
          end
        end

        context 'when updating the planned removal date' do # rubocop:disable RSpec/NestedGroups
          it 'updates the task to remove the element' do
            element.update! removal_planned_for: dates[:in_two_days]

            expect(Task.last.planned_for).to eq dates[:in_two_days]
          end
        end

        context 'when removing a planned removal date' do # rubocop:disable RSpec/NestedGroups
          it 'destroys the task to remove the element' do
            element

            expect do
              element.update! removal_planned_for: nil
            end.to change(Task, :count).by(-1)
          end

          it 'does not set the removal date' do
            element
            element.update! removal_planned_for: nil
            element.reload

            expect(element.removed_at).to be_nil
          end
        end
      end
    end
  end

  describe 'scopes' do
    let(:maps) { FactoryBot.create_list :map, 3 }

    before do
      maps.each do |map|
        FactoryBot.create :element, :point, map: map
        FactoryBot.create :patch, :with_element, map: map
      end
    end

    describe '.in_maps' do
      it 'returns all elements in given maps' do
        ids = [maps.first.id, maps.second.id]

        expect(described_class.in_maps(ids).count).to eq 4
      end
    end

    describe '.in_map' do
      it 'returns all elements in given map' do
        id = maps.first.id

        expect(described_class.in_map(id).count).to eq 2
      end
    end
  end

  describe '.display_name' do
    describe 'when element is a Point' do
      let(:element) { FactoryBot.create :element, :point }

      describe 'when element has a name' do
        before do
          element.update! name: 'Some element'
        end

        it 'returns the patch and resource names' do
          expect(element.display_name).to eq I18n.t('models.element.name.point_element',
                                                    resource_name: element.resource.name,
                                                    element_name:  'Some element')
        end
      end

      describe 'when element has no name' do
        it 'returns the resource name' do
          expect(element.display_name).to eq element.resource.name
        end
      end
    end

    describe 'when element is in a patch' do
      let(:patch) { FactoryBot.create :patch, :with_element, :polygon }
      let(:element) { patch.elements.first }

      describe 'when patch has no name' do
        it 'returns the patch fallback and resource names' do
          expect(element.display_name).to eq I18n.t('models.element.name.element_in_patch',
                                                    resource_name: element.resource.name,
                                                    patch_name:    I18n.t('generic.patch.name'))
        end
      end

      describe 'when patch has a name' do
        before do
          patch.update! name: 'Some patch'
        end

        it 'returns the patch and resource names' do
          expect(element.display_name).to eq I18n.t('models.element.name.element_in_patch',
                                                    resource_name: element.resource.name,
                                                    patch_name:    patch.name)
        end
      end
    end
  end

  describe '.duplicate' do
    context 'when element has no name' do
      let(:element) { FactoryBot.create :element, :point }

      it 'sets the right string' do
        duplicate = element.duplicate

        expect(duplicate.name).to eq I18n.t('models.element.duplicate_name_suffix', name: duplicate.resource.name)
      end
    end

    context 'when element has a name' do
      let(:element) { FactoryBot.create :element, :point, name: 'Something' }

      it 'changes to the right string' do
        duplicate = element.duplicate

        expect(duplicate.name).to eq I18n.t('models.element.duplicate_name_suffix', name: element.name)
      end
    end
  end

  describe '.status' do
    context 'when element was just added to a crop' do
      let(:element) { FactoryBot.build :element }

      it('is "planned"') { expect(element.status).to eq :planned }
    end

    context 'when element has a planned implantation date' do
      let(:element) { FactoryBot.build :element, implantation_planned_for: dates[:now] }

      it('is "planned"') { expect(element.status).to eq :planned }
    end

    context 'when element is implanted' do
      context 'without a planned implantation date' do
        let(:element) { FactoryBot.build :element, implanted_at: dates[:now] }

        it('is "implanted"') { expect(element.status).to eq :implanted }
      end

      context 'with a planned implantation date' do
        let(:element) { FactoryBot.build :element, implantation_planned_for: dates[:yesterday], implanted_at: dates[:now] }

        it('is "implanted"') { expect(element.status).to eq :implanted }
      end
    end

    context 'when element is planned for removal' do
      let(:element) { FactoryBot.build :element, implanted_at: dates[:yesterday], removal_planned_for: dates[:now] }

      it('is "implanted"') { expect(element.status).to eq :implanted }
    end

    context 'when element is removed' do
      let(:element) { FactoryBot.build :element, implanted_at: dates[:yesterday], removed_at: dates[:now] }

      it('is "removed"') { expect(element.status).to eq :removed }
    end
  end
end
