# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Resource, type: :model do
  describe '.common_names_format' do
    context 'when value is an array of strings' do
      let(:resource) { described_class.new common_names: %w[some other names] }

      it 'validates' do
        resource.validate

        expect(resource.errors[:common_names]).to be_empty
      end
    end

    context 'when value is not an array' do
      let(:resource) { described_class.new common_names: 123 }

      it 'does not validate' do
        resource.validate

        expect(resource.errors[:common_names].size).to eq 1
      end
    end

    context 'when value is an array with mixed content' do
      let(:resource) { described_class.new common_names: ['ok', %w[not allowed], { this: 'neither' }] }

      it 'does not validate' do
        resource.validate

        expect(resource.errors[:common_names].size).to eq 1
      end
    end
  end

  describe '.normalize_common_names' do
    let(:resource) do
      FactoryBot.build :resource, common_names: [' a', ' c ', 'b', 'c']
    end

    it 'sorts and removes doubles' do
      resource.save

      expect(resource.common_names).to eq %w[a b c]
    end
  end
end
