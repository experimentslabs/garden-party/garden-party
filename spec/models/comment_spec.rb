# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Comment do
  let(:observation) { FactoryBot.create :observation }
  let(:map) { GardenParty::MapFinder.for(observation) }

  let(:author) { FactoryBot.create :user }

  describe 'hooks' do
    describe 'after_create' do
      context 'when author is not a team member' do
        it 'creates notifications' do
          expect do
            FactoryBot.create :comment, subject: observation
          end.to change(Notification, :count).by 1
        end
      end
    end
  end
end
