# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/jwt_tokens', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:jwt_token).attributes
  end
  let(:invalid_attributes) do
    { description: nil }
  end

  include_context 'with authenticated member'

  describe 'GET /jwt_tokens' do
    it 'returns a success response' do
      FactoryBot.create :jwt_token, user: signed_in_user

      get jwt_tokens_url

      expect(response).to be_successful
    end
  end

  describe 'POST /jwt_tokens' do
    context 'with valid params' do
      it 'creates a new JwtToken' do
        expect do
          post jwt_tokens_url, params: { jwt_token: valid_attributes }
        end.to change(JwtToken, :count).by(1)
      end

      it 'redirects to the JwtToken list' do
        post jwt_tokens_url, params: { jwt_token: valid_attributes }
        expect(response).to redirect_to(jwt_tokens_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post jwt_tokens_url, params: { jwt_token: invalid_attributes }
        expect(response).to be_successful
      end

      it 'does not create a new token' do
        expect do
          post jwt_tokens_url, params: { jwt_token: invalid_attributes }
        end.not_to change(JwtToken, :count)
      end
    end
  end

  describe 'DELETE /jwt_tokens/1' do
    let(:jwt_token) { FactoryBot.create :jwt_token, user: signed_in_user }

    it 'destroys the requested jwt_token' do
      jwt_token

      expect do
        delete jwt_token_url(jwt_token)
      end.to change(JwtToken, :count).by(-1)
    end

    it 'redirects to the jwt_tokens list' do
      delete jwt_token_url(jwt_token)
      expect(response).to redirect_to(jwt_tokens_url)
    end

    context 'with a token from another user' do
      let(:jwt_token) { FactoryBot.create :jwt_token }

      it 'does not delete the token' do
        jwt_token

        expect do
          delete jwt_token_url(jwt_token)
        end.not_to change(JwtToken, :count)
      end

      it 'renders an error page' do
        delete jwt_token_url(jwt_token)
        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
