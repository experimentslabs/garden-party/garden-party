# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/account/calendar', type: :request do
  describe 'GET /account/calendar' do
    let(:token) { '' }
    let(:request_scope) { nil }
    let(:visit_page) { get "/account/calendar?token=#{token}&scope=#{request_scope}" }

    context 'with a calendar token' do
      let(:token) { FactoryBot.create(:user, :with_calendar_token).calendar_token }

      it 'returns a success response' do
        visit_page

        expect(response).to be_successful
      end

      it 'returns a vCalendar' do
        visit_page

        aggregate_failures do
          expect(response.body).to start_with 'BEGIN:VCALENDAR'
          expect(response.content_type).to eq 'text/calendar; charset=utf-8'
        end
      end
    end

    describe 'parameters' do
      let(:user) { FactoryBot.create :user, :with_calendar_token }
      let(:token) { user.calendar_token }

      before do
        map = FactoryBot.create :map, user: user
        team_mate = FactoryBot.create(:team_mate, :accepted, map: map).user
        FactoryBot.create :task, subject: map, assignee: user
        FactoryBot.create :task, subject: map, assignee: team_mate
      end

      describe 'default' do
        it 'returns calendar with assigned tasks only' do
          visit_page

          expect(response.body.scan('BEGIN:VTODO').count).to eq 1
        end
      end

      describe 'all' do
        let(:request_scope) { :all }

        it 'returns calendar with all tasks on accessible maps' do
          visit_page

          expect(response.body.scan('BEGIN:VTODO').count).to eq 2
        end
      end
    end

    context 'without a calendar token' do
      let(:token) { nil }

      it 'renders an error page' do
        visit_page

        expect(response).to have_http_status :forbidden
      end
    end

    context 'with a bad calendar token' do
      let(:token) { 'invalid' }

      it 'renders an error page' do
        visit_page

        expect(response).to have_http_status :forbidden
      end
    end
  end

  describe 'PUT /account/calendar/refresh' do
    let(:visit_page) { put '/account/calendar/refresh' }

    context 'with an authenticated user' do
      include_context 'with authenticated member'

      it 'changes the calendar token' do
        expect { visit_page }.to change(signed_in_user.reload, :calendar_token)
      end

      it 'redirects to the preferences form' do
        visit_page

        expect(response).to redirect_to edit_account_calendar_url
      end
    end

    context 'without an authenticated user' do
      it 'redirects to login page' do
        visit_page

        expect(response).to redirect_to new_user_session_url
      end
    end
  end

  describe 'DELETE /account/calendar' do
    let(:visit_page) { delete '/account/calendar' }

    context 'with an authenticated user' do
      include_context 'with authenticated member'

      before do
        signed_in_user.rotate_calendar_token!
      end

      it 'changes the calendar token' do
        expect { visit_page }.to change(signed_in_user.reload, :calendar_token).from(String).to nil
      end

      it 'redirects to the preferences form' do
        visit_page

        expect(response).to redirect_to edit_account_calendar_url
      end
    end

    context 'without an authenticated user' do
      it 'redirects to login page' do
        visit_page

        expect(response).to redirect_to new_user_session_url
      end
    end
  end
end
