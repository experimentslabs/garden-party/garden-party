# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/account', type: :request do
  include_context 'with authenticated member'

  describe 'DELETE /account' do
    let(:delete_registration) { delete '/users', params: { user: { current_password: password } } }

    context 'with the right password provided' do
      let(:password) { 'password' }

      it 'destroys the user' do
        expect { delete_registration }.to change(User, :count).by(-1)
      end

      it 'redirects to the home page' do
        delete_registration

        expect(response).to redirect_to root_url
      end
    end

    context 'with a wrong password provided' do
      let(:password) { '' }

      it 'does not destroy the user' do
        expect { delete_registration }.not_to change(User, :count)
      end

      it 'displays a page' do
        delete_registration

        expect(response).to have_http_status :unprocessable_entity
      end
    end
  end
end
