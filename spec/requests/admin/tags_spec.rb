# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/tags', type: :request do
  let(:valid_attributes) do
    { name: Faker::Lorem.word }
  end
  let(:invalid_attributes) do
    { name: '' }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/tags' do
    it 'returns a success response' do
      ActsAsTaggableOn::Tag.create! valid_attributes
      get admin_tags_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/tags/1' do
    it 'returns a success response' do
      tag = ActsAsTaggableOn::Tag.create! valid_attributes
      get admin_tag_url(tag)
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/tags/1/edit' do
    it 'returns a success response' do
      tag = ActsAsTaggableOn::Tag.create! valid_attributes
      get edit_admin_tag_url(tag)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/tags' do
    context 'with valid params' do
      it 'creates a new Tag' do
        expect do
          post admin_tags_url, params: { acts_as_taggable_on_tag: valid_attributes }
        end.to change(ActsAsTaggableOn::Tag, :count).by(1)
      end

      it 'redirects to the created Tag' do
        post admin_tags_url, params: { acts_as_taggable_on_tag: valid_attributes }
        expect(response).to redirect_to(admin_tag_path(ActsAsTaggableOn::Tag.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_tags_url, params: { acts_as_taggable_on_tag: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/tags/1' do
    let(:tag) { ActsAsTaggableOn::Tag.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested admin_tag' do
        put admin_tag_url(tag), params: { acts_as_taggable_on_tag: new_attributes }
        tag.reload
        expect(tag.name).to eq new_attributes[:name]
      end

      it 'redirects to the admin_tag' do
        put admin_tag_url(tag), params: { acts_as_taggable_on_tag: new_attributes }
        expect(response).to redirect_to(admin_tag_path(ActsAsTaggableOn::Tag.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_tag_url(tag), params: { acts_as_taggable_on_tag: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'POST /admin/tags/batch_process' do
    let(:tag_ids) { ActsAsTaggableOn::Tag.pluck :id }

    before do
      # Create and assign some tags
      FactoryBot.create_list :resource, 2, tag_list: [Faker::Lorem.word]
    end

    context 'with invalid params' do
      it 'redirects to the tags list' do
        post batch_process_admin_tags_url, params: { batch_action: 'invalid', tag_ids: tag_ids }
        expect(response).to redirect_to(admin_tags_url)
      end

      it 'returns a success response' do
        post batch_process_admin_tags_url, params: { batch_action: 'invalid', tag_ids: tag_ids }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when merging tags' do
      it 'merges tags' do
        post batch_process_admin_tags_url, params: { batch_action: 'merge', tag_ids: tag_ids }
        expect(ActsAsTaggableOn::Tag.count).to eq 1
      end

      it 'redirects to the remaining tag' do
        post batch_process_admin_tags_url, params: { batch_action: 'merge', tag_ids: tag_ids }
        expect(response).to redirect_to(admin_tag_path(ActsAsTaggableOn::Tag.last))
      end
    end

    context 'when destroying tags' do
      it 'destroys tags' do
        tag_ids = ActsAsTaggableOn::Tag.pluck :id
        post batch_process_admin_tags_url, params: { batch_action: 'destroy', tag_ids: tag_ids }
        expect(ActsAsTaggableOn::Tag.count).to eq 0
      end

      it 'redirects to the tags list' do
        tag_ids = ActsAsTaggableOn::Tag.pluck :id
        post batch_process_admin_tags_url, params: { batch_action: 'destroy', tag_ids: tag_ids }
        expect(response).to redirect_to(admin_tags_url)
      end
    end
  end

  describe 'DELETE /admin/tags/1' do
    it 'destroys the requested admin_tag' do
      tag = ActsAsTaggableOn::Tag.create! valid_attributes
      expect do
        delete admin_tag_url(tag)
      end.to change(ActsAsTaggableOn::Tag, :count).by(-1)
    end

    it 'redirects to the admin_tags list' do
      tag = ActsAsTaggableOn::Tag.create! valid_attributes
      delete admin_tag_url(tag)
      expect(response).to redirect_to(admin_tags_url)
    end
  end
end
