# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/resources', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:resource).attributes
  end
  let(:invalid_attributes) do
    { name: '' }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/resources' do
    it 'returns a success response' do
      Resource.create! valid_attributes
      get admin_resources_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/resources/1' do
    with_versioning do
      it 'returns a success response' do
        resource = Resource.create! valid_attributes
        get admin_resource_url(resource)
        expect(response).to be_successful
      end
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_admin_resource_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/resources/1/edit' do
    it 'returns a success response' do
      resource = Resource.create! valid_attributes
      get edit_admin_resource_url(resource)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/resources' do
    context 'with valid params' do
      it 'creates a new Resource' do
        expect do
          post admin_resources_url, params: { resource: valid_attributes }
        end.to change(Resource, :count).by(1)
      end

      it 'redirects to the created admin_resource' do
        post admin_resources_url, params: { resource: valid_attributes }
        expect(response).to redirect_to(admin_resource_path(Resource.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_resources_url, params: { resource: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/resources/1' do
    let(:resource) { Resource.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested admin_resource' do
        put admin_resource_url(resource), params: { resource: new_attributes }
        resource.reload
        expect(resource.name).to eq new_attributes[:name]
      end

      it 'redirects to the admin_resource' do
        put admin_resource_url(resource), params: { resource: new_attributes }
        expect(response).to redirect_to(admin_resource_path(Resource.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_resource_url(resource), params: { resource: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /admin/resources/1' do
    it 'destroys the requested admin_resource' do
      resource = Resource.create! valid_attributes
      expect do
        delete admin_resource_url(resource)
      end.to change(Resource, :count).by(-1)
    end

    it 'redirects to the admin_resources list' do
      resource = Resource.create! valid_attributes
      delete admin_resource_url(resource)
      expect(response).to redirect_to(admin_resources_url)
    end
  end
end
