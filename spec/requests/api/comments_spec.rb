# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/comments', type: :request do
  let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
  let!(:comment) { FactoryBot.create :comment, subject: comment_subject }
  let(:comment_subject) { FactoryBot.create :observation, subject: owned_map }

  let(:valid_attributes) do
    FactoryBot.attributes_for :comment, subject_type: comment_subject.class.name, subject_id: comment_subject.id
  end

  let(:invalid_attributes) do
    valid_attributes.merge content: ''
  end

  include_context 'with authenticated member'

  describe 'GET /index' do
    context 'with a given observation' do
      it 'renders a successful response' do
        get api_observation_comments_url(observation_id: comment.subject_id), as: :json

        expect(response).to be_successful
      end
    end

    context 'with a given task' do
      let(:comment_subject) { FactoryBot.create :task, subject: owned_map }

      it 'renders a successful response' do
        get api_task_comments_url(task_id: comment.subject_id), as: :json
        expect(response).to be_successful
      end
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      get api_comment_url(comment), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Comment' do
        expect do
          post api_comments_url, params: { comment: valid_attributes }, as: :json
        end.to change(Comment, :count).by(1)
      end

      it 'renders an successful response' do
        post api_comments_url, params: { comment: valid_attributes }, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'render the new comment as JSON' do
        post api_comments_url, params: { comment: valid_attributes }, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Comment' do
        expect do
          post api_comments_url, params: { comment: invalid_attributes }, as: :json
        end.not_to change(Comment, :count)
      end

      it 'renders an error response' do
        post api_comments_url, params: { comment: invalid_attributes }, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'render the error as JSON' do
        post api_comments_url, params: { comment: invalid_attributes }, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do # rubocop:disable RSpec/MultipleMemoizedHelpers
      let(:new_attributes) do
        { content: 'New comment name' }
      end

      it 'updates the requested comment' do
        patch api_comment_url(comment), params: { comment: new_attributes }, as: :json
        comment.reload
        expect(comment.content).to eq new_attributes[:content]
      end

      it 'renders a JSON response with the comment' do
        patch api_comment_url(comment), params: { comment: new_attributes }, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders the comment as JSON' do
        patch api_comment_url(comment), params: { comment: new_attributes }, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end

    context 'with invalid parameters' do
      it 'renders an error response' do
        patch api_comment_url(comment), params: { comment: invalid_attributes }, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'render the error as JSON' do
        patch api_comment_url(comment), params: { comment: invalid_attributes }, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PUT /remove' do
    it 'censors the requested comment' do
      expect do
        put remove_api_comment_url(comment), as: :json
      end.to change { comment.reload.removed_at }.from(nil)
    end
  end
end
