# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/observations', type: :request do
  let(:map) { FactoryBot.create :map, user: signed_in_user }
  let(:owned_element) do
    patch = FactoryBot.create :patch, :with_element, map: map
    patch.elements.first
  end
  let(:valid_attributes) do
    FactoryBot.build(:observation, subject: owned_element, user: map.user).attributes
  end

  let(:invalid_attributes) do
    valid_attributes.merge! 'title' => nil
  end

  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/elements/1/observations' do
    it 'renders a successful response' do
      FactoryBot.create :observation, subject: owned_element
      get api_element_observations_url(element_id: owned_element.id), headers: valid_headers, as: :json

      expect(response).to be_successful
    end
  end

  describe 'GET /api/patch/1/observations' do
    it 'renders a successful response' do
      patch = FactoryBot.create :patch, map: map
      FactoryBot.create :observation, subject: patch
      get api_patch_observations_url(patch_id: patch.id), headers: valid_headers, as: :json

      expect(response).to be_successful
    end
  end

  describe 'GET /api/path/1/observations' do
    it 'renders a successful response' do
      path = FactoryBot.create :path, map: map
      FactoryBot.create :observation, subject: path
      get api_path_observations_url(path_id: path.id), headers: valid_headers, as: :json

      expect(response).to be_successful
    end
  end

  describe 'GET /api/observations/1' do
    it 'renders a successful response' do
      observation = Observation.create! valid_attributes
      get api_observation_url(observation), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /api/observations' do
    context 'with valid parameters' do
      it 'creates a new Observation' do
        expect do
          post api_observations_url, params: { observation: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Observation, :count).by(1)
      end

      it 'renders an successful response' do
        post api_observations_url, params: { observation: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'renders the new Observation as JSON' do
        post api_observations_url, params: { observation: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Observation' do
        expect do
          post api_observations_url, params: { observation: invalid_attributes }, as: :json
        end.not_to change(Observation, :count)
      end

      it 'renders an error response' do
        post api_observations_url, params: { observation: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        post api_observations_url, params: { observation: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'PUT /api/observations/1' do
    context 'with valid parameters' do # rubocop:disable RSpec/MultipleMemoizedHelpers
      let(:new_attributes) do
        { content: 'Something new' }
      end

      it 'updates the requested Observation' do
        observation = Observation.create! valid_attributes
        patch api_observation_url(observation), params: { observation: new_attributes }, headers: valid_headers, as: :json
        observation.reload
        expect(observation.content).to eq new_attributes[:content]
      end

      it 'renders a success response' do
        observation = Observation.create! valid_attributes
        patch api_observation_url(observation), params: { observation: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders a JSON response with the observation' do
        observation = Observation.create! valid_attributes
        patch api_observation_url(observation), params: { observation: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'renders an error response' do
        observation = Observation.create! valid_attributes
        patch api_observation_url(observation), params: { observation: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        observation = Observation.create! valid_attributes
        patch api_observation_url(observation), params: { observation: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'DELETE /api/observations/1' do
    it 'destroys the requested observation' do
      observation = Observation.create! valid_attributes
      expect do
        delete api_observation_url(observation), headers: valid_headers, as: :json
      end.to change(Observation, :count).by(-1)
    end
  end
end
