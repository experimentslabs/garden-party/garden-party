# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/resource_interactions_groups', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:resource_interactions_group).attributes
  end

  let(:invalid_attributes) do
    { name: '' }
  end

  let(:valid_headers) do
    {}
  end

  context 'with authenticated member' do
    include_context 'with authenticated member'
    describe 'GET /api/index' do
      it 'renders a successful response' do
        ResourceInteractionsGroup.create! valid_attributes
        get api_resource_interactions_groups_url, headers: valid_headers, as: :json
        expect(response).to be_successful
      end
    end

    describe 'GET /api/resource_interactions_groups/1' do
      it 'renders a successful response' do
        resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
        get api_resource_interactions_group_url(resource_interactions_group), as: :json
        expect(response).to be_successful
      end
    end

    describe 'POST /api/create' do
      context 'with valid parameters' do
        it 'creates a new ResourceInteractionsGroup' do
          expect do
            post api_resource_interactions_groups_url, params: { resource_interactions_group: valid_attributes }, headers: valid_headers, as: :json
          end.to change(ResourceInteractionsGroup, :count).by(1)
        end

        it 'renders an successful response' do
          post api_resource_interactions_groups_url, params: { resource_interactions_group: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it 'renders the new ResourceInteractionsGroup as JSON' do
          post api_resource_interactions_groups_url, params: { resource_interactions_group: valid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new ResourceInteractionsGroup' do
          expect do
            post api_resource_interactions_groups_url, params: { resource_interactions_group: invalid_attributes }, as: :json
          end.not_to change(ResourceInteractionsGroup, :count)
        end

        it 'renders an error response' do
          post api_resource_interactions_groups_url, params: { resource_interactions_group: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'renders the error as JSON' do
          post api_resource_interactions_groups_url, params: { resource_interactions_group: invalid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end
    end

    describe 'PUT /api/resource_interactions_groups/1' do
      context 'with valid parameters' do
        let(:new_attributes) do
          { name: 'New name' }
        end

        it 'updates the requested ResourceInteractionsGroup' do
          resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
          patch api_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: new_attributes }, headers: valid_headers, as: :json
          resource_interactions_group.reload
          expect(resource_interactions_group.name).to eq new_attributes[:name]
        end

        it 'renders a succes response' do
          resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
          patch api_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: new_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:ok)
        end

        it 'renders a JSON response with the resource_interactions_group' do
          resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
          patch api_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: new_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end

      context 'with invalid parameters' do
        it 'renders an error response' do
          resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
          patch api_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'renders the error as JSON' do
          resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
          patch api_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: invalid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'
    describe 'DELETE /api/resource_interactions_groups/1' do
      it 'destroys the requested resource_interactions_group' do
        resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
        expect do
          delete api_resource_interactions_group_url(resource_interactions_group), headers: valid_headers, as: :json
        end.to change(ResourceInteractionsGroup, :count).by(-1)
      end
    end
  end
end
