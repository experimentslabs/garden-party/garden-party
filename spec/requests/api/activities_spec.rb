# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/activities', type: :request do
  let(:map) { FactoryBot.create :map, user: signed_in_user }
  let(:valid_attributes) do
    layer = map.layers.first

    FactoryBot.build(:activity, subject: layer, action: 'test', user: signed_in_user).attributes
  end

  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/maps/1/all_activities' do
    it 'renders a successful response' do
      Activity.create! valid_attributes
      get api_map_all_activities_url(map_id: map.id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/elements/1/activities' do
    it 'renders a successful response' do
      element = FactoryBot.create :element, :point, map: map
      get api_element_activities_url(element_id: element.id), headers: valid_headers, as: :json

      expect(response).to be_successful
    end
  end

  describe 'GET /api/patch/1/activities' do
    it 'renders a successful response' do
      patch = FactoryBot.create :patch, map: map
      get api_patch_activities_url(patch_id: patch.id), headers: valid_headers, as: :json

      expect(response).to be_successful
    end
  end

  describe 'GET /api/path/1/activities' do
    it 'renders a successful response' do
      path = FactoryBot.create :path, map: map
      get api_path_activities_url(path_id: path.id), headers: valid_headers, as: :json

      expect(response).to be_successful
    end
  end

  describe 'GET /api/activities/1' do
    it 'renders a successful response' do
      activity = Activity.create! valid_attributes
      get api_activity_url(activity), as: :json
      expect(response).to be_successful
    end
  end
end
