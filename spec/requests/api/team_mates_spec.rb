# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/team_mates', type: :request do
  include_context 'with authenticated member'

  let(:valid_attributes) do
    map = FactoryBot.create :map, user: signed_in_user
    FactoryBot.build(:team_mate, map: map).attributes
  end

  let(:invalid_attributes) do
    valid_attributes.merge user_id: nil
  end

  let(:valid_headers) do
    {}
  end

  describe 'GET /api/maps/1/index' do
    it 'renders a successful response' do
      team_mate = TeamMate.create! valid_attributes
      get api_map_team_mates_url(map_id: team_mate.map_id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/team_mates/1' do
    it 'renders a successful response' do
      team_mate = TeamMate.create! valid_attributes
      get api_team_mate_url(team_mate), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /api/create' do
    context 'with valid parameters' do
      it 'creates a new TeamMate' do
        # Create the map and original TeamMate
        valid_attributes

        expect do
          post api_team_mates_url, params: { team_mate: valid_attributes }, headers: valid_headers, as: :json
        end.to change(TeamMate, :count).by(1)
      end

      it 'renders an successful response' do
        post api_team_mates_url, params: { team_mate: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'renders the new TeamMate as JSON' do
        post api_team_mates_url, params: { team_mate: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new TeamMate' do
        # Create the map and original TeamMate
        invalid_attributes

        expect do
          post api_team_mates_url, params: { team_mate: invalid_attributes }, as: :json
        end.not_to change(TeamMate, :count)
      end

      it 'renders an error response' do
        post api_team_mates_url, params: { team_mate: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        post api_team_mates_url, params: { team_mate: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'PATCH /api/team_mates/1/accept' do
    let(:team_mate) { FactoryBot.create :team_mate, user: signed_in_user }

    it 'accepts the invitation' do
      patch accept_api_team_mate_url(team_mate), headers: valid_headers, as: :json
      team_mate.reload
      expect(team_mate.accepted_at).not_to be_nil
    end

    it 'renders an successful response' do
      patch accept_api_team_mate_url(team_mate), headers: valid_headers, as: :json
      expect(response).to have_http_status(:ok)
    end

    it 'renders the new TeamMate as JSON' do
      patch accept_api_team_mate_url(team_mate), headers: valid_headers, as: :json
      expect(response.content_type).to match(a_string_including('application/json'))
    end
  end

  describe 'DELETE /api/team_mates/1/refuse' do
    it 'destroys the invitation' do
      team_mate = FactoryBot.create :team_mate, user: signed_in_user
      expect do
        delete refuse_api_team_mate_url(team_mate), headers: valid_headers, as: :json
      end.to change(TeamMate, :count).by(-1)
    end
  end

  describe 'DELETE /api/team_mates/1/leave' do
    it 'destroys the requested team_mate' do
      team_mate = FactoryBot.create :team_mate, user: signed_in_user
      expect do
        delete leave_api_team_mate_url(team_mate), headers: valid_headers, as: :json
      end.to change(TeamMate, :count).by(-1)
    end
  end

  describe 'DELETE /api/team_mates/1/remove' do
    it 'destroys the requested team_mate' do
      map = FactoryBot.create :map, user: signed_in_user
      team_mate = FactoryBot.create :team_mate, map: map
      expect do
        delete remove_api_team_mate_url(team_mate), headers: valid_headers, as: :json
      end.to change(TeamMate, :count).by(-1)
    end
  end
end
