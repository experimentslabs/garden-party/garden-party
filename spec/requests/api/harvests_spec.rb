# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/harvests', type: :request do
  let(:map) { FactoryBot.create :map, user: signed_in_user }
  let(:element) { FactoryBot.create :element, :point, map: map }

  let(:valid_attributes) do
    FactoryBot.build(:harvest, element: element).attributes
  end

  let(:invalid_attributes) do
    { unit: nil, element_id: element.id }
  end

  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/maps/:map_id/all_harvests' do
    it 'renders a successful response' do
      Harvest.create! valid_attributes
      get api_map_all_harvests_url(map_id: map.id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/elements/:element_id/harvests' do
    it 'renders a successful response' do
      Harvest.create! valid_attributes
      get api_element_harvests_url(element_id: element.id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/harvests/1' do
    it 'renders a successful response' do
      harvest = Harvest.create! valid_attributes
      get api_harvest_url(harvest), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /api/create' do
    context 'with valid parameters' do
      it 'creates a new Harvest' do
        expect do
          post api_harvests_url, params: { harvest: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Harvest, :count).by(1)
      end

      it 'renders an successful response' do
        post api_harvests_url, params: { harvest: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'renders the new Harvest as JSON' do
        post api_harvests_url, params: { harvest: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Harvest' do
        expect do
          post api_harvests_url, params: { harvest: invalid_attributes }, as: :json
        end.not_to change(Harvest, :count)
      end

      it 'renders an error response' do
        post api_harvests_url, params: { harvest: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        post api_harvests_url, params: { harvest: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'PUT /api/harvests/1' do
    context 'with valid parameters' do # rubocop:disable RSpec/MultipleMemoizedHelpers
      let(:new_attributes) do
        { quantity: 10 }
      end

      it 'updates the requested Harvest' do
        harvest = Harvest.create! valid_attributes
        patch api_harvest_url(harvest), params: { harvest: new_attributes }, headers: valid_headers, as: :json
        harvest.reload
        expect(harvest.quantity).to eq new_attributes[:quantity]
      end

      it 'renders a succes response' do
        harvest = Harvest.create! valid_attributes
        patch api_harvest_url(harvest), params: { harvest: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders a JSON response with the harvest' do
        harvest = Harvest.create! valid_attributes
        patch api_harvest_url(harvest), params: { harvest: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'renders an error response' do
        harvest = Harvest.create! valid_attributes
        patch api_harvest_url(harvest), params: { harvest: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        harvest = Harvest.create! valid_attributes
        patch api_harvest_url(harvest), params: { harvest: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'DELETE /api/harvests/1' do
    it 'destroys the requested harvest' do
      harvest = Harvest.create! valid_attributes
      expect do
        delete api_harvest_url(harvest), headers: valid_headers, as: :json
      end.to change(Harvest, :count).by(-1)
    end
  end
end
