# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/notifications', type: :request do
  include_context 'with authenticated member'

  let(:valid_attributes) do
    FactoryBot.build(:notification, recipient: signed_in_user).attributes
  end

  let(:invalid_attributes) do
    { archived_at: 'abdcef' }
  end

  let(:valid_headers) do
    {}
  end

  describe 'GET /' do
    it 'renders a successful response' do
      Notification.create! valid_attributes
      get api_notifications_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /1' do
    it 'renders a successful response' do
      notification = Notification.create! valid_attributes
      get api_notification_url(notification), as: :json
      expect(response).to be_successful
    end
  end

  describe 'PATCH /1/archive' do
    context 'with valid parameters' do
      let(:new_attributes) do
        { archived_at: Time.current }
      end

      it 'updates the requested Notification' do
        notification = Notification.create! valid_attributes
        patch archive_api_notification_url(notification), headers: valid_headers, as: :json
        notification.reload
        expect(notification.archived_at).not_to be_nil
      end

      it 'renders a success response' do
        notification = Notification.create! valid_attributes
        patch archive_api_notification_url(notification), params: { notification: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders a JSON response with the notification' do
        notification = Notification.create! valid_attributes
        patch archive_api_notification_url(notification), params: { notification: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe 'PATCH /archive_all' do
    before do
      FactoryBot.create_list :notification, 3, recipient: signed_in_user
      FactoryBot.create_list :notification, 3, recipient: signed_in_user, archived_at: Time.current
    end

    it 'marks all unread notifications as archived' do
      patch archive_all_api_notifications_url, headers: valid_headers, as: :json
      expect(Notification.archived.count).to eq(6)
    end

    it 'renders a success response' do
      patch archive_all_api_notifications_url, headers: valid_headers, as: :json
      expect(response).to have_http_status(:ok)
    end

    it 'renders a JSON response with the updated notifications' do
      patch archive_all_api_notifications_url, headers: valid_headers, as: :json
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end
  end

  describe 'DELETE /destroy_all_archived' do
    before do
      FactoryBot.create_list :notification, 3, recipient: signed_in_user
      FactoryBot.create_list :notification, 3, recipient: signed_in_user, archived_at: Time.current
    end

    it 'marks all unread notifications as archived' do
      expect do
        delete destroy_all_archived_api_notifications_url, headers: valid_headers, as: :json
      end.to change(Notification, :count).by(-3)
    end

    it 'renders a success response' do
      delete destroy_all_archived_api_notifications_url, headers: valid_headers, as: :json
      expect(response).to have_http_status(:ok)
    end

    it 'renders a JSON response with all the notifications' do
      delete destroy_all_archived_api_notifications_url, headers: valid_headers, as: :json
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end
  end

  describe 'DELETE /1' do
    it 'destroys the requested notification' do
      notification = Notification.create! valid_attributes
      expect do
        delete api_notification_url(notification), headers: valid_headers, as: :json
      end.to change(Notification, :count).by(-1)
    end
  end
end
