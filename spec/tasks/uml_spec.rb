# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'uml: tasks' do
  describe 'uml:models' do
    it 'does not fail' do
      expect do
        Rake::Task['uml:models'].invoke
      end.not_to raise_exception
    end
  end
end
