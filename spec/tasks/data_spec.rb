# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'data: tasks' do
  describe 'data:export' do
    let(:export_file) { Rails.root.join 'tmp', 'test_export.yml' }

    old_output_value = nil

    before do
      old_output_value = ENV.fetch('OUTPUT', nil)
      ENV['OUTPUT']    = export_file.to_s
      FileUtils.rm_f export_file
      FactoryBot.create_list :resource_interaction, 5
    end

    after do
      ENV['OUTPUT'] = old_output_value
      FileUtils.rm_f export_file
    end

    it 'does not fail' do
      expect do
        Rake::Task['data:export'].invoke
      end.not_to raise_exception
    end
  end
end
