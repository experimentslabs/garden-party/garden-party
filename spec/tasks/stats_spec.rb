# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'stats: tasks' do
  describe 'stats:show' do
    before do
      FactoryBot.create_list :element, 2, :point
    end

    it 'does not fail' do
      expect do
        Rake::Task['stats:show'].invoke
      end.not_to raise_exception
    end
  end
end
