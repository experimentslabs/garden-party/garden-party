# frozen_string_literal: true

require 'rails_helper'
# require 'rake'

RSpec.describe 'icons: tasks' do
  describe 'icons:generate' do
    let(:export_file) { Rails.root.join 'tmp', 'test_icons.svg' }

    old_output_value = nil

    before do
      old_output_value = ENV.fetch('OUTPUT', nil)
      ENV['OUTPUT']    = export_file.to_s

      FileUtils.rm_f export_file
      FactoryBot.create_list :resource, 10
    end

    after do
      ENV['OUTPUT'] = old_output_value

      FileUtils.rm_f export_file
    end

    it 'does not fail' do
      expect do
        Rake::Task['icons:generate'].invoke
      end.not_to raise_exception
    end
  end
end
