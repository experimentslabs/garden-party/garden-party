# Readme

Garden Party is an OpenSource application to help gardening newbies to manage their garden.

- [Presentation website](https://garden-party.io)
- [Matrix chatrooms](https://app.element.io/#/room/#garden-party:matrix.org)
  - [general discussion](https://matrix.to/#/#gp:matrix.org)
  - [development/technical discussions](https://matrix.to/#/#gp-dev:matrix.org)
- [Decision board](https://framavox.org/garden-party/) (Lumio on Framavox; create an account _before_ joining the group)

Known open instances:

- [French instance](https://garden-party.experimentslabs.com)

We're mostly francophones, but we can handle english discussions.

## Documentation

These documents can also be viewed on the [online technical documentation](https://doc.garden-party.io).

- Community-related:
  - [Code of conduct](CODE_OF_CONDUCT.md)
- Development-related:
  - [Development documentation](docs/development.md) - Tooling, tasks, ...
  - [Contribution guide](CONTRIBUTING.md)
- Production-related:
  - [Migration guide](MIGRATION_GUIDE.md) - Upgrade instructions, one version at a time
  - [Production documentation](docs/production.md)
  - [Docker](docs/production-docker.md)
  - [Instance maintenance](docs/maintenance.md) - Maintenance tips

If you only want to test the application, the easiest way it to use the [Docker](docs/production-docker.md) image.

## Licences

- The code is under the [MIT](LICENCE) license.
- Seeds data comes from
  [Garden Party Data repository](https://gitlab.com/experimentslabs/garden-party/data),
  and is under the [CC-0](LICENCE_CC0) license.
- Images in `app/assets/images` and `src/icons` are crafted with love for Garden
  Party, under the [CC-0](LICENCE_CC0). Exception is made for
  `app/assets/images/icons.svg` which is a generated compilation of symbols from
  [PhosphorIcons](https://github.com/phosphor-icons/phosphor-home) and Garden
  Party specific symbols. PhosphorIcons symbols are under the MIT licence.
  - `app/assets/images/osm_map.svg` is based on [a file from Wikimedia](https://commons.wikimedia.org/wiki/File:Shokunin_world_map_more_detail.svg),
   under the [CC-0](LICENCE_CC0) license.
