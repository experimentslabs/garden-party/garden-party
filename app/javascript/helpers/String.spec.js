import { describe, expect, it } from 'vitest'
import { underscore, upCaseFirst } from './String'

describe('string methods', () => {
  describe('.underscore', () => {
    it('converts CamelCased strings to underscored_ones', () => {
      expect.assertions(1)

      expect(underscore('CamelCased')).toBe('camel_cased')
    })
  })
  describe('.upCaseFirst', () => {
    it('upcases string with locale support', () => {
      expect.assertions(1)

      expect(upCaseFirst('italya', 'tr')).toBe('İtalya')
    })
  })
})
