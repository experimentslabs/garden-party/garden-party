import { describe, beforeEach, it, expect } from 'vitest'
import Resource from '../vue/classes/models/Resource'
import { resourceFactory } from '../specs/factories/resources'
import { RESOURCE_CROP_ROTATION_GROUPS } from '../helpers/constants'
import {
  determineNextGroupsFor,
  nextRotationGroup,
  rotationGroupsFor,
  rotationGroupsWithCountFor,
} from './RotationGroups'

describe('RotationGroups', () => {
  let elements
  beforeEach(() => {
    elements = [
      { resource: new Resource(resourceFactory.build({ crop_rotation_group: RESOURCE_CROP_ROTATION_GROUPS[0] })) },
      { resource: new Resource(resourceFactory.build({ crop_rotation_group: RESOURCE_CROP_ROTATION_GROUPS[0] })) },
      { resource: new Resource(resourceFactory.build({ crop_rotation_group: RESOURCE_CROP_ROTATION_GROUPS[3] })) },
      { resource: new Resource(resourceFactory.build({ crop_rotation_group: RESOURCE_CROP_ROTATION_GROUPS[2] })) },
    ]
  })

  describe('rotationGroupsFor', () => {
    it('extracts the rotation groups names', () => {
      expect.assertions(1)

      expect(rotationGroupsFor(elements)).toStrictEqual([RESOURCE_CROP_ROTATION_GROUPS[0], RESOURCE_CROP_ROTATION_GROUPS[3], RESOURCE_CROP_ROTATION_GROUPS[2]])
    })
  })

  describe('rotationGroupsWithCountFor', () => {
    it('extracts the groups with a percentage', () => {
      expect.assertions(1)

      const expected = {}
      expected[RESOURCE_CROP_ROTATION_GROUPS[0]] = 50
      expected[RESOURCE_CROP_ROTATION_GROUPS[3]] = 25
      expected[RESOURCE_CROP_ROTATION_GROUPS[2]] = 25

      expect(rotationGroupsWithCountFor(elements)).toStrictEqual(expected)
    })
  })
  describe('nextRotationGroup', () => {
    it('returns the right group', () => {
      expect.assertions(2)

      expect(nextRotationGroup(RESOURCE_CROP_ROTATION_GROUPS[0])).toStrictEqual(RESOURCE_CROP_ROTATION_GROUPS[1])
      expect(nextRotationGroup(RESOURCE_CROP_ROTATION_GROUPS[RESOURCE_CROP_ROTATION_GROUPS.length - 1])).toStrictEqual(RESOURCE_CROP_ROTATION_GROUPS[0])
    })
  })
  describe('determineNextGroupsFor', () => {
    it('lists the next groups for entries with the defined threshold', () => {
      expect.assertions(1)

      const expected = {}
      expected[RESOURCE_CROP_ROTATION_GROUPS[1]] = 50
      expect(determineNextGroupsFor(elements)).toStrictEqual(expected)
    })
  })
})
