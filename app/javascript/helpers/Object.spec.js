import { describe, expect, it } from 'vitest'
import { compactObject } from './Object'

describe('compactObject', () => {
  it('removes nulls and undefineds', () => {
    expect.assertions(1)

    const subject = { a: true, b: 1, c: null, d: undefined, e: 'string' }
    const expected = { a: true, b: 1, e: 'string' }
    expect(compactObject(subject)).toStrictEqual(expected)
  })

  it('does not operate recursively', () => {
    expect.assertions(1)

    const subject = { a: 'value', b: { a: null, b: { a: undefined } }, c: null }
    const expected = { a: 'value', b: { a: null, b: { a: undefined } } }
    expect(compactObject(subject)).toStrictEqual(expected)
  })
})
