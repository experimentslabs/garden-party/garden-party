import { RESOURCE_CROP_ROTATION_GROUPS } from './constants'

/**
 * @typedef {import('../vue/classes/models/Element.js')} Element
 */

/**
 * Extracts the crop rotation group from a list of elements
 *
 * @param   {Element[]} elements - List of elements
 * @returns {string[]}           - List of crop rotation groups
 */
export function rotationGroupsFor (elements) {
  const groups = new Set()
  elements.forEach((e) => {
    if (!e.resource.cropRotationGroup) return

    groups.add(e.resource.cropRotationGroup)
  })

  return [...groups]
}

/**
 * Extracts the crop rotation groups from a list of elements, with the amount of each group in it, in percentage
 *
 * @param   {Element[]} elements - List of elements
 * @returns {object}             - Hash with rotation groups as keys and percentage of presence as value
 */
export function rotationGroupsWithCountFor (elements) {
  const groups = {}
  let total = 0
  elements.forEach((e) => {
    if (!e.resource.cropRotationGroup) return

    groups[e.resource.cropRotationGroup] = groups[e.resource.cropRotationGroup] || 0
    groups[e.resource.cropRotationGroup] += 1
    total += 1
  })

  Object.keys(groups).forEach(g => { groups[g] = Math.round(groups[g] * 100 / total) })

  return groups
}

/**
 * Gets the next rotation group for the given one
 *
 * @param   {string} group - A rotation group
 * @returns {string}       - The next rotation group
 */
export function nextRotationGroup (group) {
  if (!RESOURCE_CROP_ROTATION_GROUPS.includes(group)) throw new Error(`Unknown crop rotation group group ${group}`)

  const index = RESOURCE_CROP_ROTATION_GROUPS.findIndex(entry => entry === group)

  if (index === RESOURCE_CROP_ROTATION_GROUPS.length - 1) return RESOURCE_CROP_ROTATION_GROUPS[0]
  return RESOURCE_CROP_ROTATION_GROUPS[index + 1]
}

/**
 * Determines the next rotation groups given a set of elements.
 *
 * If many groups are present in the initial set, they are filtered out based on the minimum percentage of presence.
 *
 * @param   {Element[]} elements   - List of elements
 * @param   {number}    minPercent - Minimum presence percentage to keep a group
 * @returns {object}               Hash with groups as key and their percentage as value
 */
export function determineNextGroupsFor (elements, minPercent = 33) {
  const amounts = rotationGroupsWithCountFor(elements)
  const groups = Object.keys(amounts)

  const results = {}
  groups.filter(g => amounts[g] >= minPercent).forEach(g => { results[nextRotationGroup(g)] = amounts[g] })

  return results
}
