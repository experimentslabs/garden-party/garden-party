/**
 * Creates an array of given length filled with given value
 *
 * @param   {number} length - Array length
 * @param   {any}    value  - Default value
 * @returns {any[]}         The new array
 */
export function filledArray (length, value) {
  return Array(length).fill(value, 0, length)
}

/**
 * Picks a random entry from given array
 *
 * @param   {Array} array - Source list
 * @returns {*}           - Random element
 */
export function randomEntry (array) {
  return array[Math.floor(Math.random() * array.length)]
}

/**
 * Replaces or inserts an item in an array of items
 *
 * The array is modified in place.
 *
 * @param   {object[]} array               - Array to modify
 * @param   {object}   item                - Item to add or update
 * @param   {object}   options             - Additional options
 * @param   {?string}  options.key         - Comparison key
 * @param   {?string}  options.newPosition - Where to put the new item: `start` or `end` (default: `start`)
 *
 * @returns {object[]}                     The updated array
 */
export function updateOrReplaceObject (array, item, { key, newPosition } = {}) {
  key = key || 'id'
  newPosition = newPosition || 'start'

  const index = array.findIndex((entry) => entry[key] === item[key])
  if (index > -1) array[index] = item
  else if (newPosition === 'start') array.unshift(item)
  else if (newPosition === 'end') array.push(item)

  return array
}

/**
 * Removes an object from an array
 *
 * @param   {object[]} array  - The list
 * @param   {object}   object - The object to remove
 * @param   {string}   [key]  - Optional key for comparison. Defaults to `id`.
 *
 * @returns {object[]}        The new list
 */
export function removeObject (array, object, key = 'id') {
  const index = array.findIndex(h => h[key] === object[key])
  if (index > -1) array.splice(index, 1)

  return array
}

/**
 * Removes an object from an array if a key matches a value
 *
 * @param   {object[]} array    - The list
 * @param   {*}        keyValue - The value to find the entry
 * @param   {string}   [key]    - Optional key for comparison. Defaults to `id`.
 *
 * @returns {object[]}          The new list
 */
export function removeObjectByKey (array, keyValue, key = 'id') {
  const index = array.findIndex(h => h[key] === keyValue)
  if (index > -1) array.splice(index, 1)

  return array
}
