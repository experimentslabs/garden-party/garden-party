/**
 * Removes keys with null/undefined values from object
 *
 * @param   {object} object - Object to filter
 * @returns {object}        Object without the null/undefined values
 */
export function compactObject (object) {
  const newObject = {}

  for (const key in object) {
    if (object[key] !== null && object[key] !== undefined) newObject[key] = object[key]
  }

  return newObject
}
