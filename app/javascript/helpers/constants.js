/*
  Constants and enums list generated with "rake js:generate:constants".
  Source template is in "lib/classes/front_generator/templates/constants.js.erb"

  Do not edit manually.
 */

/* ---------------------------------------------------------------------------------------------------------------------
 Enums
 -------------------------------------------------------------------------------------------------------------------- */

// I18n keys: 'activerecord.attributes.element/implantation_mode.<value>'
export const ELEMENT_IMPLANTATION_MODES = ['soil_sowing', 'potted_sowing', 'potted_shoot', 'shoot_in_soil']

// I18n keys: 'activerecord.attributes.family/kingdom.<value>'
export const FAMILY_KINGDOMS = ['plant', 'animal']

// I18n keys: 'activerecord.attributes.harvest/unit.<value>'
export const HARVEST_UNITS = ['gram', 'piece', 'litre']

// I18n keys: 'activerecord.attributes.resource/crop_rotation_group.<value>'
export const RESOURCE_CROP_ROTATION_GROUPS = ['leaf', 'manure', 'legume', 'root']

// I18n keys: 'activerecord.attributes.resource_interaction/nature.<value>'
export const RESOURCE_INTERACTION_NATURES = ['competition', 'mutualism', 'neutralism', 'parasitism', 'amensalism', 'commensalism', 'unspecified_positive', 'unspecified_negative']

// Rotation groups
// I18n keys: 'activerecord.attributes.resource/crop_rotation_group.<value>'
export const ROTATION_GROUP_ORDER = ['leaf', 'manure', 'legume', 'root']
export const ROTATION_GROUP_POSITIONS = { leaf: 0, manure: 1, legume: 2, root: 3 }
