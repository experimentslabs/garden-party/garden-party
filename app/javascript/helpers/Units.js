/**
 * @typedef Converter
 * @type {object}
 * @property {string}                   unit    - Target unit
 * @property {function(number): number} convert - Converts value to target unit
 */

/**
 * @type {{[key: string]: Converter}}
 * @see https://metricsystem.net
 * @see https://metricsystem.net/non-si-units/accepted-for-use-with-si/
 */
export const converters = {
  // i18n-tasks-use t('js.unit.ton')
  ton: {
    symbol: 't',
    unit: 'gram',
    convert: v => v * 1000 * 1000,
  },
  // i18n-tasks-use t('js.unit.kilogram')
  kilogram: {
    symbol: 'kg',
    unit: 'gram',
    convert: v => v * 1000,
  },
  // i18n-tasks-use t('js.unit.gram')
  gram: {
    symbol: 'g',
    unit: 'gram',
    convert: v => v,
  },
  // i18n-tasks-use t('js.unit.piece')
  piece: {
    symbol: 'pce',
    unit: 'piece',
    convert: v => v,
  },
  // i18n-tasks-use t('js.unit.centilitre')
  centilitre: {
    symbol: 'cl',
    unit: 'litre',
    convert: v => v / 100,
  },
  // i18n-tasks-use t('js.unit.millilitre')
  millilitre: {
    symbol: 'ml',
    unit: 'litre',
    convert: v => v / 1000,
  },
  // i18n-tasks-use t('js.unit.litre')
  litre: {
    symbol: 'l',
    unit: 'litre',
    convert: v => v,
  },
  // i18n-tasks-use t('js.unit.cubic_meter')
  cubic_meter: {
    symbol: 'm3',
    unit: 'litre',
    convert: v => v * 1000,
  },
}

export const Units = Object.keys(converters)

/** Hash of unit: symbol */
export const unitsSymbols = {}
Units.forEach(key => { unitsSymbols[key] = converters[key].symbol })

/** @typedef {{unit: string, value: number}} UnitAndValue */

/**
 * Converts litres to a more appropriate unit
 *
 * @param   {number}       value - Value in litre
 * @returns {UnitAndValue}       - Object with new value and unit
 */
export function convertVolume (value) {
  if ((Math.abs(value)) / 1000 > 1) return { value: value / 1000, unit: 'cubic_meter' }
  else if ((Math.abs(value)) > 1) return { value, unit: 'litre' }
  else if ((Math.abs(value)) * 100 > 1) return { value: value * 100, unit: 'centilitre' }

  return { value: value * 1000, unit: 'millilitre' }
}

/**
 * Converts grams to a more appropriate unit
 *
 * @param   {number}       value - Value in grams
 * @returns {UnitAndValue}       - Object with new value and unit
 */
export function convertWeight (value) {
  if (Math.abs(value) / 1000000 >= 1) return { value: value / 1000000, unit: 'ton' }
  else if (Math.abs(value) / 1000 >= 1) return { value: value / 1000, unit: 'kilogram' }

  return { value, unit: 'gram' }
}

/**
 * Converts grams, litres and pieces to a more readable value
 *
 * @param   {number}                 value - Base value in grams, litres or pieces
 * @param   {'piece'|'litre'|'gram'} unit  - Base unit
 * @returns {UnitAndValue}                 - Object with new value and unit
 */
export function convertUnit (value, unit) {
  switch (unit) {
    case 'piece':
      return { value, unit }
    case 'litre':
      return convertVolume(value)
    case 'gram':
      return convertWeight(value)
  }
}
