import { describe, expect, it } from 'vitest'
import prettyLength from './pretty_length'

describe('prettyLength', () => {
  it('adds the appropriate unit', () => {
    expect.assertions(4)

    expect(prettyLength(1)).toBe('1m')
    expect(prettyLength(0.1)).toBe('10cm')
    expect(prettyLength(0.01)).toBe('1cm')
    expect(prettyLength(0.001)).toBe('1mm')
  })
})
