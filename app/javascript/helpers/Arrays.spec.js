import { describe, expect, it, beforeEach } from 'vitest'
import { filledArray, randomEntry, updateOrReplaceObject, removeObject, removeObjectByKey } from './Arrays'

/**
 * Generates a list of objects
 *
 * @param   {number}   amount - Amount of items in the list
 *
 * @returns {object[]}        The list
 */
function arrayOfSimpleObjects (amount = 5) {
  const array = []
  for (let i = 1; i <= amount; i++) array.push({ id: i, name: `Name ${i}` })

  return array
}

describe('filledArray', () => {
  it('initializes the array with the same value', () => {
    expect.assertions(6)

    const result = filledArray(5, 'abcd')
    expect(result).toHaveLength(5)
    result.forEach((r) => {
      expect(r).toBe('abcd')
    })
  })
})

describe('randomEntry', () => {
  it('returns a random entry from an array', () => {
    expect.assertions(1)

    const array = [1, 2, 3, 4, 5]
    expect(array).toContain(randomEntry(array))
  })
})

describe('updateOrReplaceObject', () => {
  let array

  beforeEach(() => {
    array = arrayOfSimpleObjects()
  })

  it('replaces updated objects', () => {
    expect.assertions(2)

    updateOrReplaceObject(array, { id: 1, name: 'New name' })

    expect(array[0].name).toBe('New name')
    expect(array).toHaveLength(5)
  })

  it('prepends new objects', () => {
    expect.assertions(2)

    const object = { id: 6, name: 'New name' }
    updateOrReplaceObject(array, object, { newPosition: 'start' })

    expect(array[0]).toStrictEqual(object)
    expect(array).toHaveLength(6)
  })

  it('appends new objects', () => {
    expect.assertions(2)

    const object = { id: 6, name: 'New name' }
    updateOrReplaceObject(array, object, { newPosition: 'end' })

    expect(array[5]).toStrictEqual(object)
    expect(array).toHaveLength(6)
  })

  it('has a configurable key', () => {
    expect.assertions(2)

    const object = { id: 10, name: 'Name 1' }
    updateOrReplaceObject(array, object, { key: 'name' })

    expect(array[0]).toStrictEqual(object)
    expect(array).toHaveLength(5)
  })
})

describe('removeObject', () => {
  let array

  beforeEach(() => {
    array = arrayOfSimpleObjects()
  })

  describe('when object exists', () => {
    it('removes the object', () => {
      expect.assertions(2)

      removeObject(array, { id: 1, name: 'whatever' })

      expect(array).toHaveLength(4)
      expect(array.findIndex(o => o.id === 1)).toBe(-1)
    })
  })

  describe('when object does not exists', () => {
    it('does not remove anything', () => {
      expect.assertions(1)

      removeObject(array, { id: 0, name: 'whatever' })

      expect(array).toHaveLength(5)
    })
  })

  it('handles custom key', () => {
    expect.assertions(2)

    removeObject(array, { id: 'whatever', name: 'Name 1' }, 'name')

    expect(array).toHaveLength(4)
    expect(array.findIndex(o => o.name === 'Name 1')).toBe(-1)
  })
})

describe('removeObjectByKey', () => {
  let array

  beforeEach(() => {
    array = arrayOfSimpleObjects()
  })

  describe('when object exists', () => {
    it('removes the object', () => {
      expect.assertions(2)

      removeObjectByKey(array, 'Name 1', 'name')

      expect(array).toHaveLength(4)
      expect(array.findIndex(o => o.name === 'Name 1')).toBe(-1)
    })
  })

  describe('when object does not exists', () => {
    it('does not remove anything', () => {
      expect.assertions(1)

      removeObjectByKey(array, 100, 'id')

      expect(array).toHaveLength(5)
    })
  })
})
