const base = {
  Point: 'point',
  Polygon: 'polygon',
  Circle: 'circle',
  LineString: 'multiline',
  Virtual: 'virtual',
}
export const shapeIcons = {}
Object.keys(base).forEach(k => {
  shapeIcons[k] = {
    base: base[k],
    add: `${base[k]}-add`,
    edit: `${base[k]}-edit`,
  }
})

/**
 * Gets the geometry icon for the given type
 *
 * @param   {string} geometryType - Type
 * @returns {string}              Icon name
 */
export function geometryIcon (geometryType) {
  return shapeIcons[geometryType].base
}
