import Base from './Base'
import ResourceNote from '../../classes/models/ResourceNote'

export default {
  state: {
    /** @type {ResourceNote[]} */
    resourceNotes: [],
  },
  mutations: {
    resetResourceNotes: Base.mutations.resetState('resourceNotes'),
    setResourceNote: Base.mutations.setEntity('resourceNotes'),
    deleteResourceNote: Base.mutations.deleteEntity('resourceNotes'),
  },
  actions: {
    loadResourceNotes: Base.actions.loadEntities(ResourceNote, 'setResourceNote'),
    loadResourceNote: Base.actions.loadEntity(ResourceNote, 'setResourceNote'),
    createResourceNote: Base.actions.createEntity(ResourceNote, 'setResourceNote', 'resourceNote'),
    updateResourceNote: Base.actions.updateEntity(ResourceNote, 'setResourceNote', 'resourceNote'),
    saveResourceNote: Base.actions.saveEntity('createResourceNote', 'updateResourceNote'),
    destroyResourceNote: Base.actions.destroyEntity(ResourceNote, 'deleteResourceNote'),
  },
  getters: {
    resourceNotes: state => state.resourceNotes
      .sort((a, b) => b.updatedAt - a.updatedAt),
    resourceNotesForResource: (_state, getters) => resourceId => getters.resourceNotes.filter(note => note.resourceId === resourceId),
    resourceNote: Base.getters.entity('resourceNotes'),
    searchResourceNotes: Base.getters.search('resourceNotes', 'content'),
  },
}
