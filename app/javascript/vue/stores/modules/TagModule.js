import Base from './Base'
import { upCaseFirst } from '../../../helpers/String'

/**
 * Mutation to add tag to the state
 *
 * Extracted to be reusable
 *
 * @param {object} state - Current state
 * @param {object} tag   - Tag to add
 */
function setTag (state, tag) {
  const index = state.tags.findIndex((t) => t.base === tag.base)
  if (index > -1) {
    // Merge values
    state.tags[index].name = tag.name
    state.tags[index].items = new Set([...state.tags[index].items, ...tag.items])
  } else {
    state.tags.push(tag)
  }
}

export default {
  state: {
    /** @type {object[]} */
    tags: [],
  },
  mutations: {
    setTag,
    setTagString: (state, tag) => {
      const nibbles = tag.split('::')
      const base = nibbles.shift()
      const name = upCaseFirst(base)

      setTag(state, { name, base, items: new Set(nibbles) })
    },
  },
  getters: {
    tags: Base.getters.entities('tags', 'name'),
  },
}
