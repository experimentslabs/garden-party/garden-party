import Base from './Base'
import Observation from '../../classes/models/Observation'

export default {
  state: {
    /** @type {Observation[]} */
    observations: [],
  },
  mutations: {
    resetObservations: Base.mutations.resetState('observations'),
    setObservation: Base.mutations.setEntity('observations'),
    deleteObservation: Base.mutations.deleteEntity('observations'),
  },
  actions: {
    loadObservations: Base.actions.loadEntities(Observation, 'setObservation'),
    loadObservationsBySubject ({ commit }, subject) {
      return Observation.getObservationsForSubject(subject.className, subject.id)
        .then((observations) => {
          observations.forEach(observation => { commit('setObservation', observation) })
        })
    },
    loadObservationsByMap ({ commit }, mapId) {
      return Observation.getObservationsForMap(mapId)
        .then((observations) => {
          observations.forEach(observation => { commit('setObservation', observation) })
        })
    },
    loadObservation: Base.actions.loadEntity(Observation, 'setObservation'),
    createObservation ({ commit }, payload) {
      return Observation.create(payload)
        .then((item) => {
          commit('setObservation', item)
          // Update the subject's counter
          item.subject.observationsCount += 1

          // Return new instance
          return Promise.resolve(item)
        })
    },
    updateObservation: Base.actions.updateEntity(Observation, 'setObservation', 'observation'),
    saveObservation: Base.actions.saveEntity('createObservation', 'updateObservation'),
    destroyObservation: Base.actions.destroyEntity(Observation, 'deleteObservation'),
  },
  getters: {
    observation: Base.getters.entity('observations'),
    observations: state => state.observations
      .sort((a, b) => b.madeAt - a.madeAt),
    observationsBySubject: state => (subject) => state.observations
      .filter(a => a.subjectId === subject.id && a.subjectType === subject.className)
      .sort((a, b) => b.madeAt - a.madeAt),
    searchObservations: Base.getters.search('observations', 'title'),
  },
}
