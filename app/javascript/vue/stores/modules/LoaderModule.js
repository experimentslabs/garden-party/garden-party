import { randomString } from '@experiments-labs/rise_ui/src/lib/String.js'
import { removeObjectByKey } from '@/helpers/Arrays'

export default {
  state: {
    loaders: [],
  },
  mutations: {
    addLoader (state, { key, group, message }) {
      state.loaders.push({ key, group, message })
    },
    removeLoader (state, key) {
      removeObjectByKey(state.loaders, key, 'key')
    },
  },
  actions: {
    load ({ commit }, { promise, message, group = null }) {
      group = group || randomString()
      const key = randomString(8)

      commit('addLoader', { key, group, message })
      return promise
        .then((result) => {
          return Promise.resolve(result)
        })
        .catch((err) => Promise.reject(err))
        .then(() => { commit('removeLoader', key) })
    },
    loadGroup ({ dispatch }, { name, loaders }) {
      return Promise.all(loaders.map((l) => {
        dispatch('load', {
          promise: l.promise,
          message: l.message,
          group: name,
        })
        return l.promise
      }))
    },
  },
  getters: {
    loader: state => group => state.loaders.filter(l => l.group === group),
  },
}
