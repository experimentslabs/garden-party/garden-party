import Base from './Base'
import Map from '../../classes/models/Map'
import EventBus from '../../tools/EventBus'

export default {
  state: {
    /** @type {Map[]} */
    maps: [],
    /** @type {Map} */
    currentMap: null,
  },
  mutations: {
    resetMaps: Base.mutations.resetState('maps'),
    setMap: Base.mutations.setEntity('maps'),
    deleteMap: Base.mutations.deleteEntity('maps'),
    setCurrentMap: (state, map) => { state.currentMap = map },
  },
  actions: {
    loadMaps: ({ commit, getters }) => Map.getIndex()
      .then(items => {
        const newIds = []
        items.forEach(item => {
          newIds.push(item.id)
          commit('setMap', item)
        })
        const existingIds = getters.maps.map(m => m.id)

        // Remove absents
        existingIds.forEach((id) => {
          if (!newIds.includes(id)) commit('deleteMap', id)
        })

        return items
      }),
    loadMap: Base.actions.loadEntity(Map, 'setMap'),
    createMap: Base.actions.createEntity(Map, 'setMap', 'map'),
    updateMap ({ commit, getters }, payload) {
      return Map.update(payload)
        .then(item => {
          const oldMapHideBackground = getters.map(item.id).hideBackground

          commit('setMap', item)
          const newMap = getters.map(item.id)

          if (oldMapHideBackground !== newMap.hideBackground) {
            EventBus.emit('change-background-layer-visibility')
          }

          // Return updated instance
          return Promise.resolve(newMap)
        })
    },
    saveMap: Base.actions.saveEntity('createMap', 'updateMap'),
    destroyMap: Base.actions.destroyEntity(Map, 'deleteMap'),
    setCurrentMap: ({ commit, getters }, mapId) => commit('setCurrentMap', getters.map(mapId)),
  },
  getters: {
    maps: Base.getters.entities('maps', 'name'),
    ownedMaps: (_state, getters) => getters.maps.filter(map => map.userId === getters.account.id),
    otherMaps: (_state, getters) => getters.maps.filter(map => map.userId !== getters.account.id),
    map: Base.getters.entity('maps'),
    currentMap: state => state.currentMap,
    isMapOwner: (state, getters) => getters.currentMap && getters.currentMap?.userId === getters.account?.id,
  },
}
