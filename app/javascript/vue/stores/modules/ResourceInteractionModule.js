import Base from './Base'
import ResourceInteraction from '../../classes/models/ResourceInteraction'

export default {
  state: {
    /** @type {ResourceInteraction[]} */
    resourceInteractions: [],
  },
  mutations: {
    resetResourceInteractions: Base.mutations.resetState('resourceInteractions'),
    setResourceInteraction: Base.mutations.setEntity('resourceInteractions'),
    deleteResourceInteraction: Base.mutations.deleteEntity('resourceInteractions'),
  },
  actions: {
    loadResourceInteractions: Base.actions.loadEntities(ResourceInteraction, 'setResourceInteraction'),
    loadResourceInteraction: Base.actions.loadEntity(ResourceInteraction, 'setResourceInteraction'),
    createResourceInteraction: Base.actions.createEntity(ResourceInteraction, 'setResourceInteraction', 'resourceInteraction'),
    updateResourceInteraction: Base.actions.updateEntity(ResourceInteraction, 'setResourceInteraction', 'resourceInteraction'),
    saveResourceInteraction: Base.actions.saveEntity('createResourceInteraction', 'updateResourceInteraction'),
    destroyResourceInteraction: Base.actions.destroyEntity(ResourceInteraction, 'deleteResourceInteraction'),
  },
  getters: {
    resourceInteractions: state => state.resourceInteractions,
    resourceInteraction: Base.getters.entity('resourceInteractions'),
    resourceInteractionsByGroup: state => groupId => state.resourceInteractions
      .filter(interaction => interaction.subjectId === groupId || interaction.targetId === groupId),
    positiveInteractionsByGroup: (state) => (groupId = null) => {
      const list = []
      if (!groupId) return list

      // Using "for" instead of ".filter" for performance
      for (let i = 0, total = state.resourceInteractions.length; i < total; i++) {
        if (state.resourceInteractions[i].isPositiveWith(groupId)) list.push(state.resourceInteractions[i])
      }

      return list
    },
    negativeInteractionsByGroup: (state) => (groupId) => {
      const list = []
      if (!groupId) return list

      // Using "for" instead of ".filter" for performance
      for (let i = 0, total = state.resourceInteractions.length; i < total; i++) {
        if (state.resourceInteractions[i].isNegativeWith(groupId)) list.push(state.resourceInteractions[i])
      }

      return list
    },
    neutralInteractionsByGroup: (state) => (groupId) => {
      const list = []
      if (!groupId) return list

      // Using "for" instead of ".filter" for performance
      for (let i = 0, total = state.resourceInteractions.length; i < total; i++) {
        if (state.resourceInteractions[i].isNeutralWith(groupId)) list.push(state.resourceInteractions[i])
      }

      return list
    },
  },
}
