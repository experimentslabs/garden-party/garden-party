import { beforeEach, describe, expect, it, vi } from 'vitest'
import TagModule from './TagModule'

// Ignore all possible API calls
vi.mock('../../tools/api', () => ({ default: {} }))

let stateMock

describe('TagModule', () => {
  beforeEach(() => {
    stateMock = { tags: [] }
  })

  describe('mutations', () => {
    describe('.setTag', () => {
      describe('when tag does not exist', () => {
        it('creates simple tags', () => {
          expect.assertions(1)

          const tag = { base: 'sometag', name: 'SomeTag', items: new Set([]) }
          const expected = [{ base: 'sometag', name: 'SomeTag', items: new Set([]) }]
          TagModule.mutations.setTag(stateMock, tag)

          expect(stateMock.tags).toStrictEqual(expected)
        })
        it('creates compound tags', () => {
          expect.assertions(1)

          const tag = { base: 'sometag', name: 'SomeTag', items: new Set(['yes', 'ok']) }
          const expected = [{ base: 'sometag', name: 'SomeTag', items: new Set(['yes', 'ok']) }]
          TagModule.mutations.setTag(stateMock, tag)

          expect(stateMock.tags).toStrictEqual(expected)
        })
      })
      describe('when tag exists', () => {
        beforeEach(() => {
          stateMock = {
            tags: [
              { base: 'simple', name: 'Simple', items: new Set([]) },
              { base: 'compound', name: 'Compound', items: new Set(['value']) },
            ],
          }
        })
        it('completes simple tags', () => {
          expect.assertions(1)

          const tag = { base: 'simple', name: 'Simple', items: new Set(['hello']) }
          const expected = [
            { base: 'simple', name: 'Simple', items: new Set(['hello']) },
            { base: 'compound', name: 'Compound', items: new Set(['value']) },
          ]
          TagModule.mutations.setTag(stateMock, tag)

          expect(stateMock.tags).toStrictEqual(expected)
        })
        it('completes compound tags', () => {
          expect.assertions(1)

          const tag = { base: 'compound', name: 'Compound', items: new Set(['value2', 'value3']) }
          const expected = [
            { base: 'simple', name: 'Simple', items: new Set([]) },
            { base: 'compound', name: 'Compound', items: new Set(['value', 'value2', 'value3']) },
          ]
          TagModule.mutations.setTag(stateMock, tag)

          expect(stateMock.tags).toStrictEqual(expected)
        })
      })
    })
    describe('.setTagString', () => {
      describe('when tag does not exist', () => {
        it('creates new simple tags', () => {
          expect.assertions(1)

          TagModule.mutations.setTagString(stateMock, 'simple')
          const expected = [{ name: 'Simple', base: 'simple', items: new Set([]) }]
          expect(stateMock.tags).toStrictEqual(expected)
        })
        it('creates new compound tags', () => {
          expect.assertions(1)

          TagModule.mutations.setTagString(stateMock, 'compound::value')
          const expected = [{ name: 'Compound', base: 'compound', items: new Set(['value']) }]
          expect(stateMock.tags).toStrictEqual(expected)
        })
      })
      describe('when tag exists', () => {
        beforeEach(() => {
          stateMock = {
            tags: [
              { base: 'simple', name: 'Simple', items: new Set([]) },
              { base: 'compound', name: 'Compound', items: new Set(['value']) },
            ],
          }
        })

        it('updates new simple tags', () => {
          expect.assertions(1)

          TagModule.mutations.setTagString(stateMock, 'simple::not anymore')
          const expected = [
            { name: 'Simple', base: 'simple', items: new Set(['not anymore']) },
            { name: 'Compound', base: 'compound', items: new Set(['value']) },
          ]
          expect(stateMock.tags).toStrictEqual(expected)
        })
        it('updates new compound tags', () => {
          expect.assertions(1)

          TagModule.mutations.setTagString(stateMock, 'compound::new')
          const expected = [
            { name: 'Simple', base: 'simple', items: new Set([]) },
            { name: 'Compound', base: 'compound', items: new Set(['value', 'new']) },
          ]
          expect(stateMock.tags).toStrictEqual(expected)
        })
      })
    })
  })
})
