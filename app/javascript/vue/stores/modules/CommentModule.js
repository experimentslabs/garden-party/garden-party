import Base from './Base'
import Comment from '../../classes/models/Comment'

export default {
  state: {
    /** @type {Comment[]} */
    comments: [],
  },
  mutations: {
    resetComments: Base.mutations.resetState('comments'),
    setComment: Base.mutations.setEntity('comments'),
    deleteComment: Base.mutations.deleteEntity('comments'),
  },
  actions: {
    loadComments ({ commit }, observationId) {
      return Comment.index(observationId)
        .then(items => { items.forEach(item => { commit('setComment', item) }) })
    },
    loadComment: Base.actions.loadEntity(Comment, 'setComment'),
    createComment: Base.actions.createEntity(Comment, 'setComment', 'comment'),
    updateComment: Base.actions.updateEntity(Comment, 'setComment', 'comment'),
    saveComment: Base.actions.saveEntity('createComment', 'updateComment'),
    destroyComment: Base.actions.destroyEntity(Comment, 'deleteComment'),
  },
  getters: {
    comments: Base.getters.entities('comments', 'id'),
    comment: Base.getters.entity('comments'),
  },
}
