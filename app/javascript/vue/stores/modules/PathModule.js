import Base from './Base'
import Path from '../../classes/models/Path'
import EventBus from '../../tools/EventBus'

export default {
  state: {
    /** @type {Path[]} */
    paths: [],
  },
  mutations: {
    resetPaths: Base.mutations.resetState('paths'),
    setPath: Base.mutations.setEntity('paths'),
    deletePath: Base.mutations.deleteEntity('paths'),
  },
  actions: {
    loadPaths: Base.actions.loadEntities(Path, 'setPath'),
    loadPath: Base.actions.loadEntity(Path, 'setPath'),
    updatePath: ({ commit, getters }, payload) => Path.update(payload)
      .then(path => {
        const oldLayer = getters.path(path.id).layerId

        commit('setPath', path)
        const updatedPath = getters.path(path.id)

        if (oldLayer !== updatedPath.layerId) {
          EventBus.emit('change-path-layer', { path: updatedPath, fromPathId: oldLayer })
        }
        EventBus.emit('updated-path', updatedPath)

        // Return updated instance
        return Promise.resolve(updatedPath)
      }),
    savePath: Base.actions.saveEntity('createPath', 'updatePath'),
    destroyPath: ({ commit, getters }, id) =>
      Path.destroy(id)
        .then(() => {
          const path = getters.path(id)

          commit('deletePath', id)
          EventBus.emit('destroyed-path', path)

          return Promise.resolve()
        }),
    createPath ({ commit, getters }, { payload, feature }) {
      return Path.create(payload)
        .then(item => {
          commit('setPath', item)
          // Return new instance
          const path = getters.path(item.id)
          path.setFeature(feature)
          return Promise.resolve(path)
        })
    },
  },
  getters: {
    paths: state => state.paths,
    path: Base.getters.entity('paths'),
    pathsInLayer: state => layerId => state.paths.filter(p => p.layerId === layerId),
  },
}
