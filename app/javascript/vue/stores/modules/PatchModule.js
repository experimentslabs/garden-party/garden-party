import Base from './Base'
import Patch from '../../classes/models/Patch'
import EventBus from '../../tools/EventBus'

export default {
  state: {
    /** @type {Patch[]} */
    patches: [],
  },
  mutations: {
    resetPatches: Base.mutations.resetState('patches'),
    setPatch: Base.mutations.setEntity('patches'),
    deletePatch: Base.mutations.deleteEntity('patches'),
  },
  actions: {
    loadPatches: Base.actions.loadEntities(Patch, 'setPatch'),
    loadPatch: Base.actions.loadEntity(Patch, 'setPatch'),
    virtualizePatch: ({ commit, getters }, { payload, originalPatch }) => {
      payload.geometry = null
      payload.layer_id = null

      return Patch.update(payload)
        .then((patch) => {
          // Anyone listening may remove the old patch from its refs
          EventBus.emit('virtualize-patch', originalPatch)

          commit('setPatch', patch)
          const updatedPatch = getters.patch(patch.id)

          // Return updated instance
          return Promise.resolve(updatedPatch)
        })
    },
    updatePatch: ({ commit, getters }, { payload }) => Patch.update(payload)
      .then(patch => {
        const oldPatch = getters.patch(patch.id)
        const oldLayer = oldPatch.layerId
        const wasVirtual = oldPatch.isVirtual

        commit('setPatch', patch)
        const updatedPatch = getters.patch(patch.id)

        if (!wasVirtual && oldLayer !== updatedPatch.layerId) {
          EventBus.emit('change-patch-layer', { patch: updatedPatch, fromLayerId: oldLayer })
        }

        // Return updated instance
        return Promise.resolve(updatedPatch)
      }),
    savePatch: Base.actions.saveEntity('createPatch', 'updatePatch'),
    destroyPatch: ({ commit, getters }, id) => Patch.destroy(id)
      .then(() => {
        const patch = getters.patch(id)
        patch.elements.forEach(element => { commit('deleteElement', element.id) })

        commit('deletePatch', id)
        EventBus.emit('destroyed-patch', patch)

        return Promise.resolve()
      }),
    createPatch ({ commit, dispatch, getters }, { payload, feature }) {
      return Patch.create(payload)
        .then(item => {
          return dispatch('loadPatchElements', item.id)
            // Add and return the new patch once its elements are loaded
            .then(() => {
              commit('setPatch', item)
              // Return new instance
              const patch = getters.patch(item.id)
              if (feature) patch.setFeature(feature)

              return Promise.resolve(patch)
            })
        })
    },
  },
  getters: {
    patch: Base.getters.entity('patches'),
    patches: state => state.patches.sort((a, b) => {
      if (a.name && b.name) return a.name.localeCompare(b.name)
      if (a.name && !b.name) return -1
      if (!a.name && b.name) return 1

      return a.createdAt.valueOf() - b.createdAt.valueOf()
    }),
    placeablePatches: (_state, getters) => getters.patches.filter(p => p.geometryType !== 'Virtual'),
    virtualPatches: (_state, getters) => getters.patches.filter(p => p.geometryType === 'Virtual'),
    patchesByIds: state => ids => state.patches.filter(p => ids.indexOf(p.id) > -1),
    patchesInLayer: state => layerId => state.patches.filter(p => p.layerId === layerId),
  },
}
