import Account from '../../classes/models/Account'

export default {
  state: {
    /** @type {Account} */
    account: null,
  },
  mutations: {
    setAccount (state, account) { state.account = account },
    deleteAccount (state) { state.account = null },
  },
  actions: {
    loadAccount: ({ commit }, failOnError = true) => Account.getAccount(failOnError)
      .then(account => { commit('setAccount', account) }),
    updateAccount: ({ commit }, payload) => Account.update(payload)
      .then(account => { commit('setAccount', account) }),
  },
  getters: {
    account: state => state.account,
    signedIn: state => !!state.account,
  },
}
