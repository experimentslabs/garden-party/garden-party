import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import Base from './Base'
import { testModelFactory, TestModel } from '../../../specs/fakes/TestModel'

// Ignore all possible API calls
vi.mock('../../tools/api', () => ({ default: {} }))

const STORE_KEY = 'tests'

// VueX methods mocks
let commitMock
let dispatchMock
let getterMocks
// VueX State
let stateMock
// Data mocks
let oneEntityMock
let manyEntitiesMock

describe('Base module', () => {
  beforeEach(() => {
    commitMock = vi.fn()
    dispatchMock = vi.fn()
    getterMocks = {
      entities: vi.fn(() => testModelFactory.buildList(2)),
      entity: vi.fn(id => testModelFactory.build({ id })),
      search: vi.fn(term => testModelFactory.buildList(2, { name: term })),
    }
    // Generating state
    stateMock = {}
    stateMock[STORE_KEY] = [
      new TestModel(testModelFactory.build({ name: 'something here' })),
      new TestModel(testModelFactory.build({ name: 'here we are too' })),
      new TestModel(testModelFactory.build({ name: 'another one' })),
    ]
  })

  describe('mutations', () => {
    let mutation

    describe('setEntity', () => {
      beforeEach(() => {
        mutation = Base.mutations.setEntity(STORE_KEY)
      })

      describe('when entity exists', () => {
        beforeEach(() => {
          oneEntityMock = new TestModel({
            ...stateMock[STORE_KEY][0],
            name: 'new name',
          })
        })

        it('does not create a new entry', () => {
          expect.assertions(1)

          const currentLength = stateMock[STORE_KEY].length
          mutation(stateMock, oneEntityMock)
          expect(stateMock[STORE_KEY]).toHaveLength(currentLength)
        })

        it('updates the existing one', () => {
          expect.assertions(1)

          mutation(stateMock, oneEntityMock)
          const updatedEntity = stateMock[STORE_KEY].find(e => e.id === oneEntityMock.id)
          expect(updatedEntity).toStrictEqual(oneEntityMock)
        })

        it('calls model.updateAttributes', async () => {
          expect.assertions(1)

          const initialEntity = stateMock[STORE_KEY].find(e => e.id === oneEntityMock.id)
          const spy = vi.spyOn(initialEntity, 'updateAttributes')
          await mutation(stateMock, oneEntityMock)
          expect(spy).toHaveBeenCalledWith({ id: initialEntity.id, name: 'new name' })
        })
      })

      describe('when entity does not exists', () => {
        beforeEach(() => {
          oneEntityMock = new TestModel(testModelFactory.build())
        })

        it('creates a new entry', () => {
          expect.assertions(1)

          const currentLength = stateMock[STORE_KEY].length
          mutation(stateMock, oneEntityMock)
          expect(stateMock[STORE_KEY]).toHaveLength(currentLength + 1)
        })
      })
    })

    describe('deleteEntity', () => {
      beforeEach(() => {
        mutation = Base.mutations.deleteEntity(STORE_KEY)
      })
      describe('when entity exists', () => {
        it('removes the entity', () => {
          expect.assertions(1)

          const currentLength = stateMock[STORE_KEY].length
          mutation(stateMock, stateMock[STORE_KEY][0].id)
          expect(stateMock[STORE_KEY]).toHaveLength(currentLength - 1)
        })
      })
      describe('when entity does not exists', () => {
        it('throws an error', () => {
          expect.assertions(1)

          const executor = () => new Promise(() => mutation(stateMock, 0))
          expect(executor()).rejects.toThrow('can\'t remove entity with id')
        })
      })
    })
  })

  describe('actions', () => {
    let action

    describe('loadEntities', () => {
      beforeEach(() => {
        manyEntitiesMock = testModelFactory.buildList(2)
        vi.spyOn(TestModel, 'getIndex').mockImplementation(() => new Promise(resolve => resolve(manyEntitiesMock)))
        action = Base.actions.loadEntities(TestModel, 'setEntity')
      })
      afterEach(() => { TestModel.getIndex.mockReset() })

      it('calls model.getIndex', async () => {
        expect.assertions(1)

        await action({ commit: commitMock })
        expect(TestModel.getIndex).toHaveBeenCalledTimes(1)
      })

      it('mutates state for each entry', async () => {
        expect.assertions(1)

        await action({ commit: commitMock })
        expect(commitMock).toHaveBeenCalledTimes(2)
      })
    })

    describe('loadEntity', () => {
      beforeEach(() => {
        oneEntityMock = testModelFactory.build()
        vi.spyOn(TestModel, 'get').mockImplementation(() => new Promise(resolve => resolve(oneEntityMock)))
        action = Base.actions.loadEntity(TestModel, 'setEntity', oneEntityMock)
      })
      afterEach(() => { TestModel.get.mockReset() })

      it('calls model.get', async () => {
        expect.assertions(1)

        await action({ commit: commitMock }, 1)
        expect(TestModel.get).toHaveBeenCalledTimes(1)
      })

      it('mutates state with "setEntity"', async () => {
        expect.assertions(1)

        await action({ commit: commitMock }, 1)
        expect(commitMock).toHaveBeenCalledWith('setEntity', oneEntityMock)
      })
    })

    describe('createEntity', () => {
      beforeEach(() => {
        const oneEntityPayloadMock = testModelFactory.build()
        delete oneEntityPayloadMock.id
        oneEntityMock = { ...oneEntityPayloadMock, id: 1 }
        vi.spyOn(TestModel, 'create').mockImplementation(() => new Promise(resolve => resolve(oneEntityMock)))
        action = Base.actions.createEntity(TestModel, 'setEntity', 'entity')
      })
      afterEach(() => { TestModel.create.mockReset() })

      it('calls model.create', async () => {
        expect.assertions(1)

        await action({ commit: commitMock, getters: getterMocks }, { name: 'test' })
        expect(TestModel.create).toHaveBeenCalledTimes(1)
      })

      it('mutates state with "setEntity"', async () => {
        expect.assertions(1)

        await action({ commit: commitMock, getters: getterMocks }, { name: 'test' })
        expect(commitMock).toHaveBeenCalledWith('setEntity', oneEntityMock)
      })
    })

    describe('updateEntity', () => {
      beforeEach(() => {
        oneEntityMock = testModelFactory.build()
        vi.spyOn(TestModel, 'update').mockImplementation(() => new Promise(resolve => resolve(oneEntityMock)))
        action = Base.actions.updateEntity(TestModel, 'setEntity', 'entity')
      })
      afterEach(() => { TestModel.update.mockReset() })

      it('calls model.update', async () => {
        expect.assertions(1)

        await action({
          commit: commitMock,
          getters: getterMocks,
        }, { name: 'test' })
        expect(TestModel.update).toHaveBeenCalledTimes(1)
      })

      it('mutates state with "setEntity"', async () => {
        expect.assertions(1)

        await action({
          commit: commitMock,
          getters: getterMocks,
        }, { name: 'test' })
        expect(commitMock).toHaveBeenCalledWith('setEntity', oneEntityMock)
      })
    })

    describe('saveEntity', () => {
      beforeEach(() => {
        action = Base.actions.saveEntity('createEntity', 'updateEntity')
      })

      describe('when entity exists', () => {
        it('updates the entity', async () => {
          expect.assertions(1)

          await action({ dispatch: dispatchMock }, testModelFactory.build({ id: 1 }))
          expect(dispatchMock).toHaveBeenCalledTimes(1)
        })

        it('calls the "updateEntity" action', async () => {
          expect.assertions(1)

          const entity = testModelFactory.build({ id: 1 })
          await action({ dispatch: dispatchMock }, entity)
          expect(dispatchMock).toHaveBeenCalledWith('updateEntity', entity)
        })
      })

      describe('when entity does not exist', () => {
        it('creates the entity', async () => {
          expect.assertions(1)

          await action({ dispatch: dispatchMock }, testModelFactory.build({ id: 1 }))
          expect(dispatchMock).toHaveBeenCalledTimes(1)
        })

        it('calls the "createEntity" action', async () => {
          expect.assertions(1)

          const entity = testModelFactory.build({ id: null })
          await action({ dispatch: dispatchMock }, entity)
          expect(dispatchMock).toHaveBeenCalledWith('createEntity', entity)
        })
      })
    })

    describe('destroyEntity', () => {
      beforeEach(() => {
        vi.spyOn(TestModel, 'destroy').mockImplementation(() => new Promise(resolve => resolve()))
        action = Base.actions.destroyEntity(TestModel, 'deleteEntity')
      })

      afterEach(() => { TestModel.destroy.mockReset() })

      it('calls model.destroy', async () => {
        expect.assertions(1)

        await action({ commit: commitMock }, 1)
        expect(TestModel.destroy).toHaveBeenCalledTimes(1)
      })

      it('mutates state with "deleteEntity"', async () => {
        expect.assertions(1)

        await action({ commit: commitMock, getters: getterMocks }, 1)
        expect(commitMock).toHaveBeenCalledWith('deleteEntity', 1)
      })
    })
  })

  describe('getters', () => {
    let getter

    describe('entities', () => {
      beforeEach(() => {
        getter = Base.getters.entities(STORE_KEY, 'name')
      })
      it('returns a list', () => {
        expect.assertions(1)

        expect(getter(stateMock)).toHaveLength(3)
      })
      it('list is ordered by name', () => {
        expect.assertions(1)

        const names = getter(stateMock).map(v => v.name)
        const expected = [
          'another one',
          'here we are too',
          'something here',
        ]
        expect(names).toStrictEqual(expected)
      })
    })

    describe('entity', () => {
      beforeEach(() => {
        getter = Base.getters.entity(STORE_KEY)
      })

      describe('when entity exists', () => {
        it('returns the entity', () => {
          expect.assertions(1)

          const entity = stateMock[STORE_KEY][0]
          expect(getter(stateMock)(entity.id)).toStrictEqual(entity)
        })
      })

      describe('when entity does not exists', () => {
        it('returns an undefined value', () => {
          expect.assertions(1)

          expect(getter(stateMock)(0)).toBeUndefined()
        })
      })
    })

    describe('search', () => {
      beforeEach(() => {
        getter = Base.getters.search(STORE_KEY, 'name')
      })

      it('returns a list', () => {
        expect.assertions(1)

        expect(getter(stateMock)('here')).toHaveLength(2)
      })

      it('list is ordered by name', () => {
        expect.assertions(1)

        const names = getter(stateMock)('here').map(v => v.name)
        const expected = [
          'here we are too',
          'something here',
        ]
        expect(names).toStrictEqual(expected)
      })
    })
  })
})
