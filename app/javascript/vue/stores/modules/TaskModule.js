import Base from './Base'
import Task from '../../classes/models/Task'
import Element from '../../classes/models/Element'
import Map from '../../classes/models/Map'
import Patch from '../../classes/models/Patch'
import Path from '../../classes/models/Path'

/**
 * Compares tasks by date of achievement. If item is not yet done, planning
 * date is used.
 *
 * @param   {Task}   a - First task to compare
 * @param   {Task}   b - Second task to compare
 * @returns {number}   Sort order
 */
function sortTasks (a, b) {
  const date1 = a.doneAt || a.plannedFor
  const date2 = b.doneAt || b.plannedFor
  return date2 - date1
}

export default {
  state: {
    /** @type {Task[]} */
    tasks: [],
    taskNames: [],
  },
  mutations: {
    resetTasks: Base.mutations.resetState('tasks'),
    setTask: Base.mutations.setEntity('tasks'),
    deleteTask: Base.mutations.deleteEntity('tasks'),
    setTaskNames: (state, names) => { state.taskNames = names },
    addTaskName: (state, name) => { if (state.taskNames.indexOf(name) === -1) state.taskNames.push(name) },
  },
  actions: {
    loadTasks: Base.actions.loadEntities(Task, 'setTask'),
    loadTask: Base.actions.loadEntity(Task, 'setTask'),
    setUpdatedTaskSubject ({ commit }, task) {
      if (!task.updatedSubject) return

      switch (task.subjectType) {
        case 'Element':
          commit('setElement', new Element(task.updatedSubject))
          break
        case 'Map':
          commit('setMap', new Map(task.updatedSubject))
          break
        case 'Patch':
          commit('setPatch', new Patch(task.updatedSubject))
          break
        case 'Path':
          commit('setPath', new Path(task.updatedSubject))
          break
      }
    },
    createTask ({ commit, dispatch, getters }, payload) {
      return Task.create(payload)
        .then((task) => {
          commit('setTask', task)
          dispatch('setUpdatedTaskSubject', task)
          // Return new instance
          return Promise.resolve(getters.task(task.id))
        })
    },
    updateTask ({ commit, dispatch, getters }, payload) {
      return Task.update(payload)
        .then((task) => {
          commit('setTask', task)
          dispatch('setUpdatedTaskSubject', task)
          // Return new instance
          return Promise.resolve(getters.task(task.id))
        })
    },
    destroyTask ({ dispatch, commit }, id) {
      return Task.destroy(id)
        .then((task) => {
          commit('deleteTask', id)
          dispatch('setUpdatedTaskSubject', {
            subjectType: task.subject_type,
            subjectId: task.subject_id,
            updatedSubject: task.subject,
          })
        })
    },
    loadTasksAllForMap ({ commit }, { mapId, filters = {} }) {
      return Task.getAllForMap(mapId, filters)
        .then((items) => {
          commit('resetTasks')
          items.forEach(item => { commit('setTask', item) })

          return items
        })
    },
    loadTasksForSubject ({ commit }, { subject, filters = {} }) {
      return Task.getTasksForSubject(subject.className, subject.id, filters)
        .then((items) => {
          commit('resetTasks')
          items.forEach(item => { commit('setTask', item) })

          return items
        })
    },
    loadTaskNames: ({ commit }, mapId) => Task.getNames(mapId)
      .then(data => commit('setTaskNames', data)),
    addTaskName: ({ commit }, name) => { commit('addTaskName', name) },
  },
  getters: {
    task: Base.getters.entity('tasks'),
    tasks: state => state.tasks
      .sort((a, b) => sortTasks(a, b)),
    taskNames: state => state.taskNames.sort((a, b) => a.localeCompare(b)),
    tasksBySubject: state => (subject) => state.tasks
      .filter(a => a.subjectId === subject.id && a.subjectType === subject.className)
      .sort((a, b) => sortTasks(a, b)),
    pendingTasks: state => state.tasks.filter(a => a.doneAt === null)
      .sort((a, b) => sortTasks(a, b))
      .reverse(),
  },
}
