import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import App from './App.vue'

function makeWrapper () {
  return shallowMount(App, {
    global: {
      plugins: [
        createStore({
          actions: {
            loadDataDifferences: () => Promise.resolve(),
            loadNames: () => Promise.resolve(),
          },
        }),
      ],
      stubs: {
        RouterView: true,
      },
    },
  })
}

let wrapper

describe('App', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
