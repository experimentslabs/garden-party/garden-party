import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'root',
    component: () => import('./components/Families.vue'),
  },
  {
    path: '/genera',
    name: 'genera',
    component: () => import('./components/Genera.vue'),
  },
  {
    path: '/resources',
    name: 'resources',
    component: () => import('./components/Resources.vue'),
  },
  {
    path: '/resource_interactions_groups',
    name: 'resource_interactions_groups',
    component: () => import('./components/ResourceInteractionsGroup.vue'),
  },
  {
    path: '/resource_interactions',
    name: 'resource_interactions',
    component: () => import('./components/ResourceInteraction.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404_page',
    component: () => import('../../common/404Page.vue'),
  },
]

const router = createRouter({
  routes,
  history: createWebHashHistory(),
})

export default router
