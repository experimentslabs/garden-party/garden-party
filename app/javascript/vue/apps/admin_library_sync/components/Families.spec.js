import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import Families from './Families.vue'

let wrapper

describe('Families', () => {
  function makeWrapper () {
    return shallowMount(Families, {
      global: {
        plugins: [
          createStore({
            getters: { familiesToSync: () => [] },
          }),
        ],
      },
    })
  }

  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
