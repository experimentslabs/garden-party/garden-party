import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import Resources from './Resources.vue'

function makeWrapper () {
  return shallowMount(Resources, {
    global: {
      plugins: [
        createStore({
          getters: { resourcesToSync: () => [] },
        }),
      ],
    },
  })
}

let wrapper

describe('Resources', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
