import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import TrustedFamily from '../../../../classes/models/trustedData/TrustedFamily'

import Diffs from './Diffs.vue'

function makeWrapper () {
  return shallowMount(Diffs, {
    props: {
      entitiesToSync: [],
      trustedClass: TrustedFamily,
    },
  })
}

let wrapper

describe('Diffs', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
