import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import TrustedFamily from '../../../../classes/models/trustedData/TrustedFamily'
import { trustedFamilyFactory } from '../../../../../specs/factories/trustedFamilies'

import Diff from './Diff.vue'

function makeWrapper () {
  return mount(Diff, {
    props: {
      trusted: new TrustedFamily(trustedFamilyFactory.build()),
      trustedClass: TrustedFamily,
    },
  })
}

let wrapper

describe('Diff', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
