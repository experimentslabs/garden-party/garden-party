import { describe, beforeEach, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'

import DiffLine from './DiffLine.vue'

function makeWrapper () {
  return mount(DiffLine, {
    props: {
      field: 'a_field',
    },
  })
}

let wrapper

describe('DiffLine', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
