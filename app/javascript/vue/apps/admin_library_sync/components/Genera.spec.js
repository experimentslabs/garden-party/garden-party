import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import Genera from './Genera.vue'

function makeWrapper () {
  return shallowMount(Genera, {
    global: {
      plugins: [
        createStore({
          getters: { generaToSync: () => [] },
        }),
      ],
    },
  })
}

let wrapper

describe('Genera', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
