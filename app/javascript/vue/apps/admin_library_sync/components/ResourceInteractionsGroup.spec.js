import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import ResourceInteractionsGroup from './ResourceInteractionsGroup.vue'

function makeWrapper () {
  return shallowMount(ResourceInteractionsGroup, {
    global: {
      plugins: [
        createStore({
          getters: { resourceInteractionsGroupsToSync: () => [] },
        }),
      ],
    },
  })
}

let wrapper

describe('ResourceInteractionsGroup', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
