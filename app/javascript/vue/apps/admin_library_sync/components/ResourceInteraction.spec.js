import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import ResourceInteraction from './ResourceInteraction.vue'

function makeWrapper () {
  return shallowMount(ResourceInteraction, {
    global: {
      plugins: [
        createStore({
          getters: { resourceInteractionsToSync: () => [] },
        }),
      ],
    },
  })
}

let wrapper

describe('ResourceInteraction', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
