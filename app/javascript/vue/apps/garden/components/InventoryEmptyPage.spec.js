import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import InventoryEmptyPage from './InventoryEmptyPage.vue'

function makeWrapper () {
  return shallowMount(InventoryEmptyPage)
}

let wrapper

describe('InventoryEmptyPage', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
