import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import _MonthsGraphsLegend from './_MonthsGraphsLegend.vue'

function makeWrapper () {
  return shallowMount(_MonthsGraphsLegend)
}

let wrapper

describe('_MonthsGraphsLegend', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
