import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import _DestroyWarning from './_DestroyWarning.vue'

function makeWrapper () {
  return shallowMount(_DestroyWarning)
}

let wrapper

describe('_DestroyWarning', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
