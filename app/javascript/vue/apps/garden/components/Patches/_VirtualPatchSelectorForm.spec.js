import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import _VirtualPatchSelectorForm from './_VirtualPatchSelectorForm.vue'

function makeWrapper () {
  return shallowMount(_VirtualPatchSelectorForm)
}

let wrapper

describe('_VirtualPatchSelectorForm', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
