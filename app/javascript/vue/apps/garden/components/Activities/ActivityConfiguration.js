/**
 * @typedef {import('vue')}                                 Vue
 * @typedef {import('../../../../classes/models/Activity')} Activity
 */

/**
 * Checks if an activity was created by the logged-in user
 *
 * @param   {Vue}      context  - VueJS component context (this)
 * @param   {Activity} activity - Activity to process
 * @returns {boolean}           Whether or not the activity was created by logged-in user
 */
function performedBySelf (context, activity) {
  return context.$store.getters.account.id === activity.userId
}

const create = {
  icon: 'plus',
  cssClass: 'text--color-creative',
}

const update = {
  icon: 'pencil',
  cssClass: '-text--secondary',
}

const destroy = {
  icon: 'trash',
  cssClass: 'text--color-destructive',
}

export default {
  Element: {
    create: {
      ...create,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.element.create_by_self', {
            name: context.activity.subjectName,
            from: context.activity.data.from_name,
            to: context.activity.data.to_name,
          })
        }

        return context.$t('js.activities.element.create', {
          user: context.activity.username,
          element: context.activity.subjectName,
        })
      },
    },
    implant: {
      ...create,
      icon: 'arrow-bend-right-down',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.element.implant_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.element.implant', {
          user: context.activity.username,
          element: context.activity.subjectName,
        })
      },
    },
    remove: {
      icon: 'arrow-bend-right-up',
      cssClass: 'text--color-creative',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.element.remove_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.element.remove', {
          user: context.activity.username,
          element: context.activity.subjectName,
        })
      },
    },
    destroy: {
      ...destroy,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.element.destroy_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.element.destroy', {
          user: context.activity.username,
          element: context.activity.subjectName,
        })
      },
    },
    change_path: {
      icon: 'change-patch',
      cssClass: 'text--color-creative',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.element.change_path_by_self', {
            name: context.activity.subjectName,
            from: context.activity.data.from_name,
            to: context.activity.data.to_name,
          })
        }

        return context.$t('js.activities.element.change_path', {
          user: context.activity.username,
          element: context.activity.subjectName,
          from: context.activity.data.from_name,
          to: context.activity.data.to_name,
        })
      },
    },
  },
  Harvest: {
    create: {
      ...create,
      icon: 'harvest',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.harvest.create_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.harvest.create', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    quantity_fix: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.harvest.fix_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.harvest.fix', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    unit_fix: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.harvest.fix_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.harvest.fix', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    date_fix: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.harvest.fix_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.harvest.fix', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
  },
  Layer: {
    create: {
      ...create,
      icon: 'layers',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.layer.create_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.layer.create', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    rename: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.layer.rename_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.layer.rename', {
          user: context.activity.username,
          name: context.activity.subjectName,
          from: context.activity.data.change[0],
          to: context.activity.data.change[1],
        })
      },
    },
    destroy: {
      ...destroy,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.layer.destroy_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.layer.destroy', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
  },
  Observation: {
    create: {
      ...create,
      icon: 'observations',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.observation.create_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.observation.create', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    rename: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.observation.rename_by_self', {
            from: context.activity.data.change[0],
            to: context.activity.data.change[1],
          })
        }

        return context.$t('js.activities.observation.rename', {
          user: context.activity.username,
          from: context.activity.data.change[0],
          to: context.activity.data.change[1],
        })
      },
    },
    complete: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.observation.complete_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.observation.complete', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    date_fix: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.observation.date_fix_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.observation.date_fix', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    destroy: {
      ...destroy,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.observation.destroy_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.observation.destroy', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
  },
  Patch: {
    create: {
      ...create,
      message (context) {
        const name = context.activity.subjectName || context.$t('generic.patch.name')

        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.patch.create_by_self', { name })
        }

        return context.$t('js.activities.patch.create', {
          user: context.activity.username,
          name,
        })
      },
    },
    rename: {
      ...update,
      message (context) {
        const from = context.activity.data.change[0] || context.$t('generic.patch.name')

        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.patch.rename_by_self', {
            from,
            to: context.activity.data.change[1],
          })
        }

        return context.$t('js.activities.patch.rename', {
          user: context.activity.username,
          from,
          to: context.activity.data.change[1],
        })
      },
    },
    move_or_resize: {
      ...update,
      message (context) {
        const name = context.activity.subjectName || context.$t('generic.patch.name')

        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.patch.move_or_resize_by_self', { name })
        }

        return context.$t('js.activities.patch.move_or_resize', {
          user: context.activity.username,
          name,
        })
      },
    },
    layer_change: {
      ...update,
      message (context) {
        const name = context.activity.subjectName || context.$t('generic.patch.name')

        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.patch.layer_change_by_self', { name })
        }

        return context.$t('js.activities.patch.layer_change', {
          user: context.activity.username,
          name,
        })
      },
    },
    destroy: {
      ...destroy,
      message (context) {
        const name = context.activity.subjectName || context.$t('generic.patch.name')

        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.patch.destroy_by_self', { name })
        }

        return context.$t('js.activities.patch.destroy', {
          user: context.activity.username,
          name,
        })
      },
    },
  },
  Path: {
    create: {
      ...create,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.path.create_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.path.create', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    rename: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.path.rename_by_self', {
            from: context.activity.data.change[0],
            to: context.activity.data.change[1],
          })
        }

        return context.$t('js.activities.path.rename', {
          user: context.activity.username,
          from: context.activity.data.change[0],
          to: context.activity.data.change[1],
        })
      },
    },
    move_or_resize: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.path.move_or_resize_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.path.move_or_resize', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    layer_change: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.path.layer_change_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.path.layer_change', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
    destroy: {
      ...destroy,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.path.destroy_by_self', { name: context.activity.subjectName })
        }

        return context.$t('js.activities.path.destroy', {
          user: context.activity.username,
          name: context.activity.subjectName,
        })
      },
    },
  },
  Task: {
    create: {
      ...create,
      icon: 'check',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.create_by_self', {
            name: context.activity.subjectName,
            subject: context.activity.data.subject,
            assignee: context.activity.data.assignee,
          })
        }

        return context.$t('js.activities.task.create', {
          user: context.activity.username,
          name: context.activity.subjectName,
          subject: context.activity.data.subject,
          assignee: context.activity.data.assignee,
        })
      },
    },
    rename: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.rename_by_self', {
            from: context.activity.data.change[0],
            to: context.activity.data.change[1],
            subject: context.activity.data.subject,
          })
        }

        return context.$t('js.activities.task.rename', {
          user: context.activity.username,
          from: context.activity.data.change[0],
          to: context.activity.data.change[1],
          subject: context.activity.data.subject,
        })
      },
    },
    complete: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.complete_by_self', {
            name: context.activity.subjectName,
            subject: context.activity.data.subject,
          })
        }

        return context.$t('js.activities.task.complete', {
          user: context.activity.username,
          name: context.activity.subjectName,
          subject: context.activity.data.subject,
        })
      },
    },
    date_change: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.date_change_by_self', {
            name: context.activity.subjectName,
            from: context.activity.data.change[0],
            to: context.activity.data.change[1],
          })
        }

        return context.$t('js.activities.task.date_change', {
          user: context.activity.username,
          name: context.activity.subjectName,
          from: context.activity.data.change[0],
          to: context.activity.data.change[1],
        })
      },
    },
    reassign: {
      ...update,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.reassign_by_self', {
            name: context.activity.subjectName,
            assignee: context.activity.data.assignee,
            subject: context.activity.data.subject,
          })
        }

        return context.$t('js.activities.task.reassign', {
          user: context.activity.username,
          name: context.activity.subjectName,
          assignee: context.activity.data.assignee,
          subject: context.activity.data.subject,
        })
      },
    },
    finish: {
      icon: 'check-circle',
      cssClass: 'text--color-done',
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.finish_by_self', {
            name: context.activity.subjectName,
            subject: context.activity.data.subject,
          })
        }

        return context.$t('js.activities.task.finish', {
          user: context.activity.username,
          name: context.activity.subjectName,
          subject: context.activity.data.subject,
        })
      },
    },
    destroy: {
      ...destroy,
      message (context) {
        if (performedBySelf(context, context.activity)) {
          return context.$t('js.activities.task.destroy_by_self', {
            name: context.activity.subjectName,
            subject: context.activity.data.subject,
          })
        }

        return context.$t('js.activities.task.destroy', {
          user: context.activity.username,
          name: context.activity.subjectName,
          subject: context.activity.data.subject,
        })
      },
    },
  },
  TeamMate: {
    join: {
      ...create,
      icon: 'user-plus',
      message (context) {
        if (performedBySelf(context, context.activity)) return context.$t('js.activities.team_mate.join_by_self')

        return context.$t('js.activities.team_mate.join', { name: context.activity.subjectName })
      },
    },
    leave: {
      ...destroy,
      icon: 'user-minus',
      message (context) {
        if (performedBySelf(context, context.activity)) return context.$t('js.activities.team_mate.leave_by_self')

        return context.$t('js.activities.team_mate.leave', { name: context.activity.subjectName })
      },
    },
  },
}
