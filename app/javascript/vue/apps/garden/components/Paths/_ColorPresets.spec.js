import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import _ColorPresets from './_ColorPresets.vue'

function makeWrapper () {
  return shallowMount(_ColorPresets)
}

let wrapper

describe('_ColorPresets', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
