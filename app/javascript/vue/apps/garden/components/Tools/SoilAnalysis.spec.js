import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import SoilAnalysis from './SoilAnalysis.vue'

function makeWrapper () {
  return shallowMount(SoilAnalysis)
}

let wrapper

describe('SoilAnalysis', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
