import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/Home.vue'

import Store from './store'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: window.I18n.t('js.layouts.garden.main_menu.home'),
    },
  },
  {
    path: '/soil_analysis',
    name: 'soilAnalysis',
    component: () => import('./components/SoilAnalysis.vue'),
    meta: {
      title: window.I18n.t('js.layouts.garden.main_menu.soil_analysis'),
    },
  },
  {
    path: '/library',
    component: () => import('./components/Layouts/_LibraryLayout.vue'),
    meta: {
      title: window.I18n.t('js.layouts.garden.main_menu.library'),
    },
    children: [
      {
        path: '/library/:resourceId?',
        name: 'library',
        component: () => import('./components/Library.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.library'),
        },
      },
      {
        path: '/library/genera/:genusId?/:resourceId?',
        name: 'libraryByGenus',
        component: () => import('./components/LibraryByGenus.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.library'),
        },
      },
      {
        path: '/library/families/:familyId?/:resourceId?',
        name: 'libraryByFamily',
        component: () => import('./components/LibraryByFamily.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.library'),
        },
      },
      {
        path: '/library/resource_interactions_groups/:resourceInteractionsGroupId?',
        name: 'resourceInteractionsGroups',
        component: () => import('./components/ResourceInteractionsGroups.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.resource_interactions_groups'),
        },
      },
    ],
  },
  // Maps
  // --------------------------------------------------------------------------
  {
    path: '/maps/new',
    component: () => import('./components/Layouts/NewMapLayout.vue'),
    meta: {
      title: window.I18n.t('js.layouts.garden.main_menu.new_map'),
    },
    children: [
      {
        path: '/maps/new/picture',
        name: 'newPictureMap',
        component: () => import('./components/NewPictureMap.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.new_map'),
        },
      },
      {
        path: '/maps/new/osm',
        name: 'newOSMMap',
        component: () => import('./components/NewOsmMap.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.new_map'),
        },
      },
    ],
  },
  {
    path: '/maps/:mapId(\\d+)',
    component: () => import('./components/Layouts/_GardenLayout.vue'),
    // All child routes should have the "inMap" metadata set to true, as long as
    // they are related to current map.
    children: [
      {
        path: 'edit',
        name: 'editMap',
        component: () => import('./components/EditMap.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.edit_map'),
          mustBeOwner: true,
          inMap: true,
        },
      },
      {
        path: 'map',
        name: 'map',
        component: () => import('./components/Garden.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.map'),
          inMap: true,
        },
      },
      {
        path: 'inventory',
        component: () => import('./components/Layouts/_InventoryLayout.vue'),
        meta: {
          inMap: true,
          title: window.I18n.t('js.layouts.garden.main_menu.inventory'),
        },
        children: [
          {
            path: '',
            name: 'inventory',
            component: () => import('./components/InventoryEmptyPage.vue'),
          },
          {
            path: 'patches/:patchId(\\d+)',
            name: 'inventory-patch',
            component: () => import('./components/InventoryPatch.vue'),
            children: [
              {
                path: 'elements',
                component: () => import('./components/InventoryPatchElements.vue'),
                children: [
                  {
                    path: '',
                    name: 'inventory-patch-elements',
                    component: () => import('./components/InventoryEmptyPage.vue'),
                  },
                  {
                    path: ':elementId(\\d+)',
                    name: 'inventory-patch-element',
                    component: () => import('./components/InventoryElement.vue'),
                    children: [
                      {
                        path: 'observations',
                        name: 'inventory-patch-element-observations',
                        component: () => import('./components/InventoryObservations.vue'),
                      },
                      {
                        path: 'harvests',
                        name: 'inventory-patch-element-harvests',
                        component: () => import('./components/InventoryElementHarvests.vue'),
                      },
                      {
                        path: 'tasks',
                        name: 'inventory-patch-element-tasks',
                        component: () => import('./components/InventoryTasks.vue'),
                      },
                      {
                        path: 'activity',
                        name: 'inventory-patch-element-activity',
                        component: () => import('./components/InventoryActivity.vue'),
                      },
                    ],
                  },
                ],
              },
              {
                path: 'observations',
                name: 'inventory-patch-observations',
                component: () => import('./components/InventoryObservations.vue'),
              },
              {
                path: 'tasks',
                name: 'inventory-patch-tasks',
                component: () => import('./components/InventoryTasks.vue'),
              },
              {
                path: 'activity',
                name: 'inventory-patch-activity',
                component: () => import('./components/InventoryActivity.vue'),
              },
            ],
          },
          {
            path: 'elements/:elementId(\\d+)',
            name: 'inventory-element',
            component: () => import('./components/InventoryElement.vue'),
            children: [
              {
                path: 'observations',
                name: 'inventory-element-observations',
                component: () => import('./components/InventoryObservations.vue'),
              },
              {
                path: 'harvests',
                name: 'inventory-element-harvests',
                component: () => import('./components/InventoryElementHarvests.vue'),
              },
              {
                path: 'tasks',
                name: 'inventory-element-tasks',
                component: () => import('./components/InventoryTasks.vue'),
              },
              {
                path: 'activity',
                name: 'inventory-element-activity',
                component: () => import('./components/InventoryActivity.vue'),
              },
            ],
          },
        ],
      },
      {
        path: 'tasks',
        name: 'tasks',
        component: () => import('./components/Tasks.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.tasks'),
          inMap: true,
        },
      },
      {
        path: 'harvests',
        name: 'harvests',
        component: () => import('./components/Harvests.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.harvests'),
          inMap: true,
        },
      },
      {
        path: 'activities',
        name: 'activities',
        component: () => import('./components/Activities.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.activity'),
          inMap: true,
        },
      },
      {
        path: 'team',
        name: 'team',
        component: () => import('./components/Team.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.team'),
          inMap: true,
        },
      },
      {
        path: 'coordination',
        name: 'coordination',
        component: () => import('./components/Coordination.vue'),
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.coordination'),
          inMap: true,
        },
      },
      {
        path: 'timeline',
        name: 'timeline',
        component: () => import('../../common/Timeline.vue'),
        props: { enableTasks: true, observationsEditable: true },
        meta: {
          title: window.I18n.t('js.layouts.garden.main_menu.timeline'),
          inMap: true,
        },
      },
    ],
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404Page',
    component: () => import('../../common/404Page.vue'),
    meta: {
      title: window.I18n.t('js.generic.errors.page_not_found'),
    },
  },
]

const router = createRouter({
  routes,
  history: createWebHistory('app'),
})

router.beforeEach((to, from, next) => {
  if (to.params.mapId && to.meta.mustBeOwner) {
    const map = Store.getters.map(parseInt(to.params.mapId, 10))
    const user = Store.getters.account
    if (!map || !user || map.userId !== user.id) {
      next('/')
      return
    }
  }

  next()
})

export default router
