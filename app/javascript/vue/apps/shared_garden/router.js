import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/SharedMap.vue'

/** @typedef {import('vue-router')} Router */

const routes = [
  {
    path: '/',
    name: 'map',
    component: Home,
  },
  {
    path: '/observations',
    name: 'observations',
    component: () => import('../../common/Timeline.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404Page',
    component: () => import('../../common/404Page.vue'),
  },
]

/**
 * Creates the router with the correct base path
 *
 * @param   {number} mapId - Current map identifier
 * @returns {Router}       The router instance
 */
export default function (mapId) {
  return createRouter({
    routes,
    history: createWebHistory(`/shared/maps/${mapId}`),
  })
}
