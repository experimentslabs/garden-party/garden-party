import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import { elementFactory } from '../../../../specs/factories/elements'
import Element from '../../../classes/models/Element'

import _ElementContent from './_ElementContent.vue'

function makeWrapper () {
  return shallowMount(_ElementContent, {
    props: {
      element: new Element(elementFactory.build()),
    },
    global: {
      // stubs: {
      //   RouterLink: RouterLinkStub,
      // },
    },
  })
}

let wrapper

describe('_ElementContent', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
