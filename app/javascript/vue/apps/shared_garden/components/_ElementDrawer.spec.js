import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import _ElementDrawer from './_ElementDrawer.vue'

function makeWrapper () {
  return shallowMount(_ElementDrawer)
}

let wrapper

describe('_ElementDrawer', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
