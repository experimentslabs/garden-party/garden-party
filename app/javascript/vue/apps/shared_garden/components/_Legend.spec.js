import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import _Legend from './_Legend.vue'

function makeWrapper () {
  return shallowMount(_Legend, {
    global: {
      plugins: [
        createStore({
          getters: {
            resources: () => [],
          },
          actions: {
            loadSharedGarden: () => Promise.resolve([]),
          },
        }),
      ],
    },
  })
}

let wrapper

describe('_Legend', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
