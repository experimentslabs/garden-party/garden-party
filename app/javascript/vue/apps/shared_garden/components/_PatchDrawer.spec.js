import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import _PatchDrawer from './_PatchDrawer.vue'

function makeWrapper () {
  return shallowMount(_PatchDrawer)
}

let wrapper

describe('_PatchDrawer', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
