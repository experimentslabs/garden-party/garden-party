import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import Layer from '../../../classes/models/Layer'
import { layerFactory } from '../../../../specs/factories/layers'

import _Layers from './_Layers.vue'

function makeWrapper () {
  return shallowMount(_Layers, {
    global: {
      plugins: [
        createStore({
          getters: {
            layers: () => [new Layer(layerFactory.build())],
          },
          actions: {
            loadSharedGarden: () => Promise.resolve([]),
          },
        }),
      ],
    },
  })
}

let wrapper

describe('_Layers', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
