import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import { mapFactory } from '../../../specs/factories/maps'
import Map from '../../classes/models/Map'

import App from './App.vue'

function makeWrapper () {
  const map = new Map(mapFactory.build())

  return shallowMount(App, {
    global: {
      plugins: [
        createStore({
          getters: {
            map: () => () => map,
            currentMap: () => () => map,
          },
          actions: {
            loadSharedGarden: () => Promise.resolve([]),
            loadAccount: () => Promise.resolve({}),
            setCurrentMap: () => Promise.resolve(),
          },
        }),
      ],
      mocks: {
        $route: { params: { mapId: map.id } },
        $onBus: () => {},
        $mapId: map.id,
      },
      stubs: {
        RouterView: true,
      },
    },
  })
}

let wrapper

describe('App', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
