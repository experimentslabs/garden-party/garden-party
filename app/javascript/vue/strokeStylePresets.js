class StrokeStylePreset {
  /**
   * @param {string}   i18n      - Translation path
   * @param {number[]} dashArray - Array of values for dashes.
   */
  constructor (i18n, dashArray = []) {
    this.dashArrayValues = dashArray
    this.i18n = i18n
  }

  dashArray (width) {
    return this.dashArrayValues.map(n => n * width)
  }

  style (width) {
    const style = {
      strokeWidth: `${width}px`,
    }
    if (this.dashArrayValues.length > 0) style.strokeDasharray = this.dashArray(width).join(',')

    return style
  }
}

export default {
  // I18n use $t('js.stroke_style_presets.default')
  default: new StrokeStylePreset('js.stroke_style_presets.default'),
  // I18n use $t('js.stroke_style_presets.dotted')
  dotted: new StrokeStylePreset('js.stroke_style_presets.dotted', [1, 1]),
  // I18n use $t('js.stroke_style_presets.dashed')
  dashed: new StrokeStylePreset('js.stroke_style_presets.dashed', [4, 4]),
  // I18n use $t('js.stroke_style_presets.dashed2')
  dashed2: new StrokeStylePreset('js.stroke_style_presets.dashed2', [4, 4, 1, 4]),
  // I18n use $t('js.stroke_style_presets.dashed3')
  dashed3: new StrokeStylePreset('js.stroke_style_presets.dashed3', [4, 4, 1, 2, 1, 4]),
  // I18n use $t('js.stroke_style_presets.long_dashes')
  long_dashes: new StrokeStylePreset('js.stroke_style_presets.long_dashes', [8, 4]),
  // I18n use $t('js.stroke_style_presets.long_dashes2')
  long_dashes2: new StrokeStylePreset('js.stroke_style_presets.long_dashes2', [4, 8]),
  // I18n use $t('js.stroke_style_presets.long_dashes3')
  long_dashes3: new StrokeStylePreset('js.stroke_style_presets.long_dashes3', [4, 4, 8, 4]),
}
