/**
 * Adapted from https://www.telerik.com/blogs/checking-vue-3-slots-emptiness,
 * Which was also adapted from https://github.com/vuejs/vue-next/blob/ca17162e377e0a0bf3fae9d92d0fdcb32084a9fe/packages/runtime-core/src/helpers/renderSlot.ts#L77
 */

import { Comment, Fragment, Text } from 'vue'

/**
 * @typedef {import('vue').VNode} VNode
 */

/**
 * Checks if a VNode and its children are considered empty
 *
 * @param   {VNode[]} vNodes - List of VNodes
 * @returns {boolean}        Whether the node is empty or not
 */
function vNodeIsEmpty (vNodes) {
  return vNodes.every(node => {
    // Node is a comment
    if (node.type === Comment) return true
    // Node is an empty text
    if (node.type === Text && !node.children.trim()) return true

    return node.type === Fragment && vNodeIsEmpty(node.children)
  })
}

/**
 * Returns true if a slot has no content
 *
 * @param   {Function} slot - A Vue 3 slot function
 * @returns {boolean}       Whether the slot is empty or not
 */
export function isSlotEmpty (slot) {
  if (!slot) return true

  // if we get a slot that is not a function, we're in vue 2 and there is content, so it's not empty
  if (typeof slot !== 'function') return false

  return vNodeIsEmpty(slot())
}
