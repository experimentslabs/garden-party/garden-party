import GeoJSON from 'ol/format/GeoJSON'
import Feature from 'ol/Feature'
import CircleGeom from 'ol/geom/Circle'

/**
 * @typedef                                        GeoJSONObject
 * @member  {{coordinate: number[], type: string}} geometry
 * @member  {string}                               type
 * @member  {{radius: number}}                     properties
 */

/**
 * @param   {Feature}       feature - Map feature
 * @returns {GeoJSONObject}         GeoJSON representation
 * @private
 */
export function geoJSONFromFeature (feature) {
  let geoJSON
  if (feature.getGeometry().getType() === 'Circle') {
    const geometry = feature.getGeometry().getFlatCoordinates()
    geoJSON = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [geometry[0], geometry[1]],
      },
      properties: { radius: feature.getGeometry().getRadius() },
    }
  } else {
    const formatter = new GeoJSON()
    geoJSON = formatter.writeFeatureObject(feature)
  }

  return geoJSON
}

/**
 * @param   {object}  geometry - Geometry from entity
 * @param   {string}  shape    - Desired shape
 * @returns {Feature}          The feature object
 * @private
 */
export function featureFromGeometry (geometry, shape) {
  if (shape === 'Circle') {
    return new Feature(new CircleGeom([geometry.geometry.coordinates[0], geometry.geometry.coordinates[1]], geometry.properties.radius))
  } else {
    return new GeoJSON().readFeature(geometry)
  }
}
