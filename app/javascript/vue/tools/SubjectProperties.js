import { shapeIcons } from '@/vue/shapeIcons'

/**
 * @typedef {import('../classes/models/Map')} Map
 * @typedef {import('../classes/models/Element')} Element
 * @typedef {import('../classes/models/Patch')} Patch
 * @typedef {import('../classes/models/Path')} Path
 */

/**
 * Returns an icon a name to use as "subject" short description.
 *
 * @param   {Map|Element|Patch|Path}      subject - Subject to describe
 * @returns {{icon: string, name:string}}         - Icon/name object
 */
export function subjectProperties (subject) {
  switch (subject.className) {
    case 'Map':
      return { icon: 'world', name: subject.name }
    case 'Element':
      if (subject.isPoint) {
        return { icon: 'map-pin', name: subject.displayName }
      } else {
        return {
          icon: 'map-pin',
          name: I18n.t('js.todos.task.element_in_patch', {
            elementName: subject.displayName,
            patchName: subject.patch.displayName,
          }),
        }
      }
    case 'Patch':
      return {
        icon: shapeIcons[subject.geometryType].base,
        name: subject.displayName,
      }
    case 'Path':
      return {
        icon: shapeIcons[subject.geometryType].base,
        name: subject.displayName,
      }
    default:
      throw new Error(`Unhandled subject type "${subject.className}"`)
  }
}
