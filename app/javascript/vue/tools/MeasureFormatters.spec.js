import { describe, expect, it } from 'vitest'
import { formatLength, formatArea } from './MeasureFormatters'

describe('formatLength', () => {
  it('adds unit to meters', () => {
    expect.assertions(2)

    expect(formatLength(10)).toBe('10m')
    expect(formatLength(999)).toBe('999m')
  })

  it('converts large values to kilometers', () => {
    expect.assertions(2)

    expect(formatLength(1000)).toBe('1km')
    expect(formatLength(100000)).toBe('100km')
  })
})

describe('formatArea', () => {
  it('adds unit to square meters', () => {
    expect.assertions(1)

    expect(formatArea(1)).toBe('1m<sup>2</sup>')
  })

  it('rounds meters with a precision of 2 if needed', () => {
    expect.assertions(1)

    expect(formatArea(9999.1234)).toBe('9999.12m<sup>2</sup>')
  })

  it('converts large unit to square kilometers', () => {
    expect.assertions(1)

    expect(formatArea(100000)).toBe('0.1km<sup>2</sup>')
  })

  it('rounds kilometers with a precision of 2 if needed', () => {
    expect.assertions(1)

    expect(formatArea(1123400)).toBe('1.12km<sup>2</sup>')
  })
})
