import mitt from 'mitt'

const EventBus = mitt()
const listeners = {}

/**
 * Fires an event on the event bus
 *
 * @param { string} eventName - Event name
 * @param {any}     [payload] - Any payload
 */
export function $emitBus (eventName, payload = null) {
  EventBus.emit(eventName, payload)
}

/**
 * @callback       $onBusCallback
 * @param {any} [payload] - The payload
 */
/**
 * Listens for an event on the event bus
 *
 * The optional `key` parameter should be used by components that registers many
 * listeners and that are often created/destroyed. Application entrypoints are
 * not concerned as once the whole application dies (page reload), all listeners
 * dies with it.
 *
 * @param {string}         eventName - Event name
 * @param {$onBusCallback} callback  - Callback to deal with event payload
 * @param {string|null}    key       - Optional listener key
 * @example
 *    // Without keeping track of the listener:
 *    $onBus('something', (e) => doSomething)
 *    // With a key
 *    $onBus('something', (e) => doSomething, 'mainView')
 *    $onBus('something-else', (e) => doSomethingElse, 'mainView')
 *    $unKey('mainView') // Clears the listeners registered for this view
 */
export function $onBus (eventName, callback, key = null) {
  if (key) {
    if (!listeners[key]) listeners[key] = {}
    if (!listeners[key][eventName]) listeners[key][eventName] = []
    listeners[key][eventName].push(callback)
  }
  EventBus.on(eventName, callback)
}

/**
 * Remove all registered events listeners for a given key
 *
 * @param {string} key - Target key
 */
export function $unBusKey (key) {
  if (!listeners[key]) return

  for (const event in listeners[key]) {
    listeners[key][event].forEach((callback) => { EventBus.off(event, callback) })
  }
  delete listeners[key]
}

export default EventBus
