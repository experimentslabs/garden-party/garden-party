import { Toast } from 'toaster-js'

export default {
  error (message) {
    return new Toast(message, Toast.TYPE_ERROR, Toast.TIME_NORMAL)
  },
  success (message) {
    return new Toast(message, Toast.TYPE_DONE, Toast.TIME_NORMAL)
  },
}
