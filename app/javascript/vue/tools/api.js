import { compactObject } from '../../helpers/Object'
import i18n from '../app_helpers/i18n'
import Toaster from './Toaster'
import qs from 'qs'

/**
 * @typedef          ErrorObject
 * @param {string} error - The error message
 */

const ApiErrors = {
  // i18n-tasks-use t('js.api.unauthorized')
  401: 'js.api.unauthorized',
  // i18n-tasks-use t('js.api.forbidden')
  403: 'js.api.forbidden',
  // i18n-tasks-use t('js.api.page_not_found')
  404: 'js.api.page_not_found',
  // i18n-tasks-use t('js.api.unprocessable_entity')
  422: 'js.api.unprocessable_entity',
  // i18n-tasks-use t('js.api.internal_server_error')
  500: 'js.api.internal_server_error',
}

const ApiErrorKeys = Object.keys(ApiErrors)

/**
 * @param   {Response}           response - The response
 * @param   {object}             context  - Context data for errors translations
 * @returns {object|ErrorObject}          - Response or error
 */
function handleAPIError (response, context) {
  const status = response.status.toString()
  if (ApiErrorKeys.includes(status)) Toaster.error(i18n.global.t(ApiErrors[status], context))
  // i18n-tasks-use t('js.api.unknown_error')
  else Toaster.error(i18n.global.t('js.api.unknown_error', { code: status }))

  try {
    return response.json()
  } catch (SyntaxError) {
    return Promise.reject({ error: 'Unprocessable response (not JSON)' })
  }
}

/**
 * Gets CSRF token from page "meta" headers
 *
 * @returns {string|null} The token or null
 */
function extractCSRF () {
  const token = document.getElementsByName('csrf-token')
  if (token.length > 0) return token[0].content

  return null
}

/**
 * Removes keys with null values from object
 *
 * @param   {object} filters - Filters to clean up
 * @returns {string}         String to use as querystring
 */
function queryString (filters) {
  return qs.stringify(compactObject(filters))
}

/**
 * Makes an Api call and returns a promise with data
 *
 * @param   {string}          method - Request verb
 * @param   {string}          url    - Target URL
 * @param   {object|null}     [data] - Querystring parameters for GET request; payload for other
 * @returns {Promise<object>}        Response content, or an error
 */
export default function (method, url, data = null) {
  method = method.toUpperCase()

  const options = {
    method: method.toUpperCase(),
    headers: {
      Accept: 'application/json',
    },
  }

  if (method === 'GET') {
    if (data) url = `${url}?${queryString(data)}`
  } else {
    if (!data) data = {} // When a null value is passed

    if ((typeof data.append) !== 'function') {
      options.headers['Content-Type'] = 'application/json'
      options.body = JSON.stringify(data)
    } else {
      // Browser will set the right header, boundary, etc...
      options.body = data
    }

    const token = extractCSRF()
    if (token) {
      options.headers['X-CSRF-TOKEN'] = token
    }
  }

  return fetch(url, options)
    .catch(() => {
      // i18n-tasks-use t('js.api.unreachable_server')
      const message = i18n.global.t('js.api.unreachable_server')
      // Still display a message because we don't know if the error will be handled otherwise
      Toaster.error(message)
      return Promise.reject({ error: message })
    })
    .then(async (response) => {
      if (response.status >= 200 && response.status < 400) {
        if (response.status === 204) return null

        return await response.json()
      } else {
        const error = await handleAPIError(response, { url })
        return Promise.reject(error)
      }
    })
}
