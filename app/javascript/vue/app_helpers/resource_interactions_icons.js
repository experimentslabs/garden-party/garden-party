export const NATURE_ICONS = {
  unknown: 'question',
  neutral: 'smiley-meh',
  negative: 'smiley-sad',
  positive: 'smiley',
}

export const NATURE_CSS = {
  unknown: 'text--color-mute',
  neutral: 'text--color-mute',
  negative: 'text--color-destructive',
  positive: 'text--color-success',
}

/**
 * Returns the icon name to use for an interaction's nature boolean pair item
 *
 * @param   {boolean|null|undefined} natureValue - Interaction nature to check
 * @returns {string}                             Icon name
 */
export function natureIcon (natureValue) {
  if (natureValue === true) return NATURE_ICONS.positive
  if (natureValue === false) return NATURE_ICONS.negative
  if (natureValue === undefined) return NATURE_ICONS.unknown

  return NATURE_ICONS.neutral
}

/**
 * Returns the CSS class to use for an interaction's nature boolean pair item
 *
 * @param   {boolean|null|undefined} natureValue - Interaction nature to check
 * @returns {string}                             Class name
 */
export function natureCSS (natureValue) {
  if (natureValue === true) return NATURE_CSS.positive
  if (natureValue === false) return NATURE_CSS.negative

  return NATURE_CSS.neutral
}
