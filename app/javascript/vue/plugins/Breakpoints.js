import { ref } from 'vue'

/** @typedef {import('vue').App} VueApp */

/**
 * List of breakpoints.
 * Order matters, from the smallest to the largest
 */
export const handledBreakpoints = ['small', 'medium', 'large']

/**
 * Adds some breakpoints information available globally
 *
 * Provides two global methods:
 * - `$smallerThan(breakpoint)`
 * - `$largerThan(breakpoint)`
 *
 * @param {VueApp} app - Vue Application
 */
export const BreakpointsPlugin = {
  install (app) {
    const breakpoints = {}
    const screenWidth = ref(0)

    handledBreakpoints.forEach(bp => {
      const breakpoint = window.getComputedStyle(document.body)
        .getPropertyValue(`--bp-${bp}`)
        .replace('px', '')
      breakpoints[bp] = parseInt(breakpoint, 10)
    })

    app.config.globalProperties.$smallerThan = function (breakpoint) { return screenWidth.value < breakpoints[breakpoint] }
    app.config.globalProperties.$largerThan = function (breakpoint) { return screenWidth.value >= breakpoints[breakpoint] }

    /**
     * Updates the screen width value
     */
    function updateWidth () { screenWidth.value = window.innerWidth }

    window.addEventListener('resize', updateWidth)
    updateWidth()
  },
}
