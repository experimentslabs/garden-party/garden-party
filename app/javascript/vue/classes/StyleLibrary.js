import { DEVICE_PIXEL_RATIO } from 'ol/has'
import Style from 'ol/style/Style'
import FillStyle from 'ol/style/Fill'
import StrokeStyle from 'ol/style/Stroke'
import CircleStyle from 'ol/style/Circle'
import MultiPoint from 'ol/geom/MultiPoint'
import PointGeom from 'ol/geom/Point'
import multiPatternSrc from '../assets/images/multi_pattern.svg'
import emptyPatternSrc from '../assets/images/empty_pattern.svg'
import strokeStylePresets from '../strokeStylePresets'
import Path from './models/Path'
import Element from './models/Element'

/**
 * @typedef {import('ol/View')} View
 * @typedef {import('./models/Resource')}  Resource
 * @typedef {import('./models/Placeable')} Placeable
 */

/**
 * Helpers to create styles for OpenLayers features
 */
export default {
  /**
   * Preloaded patterns
   */
  preloaded: {},

  /** @type {Style[]} Style for elements in an interaction */
  INTERACTION_STYLE: [
    // Lines
    new Style({
      stroke: new StrokeStyle({ color: 'rgb(0,153,255)', width: 3 }),
      fill: new FillStyle({ color: 'rgba(255,255,255, 0.4)' }),
    }),
    // Nodes
    new Style({
      image: new CircleStyle({
        radius: 5,
        fill: new FillStyle({ color: 'rgb(255,0,153)' }),
      }),
      geometry: function (feature) {
        const coordinates = feature.getGeometry().getCoordinates()
        if (!coordinates) return null
        // Polygon
        if (coordinates[0][0] instanceof Array) return new MultiPoint(coordinates[0])
        // LineString
        else if (coordinates[0] instanceof Array) return new MultiPoint(coordinates)
        // Point
        return new PointGeom(coordinates)
      },
    }),
  ],

  /** @type {Style[]} Style for selected features */
  SELECT_STYLE: [
    // Lines
    new Style({
      stroke: new StrokeStyle({ color: 'rgb(0,153,255)', width: 3 }),
      fill: new FillStyle({ color: 'rgba(255,255,255, 0.4)' }),
    }),
    // Points
    // TODO: Improve this style to handle elements diameters.
    new Style({
      image: new CircleStyle({
        radius: 12,
        stroke: new StrokeStyle({ color: 'rgb(0,153,255)', width: 3 }),
        fill: new FillStyle({ color: 'rgba(255,255,255, 0.4)' }),
      }),
    }),
  ],

  EMPTY_STYLE_FOR_SHAPE: {
    patternId: 'emptyPattern',
    borderColor: 'darkred',
  },

  MULTI_STYLE_FOR_SHAPE: {
    patternId: 'multiPattern',
    borderColor: 'gray',
  },

  /**
   * Preload resources patterns and pattern for "multi-resource" patches.
   * If not done or done too late, elements backgrounds will be plain red.
   *
   * @returns {Promise<CanvasPattern[]>} Promise to wait before creating resources in maps
   */
  preloadPatterns () {
    const promises = []
    // Multi resource pattern
    promises.push(this.createPicture('multiPattern', 40, multiPatternSrc))
    promises.push(this.createPicture('emptyPattern', 40, emptyPatternSrc))

    return Promise.all(promises)
  },

  /**
   * Load a pattern for a given Resource
   *
   * @param   {Resource}               resource - Resource to load the pattern for
   * @returns {Promise<CanvasPattern>}          Promise that resolves when pattern is ready to use
   */
  preloadPattern (resource) {
    return this.createPicture(resource.id, 24, resource.patternPicturePath)
  },

  /**
   * Creates a pattern image
   *
   * @param   {string|number}          patternKey - The key to use in patterns list
   * @param   {number}                 size       - Pattern side size
   * @param   {string}                 source     - URL for the image
   * @returns {Promise<CanvasPattern>}            Promise that resolves when pattern is ready to use
   */
  async createPicture (patternKey, size, source) {
    const finalSize = size * DEVICE_PIXEL_RATIO

    if (this.preloaded[patternKey]) return Promise.resolve(this.preloaded[patternKey])

    return await new Promise((resolve, reject) => {
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      canvas.width = finalSize
      canvas.height = finalSize
      const picture = document.createElement('img')

      picture.addEventListener('load', async () => {
        // Actually create the pattern when image is loaded
        context.drawImage(picture, 0, 0, finalSize, finalSize)
        this.preloaded[patternKey] = context.createPattern(canvas, 'repeat')

        // Pattern is created
        return resolve(this.preloaded[patternKey])
      })
      picture.addEventListener('error', (e) => reject(e))

      picture.setAttribute('src', source)
    })
  },

  /**
   * Generates styles for a given entity
   *
   * @param   {View}      mapView   - Map's View instance
   * @param   {Placeable} placeable - Entity from which to define the style
   * @returns {Style[]}             Style to apply
   */
  async stylesFromEntity (mapView, placeable) {
    if (placeable instanceof Path) {
      return await this.stylesForShape({
        fillColor: `rgba(${placeable.backgroundColor})`,
        borderColor: `rgba(${placeable.strokeColor})`,
        strokeWidth: placeable.strokeWidth,
        strokeStylePreset: placeable.strokeStylePreset,
      })
    } else if (placeable instanceof Element) {
      const resource = placeable.resource
      return this.stylesForPoint(mapView, resource, placeable)
    } else {
      const elements = placeable.elements.filter(e => !e.isRemoved)
      const amount = elements.length

      let patternId, borderColor
      if (amount === 1) {
        const resource = elements[0].resource
        await this.preloadPattern(resource)
        patternId = resource.id
        borderColor = resource.borderColor
      } else if (amount > 1) {
        patternId = this.MULTI_STYLE_FOR_SHAPE.patternId
        borderColor = this.MULTI_STYLE_FOR_SHAPE.borderColor
      } else {
        patternId = this.EMPTY_STYLE_FOR_SHAPE.patternId
        borderColor = this.EMPTY_STYLE_FOR_SHAPE.borderColor
      }

      return await this.stylesForShape({
        borderColor,
        patternId,
      })
    }
  },

  /**
   * Generates styles for a point representation
   *
   * @param   {View}         mapView  - Map's View
   * @param   {Resource}     resource - Base resource
   * @param   {Element|null} element  - Element, if any
   * @returns {Style[]}               Style to apply to a point
   */
  stylesForPoint (mapView, resource, element = null) {
    const that = this
    const styles = []

    // Current diameter
    if (element && element.diameter) {
      styles.push(
        new Style({
          renderer: (coordinates, state) => {
            const [x, y] = coordinates
            const scale = that._getScale(mapView)

            /** @type {CanvasRenderingContext2D} */
            const ctx = state.context
            ctx.save()

            ctx.beginPath()
            ctx.arc(x, y, (element.diameter / 2) / scale, 0, 2 * Math.PI, true)
            ctx.fillStyle = resource.fillColor
            ctx.fill()
            ctx.strokeStyle = resource.borderColor
            ctx.lineWidth = 1
            ctx.stroke()

            ctx.restore()
          },
        })
      )
    }

    // Final diameter
    if (resource.diameter) {
      styles.push(
        new Style({
          renderer: (coordinates, state) => {
            const [x, y] = coordinates
            const scale = that._getScale(mapView)

            /** @type {CanvasRenderingContext2D} */
            const ctx = state.context
            ctx.save()

            ctx.beginPath()
            ctx.setLineDash([6, 4])
            ctx.arc(x, y, (resource.diameter / 2) / scale, 0, 2 * Math.PI, true)
            ctx.strokeStyle = 'rgba(0,0,0,0.3)'
            ctx.lineWidth = 1
            ctx.stroke()

            ctx.restore()
          },
        })
      )
    }

    // Icon
    styles.push(new Style({
      renderer: (coordinates, state) => {
        /** @type {CanvasRenderingContext2D} */
        const ctx = state.context
        const image = document.createElement('img')
        image.src = resource.picturePath

        ctx.drawImage(image, coordinates[0] - (image.width / 2), coordinates[1] - (image.height / 2))
      },
    }))

    return styles
  },

  /**
   * @param   {object}           params                     - Parameters
   * @param   {string}           params.fillColor           - Fill color
   * @param   {string}           params.borderColor         - Border color
   * @param   {number}           params.strokeWidth         - Stroke width
   * @param   {string|null}      [params.strokeStylePreset] - Style preset for stroke line
   * @param   {number|null}      [params.patternId]         - Pattern index in preloaded patterns
   * @param   {Resource|null}    [params.resource]          - Optional resource. Supersedes the patternId
   * @returns {Promise<Style[]>}                            Style to apply to a fillable patch
   */
  async stylesForShape ({ fillColor, borderColor, strokeWidth = 1, strokeStylePreset = null, patternId = null, resource = null }) {
    let color
    if (resource) color = await this.preloadPattern(resource)
    else if (patternId) color = this.preloaded[patternId]
    else color = fillColor

    const lineDash = (strokeStylePreset && strokeStylePresets[strokeStylePreset]) ? strokeStylePresets[strokeStylePreset].dashArray(strokeWidth) : null

    const styleFill = new FillStyle({ color })
    const strokeStyle = new StrokeStyle({ color: borderColor, width: strokeWidth, lineDash, lineCap: 'butt' })

    return Promise.resolve([
      new Style({
        // Image for nodes while drawing
        image: new CircleStyle({
          fill: styleFill,
          stroke: strokeStyle,
          radius: 5,
        }),
        // Background and line
        fill: styleFill,
        stroke: strokeStyle,
      }),
    ])
  },

  /**
   * Gets the scale to apply when creating a picture for a map
   *
   * @param   {View}   view - Map View instance
   * @returns {number}      - The scale to use to generate images
   * @private
   */
  _getScale (view) {
    const resolution = view.getResolution()
    const metersPerUnit = view.getProjection().getMetersPerUnit()
    return resolution * metersPerUnit / DEVICE_PIXEL_RATIO
  },
}
