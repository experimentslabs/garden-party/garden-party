import Placeable from './Placeable'
import api from '../../tools/api'
import { isAfter } from '@experiments-labs/rise_ui/src/lib/Dates'

/**
 * @typedef {import('./Activity')}    Activity
 * @typedef {import('./Harvest')}     Harvest
 * @typedef {import('./Observation')} Observation
 * @typedef {import('./Patch')}       Patch
 * @typedef {import('./Resource')}    Resource
 * @typedef {import('./Task')}        Task
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['implantedAt', 'resourceId', 'patchId', 'createdAt', 'updatedAt', 'removedAt', 'implantationPlannedFor', 'removalPlannedFor', 'diameter', 'name', 'geometry', 'mapId', 'layerId', 'implantationMode', 'observationsCount', 'status']

class Element extends Placeable {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                          - Data from API
   * @param {null|number} payload.id
   * @param {Date|string} payload.implanted_at
   * @param {number}      payload.resource_id
   * @param {number}      payload.patch_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {Date|string} payload.removed_at
   * @param {Date|string} payload.implantation_planned_for
   * @param {Date|string} payload.removal_planned_for
   * @param {number}      payload.diameter
   * @param {string}      payload.name
   * @param {object}      payload.geometry
   * @param {number}      payload.layer_id
   * @param {string}      payload.implantation_mode
   * @param {number}      payload.observations_count
   * @param {string}      payload.status
   */
  constructor ({ id = null, implanted_at, resource_id, patch_id, created_at, updated_at, removed_at, implantation_planned_for, removal_planned_for, diameter, name, geometry, layer_id, implantation_mode, observations_count, status }) {
    super()
    this.id = id
    this.implantedAt = implanted_at ? new Date(implanted_at) : null
    this.resourceId = resource_id
    this.patchId = patch_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.removedAt = removed_at ? new Date(removed_at) : null
    this.implantationPlannedFor = implantation_planned_for ? new Date(implantation_planned_for) : null
    this.removalPlannedFor = removal_planned_for ? new Date(removal_planned_for) : null
    this.diameter = diameter
    this.name = name
    this.geometry = geometry
    this.layerId = layer_id
    this.implantationMode = implantation_mode
    this.observationsCount = observations_count
    this.status = status
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /** @returns {Resource} Resource from VueX store */
  get resource () { return this.constructor.store.getters.resource(this.resourceId) }
  /** @returns {Patch|null} Related patch from VueX store */
  get patch () { return this.constructor.store.getters.patch(this.patchId) }
  /** @returns {Activity[]} Related activities from VueX store */
  get activities () { return this.constructor.store.getters.activitiesBySubject(this) }
  /** @returns {Harvest[]} Related harvests from VueX store */
  get harvests () { return this.constructor.store.getters.harvestsForElement(this) }
  /** @returns {Observation[]} Related observations from VueX store */
  get observations () { return this.constructor.store.getters.observationsBySubject(this) }
  /** @returns {Task[]} Related tasks from VueX store */
  get tasks () { return this.constructor.store.getters.tasksBySubject(this) }

  get isRemoved () { return this.status === 'removed' }
  get isImplanted () { return this.status === 'implanted' }
  get isPlanned () { return this.status === 'planned' }
  get isImplantationPlanned () { return this.isPlanned && !!this.implantationPlannedFor }
  get isRemovalPlanned () { return this.isImplanted && !!this.removalPlannedFor }
  get isRemovalOverdue () { return this.isRemovalPlanned && !this.isRemoved && isAfter(this.removalPlannedFor, new Date()) }
  get isImplantationOverdue () { return this.isImplantationPlanned && !this.isImplanted && isAfter(this.implantationPlannedFor, new Date()) }
  get hasImplantationDetails () { return this.isRemoved || this.isImplanted }
  get hasImplantationPlan () { return this.isImplantationPlanned || this.isRemovalPlanned }

  /**
   * @returns {string} Element resource's name
   */
  get displayName () {
    const resourceName = this.resource.name

    if (this.name) return `${this.name} (${resourceName})`
    else return resourceName
  }

  get isPoint () { return this.geometry && this.layerId }

  /**
   * Fetches the list for a given patch
   *
   * @param   {number}             patchId - Target patch identifier
   * @returns {Promise<Element[]>}         List of Element instances
   */
  static getPatchIndex (patchId) {
    return api('get', `/api/patches/${patchId}/elements`)
      .then(elements => elements.map(data => new Element(data)))
  }

  /**
   * Creates a duplicate of the element
   *
   * @param   {number}           id - Source element identifier
   * @returns {Promise<Element>}    The new element
   */
  static duplicate (id) {
    return api('post', `/api/elements/${id}/duplicate`)
      .then(data => new Element(data))
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Element' }
}

Element.updatableAttributes = ATTRIBUTES
Element.urls = {
  index (mapId) { return `/api/maps/${mapId}/elements` },
  show (id) { return `/api/elements/${id}` },
  create () { return '/api/elements' },
  update (payload) { return `/api/elements/${payload.id}` },
  destroy (id) { return `/api/elements/${id}` },
}

export default Element
