import TrustedModel from './TrustedModel'
import { underscore } from '../../../../helpers/String'

/**
 * @typedef {import('../ResourceInteraction')} ResourceInteraction
 */

/**
 * Represents a resource from a trusted source
 */
export default class TrustedResourceInteraction extends TrustedModel {
  /* eslint-disable jsdoc/require-param-description */
  /**
   * @param {object}   attributes         List of attributes
   * @param {string}   attributes.id
   * @param {string}   attributes.subject
   * @param {string}   attributes.target
   * @param {string}   attributes.nature
   * @param {string}   attributes.notes
   * @param {string[]} attributes.sources
   */
  constructor (attributes) {
    super(attributes)

    this.subject = attributes.subject
    this.target = attributes.target
    this.nature = attributes.nature
    this.notes = attributes.notes
    this.sources = attributes.sources
  }
  /* eslint-enable jsdoc/require-param-description */

  /** @inheritDoc */
  payloadToSyncField (field) {
    field = underscore(field)
    const payload = this.payloadToSync()

    // Matching between trusted data field and API fields
    if (field === 'subject') return { subject_id: payload.subject_id }
    if (field === 'target') return { target_id: payload.target_id }

    // Other fields
    const hash = {}
    hash[field] = payload[field]

    return hash
  }

  /** @inheritDoc */
  payloadToSync () {
    return {
      id: this.id,
      subject: this.subject,
      target: this.target,
      nature: this.nature,
      notes: this.notes,
      sources: this.sources.join('\n'),
    }
  }

  /** @inheritDoc */
  referencesValues () {
    return {
      subject: {
        current: this.constructor.store.getters.resourceInteractionsGroupByName(this.subject),
        trusted: this.constructor.store.getters.trustedResourceInteractionsGroupByName(this.subject),
      },
      target: {
        current: this.constructor.store.getters.resourceInteractionsGroupByName(this.target),
        trusted: this.constructor.store.getters.trustedResourceInteractionsGroupByName(this.target),
      },
    }
  }

  /** @inheritDoc */
  static get requiredReferences () { return ['subject', 'target'] }

  /** @inheritDoc */
  static get vueXSaveAction () { return 'syncResourceInteraction' }

  /** @inheritDoc */
  static get fieldsToCompare () {
    return [
      'id',
      'subject',
      'target',
      'nature',
      'notes',
      'sources',
    ]
  }
}
