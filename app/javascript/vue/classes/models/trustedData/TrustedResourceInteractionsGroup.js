import TrustedModel from './TrustedModel'
import { underscore } from '../../../../helpers/String'

/**
 * @typedef {import('../ResourceInteractionsGroup')} ResourceInteractionsGroup
 */

/**
 * Represents a resource from a trusted source
 */
export default class TrustedResourceInteractionsGroup extends TrustedModel {
  /* eslint-disable jsdoc/require-param-description */
  /**
   * @param {object} attributes             List of attributes
   * @param {string} attributes.id
   * @param {string} attributes.name
   * @param {string} attributes.description
   */
  constructor (attributes) {
    super(attributes)

    this.name = attributes.name
    this.description = attributes.description
  }
  /* eslint-enable jsdoc/require-param-description */

  /** @inheritDoc */
  payloadToSyncField (field) {
    field = underscore(field)
    const payload = this.payloadToSync()

    // Other fields
    const hash = {}
    hash[field] = payload[field]

    return hash
  }

  /** @inheritDoc */
  payloadToSync () {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
    }
  }

  /** @inheritDoc */
  referencesValues () {
    return {}
  }

  /** @inheritDoc */
  static get requiredReferences () { return [] }

  /** @inheritDoc */
  static get vueXSaveAction () { return 'syncResourceInteractionsGroup' }

  /** @inheritDoc */
  static get fieldsToCompare () {
    return [
      'id',
      'name',
      'description',
    ]
  }
}
