import TrustedModel from './TrustedModel'
import { underscore } from '../../../../helpers/String'

/**
 * @typedef {import('../Resource')} Resource
 */

/**
 * Represents a resource from a trusted source
 */
export default class TrustedResource extends TrustedModel {
  /* eslint-disable jsdoc/require-param-description */
  /**
   * @param {object}   attributes                         List of attributes
   * @param {string}   attributes.id
   * @param {string}   attributes.name
   * @param {string}   attributes.latin_name
   * @param {string[]} attributes.common_names
   * @param {string}   attributes.description
   * @param {string}   attributes.parent
   * @param {string}   attributes.interactions_group
   * @param {string}   attributes.genus
   * @param {number}   attributes.diameter
   * @param {number[]} attributes.sheltered_sowing_months
   * @param {number[]} attributes.soil_sowing_months
   * @param {number[]} attributes.harvesting_months
   * @param {string[]} attributes.tags
   * @param {string[]} attributes.sources
   */
  constructor (attributes) {
    super(attributes)

    this.name = attributes.name
    this.latin_name = attributes.latin_name
    this.common_names = attributes.common_names || []
    this.description = attributes.description || null
    this.parent = attributes.parent || null
    this.interactions_group = attributes.interactions_group || null
    this.genus = attributes.genus
    this.diameter = attributes.diameter || null
    this.sheltered_sowing_months = attributes.sheltered_sowing_months
    this.soil_sowing_months = attributes.soil_sowing_months
    this.harvesting_months = attributes.harvesting_months
    this.tags = attributes.tags || []
    this.sources = attributes.sources || []
  }
  /* eslint-enable jsdoc/require-param-description */

  /** @inheritDoc */
  payloadToSyncField (field) {
    field = underscore(field)
    const payload = this.payloadToSync()
    const hash = {}
    hash[field] = payload[field]

    return hash
  }

  /** @inheritDoc */
  payloadToSync () {
    return {
      id: this.id,
      name: this.name,
      latin_name: this.latin_name,
      common_names: this.common_names,
      description: this.description,
      parent: this.parent,
      interactions_group: this.interactions_group,
      genus: this.genus,
      diameter: this.diameter,
      sheltered_sowing_months: this.sheltered_sowing_months,
      soil_sowing_months: this.soil_sowing_months,
      harvesting_months: this.harvesting_months,
      tags: this.tags,
      sources: this.sources,
    }
  }

  /** @inheritDoc */
  referencesValues () {
    const references = {
      genus: {
        current: this.constructor.store.getters.genusByName(this.genus),
        trusted: this.constructor.store.getters.trustedGenusByName(this.genus),
      },
    }
    if (this.parent) {
      references.parent = {
        current: this.parent ? this.constructor.store.getters.resourceByName(this.parent) : null,
        trusted: this.constructor.store.getters.trustedResourceByName(this.parent),
      }
    }
    if (this.interactions_group) {
      references.interactions_group = {
        current: this.interactions_group ? this.constructor.store.getters.resourceInteractionsGroupByName(this.interactions_group) : null,
        trusted: this.constructor.store.getters.trustedResourceInteractionsGroupByName(this.interactions_group),
      }
    }

    return references
  }

  /** @inheritDoc */
  static get requiredReferences () { return ['genus', 'parent'] }

  /** @inheritDoc */
  static get vueXSaveAction () { return 'syncResource' }

  /** @inheritDoc */
  static get fieldsToCompare () {
    return [
      'id',
      'name',
      'latin_name',
      'common_names',
      'genus',
      'parent',
      'interactions_group',
      'description',
      'diameter',
      'sheltered_sowing_months',
      'soil_sowing_months',
      'harvesting_months',
      'tags',
      'sources',
    ]
  }
}
