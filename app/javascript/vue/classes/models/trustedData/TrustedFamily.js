import TrustedModel from './TrustedModel'
import { underscore } from '../../../../helpers/String'

/**
 * @typedef {import('../Family')} Family
 */

/**
 * Represents a family from a trusted source
 */
export default class TrustedFamily extends TrustedModel {
  /**
   * @param {object} attributes         List of attributes
   * @param {string} attributes.id      Trusted identifier
   * @param {string} attributes.name    Trusted identifier
   * @param {string} attributes.kingdom Trusted kingdom
   * @param {string} attributes.source  Trusted source
   */
  constructor (attributes) {
    super(attributes)

    this.name = attributes.name
    this.kingdom = attributes.kingdom
    this.source = attributes.source || null
  }

  /** @inheritDoc */
  payloadToSyncField (field) {
    field = underscore(field)
    const payload = this.payloadToSync()

    const hash = {}
    hash[field] = payload[field]

    return hash
  }

  /** @inheritDoc */
  payloadToSync () {
    return {
      id: this.id,
      name: this.name,
      kingdom: this.kingdom,
      source: this.source,
    }
  }

  /** @inheritDoc */
  referencesValues () { return {} }

  /** @inheritDoc */
  static get requiredReferences () { return [] }

  /** @inheritDoc */
  static get vueXSaveAction () { return 'syncFamily' }

  /** @inheritDoc */
  static get fieldsToCompare () { return ['id', 'name', 'kingdom', 'source'] }
}
