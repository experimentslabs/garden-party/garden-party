import TrustedModel from './TrustedModel'
import { underscore } from '../../../../helpers/String'

/**
 * @typedef {import('../Genus')} Genus
 */

/**
 * Represents a genus from a trusted source
 */
export default class TrustedGenus extends TrustedModel {
  /**
   * @param {object} attributes        List of attributes
   * @param {string} attributes.id     Trusted identifier
   * @param {string} attributes.family Trusted family name
   * @param {string} attributes.name   Trusted identifier
   * @param {string} attributes.source Trusted source
   */
  constructor (attributes) {
    super(attributes)

    this.name = attributes.name
    this.family = attributes.family
    this.source = attributes.source || null
  }

  /** @inheritDoc */
  payloadToSyncField (field) {
    field = underscore(field)
    const payload = this.payloadToSync()

    // Matching between trusted data field and API fields
    if (field === 'family') return { family_id: payload.family_id }

    // Other fields
    const hash = {}
    hash[field] = payload[field]

    return hash
  }

  /** @inheritDoc */
  payloadToSync () {
    return {
      id: this.id,
      name: this.name,
      family: this.family,
      source: this.source,
    }
  }

  /** @inheritDoc */
  referencesValues () {
    return {
      family: {
        current: this.constructor.store.getters.familyByName(this.family),
        trusted: this.constructor.store.getters.trustedFamilyByName(this.family),
      },
    }
  }

  /** @inheritDoc */
  static get requiredReferences () { return ['family'] }

  /** @inheritDoc */
  static get vueXSaveAction () { return 'syncGenus' }

  /** @inheritDoc */
  static get fieldsToCompare () { return ['id', 'name', 'family', 'source'] }
}
