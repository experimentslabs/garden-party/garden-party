import Placeable from './Placeable'

/**
 * @typedef {import('./Layer')}         Layer
 * @typedef {import('ol/Feature')}      Feature
 * @typedef {import('ol/layer/Vector')} VectorLayer
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'geometry', 'mapId', 'createdAt', 'updatedAt', 'layerId', 'geometryType']

class Patch extends Placeable {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                    - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {object}      payload.geometry
   * @param {number}      payload.map_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {number}      payload.layer_id
   * @param {number}      payload.observations_count
   * @param {string}      payload.geometry_type
   */
  constructor ({ id = null, name, geometry, map_id, created_at, updated_at, layer_id, observations_count, geometry_type }) {
    super()
    this.id = id
    this.name = name
    this.geometry = geometry
    this.mapId = map_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.layerId = layer_id
    this.observationsCount = observations_count
    this.geometryType = geometry_type
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {boolean} True if patch contains no element.
   */
  get isEmpty () {
    return this.elements.length === 0
  }

  /**
   * Gets the patch elements from VueX store
   *
   * @returns {Element[]} List of elements on this patch
   */
  get elements () { return this.constructor.store.getters.elementsByPatchId(this.id) }

  get activeElements () { return this.constructor.store.getters.activeElementsByPatchId(this.id) }
  get lastYearElements () { return this.constructor.store.getters.lastYearElementsByPatchId(this.id) }

  /**
   * Determines the patch name from patch type and content
   *
   * @returns {string} Determined patch name
   */
  get displayName () {
    return this.name || I18n.t('generic.patch.name')
  }

  /**
   * Whether this patch is considered virtual
   *
   * @returns {boolean} True if the patch is virtual
   */
  get isVirtual () {
    return this.geometryType === 'Virtual'
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Patch' }
}

Patch.updatableAttributes = ATTRIBUTES
Patch.urls = {
  index (mapId) { return `/api/maps/${mapId}/patches` },
  show (id) { return `/api/patches/${id}` },
  create () { return '/api/patches' },
  update (payload) { return `/api/patches/${payload.id}` },
  destroy (id) { return `/api/patches/${id}` },
}

export default Patch
