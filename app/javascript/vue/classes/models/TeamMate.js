import Model from './Model'
import api from '../../tools/api'
import User from './User'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['userId', 'mapId', 'user', 'acceptedAt', 'createdAt', 'updatedAt']

class TeamMate extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload             - Data from API
   * @param {null|number} payload.id
   * @param {number}      payload.user_id
   * @param {number}      payload.map_id
   * @param {object}      payload.user
   * @param {object}      payload.map
   * @param {Date|string} payload.accepted_at
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   */
  constructor ({ id = null, user_id, map_id, user, map, accepted_at, created_at, updated_at }) {
    super()
    this.id = id
    this.userId = user_id
    this.mapId = map_id
    this.user = new User(user)
    this.acceptedAt = accepted_at ? new Date(accepted_at) : null
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    // This is not a Map, only a few fields from it
    this.map = map
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'TeamMate' }

  static resendInvitation (id) {
    return api('patch', `/api/team_mates/${id}/resend`)
      .then(entity => new this(entity))
  }

  static acceptInvitation (id) {
    return api('patch', `/api/team_mates/${id}/accept`)
      .then(entity => new this(entity))
  }

  static refuseInvitation (id) {
    return api('delete', `/api/team_mates/${id}/refuse`)
  }

  static leave (id) {
    return api('delete', `/api/team_mates/${id}/leave`)
  }

  static remove (id) {
    return api('delete', `/api/team_mates/${id}/remove`)
  }

  /** Override as there is no route in the api anyway */
  static update () { throw new Error('Not implemented') }
  /** Override as there is no route in the api anyway */
  static destroy () { throw new Error('Not implemented') }
}

TeamMate.updatableAttributes = ATTRIBUTES
TeamMate.urls = {
  index (mapId) { return `/api/maps/${mapId}/team_mates` },
  show (id) { return `/api/team_mates/${id}` },
  create () { return '/api/team_mates' },
}

export default TeamMate
