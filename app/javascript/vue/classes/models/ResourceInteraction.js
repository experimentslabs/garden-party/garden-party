import Model from './Model'

/**
 * @typedef {import('./Resource')}                 Resource
 * @typedef {import('./ResourceInteractionsGroup')} ResourceInteractionsGroup
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['nature', 'notes', 'subjectId', 'targetId', 'sources', 'createdAt', 'updatedAt', 'syncId']

/**
 * List of possible natures and the impact on subject and target.
 * Possible values are:
 * - true: positive impact
 * - false: negative impact
 * - null: neutral impact
 * - undefined: Unknown impact
 * The first entry in the array is the impact on subject, se second is the
 * impact on target.
 */
export const NATURES = {
  unspecified_positive: [true, undefined],
  unspecified_negative: [false, undefined],
  mutualism: [true, true],
  commensalism: [true, null],
  parasitism: [true, false],
  neutralism: [null, null],
  amensalism: [null, false],
  competition: [false, false],
}

export const NATURE_KEYS = Object.keys(NATURES)

// Some constants that can't be declared in model as JS don't support class-level
// constants (as `static const foo = 'bar'`)
const POSITIVE_TO_SUBJECT = NATURE_KEYS.filter(k => NATURES[k][0] === true)
const POSITIVE_TO_TARGET = NATURE_KEYS.filter(k => NATURES[k][1] === true)
const NEGATIVE_TO_SUBJECT = NATURE_KEYS.filter(k => NATURES[k][0] === false)
const NEGATIVE_TO_TARGET = NATURE_KEYS.filter(k => NATURES[k][1] === false)
const NEUTRAL_TO_SUBJECT = NATURE_KEYS.filter(k => NATURES[k][0] === null)
const NEUTRAL_TO_TARGET = NATURE_KEYS.filter(k => NATURES[k][1] === null)

class ResourceInteraction extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload            - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.nature
   * @param {string}      payload.notes
   * @param {number}      payload.subject_id
   * @param {number}      payload.target_id
   * @param {string}      payload.sources
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {string}      payload.sync_id
   */
  constructor ({ id = null, nature, notes, subject_id, target_id, sources, created_at, updated_at, sync_id }) {
    super()
    this.id = id
    this.nature = nature
    this.notes = notes
    this.subjectId = subject_id
    this.targetId = target_id
    this.sources = sources
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.syncId = sync_id
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Find the subject in VueX store
   *
   * @returns {ResourceInteractionsGroup} Subject group
   */
  get subject () { return this.constructor.store.getters.resourceInteractionsGroup(this.subjectId) }

  /**
   * Find the target in VueX store
   *
   * @returns {ResourceInteractionsGroup} Target group
   */
  get target () { return this.constructor.store.getters.resourceInteractionsGroup(this.targetId) }

  /**
   * Checks if the interaction is positive to one of the members
   *
   * @returns {boolean} True if the interaction is positive to one of the members
   */
  get isPositive () { return POSITIVE_TO_SUBJECT.includes(this.nature) || POSITIVE_TO_TARGET.includes(this.nature) }

  /**
   * Checks if the interaction is negative to one of the members
   *
   * @returns {boolean} True if the interaction is negative to one of the members
   */
  get isNegative () { return NEGATIVE_TO_SUBJECT.includes(this.nature) || NEGATIVE_TO_TARGET.includes(this.nature) }

  /**
   * Checks if the interaction is neutral to one of the members
   *
   * @returns {boolean} True if the interaction is neutral to one of the members
   */
  get isNeutral () { return NEUTRAL_TO_SUBJECT.includes(this.nature) || NEUTRAL_TO_TARGET.includes(this.nature) }

  /**
   * Returns the other group in this interaction
   *
   * @param   {number}        groupId - First group identifier
   * @returns {Resource|null}         The other group
   */
  getOtherGroup (groupId) {
    if (groupId === this.subjectId) return this.target
    else if (groupId === this.targetId) return this.subject

    return null
  }

  /**
   * Returns true when given group is positively impacted by the other
   *
   * @param   {number}  groupId - Resource identifier
   * @returns {boolean}         - True when group is positively impacted, false otherwise (negative, neutral or undefined)
   */
  isPositiveWith (groupId) {
    if (this.subjectId === groupId) return ResourceInteraction.positiveToSubject.includes(this.nature)
    if (this.targetId === groupId) return ResourceInteraction.positiveToTarget.includes(this.nature)

    // When neutral or not in the interaction
    return false
  }

  /**
   * Returns true when given group is negatively impacted by the other
   *
   * @param   {number}  groupId - Resource identifier
   * @returns {boolean}         - True when group is negatively impacted, false otherwise (positive, neutral or undefined)
   */
  isNegativeWith (groupId) {
    if (this.subjectId === groupId) return ResourceInteraction.negativeToSubject.includes(this.nature)
    if (this.targetId === groupId) return ResourceInteraction.negativeToTarget.includes(this.nature)

    // When neutral or not in the interaction
    return false
  }

  /**
   * Returns true when given group is neutrally impacted by the other
   *
   * @param   {number}  groupId - Resource identifier
   * @returns {boolean}         - True when group is negatively impacted, false otherwise (positive, neutral or undefined)
   */
  isNeutralWith (groupId) {
    if (this.subjectId === groupId) return ResourceInteraction.neutralToSubject.includes(this.nature)
    if (this.targetId === groupId) return ResourceInteraction.neutralToTarget.includes(this.nature)

    // When not neutral or not in the interaction
    return false
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'ResourceInteraction' }

  static get positiveToSubject () { return POSITIVE_TO_SUBJECT }

  static get positiveToTarget () { return POSITIVE_TO_TARGET }

  static get negativeToSubject () { return NEGATIVE_TO_SUBJECT }

  static get negativeToTarget () { return NEGATIVE_TO_TARGET }

  static get neutralToSubject () { return NEUTRAL_TO_SUBJECT }

  static get neutralToTarget () { return NEUTRAL_TO_TARGET }
}

ResourceInteraction.updatableAttributes = ATTRIBUTES
ResourceInteraction.urls = {
  index () { return '/api/resource_interactions' },
  show (id) { return `/api/resource_interactions/${id}` },
  create () { return '/api/resource_interactions' },
  update (payload) { return `/api/resource_interactions/${payload.id}` },
  destroy (id) { return `/api/resource_interactions/${id}` },
}

export default ResourceInteraction
