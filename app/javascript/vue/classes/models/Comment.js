import api from '../../tools/api'
import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['content', 'removedAt', 'removedByMapOwner', 'userId', 'subjectType', 'subjectId', 'createdAt', 'updatedAt']

class Comment extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                      - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.content
   * @param {Date|string} payload.removed_at
   * @param {boolean}     payload.removed_by_map_owner
   * @param {string}      payload.username
   * @param {number}      payload.user_id
   * @param {string}      payload.subject_type
   * @param {number}      payload.subject_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   */
  constructor ({ id = null, content, removed_at, removed_by_map_owner, user_id, username, subject_type, subject_id, created_at, updated_at }) {
    super()
    this.id = id
    this.content = content
    this.removedAt = removed_at ? new Date(removed_at) : null
    this.removedByMapOwner = removed_by_map_owner
    this.userId = user_id
    this.username = username
    this.subjectType = subject_type
    this.subjectId = subject_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Fetches the list
   *
   * @param   {*}                params - Parameters for API call
   * @returns {Promise<Model[]>}        List of entities
   */
  static getIndex ({ subject }) {
    let url
    switch (subject.className) {
      case 'Observation':
        url = this.urlsList.indexForObservation(subject.id)
        break
      case 'Task':
        url = this.urlsList.indexForTask(subject.id)
        break
      default:
        throw new Error(`Unhandled subject type "${subject.className}"`)
    }
    return api('get', url)
      .then(entities => entities.map(entity => new this(entity)))
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Comment' }

  /**
   * Updates one entity
   *
   * @param   {number}         id - Comment identifier
   * @returns {Promise<Model>}    Model instance
   */
  static remove (id) {
    return api('put', this.urlsList.remove(id))
      .then(entity => new this(entity))
  }
}

Comment.updatableAttributes = ATTRIBUTES
Comment.urls = {
  indexForObservation (observationId) { return `/api/observations/${observationId}/comments` },
  indexForTask (taskId) { return `/api/tasks/${taskId}/comments` },
  show (id) { return `/api/comments/${id}` },
  create () { return '/api/comments' },
  update (payload) { return `/api/comments/${payload.id}` },
  remove (id) { return `/api/comments/${id}/remove` },
}

export default Comment
