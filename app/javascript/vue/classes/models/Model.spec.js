import { beforeEach, describe, expect, it, vi } from 'vitest'
import Model from './Model'
import { testModelFactory, TestModel } from '../../../specs/fakes/TestModel'

// Ignore all possible API calls
vi.mock('../../tools/api', () => ({ default: {} }))

describe('Model', () => {
  describe('#getClassName()', () => {
    describe('when overridden in inherited model', () => {
      it('returns a string', () => {
        expect.assertions(1)

        expect(TestModel.getClassName()).toBe('TestModel')
      })
    })
    describe('when not overridden in inherited model', () => {
      it('throws an error', () => {
        expect.assertions(1)

        const executor = () => new Promise(() => { Model.getClassName() })
        expect(executor()).rejects.toThrow('not defined')
      })
    })
  })

  describe('.updateAttributes', () => {
    let instance
    beforeEach(() => {
      instance = new TestModel(testModelFactory.build({ id: 1 }))
    })
    it('updates the instance', () => {
      expect.assertions(1)

      instance.updateAttributes({ name: 'new name' })
      expect(instance.name).toBe('new name')
    })

    it('ignores non updatable attributes', () => {
      expect.assertions(1)

      instance.updateAttributes({ id: 2 })
      expect(instance.id).toBe(1)
    })
  })
})
