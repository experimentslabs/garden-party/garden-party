import api from '../../tools/api'
import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'center', 'userId', 'createdAt', 'updatedAt', 'extentWidth', 'extentHeight', 'publiclyAvailable', 'notes', 'hideBackground', 'backgroundColor', 'picture']

class Map extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}                               payload                    - Data from API
   * @param {null|number}                          payload.id
   * @param {string}                               payload.name
   * @param {{x:number, y:number}}                 payload.center
   * @param {number}                               payload.user_id
   * @param {Date|string}                          payload.created_at
   * @param {Date|string}                          payload.updated_at
   * @param {number}                               payload.extent_width
   * @param {number}                               payload.extent_height
   * @param {boolean}                              payload.publicly_available
   * @param {string}                               payload.notes
   * @param {boolean}                              payload.hide_background
   * @param {string}                               payload.background_color
   * @param {number}                               payload.observations_count
   * @param {({url: string, size: number[]}|null)} payload.picture
   */
  constructor ({ id = null, name, center, user_id, created_at, updated_at, extent_width, extent_height, publicly_available, notes, hide_background, background_color, observations_count, picture }) {
    super()
    this.id = id
    this.name = name
    this.center = [center.x, center.y]
    this.userId = user_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.extentWidth = extent_width
    this.extentHeight = extent_height
    this.publiclyAvailable = publicly_available
    this.notes = notes
    this.hideBackground = hide_background
    this.backgroundColor = background_color
    this.observationsCount = observations_count
    this.picture = picture
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  get isOSM () {
    return this.picture === null
  }

  /**
   * Creates a new map
   *
   * @param   {object}       payload - Object with underscored keys
   * @returns {Promise<Map>}         Map instance
   */
  static create (payload) {
    const formData = new FormData()
    formData.append('map[name]', payload.name)
    formData.append('map[center][]', payload.center[0])
    formData.append('map[center][]', payload.center[1])
    formData.append('map[picture]', payload.picture)
    formData.append('map[extent_height]', payload.extent_height)
    formData.append('map[extent_width]', payload.extent_width)
    formData.append('map[publicly_available]', payload.publicly_available)
    // We're in form-data, not a JSON object. Prevent sending "null" values.
    if (payload.background_color) formData.append('map[background_color]', payload.background_color)

    return api('post', '/api/maps', formData)
      .then(map => new Map(map))
  }

  /**
   * Checks in the Account VueX store if current user is the map owner
   *
   * @returns {boolean} True when the user is the map owner
   */
  get isOwned () {
    const account = this.constructor.store.getters.account
    return account && account.id === this.userId
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Map' }
}

Map.updatableAttributes = ATTRIBUTES
Map.urls = {
  index () { return '/api/maps' },
  show (id) { return `/api/maps/${id}` },
  update (payload) { return `/api/maps/${payload.id}` },
  destroy (id) { return `/api/maps/${id}` },
}

export default Map
