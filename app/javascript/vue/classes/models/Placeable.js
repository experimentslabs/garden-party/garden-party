import Model from './Model'

/**
 * @typedef {import('ol/Feature')} Feature
 * @typedef {import('./Layer')}    Layer
 */

/**
 * Represents an entity that can be placed on a map and represented by a Feature.
 * Thus, in addition to the methods defined in this class, models MUST have the
 * following properties:
 *
 * @property {object} geometry     - GeoJSON geometry object
 * @property {number} layerId      - GardenParty {Layer} identifier
 * @property {string} geometryType - Type of geometry, that may differ from the GeoJSON geometry type (typically, Circles, which are GeoJSON Points with a radius property)
 */
export default class Placeable extends Model {
  /**
   * Gets the layer on which the element is
   *
   * @returns {(Layer|null)} The layer
   */
  get layer () { return this.constructor.store.getters.layer(this.layerId) }

  /**
   * Assigns an OpenLayer feature to the element
   *
   * @param {Feature} feature - OpenLayer feature to assign
   */
  setFeature (feature) {
    feature.setProperties({ type: this.className.toLowerCase(), id: this.id })
    this.feature = feature
  }
}
