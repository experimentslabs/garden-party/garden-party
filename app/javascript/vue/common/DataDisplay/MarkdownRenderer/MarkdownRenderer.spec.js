import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import MarkdownRenderer from './MarkdownRenderer.vue'

function makeWrapper () {
  return shallowMount(MarkdownRenderer)
}

let wrapper

describe('MarkdownRenderer', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
