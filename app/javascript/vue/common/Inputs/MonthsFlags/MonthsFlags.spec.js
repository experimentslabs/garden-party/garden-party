import { beforeEach, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'
import MonthsFlagsComponent from './MonthsFlags.vue'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

let wrapper

/**
 * @returns {Wrapper<Vue>} Mounted wrapper for the test
 */
function makeParentWrapper () {
  return mount({
    template: '<div><months-flags-component v-model="input" input-id="test" input-name="month" /></div>',
    components: { MonthsFlagsComponent },
    data () {
      return { input: [true, true, true, true, true, true, true, true, true, true, true, true] }
    },
  }, {
    global: {
      mocks: {
        $tm: () => ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'nov', 'dec'],
      },
    },
  })
}

describe('bindings with parent', () => {
  beforeEach(() => {
    wrapper = makeParentWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.element).toMatchSnapshot()
  })

  describe('interactions', () => {
    describe('when changing a value', () => {
      beforeEach(async () => {
        await wrapper.find('input:first-child').setValue(false)
      })

      it('updates parents state', () => {
        expect.assertions(1)

        expect(wrapper.vm.input[0]).toBeFalsy()
      })
    })

    describe('when changing parent state', () => {
      beforeEach(async () => {
        await wrapper.setData({ input: [false, false, false, false, false, false, false, false, false, false, false, false] })
      })

      it('updates the inputs data', () => {
        expect.assertions(1)

        const input = wrapper.find('input:first-child')
        expect(input.element.checked).toBeFalsy()
      })
    })
  })
})
