import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createStore } from 'vuex'

import Timeline from './Timeline.vue'

function makeWrapper () {
  return shallowMount(Timeline, {
    global: {
      plugins: [
        createStore({
          getters: {
            observations: () => [],
            tasks: () => [],
          },
          actions: {
            loadTasksAllForMap: () => Promise.resolve([]),
            loadObservationsByMap: () => Promise.resolve([]),
          },
        }),
      ],
      stubs: ['i18n-t'],
      mocks: {
        $route: { params: { mapId: 1 } },
      },
    },
  })
}

let wrapper

describe('Timeline', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
