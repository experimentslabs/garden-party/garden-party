import { describe, beforeEach, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import Tooltip from './Tooltip.vue'

function makeWrapper () {
  return shallowMount(Tooltip)
}

let wrapper

describe('Tooltip', () => {
  beforeEach(() => {
    wrapper = makeWrapper()
  })

  it('mounts', () => {
    expect.assertions(1)

    expect(wrapper.exists()).toBeTruthy()
  })
})
