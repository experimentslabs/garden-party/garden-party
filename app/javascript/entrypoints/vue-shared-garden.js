import { createApp } from 'vue'
import App from '../vue/apps/shared_garden/App.vue'
import store from '../vue/apps/shared_garden/store'
import routerFor from '../vue/apps/shared_garden/router'
import i18n from '../vue/app_helpers/i18n'
import { RiseUI } from '@experiments-labs/rise_ui'
import icons from '../../assets/images/icons.svg'

import { $onBus, $emitBus } from '../vue/tools/EventBus'

window.sharedMapApp = function (mapId) {
  const app = createApp(App)
    .use(i18n)
    .use(routerFor(mapId))
    .use(store)
    .use(RiseUI, {
      useVueI18n: true,
      i18nObjectAttributesPath: 'activerecord.attributes',
      iconsFile: icons,
    })
  app.config.globalProperties.$onBus = $onBus
  app.config.globalProperties.$emitBus = $emitBus
  app.config.globalProperties.$mapId = mapId
  app.mount('#app')
}
