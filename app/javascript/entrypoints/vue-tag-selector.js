/**
 * This is a workaround for TomSelect freezing the browser when a few tags are used
 *
 * As it's hard to find a vanilla JS tag selector, we opted for "vue-multiselect"
 * to have something working.
 *
 * FIXME: Using this pack is sub-optimal as it embarks vue and makes a big bundle
 * for a small functionality.
 *
 * Nevertheless, it's only used in admin areas.
 */
import { createApp } from 'vue'
import App from '../vue/widgets/TagSelector.vue'
import icons from '../../assets/images/icons.svg'

/**
 * @typedef  {object}   vueTagSelectorOptions
 * @property {string[]} tags         - List of available tags
 * @property {string[]} initialValue - Current tags list
 * @property {string}   name         - Input name
 */

/**
 * Creates a vue-multiselect mini app to use in forms
 * This helper should _only_ be used in Rails templates.
 *
 * @param {string}                id      - Dom element ID
 * @param {vueTagSelectorOptions} options - Options
 */
window.vueTagSelector = function (id, options = {}) {
  if (!options.tags) options.tags = []
  if (!options.initialValue) options.initialValue = []

  // const value = options.initialValue.split(',').map((s) => s.trim())

  const wrapper = document.getElementById(`${id}-wrapper`)
  const element = document.getElementById(wrapper.dataset.id)
  const inputName = element.getAttribute('name')

  const app = createApp(App)
  app.config.globalProperties.$_ruiIconsFiles = { default: icons }
  app.config.globalProperties.$_initialData = {
    inputName,
    value: options.initialValue,
    options: options.tags,
  }

  app.mount(`#${id}-wrapper`)
}
