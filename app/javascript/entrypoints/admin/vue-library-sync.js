import { createApp } from 'vue'
import App from '../../vue/apps/admin_library_sync/App.vue'
import router from '../../vue/apps/admin_library_sync/router'
import store from '../../vue/apps/admin_library_sync/store'
import i18n from '../../vue/app_helpers/i18n'
import { RiseUI } from '@experiments-labs/rise_ui'
import icons from '../../../assets/images/icons.svg'

document.addEventListener('DOMContentLoaded', () => {
  createApp(App)
    .use(router)
    .use(i18n)
    .use(store)
    .use(RiseUI, {
      useVueI18n: true,
      i18nObjectAttributesPath: 'true',
      iconsFile: icons,
    })
    .mount('#app')
})
