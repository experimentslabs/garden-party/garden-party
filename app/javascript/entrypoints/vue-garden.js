import { createApp } from 'vue'
import App from '../vue/apps/garden/App.vue'
import router from '../vue/apps/garden/router'
import store from '../vue/apps/garden/store'
import i18n from '../vue/app_helpers/i18n'
import VirtualScroller from 'vue-virtual-scroller'
import { RiseUI } from '@experiments-labs/rise_ui'
import { BreakpointsPlugin } from '../vue/plugins/Breakpoints'
import icons from '../../assets/images/icons.svg'

import { $onBus, $unBusKey, $emitBus } from '../vue/tools/EventBus'

document.addEventListener('DOMContentLoaded', () => {
  const app = createApp(App)
    .use(router)
    .use(store)
    .use(i18n)
    .use(VirtualScroller)
    .use(BreakpointsPlugin)
    .use(RiseUI, {
      useVueI18n: true,
      i18nObjectAttributesPath: 'activerecord.attributes',
      iconsFile: icons,
    })
  app.config.globalProperties.$emitBus = $emitBus
  app.config.globalProperties.$onBus = $onBus
  app.config.globalProperties.$unBusKey = $unBusKey
  app.mount('#app')
})
