// Generic model class
import Model from '../../vue/classes/models/Model'
import { Factory } from 'fishery'
import faker from 'faker'

export class TestModel extends Model {
  constructor ({ id, name }) {
    super()
    this.id = id
    this.name = name
  }

  static getClassName () { return 'TestModel' }
}
TestModel.updatableAttributes = ['name']
TestModel.urls = {
  index () { return '/api/tests' },
  show (id) { return `/api/tests/${id}` },
  update ({ payload }) { return `/api/tests/${payload.id}` },
  destroy (id) { return `/api/tests/${id}` },
}

export const testModelFactory = Factory.define(({ sequence }) => ({
  id: sequence,
  name: faker.lorem.words(),
}))
