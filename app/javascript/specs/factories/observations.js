import { Factory } from 'fishery'
import faker from 'faker'
import { userFactory } from './users'
import { mapFactory } from './maps'

export const observationFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const user = transientParams.user || userFactory.build()
  const map = transientParams.map || mapFactory.build()

  return {
    id: sequence,

    content: null,
    made_at: creationDate,
    pictures: [],
    subject_id: map.id,
    subject_type: 'Map',
    title: faker.lorem.sentence(),
    comments_count: 0,
    user_id: user.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
