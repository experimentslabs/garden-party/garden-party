import { Factory } from 'fishery'
import faker from 'faker'
import { mapFactory } from './maps'
import { inactiveUserFactory, userFactory } from './users'

export const teamMateFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const user = transientParams.user || userFactory.build()
  const map = transientParams.map || mapFactory.build({ transientParams: { user } })

  return {
    id: sequence,

    accepted_at: null,
    map: { name: map.name },
    map_id: map.id,
    user: {
      id: user.id,
      role: user.role,
      username: user.username,
      account_state: user.account_state,
    },
    user_id: user.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})

/**
 * Returns team mate data for an user with a non-active account
 */
export const pendingUserTeamMateFactory = teamMateFactory.params({
  accepted_at: null,
}).transient({ user: inactiveUserFactory.build() })

/**
 * Data for an accepted team mate
 */
export const acceptedTeamMateFactory = teamMateFactory.params({
  accepted_at: faker.datatype.datetime(),
})
