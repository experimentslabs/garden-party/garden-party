import { Factory } from 'fishery'
import faker from 'faker'

export const geometryLineStringFactory = Factory.define(() => ({
  type: 'Feature',
  properties: null,
  geometry: {
    type: 'LineString',
    coordinates: [
      [
        faker.datatype.number({ max: 100 }),
        faker.datatype.number({ max: 100 }),
      ], [
        faker.datatype.number({ max: 100 }),
        faker.datatype.number({ max: 100 }),
      ],
    ],
  },
}))
