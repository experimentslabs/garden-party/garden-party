import { Factory } from 'fishery'
import faker from 'faker'

export const pictureDetailsFactory = Factory.define(({ params }) => {
  const size = params.size || '200x200'

  const dimensions = size.split('x')

  return {
    size,
    url: faker.image.imageUrl(dimensions[0], dimensions[1]),
  }
})
