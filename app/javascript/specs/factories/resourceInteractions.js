import { Factory } from 'fishery'
import { resourceInteractionsGroupFactory } from './resourceInteractionsGroups'
import { randomEntry } from '../../helpers/Arrays'
import { RESOURCE_INTERACTION_NATURES } from '../../helpers/constants'

export const resourceInteractionFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const subject = transientParams.resourceInteractionsGroup || resourceInteractionsGroupFactory.build()
  const target = transientParams.resourceInteractionsGroup || resourceInteractionsGroupFactory.build()

  return {
    id: sequence,

    nature: RESOURCE_INTERACTION_NATURES[randomEntry(RESOURCE_INTERACTION_NATURES)],
    notes: null,
    sources: null,
    subject_id: subject.id,
    sync_id: null,
    target_id: target.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
