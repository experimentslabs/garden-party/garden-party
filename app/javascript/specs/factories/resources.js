import { Factory } from 'fishery'
import faker from 'faker'
import { genusFactory } from './genera'

export const resourceFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const genus = transientParams.genus || genusFactory.build()

  return {
    id: sequence,

    color: faker.internet.color(),
    child_count: 0,
    common_names: null,
    crop_rotation_group: null,
    description: null,
    diameter: null,
    generic: false,
    genus_id: genus.id,
    harvesting_months: [],
    latin_name: faker.lorem.words(3),
    name: faker.lorem.words(3),
    parent_id: transientParams.parent ? transientParams.parent.id : null,
    resource_interactions_group_id: null,
    sheltered_sowing_months: [],
    soil_sowing_months: [],
    sources: null,
    sync_id: null,
    tag_list: [],

    created_at: creationDate,
    updated_at: creationDate,
  }
})
