import { Factory } from 'fishery'
import faker from 'faker'
import { familyFactory } from './families'

export const genusFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const family = transientParams.family || familyFactory.build()

  return {
    id: sequence,

    family_id: family.id,
    name: faker.lorem.words(3),
    source: null,
    sync_id: null,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
