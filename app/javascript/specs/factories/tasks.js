import { Factory } from 'fishery'
import faker from 'faker'
import { mapFactory } from './maps'

export const taskFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const map = transientParams.map || mapFactory.build()

  return {
    id: sequence,

    action: null,
    assignee_id: null,
    done_at: null,
    name: faker.lorem.sentence(),
    notes: null,
    planned_for: null,
    comments_count: 0,
    subject_id: map.id,
    subject_type: 'Map',

    created_at: creationDate,
    updated_at: creationDate,
  }
})
