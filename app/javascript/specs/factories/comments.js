import { Factory } from 'fishery'
import faker from 'faker'
import { observationFactory } from './observations'
import { userFactory } from './users'

export const commentFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const user = transientParams.user || userFactory.build()
  const observation = transientParams.observation || observationFactory.build()

  return {
    id: sequence,

    content: faker.lorem.sentence(),
    removed_at: null,
    removed_by_map_owner: false,
    subject_id: observation.id,
    subject_type: 'Observation',
    user_id: user.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
