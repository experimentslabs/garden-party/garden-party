import { beforeEach, describe, expect, it } from 'vitest'
import * as factories from './teamMates'

let attributes

describe('Team mate factories', () => {
  describe('teamMateFactory', () => {
    beforeEach(() => {
      attributes = factories.teamMateFactory.build()
    })

    it('builds an active user', () => {
      expect.assertions(1)

      expect(attributes.user.account_state).toBe('active')
    })

    it('builds a pending team mate', () => {
      expect.assertions(1)

      expect(attributes.accepted_at).toBeNull()
    })
  })

  describe('pendingUserTeamMateFactory', () => {
    beforeEach(() => {
      attributes = factories.pendingUserTeamMateFactory.build()
    })

    it('has a pending user', () => {
      expect.assertions(1)

      expect(attributes.user.account_state).toBe('pending')
    })

    it('builds a pending team mate', () => {
      expect.assertions(1)

      expect(attributes.accepted_at).toBeNull()
    })
  })

  describe('acceptedTeamMateFactory', () => {
    beforeEach(() => {
      attributes = factories.acceptedTeamMateFactory.build()
    })
    it('has active user', () => {
      expect.assertions(1)

      expect(attributes.user.account_state).toBe('active')
    })

    it('builds an active team mate', () => {
      expect.assertions(1)

      expect(attributes.accepted_at).not.toBeNull()
    })
  })
})
