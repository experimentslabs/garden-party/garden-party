import { Factory } from 'fishery'
import faker from 'faker'
import { userFactory } from './users'

export const notificationFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const recipient = transientParams.recipient || userFactory.build()

  return {
    id: sequence,

    archived_at: null,
    content: {
      title: faker.lorem.sentence(),
      message: faker.lorem.paragraph(),
    },
    map_id: null,
    map_name: null,
    recipient_id: recipient.id,
    sender: null,
    sender_id: null,
    subject: null,
    subject_id: null,
    subject_type: null,
    type: 'SystemNotification',

    created_at: creationDate,
    updated_at: creationDate,
  }
})
