import { Factory } from 'fishery'

export const preferencesFactory = Factory.define(() => {
  return {
    map: null,
    theme: null,
  }
})
