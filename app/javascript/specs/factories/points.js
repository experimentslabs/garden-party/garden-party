import { Factory } from 'fishery'
import faker from 'faker'

export const pointFactory = Factory.define(() => {
  return {
    x: faker.datatype.float(),
    y: faker.datatype.float(),
  }
})
