import { beforeEach, describe, expect, it } from 'vitest'
import * as factories from './users'

let attributes

describe('Users factories', () => {
  describe('userFactory', () => {
    beforeEach(() => {
      attributes = factories.userFactory.build()
    })

    it('builds an active user', () => {
      expect.assertions(1)

      expect(attributes.account_state).toBe('active')
    })
  })

  describe('inactiveUserFactory', () => {
    beforeEach(() => {
      attributes = factories.inactiveUserFactory.build()
    })

    it('has a pending user', () => {
      expect.assertions(1)

      expect(attributes.account_state).toBe('pending')
    })
  })
})
