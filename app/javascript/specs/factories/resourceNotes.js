import { Factory } from 'fishery'
import faker from 'faker'
import { resourceFactory } from './resources'

export const resourceNoteFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const resource = transientParams.resource || resourceFactory.build()

  return {
    id: sequence,

    content: faker.lorem.words(3),
    resource_id: resource.id,
    user_id: null,
    username: null,
    visible: true,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
