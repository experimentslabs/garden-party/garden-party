import { Factory } from 'fishery'
import { elementFactory } from './elements'

export const updatedSubjectStatusFactory = Factory.define(({ transientParams }) => {
  const element = transientParams.element || elementFactory.build()
  return {
    subject: element,
    subject_id: element.id,
    subject_type: 'Element',
  }
})
