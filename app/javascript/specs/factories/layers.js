import { Factory } from 'fishery'
import faker from 'faker'
import { mapFactory } from './maps'

export const layerFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const map = transientParams.map || mapFactory.build()

  return {
    id: sequence,

    map_id: map.id,
    name: faker.lorem.word(),
    position: faker.datatype.number(),
    visible_by_default: true,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
