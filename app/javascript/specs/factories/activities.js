import { Factory } from 'fishery'

import { mapFactory } from './maps'
import { userFactory } from './users'
import { elementFactory } from './elements'

export const activityFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const element = transientParams.element || elementFactory.build()
  const map = transientParams.map || mapFactory.build()
  const user = transientParams.user || userFactory.build()

  return {
    id: sequence,

    action: 'create',
    data: {},
    happened_at: creationDate,
    map_id: map.id,
    subject_id: element.id,
    subject_name: element.name,
    subject_type: 'Element',
    user_id: user.id,
    username: user.username,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
