import { Factory } from 'fishery'
import { geometryCircleFactory } from './geometryCircleFactory'
import { geometryLineStringFactory } from './geometryLineStringFactory'
import { geometryPolyLineFactory } from './geometryPolyLineFactory'
import { layerFactory } from './layers'
import { mapFactory } from './maps'

export const pathFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const map = transientParams.map || mapFactory.build()
  const layer = transientParams.layer || layerFactory.build({ transientParams: { map } })

  return {
    id: sequence,

    name: null,
    background_color: '0,0,0,1',
    geometry: transientParams.geometry || geometryCircleFactory.build(),
    geometry_type: 'Circle',
    layer_id: layer.id,
    map_id: map.id,
    observations_count: 0,
    stroke_color: '0,0,0',
    stroke_style_preset: null,
    stroke_width: 1,

    created_at: creationDate,
    updated_at: creationDate,
  }
})

export const pathLineFactory = pathFactory.params({
  geometry: geometryLineStringFactory.build(),
  geometry_type: 'LineString',
})

export const pathPolygonFactory = pathFactory.params({
  geometry: geometryPolyLineFactory.build(),
  geometry_type: 'Polygon',
})
