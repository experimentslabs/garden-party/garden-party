import { Factory } from 'fishery'
import faker from 'faker'

export const userFactory = Factory.define(({ sequence }) => {
  return {
    id: sequence,

    account_state: 'active',
    role: 'user',
    username: faker.internet.userName(),
  }
})

/**
 * Data for an user with inactive account
 */
export const inactiveUserFactory = userFactory.params({
  account_state: 'pending',
})
