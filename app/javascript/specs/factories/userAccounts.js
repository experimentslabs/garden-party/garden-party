import { Factory } from 'fishery'
import faker from 'faker'

export const userAccountFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toISOString()

  return {
    id: sequence,

    email: faker.lorem.words(3),
    preferences: null,
    role: 'user',
    username: faker.internet.userName(),

    created_at: creationDate,
    updated_at: creationDate,
  }
})
