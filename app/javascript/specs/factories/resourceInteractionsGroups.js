import { Factory } from 'fishery'
import faker from 'faker'

export const resourceInteractionsGroupFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toISOString()

  return {
    id: sequence,

    description: null,
    name: faker.lorem.words(3),
    sync_id: null,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
