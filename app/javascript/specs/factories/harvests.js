import { Factory } from 'fishery'
import faker from 'faker'
import { elementFactory } from './elements'
import { userFactory } from './users'
import { randomEntry } from '../../helpers/Arrays'
import { HARVEST_UNITS } from '../../helpers/constants'

export const harvestFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const element = transientParams.element || elementFactory.build()
  const user = transientParams.user || userFactory.build()

  return {
    id: sequence,

    element_id: element.id,
    harvested_at: creationDate,
    quantity: faker.datatype.float(),
    unit: randomEntry(HARVEST_UNITS),
    user_id: user.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
