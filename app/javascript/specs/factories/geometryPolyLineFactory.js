import { Factory } from 'fishery'
import faker from 'faker'

export const geometryPolyLineFactory = Factory.define(() => {
  const startingPoint = [
    faker.datatype.number({ max: 100 }),
    faker.datatype.number({ max: 100 }),
  ]

  return {
    type: 'Feature',
    properties: null,
    geometry: {
      type: 'Polygon',
      coordinates: [
        startingPoint,
        [
          faker.datatype.number({ max: 100 }),
          faker.datatype.number({ max: 100 }),
        ],
        startingPoint,
      ],
    },
  }
})
