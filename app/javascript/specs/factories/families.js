import { Factory } from 'fishery'
import faker from 'faker'

export const familyFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toISOString()

  return {
    id: sequence,

    kingdom: 'plant',
    name: faker.lorem.word(),
    source: null,
    sync_id: null,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
