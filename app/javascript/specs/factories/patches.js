import { Factory } from 'fishery'
import { layerFactory } from './layers'
import { mapFactory } from './maps'

export const patchFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const layer = transientParams.layer || layerFactory.build()
  const map = transientParams.map || mapFactory.build()

  return {
    id: sequence,

    geometry: {}, // FIXME
    geometry_type: 'Point',
    layer_id: layer.id,
    map_id: map.id,
    name: null,
    observations_count: 0,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
