import { Factory } from 'fishery'
import faker from 'faker'
import { trustedFamilyFactory } from './trustedFamilies'

export const trustedGenusFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toString()
  const trustedFamily = transientParams.trustedFamily || trustedFamilyFactory.build()

  return {
    id: sequence,

    name: faker.lorem.word(),
    family_id: trustedFamily.id,
    source: null,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
