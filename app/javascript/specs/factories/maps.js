import { Factory } from 'fishery'
import faker from 'faker'
import { userFactory } from './users'
import { pointFactory } from './points'

export const mapFactory = Factory.define(({ sequence, params, transientParams }) => {
  const creationDate = new Date().toISOString()
  const user = transientParams.user || userFactory.build()

  return {
    id: sequence,

    background_color: null,
    center: params.center || pointFactory.build(),
    extent_height: null,
    extent_width: null,
    hide_background: false,
    name: faker.lorem.word(),
    notes: null,
    observations_count: 0,
    picture: { url: 'http://example.com/picture.png', size: [200, 200] },
    publicly_available: false,
    user_id: user.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
