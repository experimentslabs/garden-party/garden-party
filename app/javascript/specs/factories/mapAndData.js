import { Factory } from 'fishery'

export const mapAndDataFactory = Factory.define(() => {
  return {
    elements: [],
    layers: [],
    map: {},
    patches: [],
    paths: [],
    resources: [],
  }
})
