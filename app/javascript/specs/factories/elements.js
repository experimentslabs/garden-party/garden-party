import { Factory } from 'fishery'
import { patchFactory } from './patches'
import { resourceFactory } from './resources'

export const elementFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toISOString()
  const patch = transientParams.patch || patchFactory.build()
  const resource = transientParams.resource || resourceFactory.build()

  return {
    id: sequence,

    diameter: null,
    geometry: null,
    implantation_mode: null,
    implantation_planned_for: null,
    implanted_at: null,
    layer_id: null,
    name: null,
    observations_count: 0,
    patch_id: patch.id,
    removal_planned_for: null,
    removed_at: null,
    resource_id: resource.id,
    status: 'planned',

    created_at: creationDate,
    updated_at: creationDate,
  }
})

export const implantedElementFactory = elementFactory.params(() => ({
  implanted_at: new Date().toString(),
  status: 'implanted',
}))
