import { Factory } from 'fishery'
import faker from 'faker'

export const trustedFamilyFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toISOString()

  return {
    id: sequence,

    name: faker.lorem.word(),
    kingdom: 'plant',
    source: null,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
