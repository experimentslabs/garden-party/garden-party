import { Factory } from 'fishery'
import faker from 'faker'

export const geometryCircleFactory = Factory.define(() => ({
  type: 'Feature',
  properties: { radius: faker.datatype.number({ max: 50 }) },
  geometry: { type: 'Point', coordinates: [faker.datatype.number({ max: 100 }), faker.datatype.number({ max: 100 })] },
}))
