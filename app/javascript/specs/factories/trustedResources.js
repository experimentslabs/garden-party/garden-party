import { Factory } from 'fishery'
import faker from 'faker'
import { trustedGenusFactory } from './trustedGenera'

export const trustedResourceFactory = Factory.define(({ sequence, transientParams }) => {
  const creationDate = new Date().toString()
  const genus = trustedGenusFactory.build()

  return {
    id: sequence,

    name: faker.lorem.words(3),
    latin_name: faker.lorem.words(3),
    common_names: [],
    description: null,
    parent_id: transientParams.parent ? transientParams.parent.id : null,
    interaction_group: null,
    genus_id: genus.id,
    diameter: null,
    sheltered_sowing_months: [],
    soil_sowing_months: [],
    harvesting_months: [],
    tags: [],
    sources: [],

    created_at: creationDate,
    updated_at: creationDate,
  }
})
