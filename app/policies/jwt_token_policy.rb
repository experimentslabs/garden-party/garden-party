# frozen_string_literal: true

class JwtTokenPolicy < ApplicationPolicy
  class Scope < Scope
    # Only access owned JWT tokens
    def resolve
      return JwtToken.none if @user.blank?

      scope.where user_id: @user.id
    end
  end

  def index?
    logged_in?
  end

  def create?
    logged_in?
  end

  def destroy?
    owner?
  end
end
