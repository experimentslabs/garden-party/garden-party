# frozen_string_literal: true

module Api
  class LayerPolicy < ApiApplicationPolicy
    def create?
      in_team?
    end

    def show?
      in_team?
    end

    def update?
      in_team?
    end

    def destroy?
      in_team?
    end

    class Scope < Scope
      def resolve
        return Layer.none if @user.blank?

        map_ids = @user.accessible_maps.pluck :id
        scope.where(map_id: map_ids)
      end
    end
  end
end
