# frozen_string_literal: true

module Api
  class TeamMatePolicy < ApiApplicationPolicy
    def create?
      map_owner?
    end

    def show?
      map_owner?
    end

    def accept?
      invitee?
    end

    def refuse?
      invitee?
    end

    def leave?
      invitee?
    end

    def remove?
      map_owner?
    end

    class Scope < Scope
      def resolve
        return TeamMate.none if @user.blank?

        map_ids = @user.accessible_maps.pluck :id
        scope.where(map: map_ids)
      end
    end

    def map_owner?
      return false unless logged_in?

      @record&.map&.user == @user
    end

    def invitee?
      return false unless logged_in?

      @record.user == @user
    end
  end
end
