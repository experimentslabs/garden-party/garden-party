# frozen_string_literal: true

module Api
  class ApiApplicationPolicy < ApplicationPolicy
    def index?
      logged_in?
    end

    def show?
      logged_in?
    end

    def create?
      logged_in?
    end

    def update?
      logged_in?
    end

    def destroy?
      logged_in?
    end

    private

    def map
      return @map if instance_variable_defined? :@map

      @map = GardenParty::MapFinder.for @record
    end

    def map_owner?
      logged_in? && map&.user_id == @user.id
    end

    # Determines if the user in in the map's team
    def in_team?
      return false unless logged_in? && map

      map.accepted_team_mate?(@user)
    end

    def publicly_available?
      map&.publicly_available?
    end
  end
end
