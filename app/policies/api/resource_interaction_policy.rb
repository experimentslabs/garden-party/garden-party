# frozen_string_literal: true

module Api
  class ResourceInteractionPolicy < ApiApplicationPolicy
    def destroy?
      admin?
    end

    class Scope < Scope
      def resolve
        return ResourceInteraction.none if @user.blank?

        scope.all
      end
    end
  end
end
