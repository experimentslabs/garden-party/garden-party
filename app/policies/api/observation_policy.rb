# frozen_string_literal: true

module Api
  class ObservationPolicy < ApiApplicationPolicy
    def create?
      in_team?
    end

    def show?
      in_team? || publicly_available?
    end

    def update?
      owner?
    end

    def destroy?
      owner? || map_owner?
    end

    def picture_thumbnail?
      in_team? || publicly_available?
    end

    def picture?
      picture_thumbnail?
    end

    class Scope < Scope
      # Returning all observations by default; authorization must be made in
      # controller on an higher entity (as the map).
      def resolve
        scope.all
      end
    end
  end
end
