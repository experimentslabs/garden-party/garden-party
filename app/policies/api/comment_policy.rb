# frozen_string_literal: true

module Api
  class CommentPolicy < ApiApplicationPolicy
    def create?
      in_team? || (logged_in? && publicly_available?)
    end

    def show?
      in_team? || publicly_available?
    end

    def update?
      (owner? && (in_team? || publicly_available?)) || map_owner?
    end

    def remove?
      (owner? && (in_team? || publicly_available?)) || map_owner?
    end

    class Scope < Scope
      def resolve
        return Comment.none if @user.blank? || (!in_team? && !publicly_available?)

        scope.all
      end
    end
  end
end
