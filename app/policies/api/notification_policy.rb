# frozen_string_literal: true

module Api
  class NotificationPolicy < ApiApplicationPolicy
    def show?
      owner?
    end

    def archive?
      owner?
    end

    def destroy?
      owner?
    end

    def archive_all?
      index?
    end

    def destroy_all_archived?
      index?
    end

    class Scope < Scope
      def resolve
        return Notification.none if @user.blank?

        scope.where recipient: @user
      end
    end

    def owner?
      return false unless logged_in?

      @record&.recipient == @user
    end
  end
end
