# frozen_string_literal: true

module Api
  class ElementPolicy < ApiApplicationPolicy
    def create?
      in_team?
    end

    def show?
      in_team?
    end

    def update?
      in_team?
    end

    def duplicate?
      in_team?
    end

    def destroy?
      in_team?
    end

    class Scope < Scope
      def resolve
        return Element.none if @user.blank?

        # All possible maps
        map_ids = @user.accessible_maps.select :id
        # All possible patches
        patch_ids = Patch.where(map_id: map_ids).select :id
        layer_ids = Layer.where(map_id: map_ids).select :id
        # All possible elements
        scope.where(layer_id: layer_ids).or(Element.where(patch_id: patch_ids))
      end
    end
  end
end
