# frozen_string_literal: true

module Api
  class TaskPolicy < ApiApplicationPolicy
    def create?
      in_team?
    end

    def show?
      in_team?
    end

    def update?
      in_team?
    end

    def destroy?
      in_team?
    end

    class Scope < Scope
      # Returning all tasks by default; authorization must be made in
      # controller on an higher entity (as the map).
      def resolve
        scope.all
      end
    end
  end
end
