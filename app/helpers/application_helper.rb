# frozen_string_literal: true

module ApplicationHelper
  def owner?(entity, field: :user_id)
    entity[field] == current_user&.id
  end

  def admin_controller?
    controller.is_a?(Admin::AdminApplicationController)
  end
end
