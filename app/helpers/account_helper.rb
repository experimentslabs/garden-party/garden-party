# frozen_string_literal: true

module AccountHelper
  NOTIFICATION_PREFERENCE_OPTIONS = {
    both:   I18n.t('account.preferences.notification_preset.both'),
    in_app: I18n.t('account.preferences.notification_preset.in_app'),
    email:  I18n.t('account.preferences.notification_preset.email'),
    none:   I18n.t('account.preferences.notification_preset.none'),
  }.freeze

  def notification_preference_check_box(form_builder, type:, user:)
    type = type.to_s
    default = GardenParty::UserPreferences::EMAIL_NOTIFICATIONS_DEFAULTS[type]
    disabled = GardenParty::UserPreferences::IMMUTABLE_EMAIL_NOTIFICATION_PREFERENCES.include? type

    value = user.preferences.dig('email_notifications', type)
    checked = value.nil? ? default : value

    form_builder.check_box type, checked: checked, disabled: disabled
  end
end
