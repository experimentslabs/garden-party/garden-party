# frozen_string_literal: true

module VersionsHelper
  YAML_PERMITTED_CLASSES = [
    ActiveSupport::TimeWithZone,
    Time,
    ActiveSupport::TimeZone,
    ActsAsTaggableOn::TagList,
    Symbol,
    ActsAsTaggableOn::DefaultParser,
  ].freeze

  def version_committer(version)
    return '-' unless version.whodunnit

    user = User.find_by(id: version.whodunnit)
    return I18n.t('version_helper.version_committer.deleted_user', id: version.whodunnit) unless user

    user.username
  end

  def version_diff(version)
    output = ''

    version.object_changes.each_pair do |key, values|
      diff = Diffy::Diff.new(*values, include_plus_and_minus_in_html: true, context: 2, ignore_crlf: true).to_s(:html)
      output += tag.h2 key
      output += diff.html_safe # rubocop:disable Rails/OutputSafety
    end

    output.html_safe # rubocop:disable Rails/OutputSafety
  end
end
