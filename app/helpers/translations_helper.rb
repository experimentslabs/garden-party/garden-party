# frozen_string_literal: true

module TranslationsHelper
  def yes_no(bool)
    bool ? t('generic.yes') : t('generic.no')
  end
end
