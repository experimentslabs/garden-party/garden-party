# frozen_string_literal: true

module FormHelper
  # rubocop:disable Rails/OutputSafety
  def tag_selector(form, field, id, values) # rubocop:disable Metrics/MethodLength
    # The text field will be erased by the component, once created, but allows
    # to easily get its name to create a hidden field in component
    string = <<~HTML
      <div id="#{id}-wrapper" data-id="#{id}">
        #{form.text_field field, id: id, value: values.join(','), style: 'display:none'}
      </div>
      <script>
        document.addEventListener('DOMContentLoaded', function () {
          vueTagSelector('#{id}', {
            tags: #{raw(tags_for_select.to_json)},
            initialValue: #{raw(values.to_json)}
          })
        })
      </script>
    HTML

    string.html_safe
  end
  # rubocop:enable Rails/OutputSafety
end
