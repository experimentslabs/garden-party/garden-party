# frozen_string_literal: true

class DeviseMailer < Devise::Mailer
  def subject_for(key)
    string = I18n.t :"#{devise_mapping.name}_subject",
                    scope:   [:devise, :mailer, key],
                    default: [:subject, key.to_s.humanize]

    "[#{Rails.configuration.garden_party.instance[:name]}] #{string}"
  end
end
