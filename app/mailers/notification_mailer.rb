# frozen_string_literal: true

class NotificationMailer < ApplicationMailer
  before_action :set_instance_variables

  def new_comment_notification_email
    send_email I18n.t('notification_mailer.subjects.comment_notification'),
               I18n.t('notification_mailer.comments.new.subject', name: @sender.username)
  end

  def new_system_notification_email
    send_email I18n.t('notification_mailer.subjects.system_notification'), @content['title']
  end

  def new_team_mate_invite_notification_email
    send_email I18n.t('notification_mailer.subjects.team_mate_notification'),
               I18n.t('notification_mailer.team_mates.invite.subject', name: @subject.map.name)
  end

  def new_team_mate_leave_notification_email
    send_email I18n.t('notification_mailer.subjects.team_mate_notification'),
               I18n.t('notification_mailer.team_mates.leave.subject', name: @sender.username)
  end

  def new_team_mate_refuse_notification_email
    send_email I18n.t('notification_mailer.subjects.team_mate_notification'),
               I18n.t('notification_mailer.team_mates.refuse.subject')
  end

  def new_team_mate_remove_notification_email
    send_email I18n.t('notification_mailer.subjects.team_mate_notification'),
               I18n.t('notification_mailer.team_mates.remove.subject')
  end

  private

  def send_email(subject_scope, subject_string)
    mail subject: subject(subject_scope, subject_string),
         to:      @recipient.email
  end

  def set_instance_variables
    notification = params[:notification]
    @subject     = notification.subject
    @content     = notification.content
    @recipient   = notification.recipient
    @sender      = notification.sender
    @date        = notification.created_at
    @map         = notification.map
  end
end
