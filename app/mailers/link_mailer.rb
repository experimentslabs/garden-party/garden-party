# frozen_string_literal: true

class LinkMailer < ApplicationMailer
  def notify_email
    @recipient = params[:recipient]
    @action    = params[:action]
    @link      = params[:link]

    subject_scope = t('activerecord.models.link', count: 2)
    subject_string = I18n.t 'link_mailer.notify_email.subject', author: @link.user.username, title: @link.title
    mail subject: subject(subject_scope, subject_string), to: @recipient.email
  end
end
