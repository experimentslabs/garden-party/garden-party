# frozen_string_literal: true

require 'garden_party/activity_helper'
require 'garden_party/map_finder'

class Activity < ApplicationRecord
  validates :action, presence: true

  belongs_to :user
  belongs_to :map
  belongs_to :subject, polymorphic: true, optional: true

  scope :for_element, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Element') }
  scope :for_map, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Map') }
  scope :for_patch, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Patch') }
  scope :for_path, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Path') }

  before_validation do |activity|
    map = GardenParty::MapFinder.by_subject activity
    # Fallback to map owner when no user is specified (system changes)
    activity.user_id ||= Current.user&.id || map.user_id
    # Keep username for cases when an user left the team
    activity.username = activity.user.username
    activity.happened_at ||= Time.current
    activity.map_id = map.id
    activity.subject_name = GardenParty::ActivityHelper.subject_name activity
  end
end
