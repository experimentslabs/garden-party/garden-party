# frozen_string_literal: true

require 'garden_party/activities/team_mate'

class TeamMate < ApplicationRecord
  belongs_to :user
  belongs_to :map

  has_many :activities, as: :subject, dependent: :destroy
  has_many :notifications, as: :subject, dependent: :delete_all

  after_create :notify_user_invite

  after_create { |team_mate| GardenParty::Activities::TeamMate.create_create_activity team_mate }
  after_update { |team_mate| GardenParty::Activities::TeamMate.create_update_activity team_mate }
  after_destroy { |team_mate| GardenParty::Activities::TeamMate.create_destroy_activity team_mate }

  scope :accepted, -> { where.not accepted_at: nil }

  def accept!
    update! accepted_at: Time.current
    TeamMateInviteNotification.find_by(subject: self, recipient_id: user_id)&.delete
    TeamMateAcceptNotification.create_for! self
  end

  def refuse!
    TeamMateInviteNotification.find_by(subject: self, recipient_id: user_id)&.delete
    TeamMateRefuseNotification.create_for! self
    destroy!
  end

  def leave!
    TeamMateLeaveNotification.create_for! self
    destroy!
  end

  def remove!
    TeamMateRemoveNotification.create_for! self
    destroy!
  end

  private

  def notify_user_invite
    return if user_id == map.user_id

    TeamMateInviteNotification.create_for! self
  end
end
