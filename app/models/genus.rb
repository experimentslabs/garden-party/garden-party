# frozen_string_literal: true

class Genus < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :resources, dependent: :restrict_with_exception
  belongs_to :family

  has_paper_trail

  before_validation :normalize_fields

  scope :syncable_with, ->(trusted_genus) { where(sync_id: trusted_genus[:id].to_s).or(where('lower(name) = ?', trusted_genus[:name].downcase)) }

  def normalize_fields
    name&.strip!
    source&.strip!
  end

  class << self
    def normalize_fields
      find_each(&:save)
    end
  end
end
