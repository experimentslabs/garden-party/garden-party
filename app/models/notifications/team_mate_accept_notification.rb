# frozen_string_literal: true

class TeamMateAcceptNotification < Notification
  class << self
    def create_for!(team_mate)
      create! subject: team_mate, sender_id: team_mate.user_id, recipient_id: team_mate.map.user_id
    end
  end

  private

  def notify_recipient; end

  def validate_entity
    validate_subject && validate_recipient && validate_sender
  end

  # TeamMate
  def validate_subject
    return true unless subject.blank? || subject_type != 'TeamMate'

    errors.add :subject, I18n.t('activerecord.errors.models.team_mate_accept_notification.subject.bad_subject')
    false
  end

  # Map owner
  def validate_recipient
    return true unless recipient_id.blank? || recipient_id != subject&.map&.user_id

    errors.add :recipient, I18n.t('activerecord.errors.models.team_mate_accept_notification.recipient.bad_recipient')
    false
  end

  # Invitee
  def validate_sender
    return true unless sender_id.blank? || sender_id != subject&.user_id

    errors.add :sender, I18n.t('activerecord.errors.models.team_mate_accept_notification.sender.bad_sender')
    false
  end
end
