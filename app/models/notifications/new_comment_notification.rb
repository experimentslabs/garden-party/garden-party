# frozen_string_literal: true

class NewCommentNotification < Notification
  class << self
    def create_for!(comment)
      team_mates = GardenParty::MapFinder.for(comment)&.team_mates
      return unless team_mates

      team_mates.each do |team_mate|
        next if comment.user_id == team_mate.user_id

        map_id = GardenParty::MapFinder.for(comment)&.id
        create! subject: comment, sender_id: comment.user_id, recipient_id: team_mate.user_id, content: comment.content, map_id: map_id
      end
    end
  end

  private

  def notify_recipient
    return unless recipient.confirmed? && GardenParty::UserPreferences.send_email?(type, recipient)

    NotificationMailer.with(notification: self).new_comment_notification_email.deliver_later if recipient.confirmed?
  end

  def validate_entity
    validate_subject && validate_recipient && validate_sender && validate_map
  end

  def validate_map
    return true if map_id

    errors.add :map, I18n.t('activerecord.errors.models.new_comment_notification.map.blank')
    false
  end

  def validate_subject
    return true unless subject.blank? || subject_type != 'Comment'

    errors.add :subject, I18n.t('activerecord.errors.models.new_comment_notification.subject.bad_subject')
    false
  end

  def validate_recipient
    return true if recipient_id.present?

    errors.add :recipient, I18n.t('activerecord.errors.models.new_comment_notification.recipient.bad_recipient')
    false
  end

  def validate_sender
    return true unless sender_id.blank? || sender_id != subject&.user_id

    errors.add :sender, I18n.t('activerecord.errors.models.new_comment_notification.sender.bad_sender')
    false
  end
end
