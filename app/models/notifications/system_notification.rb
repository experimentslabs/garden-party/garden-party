# frozen_string_literal: true

class SystemNotification < Notification
  class << self
    def notify_all!(title:, message:)
      User.find_each do |user|
        create! content: { title: title, message: message }, recipient: user
      end
    end

    def notify!(user, title:, message:)
      create! content: { title: title, message: message }, recipient: user
    end
  end

  private

  def notify_recipient
    return unless GardenParty::UserPreferences.send_email? type, recipient

    NotificationMailer.with(notification: self).new_system_notification_email.deliver_later
  end

  def validate_entity
    errors.add :content, I18n.t('activerecord.errors.models.system_notification.content.title_missing') if content['title'].blank?
    errors.add :content, I18n.t('activerecord.errors.models.system_notification.content.message_missing') if content['message'].blank?
    errors.add :subject, I18n.t('activerecord.errors.models.system_notification.subject.should_not_be_present') if subject
  end
end
