# frozen_string_literal: true

class Family < ApplicationRecord
  enum :kingdom, { plant: 0, animal: 1 }

  validates :name, presence: true, uniqueness: true
  validates :kingdom, presence: true

  has_many :genera, dependent: :restrict_with_exception
  has_many :resources, through: :genera

  has_paper_trail

  before_validation :normalize_fields

  scope :syncable_with, ->(trusted_family) { Family.where(sync_id: trusted_family[:id].to_s).or(Family.where('lower(name) = ?', trusted_family[:name].downcase)) }

  def normalize_fields
    name&.strip!
    source&.strip!
  end

  class << self
    def normalize_fields
      find_each(&:save)
    end
  end
end
