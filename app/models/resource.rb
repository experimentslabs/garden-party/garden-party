# frozen_string_literal: true

require 'garden_party/colors'
require 'garden_party/bitfield_model_helper'

class Resource < ApplicationRecord # rubocop:disable Metrics/ClassLength
  include Bitfields
  # Convenient mapping on database fields and Bitfield prefixes, used in various
  # operations
  BITFIELD_MONTHS_ATTRIBUTES = {
    sheltered_sowing_months: :sow_sheltered_in,
    soil_sowing_months:      :sow_in_soil_in,
    harvesting_months:       :harvest_in,
  }.freeze
  # Define all the month bitfields
  BITFIELD_MONTHS_ATTRIBUTES.each_pair { |field, prefix| bitfield field, GardenParty::BitfieldModelHelper.bitfield_months(prefix) }

  enum :crop_rotation_group, {
    leaf:   0,
    manure: 1,
    legume: 2,
    root:   3,
  }

  validates :name, presence: true
  # Not yet enforced on the database level
  validates :latin_name, presence: true
  validates :color, presence: true, color_string: true
  validates :diameter, numericality: { greater_than: 0.0, allow_nil: true }
  validates :parent_id, absence: true, if: :generic?
  validate :common_names_format
  validate :sources_format

  belongs_to :genus
  has_one :family, through: :genus
  belongs_to :parent, class_name: 'Resource', optional: true, counter_cache: :child_count
  belongs_to :resource_interactions_group, optional: true
  has_many :children, class_name: 'Resource', foreign_key: :parent_id, inverse_of: :parent, dependent: :restrict_with_exception
  has_many :elements, dependent: :restrict_with_exception
  has_many :notes, class_name: 'ResourceNote', inverse_of: :resource, dependent: :destroy

  has_paper_trail
  acts_as_taggable_on :tags

  after_initialize :set_defaults
  before_validation :complete_color, :normalize_fields
  before_save :normalize_common_names

  scope :syncable_with, ->(trusted_resource) { where(sync_id: trusted_resource[:id].to_s).or(where('lower(name)= ?', trusted_resource[:name].downcase)) }
  scope :search, ->(value) { where('resources.name ILIKE :search OR resources.latin_name ILIKE :search OR resources.common_names::text ILIKE :search', search: "%#{value}%") }
  scope :parentable, -> { where parent_id: nil }

  def generic?
    child_count.positive?
  end

  def common_names=(value)
    value = value.split(',') if value.is_a? String
    super
  end

  # Converts sources to array when a string is provided
  def sources=(value)
    value = value.split("\n").map(&:strip) if value.is_a? String

    super
  end

  def normalize_fields
    latin_name&.strip!
    description&.strip!
  end

  def normalize_common_names
    self.common_names = common_names.map(&:strip).compact_blank.uniq.sort
  end

  class << self
    # Converts bitfield arrays to valid parameters
    #
    # @param attributes [Hash] - Hash from request params
    #
    # @return [Hash] - Hash of valid parameters for an create/update operation
    #
    # Example:
    #    attributes = {
    #      #...
    #      soil_sowing_months: [false, false, false, true, true, true, true, false, false, false, false, false],
    #    }
    #    convert_bitfields_arrays(attributes)
    #    #=> {
    #      sow_in_soil_in_january: false,
    #      sow_in_soil_in_february: false,
    #      #...
    #    }
    def convert_bitfields_arrays(attributes)
      BITFIELD_MONTHS_ATTRIBUTES.each_pair do |key, prefix|
        next unless attributes.key? key

        GardenParty::BitfieldModelHelper::MONTHS.each_with_index { |month, index| attributes[:"#{prefix}_#{month}"] = attributes[key][index] }
        attributes.delete key
      end

      attributes
    end

    # Turns a hash of flags to an array of booleans
    #
    # @param hash [Hash] Hash of <prefixed_month>:<boolean> as returned by `bitfield_bitfield_values`
    # @param prefix [Symbol] Prefix used in the hash
    #
    # @return [[Boolean]] List of booleans
    #
    # Example:
    #   resource = Resource.first
    #   hash     = resource.bitfield_bitfield_values(:soil_sowing_months)
    #   #=> {sow_in_soil_in_january: false, sow_in_soil_in_february: false, ...}
    #   bitfield_mont_to_a(hash, :sow_in_soil_in)
    #   #=> [false, false, false, true, true, true, true, false, false, false, false, false]
    def bitfield_month_to_a(hash, prefix)
      GardenParty::BitfieldModelHelper::MONTHS.map { |month| hash[:"#{prefix}_#{month}"] }
    end

    def normalize_fields
      find_each do |resource|
        resource.normalize_common_names
        resource.save!
      end
    end
  end

  private

  def complete_color
    self.color ||= GardenParty::Colors.from_string(name)
  end

  def set_defaults
    # Initializes JSON fields. The conditional attribution prevents failures
    # when initializing instance without all the fields (i.e.: when fields are
    # not selected in the SQL query)
    self.common_names ||= [] if attributes.key? 'common_names'
    self.sources      ||= [] if attributes.key? 'sources'
  end

  def sources_format
    unless sources.is_a? Array
      errors.add :sources, I18n.t('activerecord.errors.not_array')
      return
    end

    sources.each do |string|
      next if string.match?(/\A#{URI::DEFAULT_PARSER.make_regexp(%w[http https])}\z/)

      errors.add :sources, I18n.t('activerecord.errors.url_not_url', string: string)
    end
  end

  def common_names_format
    unless common_names.is_a? Array
      errors.add :common_names, I18n.t('activerecord.errors.not_array')
      return
    end

    common_names.each do |name|
      unless name.is_a? String
        errors.add :common_names, I18n.t('activerecord.errors.not_strings_array')
        break
      end
    end
  end
end
