# frozen_string_literal: true

class User < ApplicationRecord
  class InvitationNotAccepted < StandardError; end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :username, presence: true, uniqueness: true, format: { with: /\A[a-z0-9\-_]{3,}\z/i }
  # i18n-tasks-use t('activerecord.attributes.user.map')
  # i18n-tasks-use t('activerecord.attributes.user.theme')
  validates :preferences, preferences: true
  validates :calendar_token, uniqueness: true, if: -> { calendar_token.present? }

  has_many :maps, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :links, dependent: :destroy
  has_many :approved_links, class_name: 'Link', foreign_key: :approved_by_id, inverse_of: :approved_by, dependent: :destroy
  has_many :notifications, foreign_key: :recipient_id, inverse_of: :recipient, dependent: :destroy
  has_many :sent_notifications, class_name: 'Notification', foreign_key: :sender_id, inverse_of: :sender, dependent: :destroy
  has_many :assigned_tasks, class_name: 'Task', foreign_key: :assignee_id, inverse_of: :assignee, dependent: :nullify
  has_many :team_mates, dependent: :destroy
  has_many :resource_notes, dependent: :destroy
  has_many :activities, dependent: :destroy
  has_many :observations, dependent: :destroy
  has_many :jwt_tokens, dependent: :destroy

  # We don't use Devise's "confirmable" behavior, the only pending accounts are the ones
  # with a pending invitation
  scope :confirmed, -> { where(invitation_token: nil) }
  scope :admins, -> { where(role: 'admin') }
  scope :users, -> { where(role: 'user') }
  scope :search, lambda { |username|
    return none unless username && username.size >= 3

    where 'username ILIKE :search', search: "%#{username}%"
  }
  # Finds users that had some activity in their gardens in the last "duration" (e.g.: 1.week)
  scope :active_in_a_garden_for_the_last, ->(duration) { User.where id: Activity.select(:user_id).where(created_at: (Time.current - duration)..).group(:user_id) }
  # Finds users that did not have had some activity in their gardens in the last "duration" (e.g.: 1.week)
  scope :inactive_in_gardens_since, ->(duration) { User.where.not id: Activity.select(:user_id).where(created_at: (Time.current - duration)..).group(:user_id) }
  # Finds users that had some community activity in the last "duration" (e.g.: 1.week)
  scope :active_for_community_for_the_last, lambda { |duration|
    ids = PaperTrail::Version.where(item_type: %w[Family Genus Resource])
                             .where(created_at: (Time.current - duration)..)
                             .where.not(whodunnit: nil)
                             .group(:whodunnit)
                             .pluck(:whodunnit).map(&:to_i)
    User.where(id: ids)
  }

  before_validation :normalize_preferences

  def confirmed?
    invitation_token.blank?
  end

  def account_state
    confirmed? ? :active : :pending
  end

  def admin?
    role == 'admin'
  end

  def user?
    role == 'user'
  end

  def accepted_team_mates
    team_mates.accepted
  end

  def accessible_maps
    Map.where(id: maps.select(:id))
       .or(Map.where(id: accepted_team_mates.select(:map_id)))
  end

  # Prevent resetting password when invitation is not accepted
  def send_reset_password_instructions
    raise InvitationNotAccepted unless invitation_token.nil?

    super
  end

  def rotate_calendar_token!
    new_token = SecureRandom.hex
    return rotate_calendar_token! unless User.find_by(calendar_token: new_token).nil?

    update! calendar_token: new_token
  end

  private

  def normalize_preferences
    return unless preferences_changed?

    self.preferences = GardenParty::UserPreferences.new(preferences).to_h
  end
end
