# frozen_string_literal: true

require 'garden_party/activities/patch'

class Patch < ApplicationRecord
  include HasGeometry

  has_paper_trail

  validates :geometry, presence: true, feature: { types: %w[Circle Polygon] }, allow_blank: false, unless: :virtual?
  validates :geometry, absence: true, if: :virtual?

  belongs_to :map
  belongs_to :layer, optional: true
  has_many :activities, as: :subject, dependent: :destroy
  has_many :elements, dependent: :destroy, inverse_of: :patch
  has_many :observations, as: :subject, dependent: :destroy
  has_many :tasks, as: :subject, dependent: :destroy

  scope :in_map, ->(map_id) { in_maps([map_id]) }
  scope :in_maps, ->(map_ids) { where(map_id: map_ids) }
  scope :virtual, -> { where(geometry: nil) }

  after_create { |patch| GardenParty::Activities::Patch.create_create_activity(patch) }
  after_update { |patch| GardenParty::Activities::Patch.create_update_activity(patch) }
  after_destroy { |patch| GardenParty::Activities::Patch.create_destroy_activity(patch) }

  def virtualize!
    self.geometry = nil
    self.layer    = nil

    save
  end

  def map_owner
    map.user
  end
end
