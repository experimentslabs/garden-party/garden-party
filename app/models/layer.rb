# frozen_string_literal: true

require 'garden_party/activities/layer'

class Layer < ApplicationRecord
  validates :name, presence: true
  validates :position, with: :validate_position

  belongs_to :map
  has_many :elements, dependent: :destroy
  has_many :patches, dependent: :restrict_with_exception
  has_many :paths, dependent: :destroy
  has_many :activities, as: :subject, dependent: :destroy

  scope :in_map, ->(map_id) { in_maps([map_id]) }
  scope :in_maps, ->(map_ids) { where(map_id: map_ids) }

  before_validation :set_position, on: :create
  after_save :update_positions
  after_create { |layer| GardenParty::Activities::Layer.create_create_activity layer }
  after_update { |layer| GardenParty::Activities::Layer.create_update_activity layer }
  after_destroy { |layer| GardenParty::Activities::Layer.create_destroy_activity layer }

  private

  def validate_position
    max = map.layers.count
    max += 1 if new_record?
    range = (1..max)

    errors.add :position, I18n.t('activerecord.errors.is_out_of_range', start: 0, end: max) unless range.include? position
  end

  def set_position
    self.position ||= map.layers.count + 1
  end

  def update_positions
    return unless saved_change_to_position?

    previous = saved_change_to_position[0] || (map.layers.count + 1)
    new      = saved_change_to_position[1]
    return if previous == new

    if previous > new
      amount = +1
      comparators = ['>=', '<=']
    elsif previous < new
      amount = -1
      comparators = ['<=', '>=']
    end

    move_layers(from: new, to: previous, amount: amount, comparators: comparators)
  end

  def move_layers(from:, to:, amount:, comparators:)
    layers = map.layers.order(position: :asc)
    # Exclude current layer if already persisted
    layers = layers.where.not(id: id) if id

    layers = layers.where("position #{comparators[0]} ? AND position #{comparators[1]} ?", from, to)
    layers.find_each do |layer|
      layer.update_column :position, layer.position + amount # rubocop:disable Rails/SkipsModelValidations
    end
  end
end
