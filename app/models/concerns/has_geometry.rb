# frozen_string_literal: true

require 'active_support/concern'

module HasGeometry
  extend ActiveSupport::Concern

  included do
    def point?
      return false unless geometry.is_a? Hash

      geometry.dig('geometry', 'type') == 'Point' && !geometry['properties']&.key?('radius')
    end

    def circle?
      return false unless geometry.is_a? Hash

      geometry.dig('geometry', 'type') == 'Point' && geometry['properties']&.key?('radius')
    end

    def polygon?
      return false unless geometry.is_a? Hash

      geometry.dig('geometry', 'type') == 'Polygon'
    end

    def line_string?
      return false unless geometry.is_a? Hash

      geometry.dig('geometry', 'type') == 'LineString'
    end

    def virtual?
      geometry.blank?
    end

    def geometry_type
      return 'Virtual' if virtual?
      return 'Point' if point?
      return 'Circle' if circle?

      geometry.dig('geometry', 'type')
    end
  end
end
