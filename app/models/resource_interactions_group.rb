# frozen_string_literal: true

class ResourceInteractionsGroup < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :interactions_on, class_name: 'ResourceInteraction', foreign_key: :subject_id, inverse_of: :subject, dependent: :destroy
  has_many :interactions_from, class_name: 'ResourceInteraction', foreign_key: :target_id, inverse_of: :target, dependent: :destroy
  has_many :resources, dependent: :nullify

  has_paper_trail

  scope :syncable_with, ->(trusted_resource) { where(sync_id: trusted_resource[:id].to_s).or(where('lower(name)= ?', trusted_resource[:name].downcase)) }
end
