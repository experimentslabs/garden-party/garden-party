# frozen_string_literal: true

require 'garden_party/tasks/element'
require 'garden_party/activities/element'

class Element < ApplicationRecord
  include HasGeometry

  has_paper_trail

  enum :implantation_mode, { soil_sowing: 0, potted_sowing: 1, potted_shoot: 2, shoot_in_soil: 3 }

  validates :implanted_at, presence: true, if: :removed_at
  validates :implanted_at, presence: true, if: :removal_planned_for
  validates :diameter, numericality: { greater_than: 0.0, allow_nil: true }
  # Point or patch ?
  validates :geometry, feature: { types: %w[Point], allow_nil: false }, if: :point?
  validates :layer, :geometry, presence: true, if: :point?
  validates :layer, :geometry, absence: true, unless: :point?
  validates :patch, absence: true, if: :point?
  validates :patch, presence: true, unless: :point?
  # Dates logic: removal should be after implantation
  validate :valid_removal_plan_date?
  validate :valid_removal_date?

  belongs_to :resource
  belongs_to :patch, optional: true, inverse_of: :elements
  belongs_to :layer, optional: true
  has_many :activities, as: :subject, dependent: :destroy
  has_many :observations, as: :subject, dependent: :destroy
  has_many :tasks, as: :subject, dependent: :destroy
  has_many :harvests, dependent: :destroy

  scope :still_implanted, -> { where(removed_at: nil).where.not(implanted_at: nil) }
  scope :removed, -> { where.not(removed_at: nil) }
  scope :planned, -> { where(implanted_at: nil) }
  # All elements, in a patch or layer related to a map
  scope :in_map, ->(map_id) { in_maps([map_id]) }
  scope :in_maps, lambda { |map_ids|
    where(patch_id: nil, layer_id: Layer.in_maps(map_ids).select(:id))
      .or(where(patch_id: Patch.in_maps(map_ids).select(:id)))
  }

  after_create { |element| GardenParty::Activities::Element.create_create_activity element }
  after_update { |element| GardenParty::Activities::Element.create_update_activity element }
  after_destroy { |element| GardenParty::Activities::Element.create_destroy_activity element }
  after_save :update_tasks, unless: :skip_task_hooks?

  def map_owner
    map.user
  end

  # Either the patch's map or the element's one.
  def map
    layer&.map || patch&.map
  end

  def status
    return :removed if removed_at.present?
    return :implanted if implanted_at.present?

    :planned
  end

  def valid_removal_plan_date?
    return false if removal_planned_for.blank? || implanted_at.blank?

    errors.add :removal_planned_for, I18n.t('activerecord.errors.element.date_before_implantation') if removal_planned_for < implanted_at
  end

  def valid_removal_date?
    return false if removed_at.blank? || implanted_at.blank?

    errors.add :removed_at, I18n.t('activerecord.errors.element.date_before_implantation') if removed_at < implanted_at
  end

  # Called from a Task when it changes
  #
  # @param task [Task]
  def task_callback(task)
    @skip_task_hooks = true
    update! GardenParty::Tasks::Element.implied_task_changes task
    @skip_task_hooks = false
  end

  def display_name
    resource_name = resource.name

    if point?
      return resource_name if attributes['name'].blank?

      I18n.t('models.element.name.point_element', resource_name: resource_name, element_name: attributes['name'])
    else
      patch_name = patch.name || I18n.t('generic.patch.name')
      I18n.t('models.element.name.element_in_patch', resource_name: resource_name, patch_name: patch_name)
    end
  end

  def duplicate
    new_version = dup
    base_name = new_version.name || new_version.resource.name
    new_version.name = I18n.t('models.element.duplicate_name_suffix', name: base_name)

    new_version
  end

  private

  def skip_task_hooks?
    @skip_task_hooks
  end

  def update_tasks
    GardenParty::Tasks::Element.update_tasks self
  end
end
