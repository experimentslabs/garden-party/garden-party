# frozen_string_literal: true

class ResourceInteraction < ApplicationRecord
  # Notes: https://en.wikipedia.org/wiki/Biological_interaction
  # |              | subject | target |
  # |--------------|---------|--------|
  # | competition  |    -    |    -   |
  # | mutualism    |    +    |    +   |
  # | neutralism   |    /    |    /   |
  # | parasitism   |    +    |    -   |
  # | amensalism   |    /    |    -   |
  # | commensalism |    +    |    /   |
  #
  # Additional interactions
  # |                      | subject | target |
  # |----------------------|---------|--------|
  # | unspecified_positive |    +    |    ?   |
  # | unspecified_negative |    -    |    ?   |
  #
  # "Unspecified" are a quick way to gather data: user don't know what the
  # interaction does specifically, only that it is a _good_ or _bad_ interaction.
  enum :nature, { competition: 0, mutualism: 1, neutralism: 2, parasitism: 3, amensalism: 4, commensalism: 5, unspecified_positive: 6, unspecified_negative: 7 }

  validates :nature, presence: true
  validates :subject_id, uniqueness: { scope: :target_id }
  validate :validate_pair

  belongs_to :subject, class_name: 'ResourceInteractionsGroup'
  belongs_to :target, class_name: 'ResourceInteractionsGroup'

  before_validation :sanitize_sources

  has_paper_trail

  scope :syncable_with, lambda { |trusted_resource|
    trusted_subject_id = ResourceInteractionsGroup.find_by(name: trusted_resource[:subject])&.id
    trusted_target_id = ResourceInteractionsGroup.find_by(name: trusted_resource[:target])&.id

    where(sync_id: trusted_resource[:id].to_s)
      .or(where(subject_id: trusted_subject_id, target_id: trusted_target_id))
      .or(where(subject_id: trusted_target_id, target_id: trusted_subject_id))
  }

  private

  def sanitize_sources
    return unless sources

    self.sources = sources.split("\n").filter_map(&:strip).join("\n").presence
  end

  def validate_pair
    if target_id == subject_id
      errors.add :target, I18n.t('activerecord.errors.models.resource_interaction.target.has_same_resource')
      return
    end

    # Database has unique index on [subject_id, target_id] but it seems not to be
    # possible to validate for any order of the pair ([subject_id, target_id] or [target_id, subject_id]).
    # We check this here but won't prevent race conditions.
    existing = ResourceInteraction.where('id != ? AND (subject_id = ? and target_id = ?) OR (subject_id = ? and target_id = ?)', id, subject_id, target_id, target_id, subject_id).first
    errors.add :target, I18n.t('activerecord.errors.models.resource_interaction.target.pair_already_exists') if existing
  end
end
