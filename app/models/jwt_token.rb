# frozen_string_literal: true

# Stores the JWT tokens
class JwtToken < ApplicationRecord
  SIGNING_ALGORITHM = 'HS256'

  validates :description, presence: true, allow_blank: false
  validates :expires_at, presence: true, allow_blank: false

  belongs_to :user

  scope :still_valid, -> { where 'expires_at > ?', Time.zone.now }

  def expired?
    return true if expires_at.blank?

    expires_at.before? DateTime.current
  end

  def expires_in
    return nil if expires_at.blank?

    # "expires_at" is a TimeWithZone, and subtracting a DateTime results in an amount of seconds,
    # not in hours as for two DateTime...
    (expires_at - DateTime.current).to_i
  end

  def token
    raise 'Record should be persisted to generate the token' unless persisted?

    # Not _yet used_ values:
    # - Not Before Time: nbf
    # - Audience: aud
    # - Subject: sub
    payload = {
      # JWT ID
      jti: id,
      # Expiration Time
      exp: expires_at.to_i,
      # Issued At
      iat: created_at.to_i,
      # Issuer
      iss: Rails.configuration.garden_party.default_url_options[:host],
    }

    JWT.encode payload, self.class.signing_secret, SIGNING_ALGORITHM
  end

  class << self
    def decode(token)
      JWT.decode(token, signing_secret, true, { algorithm: SIGNING_ALGORITHM }).first
    end

    # Only returns a user if token is still valid
    def user_from(token)
      token = decode(token)
      jwt_token = still_valid.find_by id: token['jti']
      jwt_token.user
    rescue JWT::DecodeError
      nil
    end

    def signing_secret
      Rails.configuration.garden_party.secret_keys[:jwt]
    end
  end
end
