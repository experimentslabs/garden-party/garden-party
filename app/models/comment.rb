# frozen_string_literal: true

class Comment < ApplicationRecord
  validates :content, presence: true, allow_blank: false
  validates :subject_type, inclusion: %w[Observation Task]

  belongs_to :user
  belongs_to :subject, polymorphic: true, counter_cache: true

  after_create :notify_users

  # @param by [User] User who removes the comment.
  def remove!(by:)
    update! removed_at: Time.current, removed_by_map_owner: by.id != user_id
  end

  private

  def notify_users
    NewCommentNotification.create_for! self
  end
end
