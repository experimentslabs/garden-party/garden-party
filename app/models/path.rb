# frozen_string_literal: true

require 'garden_party/activities/path'

class Path < ApplicationRecord
  include HasGeometry

  has_paper_trail

  validates :geometry, feature: { types: %w[Circle LineString Polygon] }
  validates :stroke_width, presence: true
  validates :stroke_color, presence: true, alpha_color_string: true
  validates :background_color, presence: true, alpha_color_string: true
  validates :stroke_style_preset, presence: false, inclusion: { in: [nil, 'default', 'dotted', 'dashed', 'long_dashes', 'long_dashes2', 'long_dashes3', 'dashed2', 'dashed3'] }

  belongs_to :map
  belongs_to :layer
  has_many :activities, as: :subject, dependent: :destroy
  has_many :observations, as: :subject, dependent: :destroy
  has_many :tasks, as: :subject, dependent: :destroy

  scope :in_map, ->(map_id) { in_maps(map_id) }
  scope :in_maps, ->(map_ids) { joins(:layer).merge(Layer.in_map(map_ids)) }

  after_create { |path| GardenParty::Activities::Path.create_create_activity(path) }
  after_update { |path| GardenParty::Activities::Path.create_update_activity(path) }
  after_destroy { |path| GardenParty::Activities::Path.create_destroy_activity(path) }

  def map_owner
    map.user
  end
end
