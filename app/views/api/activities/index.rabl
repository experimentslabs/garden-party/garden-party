# frozen_string_literal: true

collection @activities

extends 'api/activities/activity'
