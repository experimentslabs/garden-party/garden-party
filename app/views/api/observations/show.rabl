# frozen_string_literal: true

object @observation

extends 'api/observations/observation'
