# frozen_string_literal: true

attributes :id,
           :title,
           :content,
           :made_at,
           :user_id,
           :subject_id,
           :subject_type,
           :comments_count,
           :created_at,
           :updated_at

node(:pictures) do |observation|
  observation.pictures.map do |picture|
    {
      source:    picture_api_observation_url(observation, picture),
      thumbnail: picture_thumbnail_api_observation_url(observation, picture),
    }
  end
end
