# frozen_string_literal: true

collection @observations

extends 'api/observations/observation'
