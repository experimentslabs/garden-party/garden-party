# frozen_string_literal: true

collection @families
cache @families.cache_key_with_version

extends 'api/families/family'
