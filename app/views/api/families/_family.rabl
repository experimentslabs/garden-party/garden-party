# frozen_string_literal: true

attributes :id,
           :name,
           :kingdom,
           :source,
           :sync_id,
           :created_at,
           :updated_at
