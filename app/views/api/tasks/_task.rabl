# frozen_string_literal: true

attributes :id,
           :name,
           :notes,
           :planned_for,
           :done_at,
           :assignee_id,
           :subject_id,
           :subject_type,
           :action,
           :comments_count,
           :created_at,
           :updated_at

if @render_subject
  node(:subject) do |task|
    next nil unless task.action && task.subject_type

    case task.subject_type
    when 'Element'
      partial 'api/elements/_element', object: task.subject
    when 'Map'
      partial 'api/maps/_map', object: task.subject
    when 'Patch'
      partial 'api/patches/_patch', object: task.subject
    when 'Path'
      partial 'api/paths/_path', object: task.subject
    end
  end
end
