# frozen_string_literal: true

collection @tasks

extends 'api/tasks/task'
