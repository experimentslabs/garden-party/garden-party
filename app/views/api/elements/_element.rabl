# frozen_string_literal: true

attributes :id,
           :resource_id,
           :implanted_at, :implantation_planned_for,
           :implantation_mode,
           :removed_at, :removal_planned_for,
           :status,
           :observations_count,
           :created_at, :updated_at,
           # Point
           :name,
           :diameter,
           :geometry, :layer_id,
           # In patch
           :patch_id
