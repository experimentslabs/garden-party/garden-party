# frozen_string_literal: true

collection @elements

extends 'api/elements/element'
