# frozen_string_literal: true

object false

elements = Element.in_map @map.id

code :map do |_m|
  partial 'api/maps/public_map', object: @map
end

code :patches do |_m|
  partial 'api/patches/index', object: Patch.in_map(@map.id)
end

code :paths do |_m|
  partial 'api/paths/index', object: Path.in_map(@map.id)
end

code :layers do |_m|
  partial 'api/layers/index', object: Layer.in_map(@map.id)
end

code :elements do |_m|
  partial 'api/elements/index', object: elements
end

code :resources do |_m|
  ids = elements.pluck(:resource_id).uniq
  partial 'api/resources/index', object: Resource.where(id: ids), locals: { disable_cache: true }
end
