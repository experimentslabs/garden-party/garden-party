# frozen_string_literal: true

attributes :id,
           :name,
           :center,
           :publicly_available,
           :hide_background,
           :background_color,
           :extent_height,
           :extent_width,
           :observations_count,
           :user_id,
           :created_at,
           :updated_at
node(:picture) do |map|
  next nil unless map.picture.attached?

  {
    url:  picture_api_map_url(map),
    size: map.picture_size,
  }
end
