# frozen_string_literal: true

attributes :id,
           :name,
           :latin_name,
           :common_names,
           :color,
           :parent_id,
           :description,
           :sources,
           :diameter,
           :crop_rotation_group,
           :genus_id,
           :resource_interactions_group_id,
           :sync_id,
           :created_at,
           :updated_at

# i18n-tasks-use t('activerecord.attributes.resource.generic')
node :generic, &:generic?

node :tag_list do |resource|
  resource.tags.pluck :name
end

# Bit fields
Resource::BITFIELD_MONTHS_ATTRIBUTES.each_pair do |field, prefix|
  node(field) { |r| Resource.bitfield_month_to_a r.bitfield_values(field), prefix }
end
