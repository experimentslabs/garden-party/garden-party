# frozen_string_literal: true

@disable_cache ||= false

collection @resources

cache @resources.cache_key_with_version unless @disable_cache

extends 'api/resources/resource'
