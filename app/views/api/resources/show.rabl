# frozen_string_literal: true

object @resource

extends 'api/resources/resource'
