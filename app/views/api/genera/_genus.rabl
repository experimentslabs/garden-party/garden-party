# frozen_string_literal: true

attributes :id,
           :name,
           :source,
           :family_id,
           :sync_id,
           :created_at,
           :updated_at
