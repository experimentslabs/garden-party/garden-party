# frozen_string_literal: true

collection @genera
cache @genera.cache_key_with_version

extends 'api/genera/genus'
