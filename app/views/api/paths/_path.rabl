# frozen_string_literal: true

attributes :id,
           :name,
           :stroke_width,
           :stroke_color,
           :stroke_style_preset,
           :background_color,
           :map_id,
           :layer_id,
           :geometry,
           :geometry_type,
           :observations_count,
           :created_at,
           :updated_at
