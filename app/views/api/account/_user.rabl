# frozen_string_literal: true

attributes :id,
           :role,
           :email,
           :username,
           :created_at,
           :updated_at

node :preferences do |user|
  GardenParty::UserPreferences.new(user.preferences).to_h
end
