# frozen_string_literal: true

attributes :id,
           :name,
           :map_id,
           :layer_id,
           :geometry,
           :geometry_type,
           :observations_count,
           :created_at,
           :updated_at
