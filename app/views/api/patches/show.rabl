# frozen_string_literal: true

object @patch

extends 'api/patches/patch'
