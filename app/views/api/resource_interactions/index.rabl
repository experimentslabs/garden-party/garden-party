# frozen_string_literal: true

collection @resource_interactions
cache @resource_interactions.cache_key_with_version

extends 'api/resource_interactions/resource_interaction'
