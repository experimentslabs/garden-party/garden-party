# frozen_string_literal: true

attributes :id, :nature, :notes, :sources, :subject_id, :target_id, :sync_id, :created_at, :updated_at
