# frozen_string_literal: true

collection @notifications

extends 'api/notifications/notification'
