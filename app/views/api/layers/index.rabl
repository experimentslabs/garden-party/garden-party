# frozen_string_literal: true

collection @layers

extends 'api/layers/layer'
