# frozen_string_literal: true

collection @team_mates

extends 'api/team_mates/team_mate'
