# frozen_string_literal: true

attributes :id,
           :role,
           :username,
           :account_state
