# frozen_string_literal: true

collection @users

extends 'api/users/user'
