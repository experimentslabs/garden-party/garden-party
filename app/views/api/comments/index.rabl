# frozen_string_literal: true

collection @comments

extends 'api/comments/comment'
