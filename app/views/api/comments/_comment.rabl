# frozen_string_literal: true

attributes :id,
           :user_id,
           :removed_at,
           :removed_by_map_owner,
           :subject_type,
           :subject_id,
           :created_at,
           :updated_at

node(:username) { |comment| comment.user.username }
node(:content) do |comment|
  if comment.removed_at && comment.removed_by_map_owner
    I18n.t('comments.comment.removed_by_map_owner_admin') if comment.removed_by_map_owner
  elsif comment.removed_at
    I18n.t('comments.comment.removed_by_owner')
  else
    comment.content unless comment.removed_at
  end
end
