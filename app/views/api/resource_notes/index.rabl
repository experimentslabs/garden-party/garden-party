# frozen_string_literal: true

collection @resource_notes

extends 'api/resource_notes/resource_note'
