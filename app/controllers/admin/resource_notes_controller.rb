# frozen_string_literal: true

module Admin
  class ResourceNotesController < AdminApplicationController
    before_action :set_resource_note, only: [:destroy]

    # DELETE /admin/resource_notes/1
    def destroy
      @resource_note.destroy
      redirect_to admin_resource_url(@resource_note.resource_id), notice: I18n.t('admin.resource_notes.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_note
      @resource_note = ResourceNote.find(params[:id])

      authorize @resource_note
    end
  end
end
