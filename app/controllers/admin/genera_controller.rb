# frozen_string_literal: true

module Admin
  class GeneraController < AdminApplicationController
    before_action :set_genus, only: [:show, :edit, :update, :destroy]

    # GET /admin/genera
    def index
      @genera = policy_scope(Genus)

      @genera = filter @genera

      @genera = search_order_and_paginate @genera, search_fields: ['name'], allowed_sort_fields: ['genera.name'], default_sort_field: 'genera.name', default_sort_dir: 'asc'
    end

    # GET /admin/genera/1
    def show; end

    # GET /admin/genera/new
    def new
      authorize Genus

      @genus = Genus.new
    end

    # GET /admin/genera/1/edit
    def edit; end

    # POST /admin/genera
    def create
      authorize Genus

      @genus = Genus.new(genus_params)

      if @genus.save
        redirect_to admin_genus_path(@genus), notice: I18n.t('admin.genera.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/genera/1
    def update
      if @genus.update(genus_params)
        redirect_to admin_genus_path(@genus), notice: I18n.t('admin.genera.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/genera/1
    def destroy
      @genus.destroy
      redirect_to admin_genera_url, notice: I18n.t('admin.genera.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_genus
      @genus = Genus.find(params[:id])

      authorize @genus
    end

    # Only pick allowed parameters from request
    def genus_params
      params.require(:genus).permit(:name, :description, :source, :family_id, :sync_id)
    end

    def filter(query)
      case params[:source]
      when 'with'
        query = query.where.not(source: '')
      when 'without'
        query = query.where(source: '')
      end

      case params[:resources]
      when 'without'
        query = query.where.missing(:resources)
      end

      query
    end
  end
end
