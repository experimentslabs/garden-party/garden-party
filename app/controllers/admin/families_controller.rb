# frozen_string_literal: true

module Admin
  class FamiliesController < AdminApplicationController
    before_action :set_family, only: [:show, :edit, :update, :destroy]

    # GET /admin/families
    def index
      @families = policy_scope(Family)
      @families = filter @families

      @families = search_order_and_paginate @families, search_fields: ['name'], allowed_sort_fields: ['families.name'], default_sort_field: 'families.name', default_sort_dir: 'asc'
    end

    # GET /admin/families/1
    def show; end

    # GET /admin/families/new
    def new
      authorize Family

      @family = Family.new
    end

    # GET /admin/families/1/edit
    def edit; end

    # POST /admin/families
    def create
      authorize Family

      @family = Family.new(family_params)

      if @family.save
        redirect_to admin_family_path(@family), notice: I18n.t('admin.families.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/families/1
    def update
      if @family.update(family_params)
        redirect_to admin_family_path(@family), notice: I18n.t('admin.families.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/families/1
    def destroy
      @family.destroy
      redirect_to admin_families_url, notice: I18n.t('admin.families.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_family
      @family = Family.find(params[:id])

      authorize @family
    end

    # Only pick allowed parameters from request
    def family_params
      params.require(:family).permit(:name, :kingdom, :description, :source, :sync_id)
    end

    def filter(query) # rubocop:disable Metrics/MethodLength
      case params[:source]
      when 'with'
        query = query.where.not(source: '')
      when 'without'
        query = query.where(source: '')
      end

      case params[:kingdom]
      when 'animal'
        query = query.where(kingdom: :animal)
      when 'plant'
        query = query.where(kingdom: :plant)
      end

      case params[:genera]
      when 'without'
        query = query.where.missing(:genera)
      end

      query
    end
  end
end
