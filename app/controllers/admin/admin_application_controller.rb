# frozen_string_literal: true

module Admin
  class AdminApplicationController < ApplicationController
    before_action :authenticate_admin!

    private

    def authenticate_admin!
      authenticate_user!

      redirect_to new_user_session_url, status: :unauthorized unless current_user.admin?
      Current.user = current_user
    end

    def policy_scope(scope)
      super([:admin, scope])
    end

    def authorize(record, query = nil)
      super([:admin, record], query)
    end

    # Updates the query with search term if any, sort order and pagination
    #
    # @param query               [ActiveRecord::Relation] The query to update
    # @param default_sort_field  [String]                 Fallback field if none found in request
    # @param default_sort_dir    ['asc', 'desc']          Default sort direction if none found in request
    # @param search_fields       [[String]]               List of fields to search in
    # @param allowed_sort_fields [[String]]               List of fields on which sorting can be done
    #
    # @return [ActiveRecord::Relation] The updated query
    def search_order_and_paginate(query,  default_sort_field:, default_sort_dir: nil, search_fields: [], allowed_sort_fields: [])
      query = order_query query, allowed: allowed_sort_fields, default_field: default_sort_field, default_dir: default_sort_dir if allowed_sort_fields.size.positive?
      query = search_or_build_query query, search_fields if params[:search]
      paginate_query query
    end

    # Paginates a query and returns it
    #
    # @param query [ActiveRecord::Relation] The query to update
    #
    # @return [ActiveRecord::Relation] The updated query
    def paginate_query(query)
      query = query.page(params[:page])
      query = query.per(params[:per_page]) if params[:per_page]

      query
    end

    # Updates a query with a search on given fields.
    # It will try to run the "search" scope on model and fallback to building the
    # query based on "search_fields"
    #
    # @param query         [ActiveRecord::Relation] The query to update
    # @param search_fields [string[]]               List of fields on which to search the term
    #
    # @return [ActiveRecord::Relation] The updated query
    def search_or_build_query(query, search_fields = [])
      model = query.class.to_s.deconstantize&.constantize
      # Let the model use its search feature if defined
      return query.search(params[:search]) if model.respond_to? :search

      # Otherwise, build the request based on "search_fields"
      return build_search_query query, model: model, search_fields: search_fields if search_fields.size.positive?

      raise "Search scope not found in #{model} and no search fields defined"
    end

    # Updates a query with a search on given fields
    #
    # @param query         [ActiveRecord::Relation] The query to update
    # @param model         [ActiveRecord::Base]     Model class
    # @param search_fields [string[]]               List of fields on which to search the term
    #
    # @return [ActiveRecord::Relation] The updated query
    def build_search_query(query, model:, search_fields:)
      search_term = "%#{params[:search].downcase}%"
      query = query.where("lower(#{search_fields.first}) LIKE :search", search: search_term)
      search_fields.shift
      if search_fields.size.positive?
        search_fields.each do |field|
          query = query.or(model.where("lower(#{field}) LIKE :search", search: search_term))
        end
      end

      query
    end

    # Orders a query if request parameter is in an allowed list of fields
    #
    # @param query         [ActiveRecord::Relation] Query to order
    # @param allowed       [string[]]               List of field names that can be sorted
    # @param default_field [string]                 Name of the field to sort by default
    # @param default_dir   ['asc', 'desc']          Default sort direction if not found
    #
    # @return [ActiveRecord::Relation] The ordered query
    def order_query(query, allowed:, default_field:, default_dir: 'asc')
      order_dir = params[:order_dir]&.downcase || default_dir
      order_by  = params[:order_by] || default_field
      return query unless %w[asc desc].include?(order_dir) && allowed.include?(order_by)

      query.order "#{order_by} #{order_dir}"
    end
  end
end
