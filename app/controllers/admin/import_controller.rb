# frozen_string_literal: true

require 'garden_party/trusted_data/differences'

module Admin
  # Controller to handle synchronization between trusted data and library
  #
  # The entities manipulated here are not Family instances but hashes of trusted
  # data formats (unless specifically stated otherwise in methods documentation).
  # They are _similar_ but slightly differ:
  #   - the `id` properties are always the trusted data internal ID; IDs from
  #     database are in `library_id` attributes.
  #   - some attributes don't have the same name (tags vs tag_list, etc...)
  class ImportController < AdminApplicationController
    before_action :skip_policy_scope
    before_action :skip_authorization

    # GET /admin/import
    def index; end

    # GET /admin/import/library_names
    # Collections of names for references in the JS app. Data comes from the
    # database, so `id` is the real id.
    def library_names # rubocop:disable Metrics/AbcSize
      names = {
        families:                     Family.all.map { |r| { id: r.id, sync_id: r.sync_id, name: r.name } },
        genera:                       Genus.all.map { |r| { id: r.id, sync_id: r.sync_id, name: r.name } },
        resources:                    Resource.all.map { |r| { id: r.id, sync_id: r.sync_id, name: r.name } },
        resource_interactions_groups: ResourceInteractionsGroup.all.map { |r| { id: r.id, sync_id: r.sync_id, name: r.name } },
      }

      render json: names, status: :ok
    end

    # GET /admin/import/differences
    def differences
      diffs = GardenParty::TrustedData::Differences.new

      render json: diffs.list.to_json, status: :ok
    rescue GardenParty::TrustedDataError => e
      raise FailedDependencyError, e
    end

    # PUT/POST /admin/import/family
    def family
      save_or_update :family_params, Family, :family_to_td
    end

    # PUT/POST /admin/import/genus
    def genus
      save_or_update :genus_params, Genus, :genus_to_td
    end

    # PUT/POST /admin/import/resource
    def resource
      save_or_update :resource_params, Resource, :resource_to_td
    end

    # PUT/POST /admin/import/resource_interactions_group
    def resource_interactions_group
      save_or_update :resource_interactions_group_params, ResourceInteractionsGroup, :resource_interactions_group_to_td
    end

    # PUT/POST /admin/import/resource_interaction
    def resource_interaction
      save_or_update :resource_interaction_params, ResourceInteraction, :resource_interaction_to_td
    end

    private

    def family_params
      values = params.require(:import).permit(:id, :name, :kingdom, :source)
      GardenParty::TrustedData::Converter.td_to_family(values, only: values.each_key.map(&:to_sym))
    end

    def genus_params
      values = params.require(:import).permit(:id, :name, :family, :source)
      GardenParty::TrustedData::Converter.td_to_genus(values, only: values.each_key.map(&:to_sym))
    end

    def resource_params
      values = params.require(:import).permit(:id, :name, :latin_name, :genus, :parent, :description, :diameter, :interactions_group, sheltered_sowing_months: [], soil_sowing_months: [], harvesting_months: [], common_names: [], tags: [], sources: [])

      GardenParty::TrustedData::Converter.td_to_resource(values, only: values.each_key.map(&:to_sym))
    end

    def resource_interactions_group_params
      values = params.require(:import).permit(:id, :name, :description)

      GardenParty::TrustedData::Converter.td_to_resource_interactions_group(values, only: values.each_key.map(&:to_sym))
    end

    def resource_interaction_params
      values = params.require(:import).permit(:id, :subject, :target, :nature, :notes, :sources)

      GardenParty::TrustedData::Converter.td_to_resource_interaction(values, only: values.each_key.map(&:to_sym))
    end

    # Creates or update library entries from a trusted data hash
    def save_or_update(attributes_method, klass, converter_method)
      entity = klass.find_or_initialize_by id: params[:library_id]
      success_status = entity.id ? :ok : :created
      entity.assign_attributes send(attributes_method)

      if entity.save
        hash = GardenParty::TrustedData::Converter.send converter_method, entity
        hash[:library_id] = entity.id

        render json: hash, status: success_status
      else
        render json: entity.errors, status: :unprocessable_entity
      end
    end
  end
end
