# frozen_string_literal: true

module Api
  class TasksController < ApiApplicationController
    before_action :set_task, only: [:show, :update, :destroy]

    # GET /api/tasks
    def index
      list(scope: :subject)
    end

    def all_for_map
      list(scope: :map)
    end

    # GET /api/maps/:map_id/tasks/names
    def names
      authorize_map

      names = policy_scope(Task).all_for_map(params[:map_id]).select(:name).distinct(:name).pluck :name
      render json: names
    end

    # GET /api/tasks/1
    def show
      render 'api/tasks/show'
    end

    # POST /api/tasks
    def create
      @task = Task.new(create_task_params)
      authorize @task

      if @task.save
        @render_subject = true
        render 'api/tasks/show', status: :created, location: api_task_url(@task)
      else
        render json: @task.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/tasks/1
    def update
      @task.assign_attributes update_task_params
      authorize @task

      if @task.save
        @render_subject = true
        render 'api/tasks/show', status: :ok, location: api_task_url(@task)
      else
        render json: @task.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/tasks/1
    def destroy
      @task.destroy

      # Renders an empty task with subject_type, subject_id and the full subject
      render 'api/tasks/show_subject', status: :ok
    end

    private

    # Index actions
    # @param scope [:map, :subject] Whether to filter on a specific subject or get all tasks related to a map
    def list(scope:)
      authorize_map

      @tasks = policy_scope(Task).order(planned_for: :desc, done_at: :desc)

      if scope == :map
        @tasks = @tasks.all_for_map(params[:map_id])
      else
        filter_by_subject
      end

      apply_filters

      render 'api/tasks/index'
    end

    # Index filters
    def apply_filters
      filters = params[:filters]
      return unless filters

      @tasks = @tasks.pending if filters[:status] == 'pending'
      @tasks = @tasks.done if filters[:status] == 'done'
      @tasks = @tasks.overdue if filters[:status] == 'overdue'
    end

    def filter_by_subject # rubocop:disable Metrics/AbcSize
      @tasks = if params[:element_id]
                 @tasks.for_element params[:element_id]
               elsif params[:patch_id]
                 @tasks.for_patch params[:patch_id]
               elsif params[:path_id]
                 @tasks.for_path params[:path_id]
               elsif params[:map_id]
                 @tasks.for_map params[:map_id]
               else
                 Task.none
               end
    end

    # Use callbacks to share common setup or constraints between tasks.
    def set_task
      @task = Task.find(params[:id])

      authorize @task
    end

    # Only pick allowed parameters from request
    def create_task_params
      params.require(:task).permit(:name, :notes, :assignee_id, :planned_for, :done_at, :subject_id, :subject_type)
    end

    def update_task_params
      params.require(:task).permit(:name, :notes, :assignee_id, :planned_for, :done_at)
    end
  end
end
