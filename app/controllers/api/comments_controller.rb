# frozen_string_literal: true

module Api
  class CommentsController < ApiApplicationController
    before_action :set_comment, only: [:show, :update, :remove]

    # GET /api/comments
    def index
      skip_policy_scope

      @comments = Comment.where(subject: subject)

      render 'api/comments/index'
    end

    # GET /api/comments/1
    def show
      render 'api/comments/show'
    end

    # POST /api/comments
    def create
      @comment = Comment.new(create_comment_params)
      @comment.user = current_user
      authorize @comment

      if @comment.save
        render 'api/comments/show', status: :created, location: api_comment_url(@comment)
      else
        render json: @comment.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/comments/1
    def update
      if @comment.update(update_comment_params)
        render 'api/comments/show', location: api_comment_url(@comment)
      else
        render json: @comment.errors, status: :unprocessable_entity
      end
    end

    # PUT /api/comments/1/remove
    def remove
      @comment.remove!(by: current_user)

      render 'api/comments/show'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])

      authorize @comment
    end

    def create_comment_params
      parameters = params.require(:comment).permit :content, :subject_type, :subject_id
      parameters.require :subject_type

      parameters
    end

    def update_comment_params
      params.require(:comment).permit :content
    end

    def subject
      subject = if params[:observation_id]
                  Observation.find params[:observation_id]
                elsif params[:task_id]
                  Task.find params[:task_id]
                else
                  raise ActionController::ParameterMissing, I18n.t('comments.index.invalid_subject_error')
                end
      authorize(subject, :show?)

      subject
    end
  end
end
