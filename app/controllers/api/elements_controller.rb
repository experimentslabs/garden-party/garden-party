# frozen_string_literal: true

module Api
  class ElementsController < ApiApplicationController
    before_action :set_element, only: [:show, :update, :duplicate, :destroy]

    # GET /elements
    def index # rubocop:disable Metrics/AbcSize
      @elements = policy_scope(Element)
      if params[:patch_id] # Elements in a patch
        authorize Patch.find(params[:patch_id]), :show?
        @elements = @elements.where(patch_id: params[:patch_id])
      elsif params[:map_id] # All elements for a map
        authorize Map.find(params[:map_id]), :show?
        @elements = @elements.in_map(params[:map_id])
      else
        throw ActiveRecord::RecordNotFound
      end

      render 'api/elements/index'
    end

    # GET /elements/1
    def show
      render 'api/elements/show'
    end

    # POST /elements
    def create
      @element = Element.new(create_element_params)
      authorize @element

      if @element.save
        render 'api/elements/show', status: :created, location: api_element_url(@element)
      else
        render json: @element.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /elements/1
    def update
      if @element.update(update_element_params)
        render 'api/elements/show', location: api_element_url(@element)
      else
        render json: @element.errors, status: :unprocessable_entity
      end
    end

    # POST /elements/1/duplicate
    def duplicate
      @element = @element.duplicate
      if @element.save
        render 'api/elements/show', status: :created, location: api_element_url(@element)
      else
        render json: @element.errors, status: :unprocessable_entity
      end
    end

    # DELETE /elements/1
    def destroy
      @element.destroy!
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_element
      @element = Element.find(params[:id])

      authorize @element
    end

    # Only pick allowed parameters from request
    def create_element_params
      element_params %w[name layer_id geometry resource_id patch_id diameter implantation_mode implanted_at implantation_planned_for removed_at removal_planned_for]
    end

    # Only pick allowed parameters from request
    def update_element_params
      element_params %w[name layer_id geometry resource_id patch_id diameter implantation_mode implanted_at implantation_planned_for removed_at removal_planned_for]
    end

    # Only pick allowed parameters from request
    def element_params(allowed_fields)
      # Pick params one by one because validating geometry is an issue with polygons
      parameters = params.require(:element).to_unsafe_hash
      values = {}
      allowed_fields.each { |key| values[key] = parameters[key] if parameters.key? key }

      values
    end
  end
end
