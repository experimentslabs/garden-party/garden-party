# frozen_string_literal: true

module Api
  class AccountsController < ApiApplicationController
    before_action :authenticate_user!, except: [:show]
    before_action :skip_authorization
    before_action :skip_policy_scope
    before_action :set_user

    def show
      authenticate_user! unless params[:fail_on_error] == 'false'

      if user_signed_in?
        render 'api/account/show'
      else
        head :no_content
      end
    end

    def update
      preferences = @user.preferences.merge preferences_params
      if @user.update({ preferences: preferences })
        render 'api/account/show', location: api_account_url
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    private

    def set_user
      @user = current_user
    end

    def preferences_params
      params.require(:preferences).permit :map, :theme
    end
  end
end
