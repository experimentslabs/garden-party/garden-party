# frozen_string_literal: true

module Api
  class ActivitiesController < ApiApplicationController
    before_action :set_activity, only: [:show]

    # GET /api/activities
    def index
      authorize_map

      @activities = policy_scope Activity
      filter_by_subject

      render 'api/activities/index'
    end

    def all_for_map
      authorize_map
      @activities = policy_scope(Activity).where(map_id: params[:map_id])

      render 'api/activities/index'
    end

    # GET /api/activities/1
    def show
      render 'api/activities/show'
    end

    private

    def filter_by_subject # rubocop:disable Metrics/AbcSize
      @activities = if params[:element_id]
                      @activities.for_element params[:element_id]
                    elsif params[:patch_id]
                      @activities.for_patch params[:patch_id]
                    elsif params[:path_id]
                      @activities.for_path params[:path_id]
                    elsif params[:map_id]
                      @activities.for_map params[:map_id]
                    else
                      Activity.none
                    end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_activity
      @activity = Activity.find(params[:id])

      authorize @activity
    end

    # Only pick allowed parameters from request
    def activity_params
      params.fetch(:activity, {})
    end
  end
end
