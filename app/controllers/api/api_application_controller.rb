# frozen_string_literal: true

module Api
  class ApiApplicationController < ActionController::API
    include Pundit::Authorization
    include ErrorHandlers

    before_action :set_paper_trail_whodunnit
    before_action { Current.user = current_user }

    private

    def render_error(exception, fallback_message, status)
      message = exception&.message || fallback_message
      render json: { error: message }, status: status
    end

    def error_not_authorized(exception = nil)
      render_error exception, I18n.t('application_controller.errors.not_authorized'), :unauthorized
    end

    def policy_scope(scope)
      # All notification variants use the same scope, defined in model
      return super if scope <= Notification

      # Other models use an Api::ModelNamePolicy so we prefix it
      super([:api, scope])
    end

    def authorize(record, query = nil)
      # All notification variants use the same scope, defined in model
      return super if record.is_a?(Notification) || (record.is_a?(Class) && record <= Notification)

      # Other models use an Api::ModelNamePolicy so we prefix it
      super([:api, record], query)
    end

    # Authorises a map for current user, searching it from "something" in params
    def authorize_map # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      subject = if params[:element_id]
                  Element.find(params[:element_id])
                elsif params[:map_id]
                  Map.find params[:map_id]
                elsif params[:patch_id]
                  Patch.find(params[:patch_id])
                elsif params[:path_id]
                  Path.find(params[:path_id])
                elsif params[:task_id]
                  Task.find(params[:task_id])
                elsif params[:observation_id]
                  Observation.find(params[:observation_id])
                end
      map = GardenParty::MapFinder.for subject

      authorize map, :owned_or_in_team_or_shared?
    end
  end
end
