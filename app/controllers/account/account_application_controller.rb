# frozen_string_literal: true

module Account
  class AccountApplicationController < ApplicationController
    before_action :authenticate_user!
    # Ignore authorization as we only change current user's data in this controller.
    before_action :skip_authorization
  end
end
