# frozen_string_literal: true

module Account
  class PreferencesController < AccountApplicationController
    NOTIFICATION_TYPES = %w[
      new_comment_notification
      team_mate_leave_notification
      team_mate_refuse_notification
      team_mate_remove_notification
      team_mate_invite_notification
      team_mate_accept_notification
    ].freeze

    before_action :authenticate_user!
    before_action :set_user

    def general; end

    def notifications; end

    def update_general_preferences
      if @user.update(params.require(:user).permit(preferences: [:theme, :map]))
        redirect_to :account_general_preferences, notice: I18n.t('account.preferences.update_general_preferences.success')
      else
        render :general
      end
    end

    def update_notifications_preferences
      preferences = GardenParty::UserPreferences.new @user.preferences
      preferences.email_notifications = notifications_preferences_params

      if @user.update(preferences: preferences.to_h)
        redirect_to :account_notifications_preferences, notice: I18n.t('account.preferences.update_notifications_preferences.success')
      else
        render :notifications
      end
    end

    private

    def set_user
      @user = current_user
    end

    def notifications_preferences_params
      params.require(:user).require(:preferences).require(:email_notifications).permit NOTIFICATION_TYPES
    end
  end
end
