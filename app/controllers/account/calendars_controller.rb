# frozen_string_literal: true

module Account
  class CalendarsController < AccountApplicationController
    skip_before_action :authenticate_user!, only: [:show]
    before_action :set_user_from_token!, only: [:show]

    # GET /account/calendar?token=abc
    def show
      render plain: GardenParty::Tasks::Ical.calendar(filter_tasks).to_ical, content_type: 'text/calendar'
    end

    def edit; end

    # PUT /account/calendar/refresh
    def refresh
      if current_user.rotate_calendar_token!
        redirect_to edit_account_calendar_url, notice: I18n.t('account.refresh_calendar_token.success')
      else
        redirect_to edit_account_calendar_url, alert: I18n.t('account.refresh_calendar_token.failure')
      end
    end

    # DELETE /account/calendar
    def destroy
      if current_user.update! calendar_token: nil
        redirect_to edit_account_calendar_url, notice: I18n.t('account.destroy_calendar_token.success')
      else
        redirect_to edit_account_calendar_url, alert: I18n.t('account.destroy_calendar_token.failure')
      end
    end

    private

    # @returns [User]
    def set_user_from_token!
      return @user_by_token if instance_variable_defined? :@user_by_token

      token = params[:token]
      user  = User.find_by calendar_token: token if token.present?
      raise Pundit::NotAuthorizedError unless token && user

      @user_by_token = user
    end

    def filter_tasks
      tasks = if params[:scope] == 'all'
                Task.all_for_maps(@user_by_token.accessible_maps.pluck(:id))
              else
                @user_by_token.assigned_tasks.includes(:subject)
              end

      tasks.where(planned_for: 1.year.ago...1.year.after)
    end
  end
end
