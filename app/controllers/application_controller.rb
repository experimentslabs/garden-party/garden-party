# frozen_string_literal: true

class ApplicationController < ActionController::Base
  class FailedDependencyError < StandardError; end

  include Pundit::Authorization
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_paper_trail_whodunnit
  before_action { Current.user = current_user }

  rescue_from Pundit::NotAuthorizedError, with: :render_403_error

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username, { preferences: [:map, :theme] }])
    devise_parameter_sanitizer.permit(:accept_invitation, keys: [:username])
  end

  def render_error(exception, fallback_message, status)
    message = exception&.message || fallback_message
    respond_to do |format|
      format.json { render json: { error: message }, status: status }
      format.html { raise exception }
    end
  end

  def render_403_error
    Rails.configuration.consider_all_requests_local
    render file: 'public/403.html', status: :forbidden, layout: false
  end
end
