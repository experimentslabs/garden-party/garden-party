# frozen_string_literal: true

# Error handler for controllers.
#
# Note: "render_error" method has to be implemented in controllers for this concern to work.
module ErrorHandlers
  extend ActiveSupport::Concern

  included do
    rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
    rescue_from ActionController::ParameterMissing, with: :error_unprocessable
    rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
    rescue_from ApplicationController::FailedDependencyError, with: :error_failed_dependency
    rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized
  end

  private

  def error_not_found(exception = nil)
    render_error(exception, I18n.t('application_controller.errors.resource_not_found'), :not_found)
  end

  def error_unprocessable(exception = nil)
    render_error(exception, I18n.t('application_controller.errors.unprocessable_entity'), :unprocessable_entity)
  end

  def error_csrf(exception = nil)
    render_error(exception, I18n.t('application_controller.errors.invalid_csrf'), :unprocessable_entity)
  end

  def error_failed_dependency(exception = nil)
    render_error(exception, I18n.t('application_controller.errors.failed_dependency'), :failed_dependency)
  end

  def error_not_authorized(_exception = nil)
    message = I18n.t('application_controller.errors.not_authorized')
    respond_to do |format|
      format.json { render json: { error: message }, status: :unauthorized }
      format.html { redirect_to new_user_session_path }
    end
  end
end
