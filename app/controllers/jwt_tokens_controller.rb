# frozen_string_literal: true

class JwtTokensController < ApplicationController
  before_action :set_jwt_token, only: [:destroy]
  # Set tokens list on create too, as the form in on the index page and we still want the list in case
  # of a form failure.
  before_action :set_jwt_tokens, only: [:index, :create]
  before_action :authenticate_user!
  add_flash_types :last_created_token

  # GET /jwt_tokens
  def index
    set_last_created_token
  end

  # POST /jwt_tokens
  def create
    @jwt_token = JwtToken.new jwt_token_params
    @jwt_token.user = current_user

    if @jwt_token.save
      redirect_to jwt_tokens_path, last_created_token: @jwt_token.id
    else
      render :index
    end
  end

  # DELETE /jwt_tokens/1
  def destroy
    @jwt_token.destroy!
    redirect_to jwt_tokens_path, notice: I18n.t('jwt_tokens.destroy.success')
  end

  private

  def jwt_token_params
    params.require(:jwt_token).permit :description, :expires_at
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_jwt_token
    @jwt_token = JwtToken.find(params[:id])

    authorize @jwt_token
  end

  def set_jwt_tokens
    @jwt_tokens = policy_scope JwtToken.all
  end

  # Uses flash messages to check if a token was created successfully
  #
  # For flash object behaviour, see https://api.rubyonrails.org/classes/ActionDispatch/Flash/FlashHash.html
  def set_last_created_token
    return unless flash.key? :last_created_token

    @last_created_token = JwtToken.find flash['last_created_token']
    authorize @last_created_token

    # Immediately delete the key
    flash.delete :last_created_token
    flash.notice = I18n.t('jwt_tokens.create.success_notice')
    # Prevent the notice to be visible on the next action by discarding the flash object
    flash.discard
  end
end
