# frozen_string_literal: true

class HealthCheckController < ApplicationController
  def up
    head :no_content
  end
end
