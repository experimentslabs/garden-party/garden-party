# frozen_string_literal: true

class AlphaColorStringValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.blank?

    chunks = value.split(',').map(&:strip)

    unless chunks.size == 4
      record.errors.add attribute, I18n.t('activerecord.errors.incorrect_color_segments_amount')
      return
    end

    opacity = chunks.pop&.to_f
    validate_opacity_format opacity, record, attribute

    validate_chunks_format chunks, record, attribute
  end

  private

  def validate_chunks_format(chunks, record, attribute)
    chunks.each do |chunk|
      record.errors.add attribute, I18n.t('activerecord.errors.bad_color_segment') unless /\A\d{1,3}\z/.match?(chunk) && chunk.to_i < 256
    end
  end

  def validate_opacity_format(opacity, record, attribute)
    record.errors.add attribute, I18n.t('activerecord.errors.incorrect_color_opacity_value') unless opacity <= 1 && opacity >= 0
  end
end
