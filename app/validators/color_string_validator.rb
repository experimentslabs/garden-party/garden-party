# frozen_string_literal: true

class ColorStringValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.blank?

    chunks = value.split(',').map(&:strip)

    unless chunks.size == 3
      record.errors.add attribute, I18n.t('activerecord.errors.incorrect_color_segments_amount')
      return
    end

    chunks.each do |chunk|
      record.errors.add attribute, I18n.t('activerecord.errors.bad_color_segment') unless validate_chunk_format chunk
    end
  end

  private

  def validate_chunk_format(string)
    /\A\d{1,3}\z/.match?(string) && string.to_i < 256
  end
end
