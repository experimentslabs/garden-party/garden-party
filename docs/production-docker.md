# Docker and docker-compose

Docker is for now only used for production images. There is no development Dockerfile, but you can open
a [merge request](https://gitlab.com/experimentslabs/garden-party/garden-party/-/merge_requests/new) if you want to add
one.

In `/docker` are a Dockerfile and a docker-compose file.

## Docker compose

The easiest way to test the application is to use docker-compose.

The [docker-compose.yml](../docker/docker-compose.yml) contains default values for playing with the app; so you can just
copy its content somewhere and use it as-is.

If you want to customize it, copy the `.env` as well and edit the values in it.

```sh
docker-compose up

# Specify the project name if you want
docker-compose --project-name my_gp_instance
```

## Dockerfile

The Dockerfile is used in the CI to generate the image used by Docker compose.

You can still use it to generate an image locally. The only environment variable handled by it is `VERSION` which _must_
correspond to a [package](https://gitlab.com/experimentslabs/garden-party/garden-party/-/packages) version
(i.e.: `latest` (default), `unstable` or any other version as `x.y.z`, `x.y.z-something`).

```sh
# Use the latest version:
docker build .

# Use another version:
VERSION=unstable docker build.
```

The generated image can be used with the right environment variables for the instance configuration. It **does not**
include a database.

### Test a custom release with docker-compose

Some release don't have a Docker image ready to use and you will need to build it locally to be able to test them.
As an example, the `unstable` release have no docker image.

To do this, uncomment the lines in the `web` service of the `docker-compose.yml` and run

```sh
docker-compose up
```

The image will be built.
