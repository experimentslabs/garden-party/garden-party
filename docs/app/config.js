export const project = {
  name: 'Garden Party',
}

export const config = {
  componentsPath: '../../app/javascript/vue/common',
}

/**
 * Loads an HTML example file
 *
 * @param   {string}     path - Example path, like "MyComponent/MyComponent.example.default"
 * @returns {Promise<*>}      File loading promise
 */
export function loadHTMLExample (path) {
  const chunks = path.split('/')

  return import(`./src/examples/${chunks[0]}/${chunks[1]}.html?raw`)
}

/**
 * Loads a VueJS example file
 *
 * @param   {string}     path - Example path, like "MyComponent/MyComponent.example.default"
 * @returns {Promise<*>}      File loading promise
 */
export function loadVueExample (path) {
  const chunks = path.split('/')

  return import(`./src/examples/${chunks[0]}/${chunks[1]}.vue`)
}

/**
 * Loads content of a VueJS example file
 *
 * @param   {string}     path - Example path, like "MyComponent/MyComponent.example.default"
 * @returns {Promise<*>}      File loading promise
 */
export function loadVueExampleSource (path) {
  const chunks = path.split('/')

  return import(`./src/examples/${chunks[0]}/${chunks[1]}.vue?raw`)
}

/**
 * Loads content a markdown file
 *
 * @param   {string}     name - File name, without extension
 * @returns {Promise<*>}      File loading promise
 */
export function loadMarkdownFile (name) {
  return import(`../${name}.md?raw`)
}
