export default [
  {
    children: [
      { name: 'repo-page', params: { page: 'README' }, caption: 'README' },
      { name: 'repo-page', params: { page: 'CONTRIBUTING' }, caption: 'Contributing' },
      { name: 'repo-page', params: { page: 'CODE_OF_CONDUCT' }, caption: 'Code of conduct' },
    ],
  },

  {
    name: 'Instances',
    collapsible: false,
    icon: 'doc:library',
    children: [
      { name: 'markdown-page', params: { page: 'development' }, caption: 'Development' },
      { name: 'markdown-page', params: { page: 'production' }, caption: 'Production' },
      { name: 'markdown-page', params: { page: 'production-docker' }, caption: 'Production: Docker' },
      { name: 'markdown-page', params: { page: 'maintenance' }, caption: 'Maintenance' },
      { name: 'repo-page', params: { page: 'MIGRATION_GUIDE' }, caption: 'Migration guide' },
    ],
  },

  {
    name: 'Development references',
    collapsible: false,
    icon: 'doc:library',
    children: [
      { name: 'icons', caption: 'Icons' },
      { name: 'i18n', caption: 'Translations' },
      { name: 'tables-relations', caption: 'Tables relations' },
      { to: 'https://redocly.github.io/redoc/?url=https://doc.garden-party.io/openapi.json', caption: 'API ➚', target: '_blank' },
    ],
  },
]
