import HomeView from './views/HomeView.vue'

export default [
  { path: '/', component: HomeView },

  { path: '/icons', name: 'icons', component: () => import('./views/IconsView.vue') },
  { path: '/i18n', name: 'i18n', component: () => import('./views/I18nView.vue') },
  { path: '/tables-relations', name: 'tables-relations', component: () => import('./views/TablesRelationsView.vue') },
  { path: '/repo/:page', name: 'repo-page', component: () => import('./views/RootMarkdownPageView.vue') },
]
