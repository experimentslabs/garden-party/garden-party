import { DocumentationSystem } from '@experiments-labs/rise_ui/documentation_system/plugins/DocumentationSystem'
import '../../../app/javascript/entrypoints/locales/en'
import i18n from '../../../app/javascript/vue/app_helpers/i18n'
import { BreakpointsPlugin } from '../../../app/javascript/vue/plugins/Breakpoints'
import { createApp } from 'vue/dist/vue.esm-bundler'

// GardenParty helpers, locales, etc...
import iconsFile from '../../../app/assets/images/icons.svg'
import '@/entrypoints/locales/en'
import enLocale from '@/entrypoints/locales/en.json'
import * as components from './components'
import '@/helpers/markdown'
import '@/stylesheets/style.scss'

// Documentation
import { project, loadHTMLExample, loadMarkdownFile, loadVueExample, loadVueExampleSource } from '../config'
import menu from './menu'
import routes from './routes'
import documentation from './documentation.json'
import App from './App.vue'
import './styles.scss'

const app = createApp(App)

Object.keys(components).forEach((key) => {
  app.component(key, components[key])
})

app
  .use(BreakpointsPlugin)
  .use(i18n)
  .use(DocumentationSystem, {
    documentation,
    routes,
    menu,
    project,
    loadHTMLExample,
    loadVueExample,
    loadVueExampleSource,
    loadMarkdownFile,
    riseUiOptions: {
      useVueI18n: true,
      locales: enLocale,
      i18nObjectAttributesPath: 'activerecord.attributes',
      iconsFile,
    },
  })

app.mount('#app')
