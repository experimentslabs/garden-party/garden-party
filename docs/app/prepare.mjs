#!/usr/bin/env node

import path from 'path'
import fs from 'fs'
import { fileURLToPath } from 'url'
import { generateMetadata } from '@experiments-labs/rise_ui/documentation_system/utils/generator/generator.js'

// Current directory
const docsAppPath = path.dirname(fileURLToPath(import.meta.url))

generateMetadata({
  docsAppPath,
  relativeComponentsPath: '../../app/javascript/vue/common/',
})
  // Use the generated documentation to create a file with exports of the documented components,
  // so they can be easily imported and used in examples and playground without having to list them
  // manually.
  .then(async () => {
    // Importing json files in node is not yet fully supported and may change in the future.
    // Let's read and parse the file instead
    const documentation = JSON.parse(fs.readFileSync(path.resolve(docsAppPath, 'src/documentation.json')))

    let content = '// Content generated with documentation.json; do not edit manually.\n\n'

    Object.keys(documentation).sort().forEach((key) => {
      if (!documentation[key].vuejs) return
      content += `export {default as ${key}} from '${documentation[key].sourceFiles[0]}'\n`
    })

    fs.writeFileSync(path.resolve(docsAppPath, 'src/components.js'), content)
  })
  .catch((error) => { console.error(error) }) /* eslint-disable-line no-console */
