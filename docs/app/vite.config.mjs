import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          whitespace: 'preserve',
        },
      },
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '../../app/javascript/'),
    },
  },
  build: {
    outDir: '../public',
    emptyOutDir: true,
  },
  optimizeDeps: {
    include: ['highlight.js', 'markdown-it', 'debounce'],
  },
})
