#!/bin/sh
# Updates the Garden Party instance.
#
# Usage:
#    gp-update <branch>
# Example:
#    gp-update origin/master
#    gp-update 0.0.14

set -xe

cd /path/to/instance # <--- CHANGE THIS IF YOU USE THIS SCRIPT ;)

sudo -u deploy -H RAILS_ENV=production git fetch --prune
sudo -u deploy -H RAILS_ENV=production git reset --hard $1
sudo -u deploy -H yarn install --ignore-engines --check-files
sudo -u deploy -H RAILS_ENV=production bundle install
sudo -u deploy -H RAILS_ENV=production bundle exec rails db:migrate
sudo -u deploy -H RAILS_ENV=production rake i18n:js:export
sudo -u deploy -H RAILS_ENV=production bundle exec rails assets:precompile

sudo -u deploy -H touch tmp/restart.txt
