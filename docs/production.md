# Production installation from sources

<!-- Keep the toc for the documentation app -->
[[TOC]]

**Note:** This documentation focuses on installing Garden Party on Linux; all examples targets a Debian system, so you
will have to adapt them for your preferred distribution.

This documentation covers two ways of deployment, but there are many others. If you ever make Puppet module, Ansible
playbooks or any automated deployment script, [you are very welcome to propose them](../CONTRIBUTING.md).

**Pre-requisites:** To install Garden Party from the sources, you will need:

- a good knowledge of _server_ management: security, web servers, ...
- a good knowledge of _linux_: commands, filesystem, rights, users, groups, ...
- a good knowledge of the linux distribution you use
- a good knowledge of _git_: pull, reset, checkout, ignored files, etc...
- a good knowledge of _PostgreSQL_: Users, rights and database
- a small knowledge of _GardenParty_: you already have it running on your computer, in development mode, and you played a
  bit with it.
- knowledge on how to install the required NodeJS (with Yarn) and Ruby (with Bundler) versions, and how their package
  manager works.

The guide do not cover these points, feel free to [contribute](../CONTRIBUTING.md) if you think it's not enough.

That being said, let's deploy this!

## System tools

Some dependencies are required by Garden Party. For Debian:

```sh
apt install libvips42 libpq-dev
```

Also, a Node package needs to be installed globally:

```sh
yarn global add svgo@1.3.2
```

## Getting the application

You can clone the repository or use a prepared package from
a [release](https://gitlab.com/experimentslabs/garden-party/garden-party/-/releases).

The two proposed ways have their pros and cons; it depends on _how_ you want to maintain the app:

### Git or package ?

#### Packages

A package contains a precompiled assets and all the Ruby dependencies. Also, almost all the unnecessary files are removed.

**Pros:**

- You don't have to build anything on the production server; drop the files, configure and start.
- You don't need all the libraries needed to build the dependencies (both Node and Ruby dependencies needs `gcc` and other
  tools)

**Cons:**

- Updating may be a bit tricky as you will need to remove old files to replace with new
  ones. ([an upgrade script is welcome ;)](../CONTRIBUTING.md))

#### Git

Using git means you get all the sources but need to build the assets yourself.

**Pros:**

- Updating the app is easy: `git fetch && git reset --hard master` will get you the files, and you don't have to track
  outdated/removed files yourself

**Cons:**

- Dependencies have to be updated on every update
- Assets have to be recompiled on every update
- The system contains build tools

### Getting the application with Git

Clone the repository in your server and checkout the last version:

```sh
git clone https://gitlab.com/experimentslabs/garden-party/garden-party.git /srv/garden-party.myserver.org
cd /srv/garden-party.myserver.org
# Master is the last stable release, you can also target a tag if you prefer.
git checkout master
```

### Getting the application from a package

Download and unpack the package:

```sh
curl -O  https://gitlab.com/api/v4/projects/22733551/packages/generic/slim-build/latest/garden-party-latest.tar.gz
tar -xf garden-party-latest.tar.gz -C /srv/garden-party.myserver.org
```

## Configure Bundler

Configure Bundler for deployment and production dependencies only.

```sh
cd <path to garden party>
bundle config deployment 'true'
bundle config without 'development test'
```

All subsequent `bundle` commands need to have the `RAILS_ENV` environment variable set to `production` in order to act
on the correct configuration.

Either `export RAILS_ENV=production` or prefix every command like so `RAILS_ENV=production bundle xyz`.

## Configure the application

Create the master key and the `credentials.yml.enc` with a brand new _secret key_.
Use `config/credentials.default.yml` to fill the file:

```sh
bundle exec rails credentials:edit
```

Save the master key in your password safe:

```sh
cat config/master.key
```

**Note:** You may want to include `config/master.key` and `config/credentials.yml.enc` in your backups.

The next step is the database configuration. Copy `config/database.default.yml` and fill it with the right values:

```sh
cp config/database.default.yml config/database.yml
```

Then, configure the instance. Copy the defaults and edit them:

```sh
cp config/garden_party_defaults.yml config/garden_party.yml
```

For brevity, you can delete the lines you don't want to override.

## Git only: build assets

Javascript locales have to be generated in order to be served in production.
Execute the following commands to generate them and compile assets.

```sh
yarn install
bundle install --pure-lockfile

RAILS_ENV=production bundle exec rails i18n:js:export
RAILS_ENV=production bundle exec rails assets:precompile
```

**Note:** These step will be required for every upgrade

## Database

Assuming the production database has been created and the role used to connect to PostgreSQL cannot create
nor delete it, just populate it with the schema and seed data.

```sh
RAILS_ENV=production bundle exec rails db:migrate
RAILS_ENV=production bundle exec rails db:seed
```

The seeds will create an administrator with email: `admin@example.com` and password `password` if no admin exists in the
database.

## Check installation

Finally, test that Garden Party correctly starts with:

```sh
RAILS_ENV=production RAILS_MAX_THREADS=2 bundle exec rails server --environment=production --binding=::1 --log-to-stdout
```

Stop the server by pressing Ctrl-C.

## Next

These are mainly notes on things to do as a system administrator and are not documented:

- Create a user and make it own the files. This user will be responsible for upgrading them
- Create another user and make it responsible for running the server. It should have read and write access to `log/`, `tmp/`
  and `storage`.
- Configure the webserver (nginx, apache,...) with
  [Passenger](https://www.phusionpassenger.com/docs/tutorials/what_is_passenger/) or by proxying Puma (create a systemd
  unit if you do it this way),
- Manage SSL certificates (Let's encrypt)
- Plan backups for `storage/`, `config/credentials.yml.enc` and the database

_Feel free to [expand this section](../CONTRIBUTING.md) if you encounter any issue with deployment.
We may have forgotten things that differs from a basic Rails application._
