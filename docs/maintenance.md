# Instance maintenance

<!-- Keep the toc for the documentation app -->
[[TOC]]

## Updates

To update Garden Party, follow the [migration guide](MIGRATION_GUIDE.md).

## Counter caches

To prevent additional database queries, some counts are cached in _*_cache_ tables attributes. If you see that the counters
are invalid, you can re-calculate them with:

```sh
rake counter_caches:synchronize
```

## External data source

### Fetching new/updated data

An instance can be bound to a reference data source to easily get new data or
complete existing library entries.

[A repository exists](https://gitlab.com/experimentslabs/garden-party/data) with various packs of data

To define the datasource, fill `trusted_datasource_url` in `config/garden_party_instance.yml`
with the URL to the desired pack.

Note that this _should_ be done as soon as possible to avoid maintenance with an
existing library, and even if changing the source is possible with a little
fiddle in Rails console, it is discouraged.

Admins will find an "import" link in the main menu which leads to the
synchronization pages: import new elements in library or update existing entries.
The process can even be done field by field.

To import a large amount of new entries, use the rake task (it won't update
existing entries, nor change existing entries with same names):

```shell
rake data:import_new
```

### Contribute to a pack

If you want to contribute to the pack you use, you can easily export data from
your instance and complete the pack with a merge request.

```shell
rake data:export
```

## Useful rake tasks

- `stats:show` - Displays statistics about the instance.
- `resources:reset_colors` - Reset resource colors based on their name.

Other tasks are described in their own sections.
