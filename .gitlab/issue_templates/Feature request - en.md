<!--

EN:
Choose the most appropriate template in the drop-down menu.

FR:
Choisissez le modèle approprié dans la liste déroulante.

-->

## What?
<!-- Explain what you want here; try to be exhaustive in the description -->

## Why?
<!-- Explain why you want it -->

## External resources
<!-- Links to external resources that may help to understand why/how/... -->
-

## This is...
<!-- Add an "x" between appropriate brackets -->
- [ ] only an idea of something that'd be great
- [ ] something important to have, IMHO
- [ ] urgent, really

## Will I help?
<!-- Add an "x" between appropriate brackets -->
- [ ] Yes, by <!-- whatever you want to do to participate -->
- [ ] No.
