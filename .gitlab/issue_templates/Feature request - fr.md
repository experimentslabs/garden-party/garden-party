<!--

EN:
Choose the most appropriate template in the drop-down menu.

FR:
Choisissez le modèle approprié dans la liste déroulante.

-->

## Quoi ?
<!-- Expliquez ce que vous souhaitez ici, essayez de décrire au mieux. -->

## Pourquoi ?
<!-- Expliquez pourquoi vous souhaitez cette fonctionnalité -->


## Ressources
<!-- Liens vers des ressources externes, captures d'écran, images,... -->
-

## C'est...
<!-- Ajoutez un "x" entre les crochets appropriés -->
- [ ] only an idea of something that'd be great
- [ ] something important to have, IMHO
- [ ] urgent, really

## Vais-je aider ?
<!-- Ajoutez un "x" entre les crochets appropriés -->
- [ ] Oui, en <!-- ce que vous voulez faire pour participer -->
- [ ] Non.
