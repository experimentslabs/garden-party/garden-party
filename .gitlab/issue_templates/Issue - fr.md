<!--

EN:
Choose the most appropriate template in the drop-down menu.

FR:
Choisissez le modèle approprié dans la liste déroulante.

-->

## Version

- **Tag ou branche Git:** <!-- N'ouvrez pas d'issue sur d'autre branche que "develop" ou "master" -->

## Description du problème

**But recherché :**

<!-- Décrivez ce que vous souhaitez faire -->

**Comportement attendu :**

<!-- Décrivez comment vous pensez que l'application doit se comporter -->

**Comportement réel :**

<!-- Décrivez comment l'application se comporte -->

**Étapes de reproduction :**

<!-- Plus la description est complète, plus c'est facile pour les mainteneurs de reproduire le problème pour s'en occuper -->

1. faire quelque chose
2. ...

## C'est...
<!-- ajoutez un "x" entre les crochets appropriés -->
- [ ] gênant
- [ ] bloquant dans mon utilisation quotidienne
- [ ] vraiment urgent

## Vais-je aider ?
<!-- ajoutez un "x" entre les crochets appropriés -->
- [ ] Oui, en ... <!-- ce que vous pouvez faire pour aider à résoudre ce souci -->
- [ ] Non.
