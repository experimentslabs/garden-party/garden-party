<!--

EN:
Choose the most appropriate template in the drop-down menu.

FR:
Choisissez le modèle approprié dans la liste déroulante.

-->

## Version

- **Git tag or branch:** <!-- Only branches on which we take issues are "develop" and "master" -->

## Description of the issue

**Goal:**

<!-- Describe what you're trying to achieve -->

**Expected behavior:**

<!-- Describe how you think the app should behave -->

**Actual behavior:**

<!-- Describe how the app behaves -->

**Reproduction steps:**

<!-- The more descriptive, the easier it is for maintainers to reproduce the bug and handle it -->

1. do something
2. ...

## This is...
<!-- Add an "x" between appropriate brackets -->
- [ ] an annoyance
- [ ] somewhat blocking in my day to day usage
- [ ] urgent, really

## Will I help?
<!-- Add an "x" between appropriate brackets -->
- [ ] Yes, by ... <!-- whatever you want to do to participate -->
- [ ] No.
