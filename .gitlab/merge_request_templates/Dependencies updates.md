<!--
Thank you for your participation!

Please, fill the sections or remove if unnecessary.
Note that this formalism only helps us to manage the MRs easily.
-->

## Checks

- [ ] My commits follows the
      [conventional commits format](https://www.conventionalcommits.org)
- [ ] I made an update of the Node dependencies and it seems OKish
- [ ] I made an update of the Ruby dependencies, and...
  - [ ] Updated RSpec:
    - [ ] I checked the
          [upgrade notes](https://relishapp.com/rspec/docs/upgrade)
    - [ ] I checked for the configuration running `rails g
          rspec:install` and comparing files
  - [ ] Updated Cucumber:
    - [ ] I checked the
          [releases notes](https://github.com/cucumber/cucumber-rails/releases)
          for the new versions
    - [ ] I checked for its configuration running
          `rails generate cucumber:install` and comparing files
- [ ] I made a Rails minor/major update, and followed the
      [Rails migration guide](https://guides.rubyonrails.org/upgrading_ruby_on_rails.html),
      which is basically:
  - [ ] Update to the last minor with `bundle update`. Check the list
        for a basic update
  - [ ] Make tests pass
  - [ ] Change the patch version in Gemfile
  - [ ] Run `bin/rails app:update` and accept to override everything
  - [ ] Diff and commit _pertinent_ changes (typos and comments, new
        parameters, ...) but keep our configuration
  - [ ] Fix deprecations
  - [ ] Make tests pass
  - [ ] Enable every new configuration default in
        `config/initializers/new_framework_defaults_X.Y.rb`
  - [ ] Make tests pass
  - [ ] Remove `config/initializers/new_framework_defaults_X.Y.rb` and
        update `config.load_defaults` value in `config/application.rb`

