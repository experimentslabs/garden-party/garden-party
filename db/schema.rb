# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2024_11_12_150001) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activities", force: :cascade do |t|
    t.string "action", null: false
    t.jsonb "data", default: "{}", null: false
    t.datetime "happened_at", precision: nil, null: false
    t.string "subject_type"
    t.bigint "subject_id"
    t.bigint "user_id"
    t.string "username"
    t.string "subject_name"
    t.bigint "map_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["map_id"], name: "index_activities_on_map_id"
    t.index ["subject_type", "subject_id"], name: "index_activities_on_subject"
    t.index ["user_id"], name: "index_activities_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "content", null: false
    t.datetime "removed_at"
    t.boolean "removed_by_map_owner", default: false, null: false
    t.bigint "user_id", null: false
    t.string "subject_type", null: false
    t.bigint "subject_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subject_type", "subject_id"], name: "index_comments_on_subject"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "elements", force: :cascade do |t|
    t.datetime "implanted_at", precision: nil
    t.bigint "resource_id", null: false
    t.bigint "patch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "removed_at", precision: nil
    t.datetime "implantation_planned_for", precision: nil
    t.datetime "removal_planned_for", precision: nil
    t.float "diameter"
    t.string "name", comment: "Used when out of a patch"
    t.jsonb "geometry", comment: "Used when out of a patch"
    t.bigint "layer_id", comment: "Used when out of a patch"
    t.integer "implantation_mode"
    t.integer "observations_count", default: 0, null: false
    t.index ["layer_id"], name: "index_elements_on_layer_id"
    t.index ["patch_id"], name: "index_elements_on_patch_id"
    t.index ["resource_id"], name: "index_elements_on_resource_id"
  end

  create_table "families", force: :cascade do |t|
    t.string "name", null: false
    t.string "source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "kingdom", null: false
    t.string "sync_id"
    t.index ["name"], name: "index_families_on_name", unique: true
  end

  create_table "genera", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "source"
    t.bigint "family_id", null: false
    t.string "sync_id"
    t.index ["family_id"], name: "index_genera_on_family_id"
    t.index ["name"], name: "index_genera_on_name", unique: true
  end

  create_table "harvests", force: :cascade do |t|
    t.float "quantity", null: false
    t.integer "unit", null: false
    t.bigint "element_id", null: false
    t.bigint "user_id", null: false
    t.datetime "harvested_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["element_id"], name: "index_harvests_on_element_id"
    t.index ["user_id"], name: "index_harvests_on_user_id"
  end

  create_table "jwt_tokens", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.string "description", null: false
    t.datetime "expires_at", null: false
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_jwt_tokens_on_user_id"
  end

  create_table "layers", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "visible_by_default", default: true, null: false
    t.bigint "map_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position", null: false
    t.index ["map_id"], name: "index_layers_on_map_id"
  end

  create_table "links", force: :cascade do |t|
    t.string "title", null: false
    t.string "url", null: false
    t.string "description", null: false
    t.bigint "user_id", null: false
    t.bigint "approved_by_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["approved_by_id"], name: "index_links_on_approved_by_id"
    t.index ["user_id"], name: "index_links_on_user_id"
  end

  create_table "maps", force: :cascade do |t|
    t.string "name", null: false
    t.point "center", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "extent_width"
    t.float "extent_height"
    t.boolean "publicly_available", default: false, null: false
    t.text "notes"
    t.boolean "hide_background", default: false
    t.string "background_color"
    t.integer "observations_count", default: 0, null: false
    t.index ["user_id"], name: "index_maps_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "type"
    t.jsonb "content"
    t.string "subject_type"
    t.bigint "subject_id"
    t.bigint "recipient_id", null: false
    t.bigint "sender_id"
    t.datetime "archived_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "map_id"
    t.index ["map_id"], name: "index_notifications_on_map_id"
    t.index ["recipient_id"], name: "index_notifications_on_recipient_id"
    t.index ["sender_id"], name: "index_notifications_on_sender_id"
    t.index ["subject_type", "subject_id"], name: "index_notifications_on_subject"
  end

  create_table "observations", force: :cascade do |t|
    t.string "title", null: false
    t.text "content"
    t.string "subject_type", null: false
    t.bigint "subject_id", null: false
    t.bigint "user_id", null: false
    t.datetime "made_at", precision: nil, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "comments_count", default: 0, null: false
    t.index ["subject_type", "subject_id"], name: "index_observations_on_subject"
    t.index ["user_id"], name: "index_observations_on_user_id"
  end

  create_table "patches", force: :cascade do |t|
    t.string "name"
    t.jsonb "geometry"
    t.bigint "map_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "layer_id"
    t.integer "observations_count", default: 0, null: false
    t.index ["layer_id"], name: "index_patches_on_layer_id"
    t.index ["map_id"], name: "index_patches_on_map_id"
  end

  create_table "paths", force: :cascade do |t|
    t.string "name"
    t.integer "stroke_width", default: 1, null: false
    t.string "stroke_color", default: "0,0,0", null: false
    t.string "background_color", default: "0,0,0,0.2", null: false
    t.jsonb "geometry", null: false
    t.bigint "map_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "layer_id", null: false
    t.string "stroke_style_preset"
    t.integer "observations_count", default: 0, null: false
    t.index ["layer_id"], name: "index_paths_on_layer_id"
    t.index ["map_id"], name: "index_paths_on_map_id"
  end

  create_table "resource_interactions", force: :cascade do |t|
    t.integer "nature", null: false
    t.string "notes"
    t.bigint "subject_id", null: false
    t.bigint "target_id", null: false
    t.text "sources"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sync_id"
    t.index ["subject_id", "target_id"], name: "index_resource_interactions_on_subject_id_and_target_id", unique: true
    t.index ["subject_id"], name: "index_resource_interactions_on_subject_id"
    t.index ["target_id"], name: "index_resource_interactions_on_target_id"
  end

  create_table "resource_interactions_groups", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sync_id"
    t.index ["name"], name: "index_resource_interactions_groups_on_name", unique: true
  end

  create_table "resource_notes", force: :cascade do |t|
    t.text "content", null: false
    t.boolean "visible", default: true, null: false, comment: "Whether or not this content is available for the community"
    t.bigint "user_id", null: false
    t.bigint "resource_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["resource_id"], name: "index_resource_notes_on_resource_id"
    t.index ["user_id"], name: "index_resource_notes_on_user_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.bigint "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "genus_id", null: false
    t.jsonb "common_names", default: [], null: false
    t.string "color", null: false
    t.string "latin_name"
    t.float "diameter"
    t.integer "sheltered_sowing_months", default: 0, null: false, comment: "Bit field for sheltered sowing months"
    t.integer "soil_sowing_months", default: 0, null: false, comment: "Bit field for sowing in soil months"
    t.integer "harvesting_months", default: 0, null: false, comment: "Bit field for harvest months"
    t.jsonb "sources", default: [], null: false
    t.string "sync_id"
    t.integer "child_count", default: 0, null: false
    t.bigint "resource_interactions_group_id"
    t.integer "crop_rotation_group"
    t.index ["genus_id"], name: "index_resources_on_genus_id"
    t.index ["parent_id"], name: "index_resources_on_parent_id"
    t.index ["resource_interactions_group_id"], name: "index_resources_on_resource_interactions_group_id"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at", precision: nil
    t.string "tenant", limit: 128
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
    t.index ["tenant"], name: "index_taggings_on_tenant"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "tasks", force: :cascade do |t|
    t.string "name", null: false
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "done_at", precision: nil
    t.datetime "planned_for", precision: nil
    t.string "subject_type", null: false
    t.bigint "subject_id", null: false
    t.bigint "assignee_id"
    t.string "action"
    t.integer "comments_count", default: 0, null: false
    t.index ["assignee_id"], name: "index_tasks_on_assignee_id"
    t.index ["subject_type", "subject_id"], name: "index_tasks_on_subject_type_and_subject_id"
  end

  create_table "team_mates", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "map_id", null: false
    t.datetime "accepted_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["map_id", "user_id"], name: "index_team_mates_on_map_id_and_user_id", unique: true
    t.index ["map_id"], name: "index_team_mates_on_map_id"
    t.index ["user_id"], name: "index_team_mates_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "role", default: "user", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "username", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at", precision: nil
    t.datetime "invitation_sent_at", precision: nil
    t.datetime "invitation_accepted_at", precision: nil
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.jsonb "preferences", default: {"map"=>nil, "theme"=>nil}
    t.string "calendar_token"
    t.index ["calendar_token"], name: "index_users_on_calendar_token", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.datetime "created_at", precision: nil
    t.jsonb "object"
    t.jsonb "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "activities", "maps"
  add_foreign_key "activities", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "elements", "layers"
  add_foreign_key "elements", "patches"
  add_foreign_key "elements", "resources"
  add_foreign_key "genera", "families"
  add_foreign_key "harvests", "elements"
  add_foreign_key "harvests", "users"
  add_foreign_key "jwt_tokens", "users"
  add_foreign_key "layers", "maps"
  add_foreign_key "links", "users"
  add_foreign_key "links", "users", column: "approved_by_id"
  add_foreign_key "maps", "users"
  add_foreign_key "notifications", "maps"
  add_foreign_key "notifications", "users", column: "recipient_id"
  add_foreign_key "notifications", "users", column: "sender_id"
  add_foreign_key "observations", "users"
  add_foreign_key "patches", "layers"
  add_foreign_key "patches", "maps"
  add_foreign_key "paths", "layers"
  add_foreign_key "paths", "maps"
  add_foreign_key "resource_interactions", "resource_interactions_groups", column: "subject_id"
  add_foreign_key "resource_interactions", "resource_interactions_groups", column: "target_id"
  add_foreign_key "resource_notes", "resources"
  add_foreign_key "resource_notes", "users"
  add_foreign_key "resources", "genera"
  add_foreign_key "resources", "resource_interactions_groups"
  add_foreign_key "resources", "resources", column: "parent_id"
  add_foreign_key "taggings", "tags"
  add_foreign_key "tasks", "users", column: "assignee_id"
  add_foreign_key "team_mates", "maps"
  add_foreign_key "team_mates", "users"
end
