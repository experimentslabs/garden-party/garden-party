# frozen_string_literal: true

# Put your production seeds here

# Create an admin if none exist
User.create! username: 'admin', email: 'admin@example.com', password: 'password', role: :admin if User.where(role: :admin).count.zero?
