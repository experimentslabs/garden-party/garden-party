# frozen_string_literal: true

class AddDateFieldsToActivities < ActiveRecord::Migration[6.0]
  def change
    change_table :activities, bulk: true do |t|
      t.datetime :done_at, after: :notes
      t.datetime :planned_for, after: :notes
    end

    reversible do |dir|
      dir.up do
        activities = select_all('SELECT * FROM activities', 'SELECT_ACTIVITIES')
        activities.each do |activity|
          update('UPDATE activities SET done_at=? WHERE id=?', 'UPDATE_ACTIVITY', [activity['created_at'], activity['id']])
        end
      end
    end
  end
end
