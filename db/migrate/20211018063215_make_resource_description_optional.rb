# frozen_string_literal: true

class MakeResourceDescriptionOptional < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        change_column :resources, :description, :text, null: true, default: nil
      end

      dir.down do
        change_column :resources, :description, :text, null: false, default: ''
      end
    end
  end
end
