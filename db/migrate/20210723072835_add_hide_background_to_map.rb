# frozen_string_literal: true

class AddHideBackgroundToMap < ActiveRecord::Migration[6.1]
  def change
    add_column :maps, :hide_background, :boolean, default: false # rubocop:disable Rails/ThreeStateBooleanColumn
  end
end
