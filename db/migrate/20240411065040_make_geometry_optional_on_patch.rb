# frozen_string_literal: true

class MakeGeometryOptionalOnPatch < ActiveRecord::Migration[7.1]
  def change
    change_column_null :patches, :geometry, true
  end
end
