# frozen_string_literal: true

class RenameCropsToElements < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :activities, :crops
    rename_table       :crops, :elements
    rename_column      :activities, :crop_id, :element_id
    add_foreign_key    :activities, :elements
  end
end
