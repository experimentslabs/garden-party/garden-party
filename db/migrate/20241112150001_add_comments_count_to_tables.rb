# frozen_string_literal: true

class AddCommentsCountToTables < ActiveRecord::Migration[7.2]
  def change
    [:observations, :tasks].each do |table|
      add_column table, :comments_count, :integer, null: false, default: 0
    end
  end
end
