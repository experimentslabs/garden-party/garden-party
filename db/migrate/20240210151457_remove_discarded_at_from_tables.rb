# frozen_string_literal: true

class RemoveDiscardedAtFromTables < ActiveRecord::Migration[7.1]
  # Tables with "discarded_at" field, ordered by deletion order
  TABLES = [
    :observations,
    :harvests,
    :elements,
    :patches,
    :paths,
    :layers,
  ].freeze

  def change # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    reversible do |dir|
      dir.up do
        # Find all the entries to delete
        ids = {}
        ids[:layers] = select_all(<<~SQL.squish, 'LAYERS').pluck('id').join(',')
          SELECT id FROM layers WHERE discarded_at IS NOT NULL
        SQL
        ids[:paths] = select_all(<<~SQL.squish, 'PATHS').pluck('id').join(',')
          SELECT id FROM paths WHERE #{ids[:layers].present? ? "layer_id IN (#{ids[:layers]}) OR" : ''}
                                     discarded_at IS NOT NULL
        SQL
        ids[:patches] = select_all(<<~SQL.squish, 'PATCHES').pluck('id').join(',')
          SELECT id FROM patches WHERE #{ids[:layers].present? ? "layer_id IN (#{ids[:layers]}) OR" : ''}
                                       discarded_at IS NOT NULL
        SQL
        ids[:elements] = select_all(<<~SQL.squish, 'ELEMENTS').pluck('id').join(',')
          SELECT id FROM elements WHERE #{ids[:layers].present? ? "layer_id IN (#{ids[:layers]}) OR" : ''}
                                        #{ids[:patches].present? ? "patch_id IN (#{ids[:patches]}) OR" : ''}
                                        discarded_at IS NOT NULL
        SQL
        ids[:harvests] = select_all(<<~SQL.squish, 'HARVESTS').pluck('id').join(',')
          SELECT id FROM harvests WHERE #{ids[:elements].present? ? "element_id IN (#{ids[:elements]}) OR" : ''}
                                        discarded_at IS NOT NULL
        SQL
        ids[:observations] = select_all(<<~SQL.squish, 'OBSERVATIONS').pluck('id').join(',')
          SELECT id FROM observations WHERE #{ids[:paths].present? ? "(subject_id IN (#{ids[:paths]}) and subject_type = 'Path') OR" : ''}
                                            #{ids[:patches].present? ? "(subject_id IN (#{ids[:patches]}) and subject_type = 'Patch') OR" : ''}
                                            #{ids[:elements].present? ? "(subject_id IN (#{ids[:elements]}) and subject_type = 'Element') OR" : ''}
                                            discarded_at IS NOT NULL
        SQL

        # Delete data and columns
        TABLES.each do |table|
          delete "DELETE FROM #{table} WHERE id IN (#{ids[table]})", "DELETE_FROM_#{table}" if ids[table].present?
          remove_column table, :discarded_at
        end
      end

      dir.down do
        TABLES.each do |table|
          add_column table, :discarded_at, :datetime, null: true, default: nil
          add_index table, :discarded_at
        end
      end
    end
  end
end
