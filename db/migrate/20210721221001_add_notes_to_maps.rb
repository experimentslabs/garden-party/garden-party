# frozen_string_literal: true

class AddNotesToMaps < ActiveRecord::Migration[6.1]
  def change
    add_column :maps, :notes, :text, default: nil
  end
end
