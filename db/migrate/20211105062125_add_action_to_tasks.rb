# frozen_string_literal: true

# This migration adds an "action" column to tasks table
#
# This data represents the action that should be performed on the task's subject
# when it's finished.
#
# The migration also creates tasks for element's planned implantation and removal
# dates
class AddActionToTasks < ActiveRecord::Migration[6.1]
  def create_task(name:, action:, planned_for:, assignee_id:, element_id:, date:) # rubocop:disable Metrics/ParameterLists
    bindings = [
      name,
      action,
      planned_for,
      assignee_id,
      element_id,
      date,
    ]
    exec_insert "INSERT INTO tasks (name, action, planned_for, assignee_id, subject_type, subject_id, created_at, updated_at) VALUES ($1, $2, $3, $4, 'Element', $5, $6, $6)",
                'CREATE_TASK',
                bindings
  end

  def change # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    reversible do |dir|
      dir.up do
        add_column :tasks, :action, :string

        # Create tasks from implantation/removal dates
        elements = select_all 'SELECT * FROM elements'
        now      = Date.current
        elements.each do |element|
          next unless (element['implantation_planned_for'] && !element['implanted_at']) || (element['removal_planned_for'] && !element['removed_at'])

          # Find map owner
          map = select_one 'SELECT user_id from maps INNER JOIN patches p on maps.id = p.map_id WHERE p.id = $1',
                           'FIND_USER',
                           [element['patch_id'].to_i]
          user_id = map['user_id'].to_i

          # Planned implantation
          if element['implantation_planned_for'] && !element['implanted_at']
            create_task name:        I18n.t('js.elements.action_buttons.implant'),
                        action:      'implant',
                        planned_for: element['implantation_planned_for'],
                        assignee_id: user_id,
                        element_id:  element['id'].to_i,
                        date:        now
          end

          next unless element['removal_planned_for'] && !element['removed_at']

          # Planned removal
          create_task name:        I18n.t('js.elements.action_buttons.remove'),
                      action:      'remove',
                      planned_for: element['removal_planned_for'],
                      assignee_id: user_id,
                      element_id:  element['id'].to_i,
                      date:        now
        end
      end

      dir.down do
        remove_column :tasks, :action, :string
      end
    end
  end
end
