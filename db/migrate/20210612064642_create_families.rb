# frozen_string_literal: true

class CreateFamilies < ActiveRecord::Migration[6.1]
  def change
    create_table :families do |t|
      t.string :name, null: false, default: nil
      t.string :description, null: false, default: nil
      t.string :source

      t.timestamps
    end

    # Don't enforce foreign key now
    add_reference :genera, :family, foreign_key: true, null: true
  end
end
