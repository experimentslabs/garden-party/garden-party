# frozen_string_literal: true

class AddImplantationModeToElements < ActiveRecord::Migration[6.1]
  def change
    add_column :elements, :implantation_mode, :integer, null: true, default: nil
  end
end
