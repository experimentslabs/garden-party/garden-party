# frozen_string_literal: true

class EnforceNamesUniquenessOnGeneraAndFamilies < ActiveRecord::Migration[6.1]
  def change
    add_index :genera, :name, unique: true
    add_index :families, :name, unique: true
  end
end
