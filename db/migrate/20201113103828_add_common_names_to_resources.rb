# frozen_string_literal: true

class AddCommonNamesToResources < ActiveRecord::Migration[6.0]
  def change
    add_column :resources, :common_names, :jsonb, null: false, default: []
  end
end
