# frozen_string_literal: true

# This migration creates an "observations" table to store data from the old
# "activities" table when activity is an "observe" activity or has notes
#
# This is the beginning of all the activities rework.
#
# - Observations are created from activities
# - Attachments are re-attached to observations
class CreateObservations < ActiveRecord::Migration[6.1]
  BACKUP_FILE = Rails.root.join('db', 'backups', 'create_observations_migration_backup.yml')

  def find_user_id(activity)
    user_id = activity['assignee_id']
    unless user_id
      map     = case activity['subject_type']
                when 'Map'
                  select_one('SELECT * FROM maps WHERE id=$1', 'SELECT_SUBJECT', [activity['subject_id']])
                when 'Element'
                  element = select_one('SELECT * FROM elements WHERE id=$1', 'SELECT_ELEMENT', [activity['subject_id']])
                  patch   = select_one('SELECT * FROM patches WHERE id=$1', 'SELECT_PATCH', [element['patch_id']])
                  select_one('SELECT * FROM maps WHERE id=$1', 'SELECT_MAP', [patch['map_id'].to_i])
                else
                  subject = select_one("SELECT * FROM #{activity['subject_type'].tableize} WHERE id=$1", 'SELECT_SUBJECT', [activity['subject_id']])
                  select_one('SELECT * FROM maps WHERE id=$1', 'SELECT_MAP', [subject['map_id']])
                end
      user_id = map['user_id'].to_i
    end

    user_id
  end

  # @return The new observation id
  def create_observation(activity)
    # Find user who performed the activity
    user_id = find_user_id(activity)

    # Create observation from activity
    observation_bindings = [
      I18n.t('migrations.create_observation.observation_title'),
      activity['notes'],
      user_id,
      activity['subject_type'],
      activity['subject_id'],
      activity['done_at'] || activity['created_at'],
      activity['created_at'],
    ]
    insert <<~SQL.squish, 'CREATE_OBSERVATION', nil, nil, nil, observation_bindings
      INSERT INTO observations (title, content, user_id, subject_type, subject_id, made_at, created_at, updated_at)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $7)
    SQL
  end

  def change # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    reversible do |dir|
      dir.up do
        create_table :observations do |t|
          t.string :title, null: false, default: nil
          t.text :content
          t.references :subject, null: false, polymorphic: true
          t.references :user, null: false, foreign_key: true
          t.timestamp :made_at, null: false, default: nil

          t.timestamps
        end

        migrated_activity_ids = []
        backup_references = []

        # Migrate all the attachments to a an observation with activity description
        attachments = select_all 'SELECT * FROM active_storage_attachments WHERE record_type=$1', 'FIND_ATTACHMENTS', ['Activity']
        attachments.each do |attachment|
          activity = select_one 'SELECT * FROM activities WHERE id=$1', 'SELECT_ACTIVITY', [attachment['record_id'].to_i]
          observation_id = create_observation activity
          backup_references.push [activity['id'].to_i, observation_id]

          # Update activity attachment to belong to the observation
          attachment_bindings = [
            'Observation',
            observation_id,
            attachment['id'],
          ]
          update 'UPDATE active_storage_attachments SET record_type=$1, record_id=$2 WHERE id=$3', 'UPDATE_ATTACHMENT', attachment_bindings

          migrated_activity_ids.push activity['id'].to_i
        end

        # Migrate "observe" activities to observations
        activities = select_all 'SELECT * FROM activities WHERE name=$1', 'SELECT_ACTIVITIES', ['observe']
        activities.each do |activity|
          next if migrated_activity_ids.include? activity['id'].to_i

          observation_id = create_observation activity
          backup_references.push [activity['id'].to_i, observation_id]
        end

        # Save references for rollback
        File.write BACKUP_FILE, <<~YAML
          # This file was created when running "CreateObservations" migration. You should
          # keep it somewhere in case you have to rollback: it contains references to moved
          # data.
          #
          # When rolling back, put it back in "db/backups" directory so it can be picked up.
          #
          # Each pair correspond to the old "activity" entry / the new observation one.
          #{backup_references.to_yaml}
        YAML
      end

      dir.down do
        backup_references = YAML.load_file BACKUP_FILE if File.exist? BACKUP_FILE
        backup_references ||= []

        # Migrate observations to "observe" activities
        observations = select_all 'SELECT *  FROM observations'
        observations.each do |observation|
          updated = false
          backup_pair = backup_references.find { |r| r[1] == observation['id'] }

          if backup_pair
            activity = select_one 'SELECT * FROM activities WHERE id=$1', 'FIND_ACTIVITY', [backup_pair[0]]
            if activity
              updated = true
              update 'UPDATE activities SET notes=$1 WHERE id=$2', 'UPDATE_ACTIVITY', [observation['content'], backup_pair[0]]
            else
              backup_references.delete backup_pair
            end
          end

          next if updated

          bindings = [
            observation['content'],
            observation['subject_type'],
            observation['subject_id'],
            observation['user_id'],
            observation['made_at'],
            observation['created_at'],
          ]
          activity_id = insert <<~SQL.squish, 'CREATE_ACTIVITY', nil, nil, nil, bindings
            INSERT INTO activities (name, notes, subject_type, subject_id, assignee_id, done_at, created_at, updated_at)
            VALUES ('observe', $1, $2, $3, $4, $5, $6, $6)
          SQL
          backup_references.push [activity_id, observation['id']]
        end

        # Migrate observations attachments to activities
        attachments = select_all 'SELECT * FROM active_storage_attachments WHERE record_type=$1', 'FIND_ATTACHMENTS', ['Observation']
        attachments.each do |attachment|
          backup_pair = backup_references.find { |r| r[1] == attachment['record_id'] }
          attachment_bindings = [
            'Activity',
            backup_pair[0],
            attachment['id'],
          ]
          update 'UPDATE active_storage_attachments SET record_type=$1, record_id=$2 WHERE id=$3', 'UPDATE_ATTACHMENT', attachment_bindings
        end

        # Drop table
        drop_table :observations
      end
    end
  end
end
