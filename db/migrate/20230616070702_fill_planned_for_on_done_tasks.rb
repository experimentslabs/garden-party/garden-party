# frozen_string_literal: true

class FillPlannedForOnDoneTasks < ActiveRecord::Migration[7.0]
  def change
    exec_update 'UPDATE tasks set planned_for = done_at where planned_for IS NULL'
  end
end
