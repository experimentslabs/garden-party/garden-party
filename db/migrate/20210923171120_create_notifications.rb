# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[6.1]
  def change
    create_table :notifications do |t|
      t.string :type
      t.jsonb :content

      t.references :subject, null: true, polymorphic: true

      t.references :recipient, null: false, foreign_key: { to_table: :users }
      t.references :sender, null: true, foreign_key: { to_table: :users }

      t.datetime :archived_at

      t.timestamps
    end
  end
end
