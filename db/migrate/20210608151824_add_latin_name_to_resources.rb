# frozen_string_literal: true

class AddLatinNameToResources < ActiveRecord::Migration[6.1]
  def change
    add_column :resources, :latin_name, :string, default: nil
  end
end
