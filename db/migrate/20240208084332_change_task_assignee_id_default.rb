# frozen_string_literal: true

class ChangeTaskAssigneeIdDefault < ActiveRecord::Migration[7.1]
  def change
    change_column_null :tasks, :assignee_id, true
  end
end
