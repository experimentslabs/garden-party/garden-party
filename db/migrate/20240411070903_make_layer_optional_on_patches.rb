# frozen_string_literal: true

class MakeLayerOptionalOnPatches < ActiveRecord::Migration[7.1]
  def change
    change_column_null :patches, :layer_id, true
  end
end
