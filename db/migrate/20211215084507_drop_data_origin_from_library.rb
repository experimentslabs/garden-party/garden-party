# frozen_string_literal: true

class DropDataOriginFromLibrary < ActiveRecord::Migration[6.1]
  def change
    remove_column :families, :data_origin, :integer, null: false, default: 1
    remove_column :genera, :data_origin, :integer, null: false, default: 1
    remove_column :resources, :data_origin, :integer, null: false, default: 1
  end
end
