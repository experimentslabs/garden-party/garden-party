# frozen_string_literal: true

class ReCreateActivitiesTable < ActiveRecord::Migration[6.1]
  # Inserts a new activity
  def create_activity(action:, model:, id:, subject_name:, user_id:, map_id:, date:, data: {}, happened_at: nil) # rubocop:disable Metrics/ParameterLists
    user = find_user user_id
    bindings = [
      action,
      data.to_json,
      happened_at || date,
      model,
      id.to_i,
      subject_name,
      map_id.to_i,
      user_id.to_i,
      user['username'],
      date,
    ]

    exec_insert <<~SQL.squish, 'CREATE_ACTIVITY', bindings
      INSERT INTO activities (action, data, happened_at, subject_type, subject_id, subject_name, map_id, user_id, username, created_at, updated_at)
      VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $10)
    SQL
  end

  # Finds a map from an element
  def element_map(id)
    select_one <<~SQL.squish, 'SELECT_MAP', [id.to_i]
      SELECT maps.* FROM maps
        INNER JOIN patches p on maps.id = p.map_id
        INNER JOIN elements e on p.id = e.patch_id
      WHERE e.id = $1
    SQL
  end

  # Finds a polymorphic subject from its type and id
  def find_subject(type, id)
    select_one "SELECT * from #{type.tableize} WHERE id=$1",
               'FIND_SUBJECT',
               [id.to_i]
  end

  def element_name(element)
    resource = select_one 'SELECT * FROM resources WHERE id=$1', 'FIND_RESOURCE', [element['resource_id'].to_i]
    patch    = select_one 'SELECT * FROM patches WHERE id=$1', 'FIND_PATCH', [element['patch_id'].to_i]
    geometry = JSON.parse(patch['geometry'])
    patch_name = patch_name patch

    if geometry['geometry']['type'] == 'Point'
      if patch['name'].present?
        I18n.t('models.element.name.point_element', resource_name: resource['name'], element_name: patch_name)
      else
        resource['name']
      end
    else
      I18n.t('models.element.name.element_in_patch', resource_name: resource['name'], patch_name: patch_name)
    end
  end

  def find_subject_name(type, id)
    subject = find_subject type, id

    case type
    when 'Map', 'Path', 'Layer'
      subject['name']
    when 'Element'
      element_name subject
    when 'Patch'
      patch_name subject
    when 'Observation'
      subject['title']
    else
      raise "Unsupported type #{type}"
    end
  end

  def find_user(id)
    select_one 'SELECT * FROM users WHERE id=$1',
               'FIND_USER',
               [id.to_i]
  end

  # Finds a map for a given polymorphic subject type and id
  def subject_map(type, id)
    case type
    when 'Map'
      select_one('SELECT * FROM maps WHERE id=$1', 'SELECT_SUBJECT', [id.to_i])
    when 'Element'
      element_map id
    when 'Patch', 'Path'
      select_one <<~SQL.squish, 'SELECT_SUBJECT', [id.to_i]
        SELECT * FROM maps m
          WHERE m.id IN (SELECT map_id FROM #{type.tableize} s WHERE s.id=$1)
      SQL
    else
      raise "Unsupported task subject '#{task['subject_type']}'"
    end
  end

  def create_activities_from_tasks
    done_tasks = select_all('SELECT * FROM tasks', 'SELECT_TASKS')
    done_tasks.each do |task|
      map = subject_map task['subject_type'], task['subject_id']
      # Fallback to map owner when not set
      user = find_user task['assignee_id'] || map['user_id']

      subject_name = find_subject_name task['subject_type'], task['subject_id']
      data = { subject: subject_name }
      data[:assignee] = user['username'] if task['done_at'].blank?

      create_activity action:       task['done_at'].present? ? 'finish' : 'create',
                      data:         data,
                      happened_at:  task['done_at'],
                      model:        'Task',
                      id:           task['id'],
                      subject_name: task['name'],
                      user_id:      user['id'],
                      map_id:       map['id'],
                      date:         task['updated_at']
    end
  end

  def patch_name(patch)
    patch['name'].presence || I18n.t('generic.patch.name')
  end

  def create_activities_from_observations
    observations = select_all 'SELECT * FROM observations'
    observations.each do |observation|
      map = subject_map observation['subject_type'], observation['subject_id']
      create_activity action:       'create',
                      model:        'Observation',
                      id:           observation['id'],
                      subject_name: observation['title'],
                      user_id:      observation['user_id'],
                      map_id:       map['id'],
                      date:         observation['created_at']
    end
  end

  def create_activities_from_patches
    # Create activities from patch creation
    patches = select_all 'SELECT * FROM patches'
    patches.each do |patch|
      geometry = JSON.parse(patch['geometry'])
      next if geometry['geometry'] && geometry['geometry']['type'] == 'Point'

      map      = select_one 'SELECT * FROM maps WHERE id=$1', 'SELECT_MAP', [patch['map_id'].to_i]
      user_id  = map['user_id']

      create_activity action:       'create',
                      model:        'Patch',
                      id:           patch['id'],
                      subject_name: patch_name(patch),
                      user_id:      user_id,
                      map_id:       map['id'],
                      date:         patch['created_at']
    end
  end

  def create_activities_from_elements
    # Create activities from implantation and removal dates
    elements    = select_all 'SELECT * FROM elements'
    date_fields = %w[implanted_at removed_at]
    elements.each do |element|
      next unless element['implanted_at'] || element['removed_at']

      # Find user
      map     = element_map element['id']
      user_id = map['user_id']

      # Create activities for dates
      date_fields.each do |date|
        next unless element[date]

        create_activity action:       date == 'implanted_at' ? 'implant' : 'remove',
                        model:        'Element',
                        id:           element['id'],
                        subject_name: element_name(element),
                        user_id:      user_id,
                        map_id:       map['id'],
                        happened_at:  element[date],
                        date:         element['created_at']
      end
    end
  end

  def create_activities_from_team_mates
    # Create activities when team mates joins a garden
    team_mates = select_all 'SELECT * FROM team_mates WHERE accepted_at IS NOT NULL'
    team_mates.each do |team_mate|
      user = select_one 'SELECT * FROM users WHERE id=$1', 'SELECT_USER', [team_mate['user_id'].to_i]
      create_activity action:       'join',
                      model:        'TeamMate',
                      id:           team_mate['id'],
                      subject_name: user['username'],
                      user_id:      team_mate['user_id'],
                      map_id:       team_mate['map_id'],
                      date:         team_mate['accepted_at']
    end
  end

  def change
    reversible do |dir|
      dir.up do
        create_table :activities do |t|
          t.string :action, null: false, default: nil
          t.jsonb :data, null: false, default: '{}'
          t.datetime :happened_at, null: false, default: nil
          t.references :subject, polymorphic: true
          t.references :user, foreign_key: true
          # Historized username (users may be destroyed and not be accessible anymore)
          t.string :username
          # Historized subject representation (name, title or whatever)
          t.string :subject_name
          t.references :map, foreign_key: true

          t.timestamps
        end

        create_activities_from_tasks
        create_activities_from_observations
        create_activities_from_patches
        create_activities_from_elements
        create_activities_from_team_mates
      end

      dir.down do
        drop_table :activities
      end
    end
  end
end
