# frozen_string_literal: true

class AddObservationsCounterToTables < ActiveRecord::Migration[7.2]
  def change
    [:elements, :maps, :patches, :paths]
      .each do |table|
      add_column table, :observations_count, :integer, null: false, default: 0
    end
  end
end
