# frozen_string_literal: true

# Creates the resource_interactions_groups table to manage interactions on groups
# of resources instead of resources themselves.
#
# In order to achieve this, we recreate the resources_interactions table with
# references to resources_interactions_groups instead_of resources.
#
# A backup of the old resources_interactions is made, but their history is lost.
class CreateResourceInteractionsGroups < ActiveRecord::Migration[6.1]
  BACKUP_FILE = Rails.root.join('db', 'backups', 'create_resource_interactions_groups.yml')

  def change
    reversible do |dir|
      dir.up do
        create_table :resource_interactions_groups do |t|
          t.string :name, null: false, default: nil
          t.text :description, null: true, default: nil

          t.timestamps
          t.index :name, unique: true
        end

        # Backup resource interactions
        resource_interactions = select_all 'SELECT *  from resource_interactions'
        File.write BACKUP_FILE, <<~YAML
          # This file was created when running "CreateResourceInteractionsGroups" migration. You should
          # keep it somewhere in case you have to rollback: it contains destroyed data.
          #
          # When rolling back, put it back in "db/backups" directory so it can be picked up.
          #{resource_interactions.to_yaml}
        YAML

        drop_table :resource_interactions
        create_table :resource_interactions do |t|
          t.integer :nature, null: false, default: nil
          t.string :notes, null: true, default: nil
          t.references :subject, null: false, default: nil, foreign_key: { to_table: :resource_interactions_groups }
          t.references :target, null: false, default: nil, foreign_key: { to_table: :resource_interactions_groups }
          t.text :sources, null: true, default: nil

          t.timestamps
          t.index [:subject_id, :target_id], unique: true
        end

        add_reference :resources, :resource_interactions_group, foreign_key: true, null: true, default: nil

        # Delete version history
        exec_delete "DELETE FROM versions WHERE item_type='ResourceInteraction'"
      end

      dir.down do
        # Delete version history
        exec_delete "DELETE FROM versions WHERE item_type='ResourceInteraction'"
        exec_delete "DELETE FROM versions WHERE item_type='ResourceInteractionsGroup'"

        remove_column :resources, :resource_interactions_group_id

        drop_table :resource_interactions
        create_table :resource_interactions do |t|
          t.integer :nature, null: false, default: nil
          t.string :notes, null: true, default: nil
          t.text :sources, null: true, default: nil
          t.references :subject, null: false, default: nil, foreign_key: { to_table: :resources }
          t.references :target, null: false, default: nil, foreign_key: { to_table: :resources }

          t.timestamps
          t.index [:subject_id, :target_id], unique: true
        end

        if File.exist? BACKUP_FILE
          backup = YAML.safe_load_file BACKUP_FILE,
                                       permitted_classes: [ActiveRecord::Result, Time, ActiveRecord::Type::Integer, ActiveRecord::Type::Text, Range, ActiveModel::Type::String, ActiveRecord::ConnectionAdapters::PostgreSQL::OID::Timestamp],
                                       aliases:           true
        end
        backup ||= nil

        # Restore backup
        backup&.each do |interaction|
          subject_exists = select_one 'SELECT count(*) FROM resources WHERE id=$1', 'FIND_SUBJECT', [interaction['subject_id']]
          target_exists = select_one 'SELECT count(*) FROM resources WHERE id=$1', 'FIND_SUBJECT', [interaction['target_id']]

          next unless subject_exists['count'] == 1 && target_exists['count'] == 1

          binding = [
            interaction['nature'],
            interaction['notes'],
            interaction['sources'],
            interaction['subject_id'],
            interaction['target_id'],
            interaction['created_at'],
            interaction['updated_at'],
          ]

          exec_insert 'INSERT INTO resource_interactions (nature, notes, sources, subject_id, target_id, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, $7)',
                      'RESTORE_ENTRY',
                      binding
        end

        drop_table :resource_interactions_groups
      end
    end
  end
end
