# frozen_string_literal: true

# The goal of this migration is to add a name and geometry field to elements.
# It makes sense to represent "point patches" as patches with only one element
# and a "Point" geometry, but it is easier to handle element as implanted stuff.
class AddGeometryAndNameToElements < ActiveRecord::Migration[6.1]
  # Deletes a point patch and its references in other tables
  # To use when a point patch has no element
  def delete_point_patch(patch)
    exec_delete "DELETE FROM observations WHERE subject_type='Patch' AND subject_id=$1", 'DELETE_OBSERVATIONS', [patch['id'].to_i]
    exec_delete "DELETE FROM tasks WHERE subject_type='Patch' AND subject_id=$1", 'DELETE_TASKS', [patch['id'].to_i]
    exec_delete "DELETE FROM activities WHERE subject_type='Patch' AND subject_id=$1", 'DELETE_ACTIVITIES', [patch['id'].to_i]
    exec_delete 'DELETE FROM patches WHERE id=$1', 'DELETE_POINT_PATCH', [patch['id'].to_i]
  end

  def delete_element(element)
    exec_delete "DELETE FROM observations WHERE subject_type='Element' AND subject_id=$1", 'DELETE_OBSERVATIONS', [element['id'].to_i]
    exec_delete "DELETE FROM tasks WHERE subject_type='Element' AND subject_id=$1", 'DELETE_TASKS', [element['id'].to_i]
    exec_delete "DELETE FROM activities WHERE subject_type='Element' AND subject_id=$1", 'DELETE_ACTIVITIES', [element['id'].to_i]
    exec_delete 'DELETE FROM elements WHERE id=$1', 'DELETE_EXTRANEOUS_ELEMENT', [element['id']]
  end

  def change
    reversible do |dir|
      dir.up do
        add_column :elements, :name, :string, comment: 'Used when out of a patch'
        add_column :elements, :geometry, :jsonb, comment: 'Used when out of a patch'
        add_reference :elements, :map, foreign_key: true, null: true, comment: 'Used when out of a patch'
        add_reference :elements, :layer, foreign_key: true, null: true, comment: 'Used when out of a patch'

        # Make patch_id not mandatory for elements
        change_column_null :elements, :patch_id, true

        # Create map entities from patches
        patches = select_all 'SELECT * FROM patches'
        patches.each do |patch|
          geometry = JSON.parse(patch['geometry'])

          next unless geometry['geometry']['type'] == 'Point'

          # Find element
          elements = select_all 'SELECT * FROM elements WHERE patch_id=$1 ORDER BY id', 'FIND_ELEMENTS', [patch['id'].to_i]
          element = elements.first

          # Ignore empty point patches and delete them
          if element.nil?
            delete_point_patch patch
            next
          end

          # Check for other elements and delete them. A point patch should only have one element
          if elements.length > 1
            list = elements.to_a
            list.shift
            list.each do |entry|
              delete_element entry
            end
          end

          # Update element
          bindings = [
            element['id'],
            patch['name'],
            patch['geometry'],
            patch['map_id'],
            patch['layer_id'],
            patch['discarded_at'],
          ]
          exec_update <<~SQL.squish, 'UPDATE_ELEMENT', bindings
            UPDATE elements SET name=$2, geometry=$3, map_id=$4, layer_id=$5, discarded_at=$6, patch_id=NULL
              WHERE id=$1
          SQL

          # Update related tables:
          # - observations
          # - tasks
          # - activities
          bindings = [
            element['id'].to_i,
            patch['id'].to_i,
          ]
          exec_update "UPDATE observations set subject_type='Element', subject_id=$1 WHERE subject_type='Patch' AND subject_id=$2",
                      'UPDATE_OBSERVATIONS',
                      bindings
          exec_update "UPDATE tasks set subject_type='Element', subject_id=$1 WHERE subject_type='Patch' AND subject_id=$2",
                      'UPDATE_TASKS',
                      bindings

          exec_update "UPDATE activities set subject_type='Element', subject_id=$1 WHERE subject_type='Patch' AND subject_id=$2",
                      'UPDATE_ACTIVITIES',
                      bindings

          # Remove point patch
          exec_delete 'DELETE FROM patches WHERE id=$1', 'DELETE_POINT_PATCH', [patch['id'].to_i]
        end
      end

      dir.down do
        # Re-create point patches
        point_patches = select_all 'SELECT * FROM elements WHERE patch_id IS NULL'
        point_patches.each do |element|
          bindings = [
            element['name'],
            element['geometry'],
            element['map_id'],
            element['layer_id'],
            element['discarded_at'],
            element['created_at'],
            element['updated_at'],
          ]
          patch_ids = exec_insert <<~SQL.squish, 'CREATE_POINT_PATCH', bindings
            INSERT INTO patches (name, geometry, map_id, layer_id, discarded_at, created_at, updated_at)
              VALUES ($1, $2, $3, $4, $5, $6, $7)
          SQL
          patch_id = patch_ids.first['id']

          # Update element with the patch
          exec_update 'UPDATE elements SET patch_id=$1 WHERE id=$2',
                      'UPDATE_ELEMENT',
                      [
                        patch_id.to_i,
                        element['id'].to_i,
                      ]

          # Update related tables:
          # - observations
          # - tasks
          # - activities
          bindings = [
            patch_id.to_i,
            element['id'].to_i,
          ]
          exec_update "UPDATE observations set subject_type='Patch', subject_id=$1 WHERE subject_type='ELEMENT' AND subject_id=$2",
                      'UPDATE_OBSERVATIONS',
                      bindings
          exec_update "UPDATE tasks set subject_type='Patch', subject_id=$1 WHERE subject_type='ELEMENT' AND subject_id=$2",
                      'UPDATE_TASKS',
                      bindings

          exec_update "UPDATE activities set subject_type='Patch', subject_id=$1 WHERE subject_type='ELEMENT' AND subject_id=$2",
                      'UPDATE_ACTIVITIES',
                      bindings
        end

        remove_columns :elements, :name, :geometry, :map_id, :layer_id

        # Make patch_id mandatory on elements
        change_column_null :elements, :patch_id, false
      end
    end
  end
end
