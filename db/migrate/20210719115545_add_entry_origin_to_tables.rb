# frozen_string_literal: true

class AddEntryOriginToTables < ActiveRecord::Migration[6.1]
  def change # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    add_column :families, :data_origin, :integer, null: false, default: 1
    add_column :genera, :data_origin, :integer, null: false, default: 1
    add_column :resources, :data_origin, :integer, null: false, default: 1

    seeds = YAML.load_file Rails.root.join 'db', "seeds_#{I18n.locale}.yml"

    seeds[:families].each do |seed_family|
      kingdom = seed_family[:kingdom] == 'plant' ? 0 : 1
      family  = select_one 'SELECT id FROM families WHERE name=$1 AND kingdom=$2', 'FIND_FAMILY', [seed_family[:name], kingdom]
      next unless family

      update 'UPDATE families SET data_origin=0 WHERE id=$1', 'UPDATE_FAMILY', [family['id']]
    end

    seeds[:genera].each do |seed_genus|
      family = select_one 'SELECT id FROM families WHERE name=$1', 'FIND_FAMILY', [seed_genus[:family]]
      next unless family

      genus = select_one 'SELECT id FROM genera WHERE name=$1 AND family_id=$2', 'FIND_GENUS', [seed_genus[:name], family['id']]
      next unless genus

      update 'UPDATE genera SET data_origin=0 WHERE id=$1', 'UPDATE_FAMILY', [genus['id']]
    end

    seeds[:resources].each do |seed_resource|
      genus = select_one 'SELECT id FROM genera WHERE name=$1', 'FIND_GENUS', [seed_resource[:genus]]
      next unless genus

      resource = select_one 'SELECT id FROM resources WHERE name=$1 AND genus_id=$2', 'FIND_GENUS', [seed_resource[:name], genus['id']]
      next unless resource

      update 'UPDATE resources SET data_origin=0 WHERE id=$1', 'UPDATE_FAMILY', [resource['id']]
    end
  end
end
