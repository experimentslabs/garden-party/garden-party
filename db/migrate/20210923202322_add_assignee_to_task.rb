# frozen_string_literal: true

class AddAssigneeToTask < ActiveRecord::Migration[6.1]
  def change
    add_reference :activities, :assignee, foreign_key: { to_table: :users }, null: true
  end
end
