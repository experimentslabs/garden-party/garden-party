# frozen_string_literal: true

class AddStrokeStylePresetToPaths < ActiveRecord::Migration[6.1]
  def change
    add_column :paths, :stroke_style_preset, :string, null: true, default: nil
  end
end
