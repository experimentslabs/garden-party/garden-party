# frozen_string_literal: true

class CreateComments < ActiveRecord::Migration[7.2]
  def change
    create_table :comments do |t|
      t.string :content, null: false, default: nil
      t.datetime :removed_at, default: nil
      t.boolean :removed_by_map_owner, null: false, default: false

      t.references :user, null: false, foreign_key: true
      t.references :subject, null: false, polymorphic: true

      t.timestamps
    end
  end
end
