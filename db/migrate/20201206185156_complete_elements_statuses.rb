# frozen_string_literal: true

class CompleteElementsStatuses < ActiveRecord::Migration[6.0]
  def change
    quoted_time = ActiveRecord::Base.connection.quote Time.now # rubocop:disable Rails/TimeZone

    reversible do |dir|
      dir.up do
        change_table :elements, bulk: true do |t|
          t.rename :planted_at, :implanted_at
          t.datetime :implantation_planned_for
          t.datetime :removal_planned_for
        end
        execute "UPDATE elements SET implantation_planned_for=implanted_at WHERE implanted_at > #{quoted_time}"
        execute "UPDATE elements SET implanted_at=NULL WHERE implanted_at > #{quoted_time}"
        execute "UPDATE elements SET removal_planned_for=implanted_at WHERE removed_at > #{quoted_time}"
        execute "UPDATE elements SET removed_at=NULL WHERE removed_at > #{quoted_time}"
      end

      dir.down do
        execute "UPDATE elements SET implanted_at=implantation_planned_for WHERE implantation_planned_for > #{quoted_time} AND implanted_at IS NULL"
        execute "UPDATE elements SET removed_at=removal_planned_for WHERE removal_planned_for > #{quoted_time} AND removed_at IS NULL"

        change_table :elements, bulk: true do |t|
          t.remove :implantation_planned_for, :removal_planned_for
          t.rename :implanted_at, :planted_at
        end
      end
    end
  end
end
