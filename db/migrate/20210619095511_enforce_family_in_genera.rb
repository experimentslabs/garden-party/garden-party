# frozen_string_literal: true

class EnforceFamilyInGenera < ActiveRecord::Migration[6.1]
  def change
    change_column_null :genera, :family_id, false
  end
end
