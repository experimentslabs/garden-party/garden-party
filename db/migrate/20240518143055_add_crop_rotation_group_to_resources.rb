# frozen_string_literal: true

class AddCropRotationGroupToResources < ActiveRecord::Migration[7.1]
  def change
    add_column :resources, :crop_rotation_group, :integer, null: true, default: nil
  end
end
