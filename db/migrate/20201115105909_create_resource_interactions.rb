# frozen_string_literal: true

class CreateResourceInteractions < ActiveRecord::Migration[6.0]
  def change
    create_table :resource_interactions do |t|
      t.integer :nature, null: false, default: nil
      t.string  :notes

      t.references :subject, null: false, foreign_key: { to_table: :resources }
      t.references :target,  null: false, foreign_key: { to_table: :resources }

      t.timestamps
    end

    add_index :resource_interactions, [:subject_id, :target_id], unique: true
  end
end
