# frozen_string_literal: true

class AddMapIdToNotifications < ActiveRecord::Migration[7.2]
  def change
    add_reference :notifications, :map, foreign_key: true, null: true
  end
end
