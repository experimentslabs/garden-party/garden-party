# frozen_string_literal: true

class AddSyncIdToResourceInteractionsGroups < ActiveRecord::Migration[6.1]
  def change
    add_column :resource_interactions_groups, :sync_id, :string
  end
end
