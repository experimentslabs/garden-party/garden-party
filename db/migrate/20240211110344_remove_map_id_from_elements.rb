# frozen_string_literal: true

class RemoveMapIdFromElements < ActiveRecord::Migration[7.1]
  def change
    reversible do |dir|
      dir.up do
        remove_column :elements, :map_id
      end

      dir.down do
        add_reference :elements, :map, null: true, default: nil, foreign_key: true
      end
    end
  end
end
