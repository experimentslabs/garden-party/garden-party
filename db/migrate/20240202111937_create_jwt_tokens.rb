# frozen_string_literal: true

class CreateJwtTokens < ActiveRecord::Migration[7.1]
  def change
    create_table :jwt_tokens, id: :uuid do |t|
      t.string :description, null: false, default: nil
      t.datetime :expires_at, null: false, default: nil

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
