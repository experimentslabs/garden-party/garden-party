# frozen_string_literal: true

class CreateHarvests < ActiveRecord::Migration[7.0]
  def change
    create_table :harvests do |t|
      t.float :quantity, null: false, default: nil
      t.integer :unit, null: false, default: nil

      t.references :element, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.datetime :harvested_at, null: false, default: nil
      t.datetime :discarded_at
      t.timestamps

      t.index :discarded_at
    end
  end
end
