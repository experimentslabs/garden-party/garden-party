# frozen_string_literal: true

class AddSyncIdToLibrary < ActiveRecord::Migration[6.1]
  def change
    add_column :families, :sync_id, :string, default: nil
    add_column :genera, :sync_id, :string, default: nil
    add_column :resources, :sync_id, :string, default: nil
  end
end
