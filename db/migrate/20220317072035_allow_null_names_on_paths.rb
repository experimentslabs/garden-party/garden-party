# frozen_string_literal: true

class AllowNullNamesOnPaths < ActiveRecord::Migration[6.1]
  def change
    change_column_null :paths, :name, true
  end
end
