# frozen_string_literal: true

class AddSyncIdToResourceInteractions < ActiveRecord::Migration[6.1]
  def change
    add_column :resource_interactions, :sync_id, :string
  end
end
