# frozen_string_literal: true

class ChangeObjectToJsonFieldOnVersions < ActiveRecord::Migration[7.1]
  YAML_PERMITTED_CLASSES = [
    ActiveSupport::TimeWithZone,
    Time,
    ActiveSupport::TimeZone,
    ActsAsTaggableOn::TagList,
    Symbol,
    ActsAsTaggableOn::DefaultParser,
  ].freeze

  def change
    reversible do |dir|
      dir.up do
        create_temp_columns :jsonb

        select_all('SELECT * FROM versions WHERE object IS NOT NULL OR object_changes IS NOT NULL').each do |version|
          object = nil
          object_changes = nil

          if version['object'].present? # rubocop:disable Style/IfUnlessModifier
            object = YAML.safe_load(version['object'], permitted_classes: YAML_PERMITTED_CLASSES, aliases: true).to_json
          end

          if version['object_changes'].present? # rubocop:disable Style/IfUnlessModifier
            object_changes = YAML.safe_load(version['object_changes'], permitted_classes: YAML_PERMITTED_CLASSES, aliases: true).to_json
          end

          update 'UPDATE versions SET temp_object=$1, temp_object_changes=$2 WHERE id=$3', 'UPDATE', [object, object_changes, version['id']]
        end

        rename_temp_columns
      end

      dir.down do
        create_temp_columns :text

        select_all('SELECT * from versions WHERE object IS NOT NULL OR object_changes IS NOT NULL').each do |version|
          object = nil
          object_changes = nil

          object = JSON.parse(version['object']).to_yaml if version['object'].present?
          object_changes = JSON.parse(version['object_changes']).to_yaml if version['object_changes'].present?

          update 'UPDATE versions SET temp_object=$1, temp_object_changes=$2 WHERE id=$3', 'UPDATE', [object, object_changes, version['id']]
        end

        rename_temp_columns
      end
    end
  end

  def create_temp_columns(type)
    add_column :versions, :temp_object, type
    add_column :versions, :temp_object_changes, type
  end

  def rename_temp_columns
    remove_column :versions, :object
    remove_column :versions, :object_changes
    rename_column :versions, :temp_object, :object
    rename_column :versions, :temp_object_changes, :object_changes
  end
end
