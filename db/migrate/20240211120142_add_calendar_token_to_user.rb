# frozen_string_literal: true

class AddCalendarTokenToUser < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :calendar_token, :string, null: true, default: nil

    add_index :users, [:calendar_token], unique: true
  end
end
