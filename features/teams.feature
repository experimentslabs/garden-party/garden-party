Feature: Teams management
  As an user of the application
  In order to work with other people on the same map
  I want to be able to manage teams

  Background:
    Given I have an account and i'm logged in
    And I have a map named "My garden"
    And there is an user "Alice" with a map "Wild life park" in the system
    And "Alice" invited me in her garden
    And there is an user "Mattie" with a map "Green field" in the system
    And "Mattie" is in "My garden" team
    And I'm in the "Mattie"'s map team
    And there is a "Harvest cucumbers" task on a "Vegetables" patch in "My garden"
    And there is a "cut trees" task on a "Bushes" patch in "Green field", assigned to me
    And there are other pending tasks on the "Green field" map
    And I access the "My garden" map

  Scenario: Invite people by email: not on GardenParty
    Given I visit the team section
    When I invite "bob@garden-party.io" as "Bob"
    Then I see "Bob" as a pending member
    Then I see "Bob" as a pending teammate

  Scenario: Invite people by email: already registered
    Given there is an user "Bob" with email "bob@garden-party.io" in the system
    And I visit the team section
    When I invite "bob@garden-party.io"
    Then I see "Bob" as a pending teammate

  Scenario: Invite people by username
    Given I visit the team section
    When I search for "Alice" user
    And I add "Alice" to the team
    Then I see "Alice" as a pending teammate

  Scenario: Display invitation notification
    When I click on the notifications button
    Then I see a pending invitation from "Alice" on her map

  Scenario: Accept team work invitations
    And I access the notifications list
    When I accept the team invitation on "Wild life park"
    Then I see a message stating I accepted to join the team

  Scenario: List all maps I can access
    Given I access the maps list
    Then I see "Green field" map as a non-owned map

  Scenario: Leave a team
    # Accept invitation
    Given I access the notifications list
    And I accept the team invitation on "Wild life park"
    # Actual test
    And I access the "Wild life park" map
    And I visit the team section
    When I leave the team
    Then I see a message stating I left the team
    And I'm redirected to the maps list
    And I don't see the "Wild life park" map in the list

  Scenario: Remove an user from the team
    Given I visit the team section
    When I remove "Mattie" from the team
    Then I don't see "Mattie" in the teammates
    And there is no teammates but me

  Scenario: Change a map when I'm part of the team
    And I access the inventory page for "Green field"
    When I delete the "Bushes" patch
    Then the "Bushes" patch does not exist anymore

  Scenario: Create a task for someone
    Given I access the inventory page for "Green field"
    And I create a "Buy seeds" task assigned to "Mattie"
    Then the "Buy seeds" task is assigned to "Mattie"

  Scenario: Assign a task to someone
    Given I access the tasks list
    And I assign the "Harvest cucumbers" task to "Mattie"
    Then the "Harvest cucumbers" task is assigned to "Mattie"
