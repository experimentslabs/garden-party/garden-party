Feature: Soil analysis
  As an user
  In order to know my soil better
  I want to be able to have simple information on it

  Scenario: Access the page
    Given I have an account and i'm logged in
    Given I access the app
    When I access the soil analysis tool
    Then I see a page with a form and a picture
