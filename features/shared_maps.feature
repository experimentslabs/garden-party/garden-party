Feature: Shared maps
  As a member
  In order to showcase my garden
  I want to be able to give access to it in a read-only mode

  Scenario: Creating a shared map
    Given I have an account and i'm logged in
    And I create a shared map named "My delightful place"
    When I access the maps list
    Then I see a link to share "My delightful place"

  Scenario: Sharing an existing map
    And I have an account and i'm logged in
    Given I have a map named "The peaceful zone"
    When I share the "The peaceful zone" map
    When I access the maps list
    Then I see a link to share "The peaceful zone"

  Scenario: Displaying a shared map
    Given There is a shared map in the system
    When I access it with the provided link
    Then I see the map
