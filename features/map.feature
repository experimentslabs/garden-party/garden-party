Feature: Map management
  As an user
  In order to have some control over my map
  I want to be able to modify its information.

  Scenario: Display and take notes
    Given I have an account and i'm logged in
    And I have a map named "The map"
    When I access the "The map" map
    And I display the map note
    And I update the note
    Then I see the rendered new note
