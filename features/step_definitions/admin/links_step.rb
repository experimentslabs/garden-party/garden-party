# frozen_string_literal: true

When('I access the links page for administrators') do
  visit '/admin/links'
end

Given('I create a new link from administration') do
  step 'I access the links page for administrators'

  click_on I18n.t('admin.links.index.new')

  fill_in I18n.t('activerecord.attributes.link.title'), with: 'My link'
  fill_in I18n.t('activerecord.attributes.link.description'), with: 'Something useful'
  fill_in I18n.t('activerecord.attributes.link.url'), with: Faker::Internet.url

  click_on I18n.t('generic.save')
end

Given('there is a pending link titled {string} in the system') do |title|
  FactoryBot.create :link, :pending, title: title
end

When('I change the link title from {string} to {string} in administration') do |old_title, new_title|
  within 'tr', text: old_title do
    click_on I18n.t('generic.edit')
  end

  fill_in I18n.t('activerecord.attributes.link.title'), with: new_title

  click_on I18n.t('generic.save')
end

Then('I see the link titled {string} in the list') do |title|
  expect(page).to have_content title
end

When('I destroy the link {string} from administration') do |title|
  within 'tr', text: title do
    click_on I18n.t('generic.destroy')

    accept_browser_alert
  end
end

Then("I don't see the link titled {string} in the list") do |title|
  expect(page).to have_no_content title
end

Given('I approve the pending link in administration') do
  within 'tbody tr', text: I18n.t('admin.links.index.approve') do
    click_on I18n.t('admin.links.index.approve')
  end
end

Then("I don't see any pending links") do
  links = find_all 'tbody tr', text: I18n.t('admin.links.index.approve')

  expect(links.count).to eq 0
end
