# frozen_string_literal: true

Then(/^I see (\d+) (?:families?|genera|genus|links?|resource interactions?|tags?|resources?|) in admin list$/) do |amount|
  expect(find_all('tbody tr').count).to eq amount
end
