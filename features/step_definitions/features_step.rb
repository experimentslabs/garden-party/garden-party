# frozen_string_literal: true

Given('I have a map named {string}') do |name|
  @current_map = FactoryBot.create :map, name: name, user: @signed_in_user
end

Given('there is a resource named {string}') do |name|
  FactoryBot.create :resource, name: name
end

Given('there is a genus named {string}') do |name|
  FactoryBot.create :genus, name: name
end

Given('I access the {string} map') do |name|
  visit '/'
  click_on I18n.t('layouts.application.header.gardens')
  click_on I18n.t('js.layouts.garden.main_menu.home')

  click_on name
end

When('I click on the inventory link') do
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')
end

When('I click on the timeline link') do
  click_on I18n.t('js.layouts.garden.toolbar.timeline')
end

Given('I have a tree named {string}') do |name|
  FactoryBot.create :element, :point, name: name, map: @current_map, layer: @current_map.layers.first
end

Given('I access the inventory page for {string}') do |string|
  step "I access the \"#{string}\" map"
  step 'I click on the inventory link'
end

Given('I access the timeline for {string}') do |string|
  step "I access the \"#{string}\" map"
  step 'I click on the timeline link'
end

Given('I have a patch named {string} with some {string} in it') do |patch_name, resource_name|
  resource = FactoryBot.create :resource, name: resource_name
  element  = FactoryBot.build :element, resource: resource, patch: nil
  FactoryBot.create :patch, :polygon, name: patch_name, map: @current_map, elements: [element]
end
