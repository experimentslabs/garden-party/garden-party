# frozen_string_literal: true

When('I create the {string} genus') do |name|
  find('.rui-toolbar > button', text: I18n.t('js.genera.index.new_genus')).click

  within '.rui-modal form' do
    fill_in I18n.t('activerecord.attributes.genus.name'), with: name
    select_in_multiselect 'genus_family', Family.first.name

    click_on I18n.t('generic.save')
  end
end

Then('I see {string} in the library\'s genus list') do |name|
  step 'I order the library by genus'

  within '._virtual-scroll-area' do
    expect(current_scope).to have_content name
  end
end

When('I rename the {string} genus to {string}') do |old_name, new_name|
  within '.rui-list-item', text: old_name do
    click_on I18n.t('generic.edit')
  end

  within '.rui-modal form' do
    fill_in I18n.t('activerecord.attributes.genus.name'), with: new_name

    click_on I18n.t('generic.save')
  end
end
