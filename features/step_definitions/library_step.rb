# frozen_string_literal: true

Given('I access the library page in app') do
  visit '/app'
  click_on I18n.t('js.layouts.garden.main_menu.library')
end

Given('I order the library by genus') do
  within '.rui-tabs' do
    find('.rui-tabs > .rui-button', text: I18n.t('js.genera.index.genus')).click
  end
end

Given('I order the library by family') do
  within '.rui-tabs' do
    find('.rui-tabs > .rui-button', text: I18n.t('js.genera.index.family')).click
  end
end

Then(/\AI don't see the "(.*)" (?:genus|family) anymore\z/) do |name|
  within '._virtual-scroll-area' do
    expect(current_scope).to have_no_content name
  end
end

Given(/^I search for "(\w+)" (?:family|genus|resource|interactions group)$/) do |name|
  find('input[type="search"]').fill_in with: name
end
