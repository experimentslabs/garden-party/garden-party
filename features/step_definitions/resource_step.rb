# frozen_string_literal: true

When('I create the {string} resource') do |name|
  genus = Genus.first

  find('.rui-toolbar > button', text: I18n.t('js.genera.index.new_resource')).click

  within '.rui-modal form' do
    fill_in I18n.t('activerecord.attributes.resource.name'), with: name
    fill_in I18n.t('activerecord.attributes.resource.latin_name'), with: Faker::Lorem.words.join(' ')
    select_in_multiselect 'resource_genus_id', genus.name

    click_on I18n.t('generic.save')
  end
end

Then('I see {string} in the library') do |name|
  within '._virtual-scroll-area' do
    expect(current_scope).to have_content name
  end
end

When('I rename resource {string} to {string}') do |old_name, new_name|
  within '._virtual-scroll-area' do
    click_on old_name
  end

  within '.gpa-library__content__resource' do
    click_on I18n.t('generic.edit')
  end

  within '.rui-modal form' do
    fill_in I18n.t('activerecord.attributes.resource.name'), with: new_name

    click_on I18n.t('generic.save')
  end
end

Then('I don\'t see {string} in the library') do |name|
  within '._virtual-scroll-area' do
    expect(current_scope).to have_no_content name
  end
end
