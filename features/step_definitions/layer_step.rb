# frozen_string_literal: true

Given('there is a {string} layer in {string}') do |name, map_name|
  map = Map.find_by name: map_name
  FactoryBot.create :layer, name: name, map: map
end

Given('there is a {string} layer in {string} with some paths in it') do |name, map_name|
  map = Map.find_by name: map_name
  layer = FactoryBot.create :layer, name: name, map: map
  FactoryBot.create :path, layer: layer, map: map
end

Given('there is a {string} layer in {string} with some patches in it') do |name, map_name|
  map = Map.find_by name: map_name
  layer = FactoryBot.create :layer, name: name, map: map
  FactoryBot.create :patch, layer: layer, map: map
end

When('I create the {string} layer') do |name|
  click_on I18n.t('js.garden.layers.new_layer')
  fill_in I18n.t('activerecord.attributes.layer.name'), with: name
  click_on I18n.t('generic.save')
end

Then('I see the {string} layer in the list') do |name|
  within('section', text: I18n.t('js.garden.right_panel.layers.title')) do
    expect(current_scope).to have_content(name)
  end
end

Given('I rename the {string} layer to {string}') do |name, new_name|
  within('.gpa-garden-right-panel__list .rui-list-item', text: name) do
    click_on I18n.t('generic.edit')
  end

  within('.rui-modal') do
    expect(current_scope).to have_content I18n.t('js.layers.form.title_edit')

    fill_in I18n.t('activerecord.attributes.layer.name'), with: new_name
    click_on I18n.t('generic.save')
  end
end

Given('I destroy the {string} layer') do |name|
  within('.gpa-garden-right-panel__list .rui-list-item', text: name) do
    click_on I18n.t('generic.destroy')
  end
end

And('I accept the warning about an irreversible action') do
  within('.rui-modal') do
    expect(current_scope).to have_content I18n.t('js.layer.destroy_warning')

    click_on I18n.t('generic.ok')
  end
end

Then('I don\'t see the {string} layer in the list') do |name|
  within('section', text: I18n.t('js.garden.right_panel.layers.title')) do
    expect(current_scope).to have_no_content(name)
  end
end

Then('I see a warning about the layer not being empty') do
  within('.rui-modal') do
    expect(current_scope).to have_content I18n.t('js.generic.impossible_action')
  end
end

Given('I cancel the warning') do
  within('.rui-modal') { click_on I18n.t('generic.cancel') }
end
