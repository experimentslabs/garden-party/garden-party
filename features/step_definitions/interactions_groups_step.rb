# frozen_string_literal: true

Given('there is a resource interactions group named {string}') do |name|
  FactoryBot.create :resource_interactions_group, name: name
end

When('I access the interactions groups') do
  click_on I18n.t('js.layout.library.resource_interactions_groups')
end

When('I create the {string} interaction group') do |name|
  find('.rui-toolbar > button', text: I18n.t('js.genera.index.new_resource_interactions_group')).click

  within '.rui-modal' do
    fill_in I18n.t('activerecord.attributes.resource_interactions_group.name'), with: name

    click_on I18n.t('generic.save')
  end
end

Given('I access the interactions groups page in app') do
  visit '/app/library/resource_interactions_groups'
end

When('I rename {string} interactions group to {string}') do |old_name, new_name|
  click_on old_name
  click_on I18n.t('generic.edit')

  within '.rui-modal' do
    fill_in I18n.t('activerecord.attributes.resource_interactions_group.name'), with: new_name

    click_on I18n.t('generic.save')
  end
end

Then("I see {string} in the library's interactions groups list") do |name|
  within '._virtual-scroll-area' do
    expect(current_scope).to have_content name
  end
end

Then("I don't see the {string} interactions group anymore") do |name|
  within '._virtual-scroll-area' do
    expect(current_scope).to have_no_content name
  end
end
