# frozen_string_literal: true

When('I access the tasks list') do
  click_on I18n.t('js.layouts.garden.toolbar.todo')
end
