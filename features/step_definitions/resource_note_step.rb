# frozen_string_literal: true

Given(/^there is an? (visible|invisible) note on the "(.*)" resource$/) do |visibility, resource_name|
  resource = Resource.find_by name: resource_name
  FactoryBot.create :resource_note, resource: resource, visible: visibility == 'visible'
end

When('I access the {string} page in the library') do |resource_name|
  step 'I access the app'
  click_on I18n.t('js.layouts.garden.main_menu.library')
  click_on resource_name
end

Then(/^I see (\d+) (modifiable|unmodifiable) notes?$/) do |amount, modifiable|
  total        = find_all('.rui-widget').count
  owned        = find_all('.rui-widget .rui-toolbar').count
  unmodifiable = total - owned

  if modifiable == 'modifiable'
    expect(owned).to eq amount
  else
    expect(unmodifiable).to eq amount
  end
end

When('I add a note on the resource') do
  click_on I18n.t('js.resources.description_area.new_note')
  within '.rui-modal' do
    fill_in I18n.t('activerecord.attributes.resource_note.content'), with: Faker::Lorem.paragraph

    click_on I18n.t('generic.save')
  end
end

Given('I wrote a note on the {string} resource') do |resource_name|
  resource = Resource.find_by name: resource_name
  FactoryBot.create :resource_note, resource: resource, user: @signed_in_user
end

When('I change the note to {string}') do |content|
  within '.rui-widget' do
    click_on I18n.t('generic.edit')
  end

  fill_in I18n.t('activerecord.attributes.resource_note.content'), with: content
  click_on I18n.t('generic.save')
end

Then('I see a note stating {string}') do |content|
  expect(page).to have_css('.rui-widget', text: content)
end

When('I destroy the note') do
  within '.rui-widget' do
    click_on I18n.t('generic.destroy')
  end
  within('.rui-modal') do
    click_on I18n.t('generic.ok')
  end
end

Then('there is no note') do
  expect(page).to have_no_css '.gp-observation'
end
