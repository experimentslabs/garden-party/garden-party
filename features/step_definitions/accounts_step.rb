# frozen_string_literal: true

Given('I have an account as {string}') do |email|
  @signed_in_user = FactoryBot.create :user, email: email
end

Given("I have an account and i'm logged in") do
  user = FactoryBot.create :user

  step "I sign in as \"#{user.email}\""
end

Given("I have an administrator account and I'm logged in") do
  user = FactoryBot.create :user_admin

  step "I sign in as \"#{user.email}\""
end

Given('I sign in as {string}') do |email|
  @signed_in_user = User.find_by email: email

  visit '/'
  fill_in I18n.t('activerecord.attributes.user.email'), with: email
  fill_in I18n.t('activerecord.attributes.user.password'), with: 'password'

  click_on I18n.t('devise.sessions.new.sign_in')
end

Given('there is an user {string} in the system') do |name|
  user = FactoryBot.create :user, username: name
  FactoryBot.create :map, user: user
end

Given('there is an user {string} with email {string} in the system') do |username, email|
  FactoryBot.create :user, username: username, email: email
end

Given('there is an user {string} with a map {string} in the system') do |user_name, map_name|
  user = FactoryBot.create :user, username: user_name
  FactoryBot.create :map, user: user, name: map_name
end

Given('I\'m an anonymous visitor') do
  buttons = find_all 'button', text: I18n.t('layouts.application.sign_out')

  buttons.first.click if buttons.count.positive?
  @signed_in_user = nil
end

Given('I\'m signed-in as {string}') do |name|
  user = User.find_by username: name

  step "I sign in as \"#{user.email}\""
end
