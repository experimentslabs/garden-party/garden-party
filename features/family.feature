Feature: Family management
  As an user
  In order to improve resources for the community
  I want to be able to create some new families
  And I want to be able to edit existing ones

  Background:
    Given I have an account and i'm logged in
    And there is a family named "Actinidiaceae"
    And there is a family named "Aizoaceae"

  Scenario: Create new genus
    Given I access the library page in app
    When I create the "Vitaceae" family
    Then I see "Vitaceae" in the library's family list

  Scenario: Edit existing genus
    Given I access the library page in app
    And I order the library by family
    When I rename the "Actinidiaceae" family to "Something else"
    Then I see "Something else" in the library's family list
    And I don't see the "Actinidiaceae" family anymore

  Scenario: Search family
    Given I access the library page in app
    And I order the library by family
    When I search for "aiz" family
    Then I see "Aizoaceae" in the library's family list
    And I don't see the "Actinidiaceae" family anymore
