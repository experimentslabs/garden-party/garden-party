Feature: Interactions groups
  As an user of the application
  In order to organize resources and their interactions
  I want to group them

  Background:
    Given I have an account and i'm logged in
    And there is a resource interactions group named "Cabbages"
    And there is a resource interactions group named "Lettuces"


  Scenario: List interactions groups
    Given I access the library page in app
    When I access the interactions groups
    Then I see "Cabbages" in the library's interactions groups list


  Scenario: Create an interactions group
    Given I access the library page in app
    When I create the "Cucumber" interaction group
    And I access the interactions groups
    Then I see "Cucumber" in the library's interactions groups list

  Scenario: Edit an interactions group
    Given I access the interactions groups page in app
    When I rename "Cabbages" interactions group to "Leeks"
    Then I see "Leeks" in the library's interactions groups list
    And I don't see the "Cabbages" interactions group anymore

  Scenario: Search group
    Given I access the interactions groups page in app
    When I search for "let" interactions group
    Then I see "Lettuces" in the library's interactions groups list
    And I don't see the "Cabbages" interactions group anymore
