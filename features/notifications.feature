Feature: Notification management

  Background:
    Given I have an account as "paul@garden-party.io"
    And I have 2 system notifications
    And I sign in as "paul@garden-party.io"
    And I access the app

  Scenario: Display new notification in the app
    Then there is an indicator for the 2 notifications

  Scenario: Display all notifications in the app
    When I click on the notifications button
    Then I see 2 notifications in the list

  Scenario: Archive a notification
    Given I display all the notifications
    When I archive the first notification
    Then there is 1 archived notification
    And there is 1 non archived notification

  Scenario: Delete a notification
    Given I display all the notifications
    When I delete the first notification
    Then I see 1 notification in the list

  Scenario: Archive all the notifications
    Given I display all the notifications
    When I archive all the notifications
    Then there are 2 archived notifications
    And there is 0 non archived notification

  Scenario: Delete all the archived notifications
    Given I display all the notifications
    And I archive all the notifications
    When I delete all the archived notifications
    Then I see 0 notification in the list
