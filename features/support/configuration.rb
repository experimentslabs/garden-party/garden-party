# frozen_string_literal: true

require 'simplecov'
require 'capybara-screenshot/cucumber'

SimpleCov.start 'rails'

Capybara.register_driver(:selenium) do |app|
  options         = Selenium::WebDriver::Firefox::Options.new
  firefox_profile = Selenium::WebDriver::Firefox::Profile.new

  # Custom Firefox profile to use light theme.
  firefox_profile['ui.systemUsesDarkTheme'] = 0

  args = []
  args << '--headless' unless ENV.fetch('HEADLESS', 'true') == 'false'

  options.profile = firefox_profile
  options.args = args

  # Custom Firefox executable for testing
  custom_firefox_path = Rails.configuration.garden_party.tests[:firefox_path].presence
  Selenium::WebDriver::Firefox.path = custom_firefox_path if custom_firefox_path

  Capybara::Selenium::Driver.new app,
                                 browser: :firefox,
                                 options: options
end

Capybara.default_driver    = :selenium
Capybara.javascript_driver = :selenium

Capybara::Screenshot.prune_strategy = :keep_last_run

Capybara.save_path = Rails.root.join('tmp', 'cucumber_screenshots')
