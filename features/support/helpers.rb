# frozen_string_literal: true

# Helpers from RSpec helpers
require_relative '../../spec/support/system/helpers'

World(SystemHelpers::Helpers)
