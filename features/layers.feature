Feature: Layers management
  As an user
  In order to organize the elements in my garden
  I want to be able to manage them on layers

  Background:
    Given I have an account and i'm logged in
    And I have a map named "The map"
    And there is a "Pjoret" layer in "The map"
    And there is a "Paths" layer in "The map" with some paths in it
    And there is a "Patches" layer in "The map" with some patches in it
    And I access the "The map" map

  Scenario: Create a layer
    When I create the 'Underground' layer
    Then I see the 'Underground' layer in the list

  Scenario: Update a layer
    Given I rename the "Pjoret" layer to "Project"
    Then I see the "Project" layer in the list

  Scenario: Destroy an empty layer
    Given I destroy the "Pjoret" layer
    And I accept the warning about an irreversible action
    Then I don't see the "Pjoret" layer in the list

  Scenario: Destroy a layer with things in it
    Given I destroy the "Paths" layer
    Then I see a warning about the layer not being empty
    Given I cancel the warning
    And I destroy the "Patches" layer
    Then I see a warning about the layer not being empty



